#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    vector<string> split(string s, string spliter)
//    {
//        vector<string> res;
//        int i = s.find(spliter);
//        while (i != s.npos)
//        {
//            res.push_back(s.substr(0, i));
//            s = s.substr(i + 1, s.size());
//            i = s.find(spliter);
//        }
//        res.push_back(s);
//        return res;
//    }
//    bool isIPv4(string queryIP)
//    {
//        vector<string> s = split(queryIP, ".");
//        if (s.size() != 4) return false;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (s[i].size() == 0 || s[i].size() > 3) return false;
//            int num = 0;
//            for (char ch : s[i])
//                num = num * 10 + (ch - '0');
//            if (num < 0 || num>255) return false;
//            for (int j = 0; j < s[i].size(); j++)
//                if (!isdigit(s[i][j]))
//                    return false;
//            if (s[i][0] == '0' && s[i].size() != 1) return false;
//        }
//        return true;
//    }
//    bool isIPv6(string queryIP)
//    {
//        vector<string> s = split(queryIP, ":");
//        if (s.size() != 8) return false;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (s[i].size() < 1 || s[i].size() > 4) return false;
//            for (int j = 0; j < s[i].size(); j++)
//            {
//                char ch = s[i][j];
//                if (!(isdigit(ch) || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F')))
//                    return false;
//            }
//        }
//        return true;
//    }
//    string validIPAddress(string queryIP) {
//        if (isIPv4(queryIP)) return "IPv4";
//        else if (isIPv6(queryIP)) return "IPv6";
//        return "Neither";
//    }
//};


//class Solution {
//public:
//    int search(vector<int>& nums, int target) {
//        int left = 0, right = nums.size() - 1;
//        while (left <= right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > target) right = mid - 1;
//            else if (nums[mid] < target) left = mid + 1;
//            else return mid;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    int search(vector<int>& nums, int target) {
//        int left = 0, right = nums.size();
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > target) right = mid;
//            else if (nums[mid] < target) left = mid + 1;
//            else return mid;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    int searchInsert(vector<int>& nums, int target) {
//        int left = 0, right = nums.size() - 1;
//        while (left <= right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > target) right = mid - 1;
//            else if (nums[mid] < target) left = mid + 1;
//            else return mid;
//        }
//        return right + 1;
//    }
//};


//class Solution {
//public:
//    vector<int> searchRange(vector<int>& nums, int target) {
//        int n = nums.size();
//        if (n == 0) return { -1, -1 };
//        int st = 0, ed = 0;
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] != target) return { -1, -1 };
//        else st = left;
//        left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] > target) right = mid - 1;
//            else left = mid;
//        }
//        ed = right;
//        return { st, ed };
//    }
//};


//class Solution {
//public:
//    int search(vector<int>& nums, int target) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] > target) right = mid - 1;
//            else left = mid;
//        }
//        if (nums[left] == target) return left;
//        return -1;
//    }
//};


//class Solution {
//public:
//    int mySqrt(int x) {
//        if (x < 1) return 0;
//        int left = 1, right = x;
//        while (left < right)
//        {
//            long long mid = left + (right - left + 1) / 2;
//            if (mid * mid <= x) left = mid;
//            else right = mid - 1;
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    int searchInsert(vector<int>& nums, int target) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] >= target) right = mid;
//            else left = mid + 1;
//        }
//        if (nums[left] < target) return left + 1;
//        return left;
//    }
//};


//class Solution {
//public:
//    int peakIndexInMountainArray(vector<int>& arr) {
//        int n = arr.size();
//        int left = 1, right = n - 2;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (arr[mid - 1] < arr[mid]) left = mid;
//            else right = mid - 1;
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    int findPeakElement(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > nums[mid + 1]) right = mid;
//            else left = mid + 1;
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int n = nums.size();
//        int x = nums[n - 1];
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > x) left = mid + 1;
//            else right = mid;
//        }
//        return nums[left];
//    }
//};


//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1 || nums[0] < nums[n - 1]) return nums[0];
//        int x = nums[0];
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < x) right = mid;
//            else left = mid + 1;
//        }
//        return nums[left];
//    }
//};


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] == mid) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] == left) return left + 1;
//        return left;
//    }
//};