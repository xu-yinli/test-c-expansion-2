﻿#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 2e5 + 10;
//int a[N], dp[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    int res = -101;
//    for (int i = 1; i <= n; i++)
//    {
//        dp[i] = max(dp[i - 1] + a[i - 1], a[i - 1]);
//        res = max(res, dp[i]);
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//#include <string>
//using namespace std;
//
//int n;
//string s;
//
//int f()
//{
//    // 1.判断是否全都是相同字符
//    bool flag = false;
//    for (int i = 1; i < n; i++)
//    {
//        if (s[i] != s[0])
//        {
//            flag = true;
//            break;
//        }
//    }
//    if (!flag) return 0; //不存在非回文子串
//
//    // 2.判断本⾝是否是回⽂
//    flag = true;
//    int left = 0, right = n - 1;
//    while (left < right)
//    {
//        if (s[left] == s[right])
//        {
//            left++;
//            right--;
//        }
//        else
//        {
//            flag = false;
//            break;
//        }
//    }
//    if (flag) return n - 1; //是回文，输出这个字符串长度-1
//    else return n; //不是回文，输出字符串长度
//}
//
//int main()
//{
//    cin >> s;
//    n = s.size();
//    cout << f() << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int dp[1010][1010];
//
//int main()
//{
//    string s;
//    cin >> s;
//    int n = s.size();
//    for (int i = 0; i < n; i++)
//        for (int j = 0; j < n; j++)
//            if (i == j)
//                dp[i][j] = 1;
//    for (int i = n - 1; i >= 0; i--)
//    {
//        for (int j = i + 1; j < n; j++)
//        {
//            if (s[i] == s[j]) dp[i][j] = dp[i + 1][j - 1] + 2;
//            else dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
//        }
//    }
//    cout << dp[0][n - 1] << endl;
//    return 0;
//}


//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        int res = INT_MAX;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            sum += nums[right];
//            while (sum >= target)
//            {
//                int len = right - left + 1;
//                if (len < res) res = len;
//                sum -= nums[left];
//                left++;
//            }
//            right++;
//        }
//        if (res != INT_MAX) return res;
//        else return 0;
//    }
//};


//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        int len = INT_MAX;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            sum += nums[right];
//            while (sum >= target)
//            {
//                if (right - left + 1 < len) len = right - left + 1;
//                sum -= nums[left];
//                left++;
//            }
//            right++;
//        }
//        if (len != INT_MAX) return len;
//        else return 0;
//    }
//};


//class Solution {
//private:
//    int hash[128];
//public:
//    int lengthOfLongestSubstring(string s) {
//        int n = s.size();
//        int len = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash[s[right]]++;
//            while (hash[s[right]] > 1)
//            {
//                hash[s[left]]--;
//                left++;
//            }
//            if (right - left + 1 > len) len = right - left + 1;
//            right++;
//        }
//        return len;
//    }
//};


//class Solution {
//    //找出最长子数组，其0的个数不超过k个
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int n = nums.size();
//        int res = 0;
//        int zero = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            if (nums[right] == 0) zero++;
//            while (zero > k)
//            {
//                if (nums[left] == 0) zero--;
//                left++;
//            }
//            res = max(res, right - left + 1);
//            right++;
//        }
//        return res;
//    }
//};


//class Solution {
//    //找出最长子数组的长度，所有元素的和恰好等于sum-x
//public:
//    int minOperations(vector<int>& nums, int x) {
//        int n = nums.size();
//        int sum = 0;
//        for (int a : nums)
//            sum += a;
//        int target = sum - x;
//        if (target < 0) return -1;
//        int res = -1;
//        int total = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            total += nums[right];
//            while (total > target)
//            {
//                total -= nums[left];
//                left++;
//            }
//            if (total == target) res = max(res, right - left + 1);
//            right++;
//        }
//        if (res == -1) return -1;
//        else return nums.size() - res;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> hash;
//    //找出一个最长子数组的长度，子数组中不能超过两个不同的数字
//public:
//    int totalFruit(vector<int>& fruits) {
//        int n = fruits.size();
//        int res = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash[fruits[right]]++;
//            while (hash.size() > 2)
//            {
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0) hash.erase(fruits[left]);
//                left++;
//            }
//            res = max(res, right - left + 1);
//            right++;
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    int hash[100001];
//    //找出一个最长子数组的长度，子数组中不能超过两个不同的数字
//public:
//    int totalFruit(vector<int>& fruits) {
//        int n = fruits.size();
//        int res = 0;
//        int kinds = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            if (hash[fruits[right]] == 0) kinds++;
//            hash[fruits[right]]++;
//            while (kinds > 2)
//            {
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0) kinds--;
//                left++;
//            }
//            res = max(res, right - left + 1);
//            right++;
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    int hash1[26]; //统计字符串p中每个字符出现的个数
//    int hash2[26]; //统计滑动窗口中每个字符出现的个数
//    vector<int> res;
//public:
//    vector<int> findAnagrams(string s, string p) {
//        int n = s.size(), m = p.size();
//        for (auto ch : p)
//            hash1[ch - 'a']++;
//        int cnt = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash2[s[right] - 'a']++;
//            if (hash2[s[right] - 'a'] <= hash1[s[right] - 'a']) cnt++;
//            if (right - left + 1 > m)
//            {
//                if (hash2[s[left] - 'a'] <= hash1[s[left] - 'a']) cnt--;
//                hash2[s[left] - 'a']--;
//                left++;
//            }
//            if (cnt == m) res.push_back(left);
//            right++;
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    unordered_map<string, int> hash1; //保存words里面所有单词的频次
//    vector<int> res;
//public:
//    vector<int> findSubstring(string s, vector<string>& words) {
//        int n = s.size(), m = words.size();
//        for (string s : words)
//            hash1[s]++;
//        int len = words[0].size();
//        int left = 0, right = 0;
//        for (int i = 0; i < len; i++)
//        {
//            unordered_map<string, int> hash2; //统计滑动窗口内单词的频次
//            int cnt = 0;
//            int left = i, right = i;
//            while (right + len <= n)
//            {
//                hash2[s.substr(right, len)]++;
//                if (hash2[s.substr(right, len)] <= hash1[s.substr(right, len)]) cnt++;
//                if (right - left + 1 + (len - 1) > len * m)
//                {
//                    if (hash2[s.substr(left, len)] <= hash1[s.substr(left, len)]) cnt--;
//                    hash2[s.substr(left, len)]--;
//                    left += len;
//                }
//                if (cnt == m) res.push_back(left);
//                right += len;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    unordered_map<string, int> hash1; //保存words里面所有单词的频次
//    vector<int> res;
//public:
//    vector<int> findSubstring(string s, vector<string>& words) {
//        int n = s.size(), m = words.size();
//        for (string s : words)
//            hash1[s]++;
//        int len = words[0].size();
//        int left = 0, right = 0;
//        for (int i = 0; i < len; i++)
//        {
//            unordered_map<string, int> hash2; //统计滑动窗口内单词的频次
//            int cnt = 0;
//            int left = i, right = i;
//            while (right + len <= n)
//            {
//                string in = s.substr(right, len);
//                hash2[in]++;
//                if (hash1.count(in) && hash2[in] <= hash1[in]) cnt++;
//                if (right - left + 1 + (len - 1) > len * m)
//                {
//                    string out = s.substr(left, len);
//                    if (hash2[out] <= hash1[out]) cnt--;
//                    hash2[out]--;
//                    left += len;
//                }
//                if (cnt == m) res.push_back(left);
//                right += len;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    unordered_map<char, int> hash1; //统计字符串t中每个字符的频次
//    unordered_map<char, int> hash2; //统计滑动窗口中每个字符的频次
//public:
//    string minWindow(string s, string t) {
//        int n = s.size(), m = t.size();
//        for (char ch : t)
//            hash1[ch]++;
//        int res = INT_MAX;
//        int cnt = 0;
//        int st = -1;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash2[s[right]]++;
//            if (hash1.count(s[right]) && hash2[s[right]] == hash1[s[right]]) cnt++;
//            while (hash1.size() == cnt)
//            {
//                if (right - left + 1 < res)
//                {
//                    res = right - left + 1;
//                    st = left;
//                }
//                if (hash1.count(s[left]) && hash2[s[left]] == hash1[s[left]]) cnt--;
//                hash2[s[left]]--;
//                left++;
//            }
//            right++;
//        }
//        if (st == -1) return "";
//        return s.substr(st, res);
//    }
//};


//class Solution {
//private:
//    int hash1[128]; //统计字符串t中每个字符的频次
//    int hash2[128]; //统计滑动窗口中每个字符的频次
//public:
//    string minWindow(string s, string t) {
//        int n = s.size(), m = t.size();
//        int kinds = 0;
//        for (char ch : t)
//        {
//            if (hash1[ch] == 0) kinds++;
//            hash1[ch]++;
//        }
//        int res = INT_MAX;
//        int cnt = 0;
//        int st = -1;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash2[s[right]]++;
//            if (hash2[s[right]] == hash1[s[right]]) cnt++;
//            while (kinds == cnt)
//            {
//                if (right - left + 1 < res)
//                {
//                    res = right - left + 1;
//                    st = left;
//                }
//                if (hash2[s[left]] == hash1[s[left]]) cnt--;
//                hash2[s[left]]--;
//                left++;
//            }
//            right++;
//        }
//        if (st == -1) return "";
//        return s.substr(st, res);
//    }
//};