#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//typedef pair<int, int> PII;
//vector<PII> q;
//
//bool cmp(PII x, PII y)
//{
//    return x.second < y.second;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b;
//        cin >> a >> b;
//        q.push_back({ a, b });
//    }
//    sort(q.begin(), q.end(), cmp);
//    int count = 0;
//    int ed = -2e9;
//    for (auto e : q)
//    {
//        if (ed < e.first)
//        {
//            count++;
//            ed = e.second;
//        }
//    }
//    cout << count << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//vector<vector<int>> q;
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b;
//        cin >> a >> b;
//        q.push_back({ a, b });
//    }
//    sort(q.begin(), q.end());
//    int count = 0;
//    int ed = -2e9;
//    for (int i = 0; i < q.size(); i++)
//    {
//        if (ed < q[i][0])
//        {
//            count++;
//            ed = q[i][1];
//        }
//        else
//            ed = min(ed, q[i][1]);
//    }
//    cout << count << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <algorithm>
//#include <queue>
//using namespace std;
//
//typedef pair<int, int> PII;
//vector<PII> q;
//
//bool cmp(PII x, PII y)
//{
//    return x.first < y.first;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b;
//        cin >> a >> b;
//        q.push_back({ a, b });
//    }
//    sort(q.begin(), q.end(), cmp);
//    priority_queue<int, vector<int>, greater<int>> heap;
//    for (int i = 0; i < q.size(); i++)
//    {
//        if (heap.empty() || heap.top() >= q[i].first)
//            heap.push(q[i].second);
//        else
//        {
//            heap.pop();
//            heap.push(q[i].second);
//        }
//    }
//    cout << heap.size() << endl;
//    return 0;
//}


//class Solution {
//    priority_queue<int, vector<int>, greater<int>> heap;
//public:
//    int minGroups(vector<vector<int>>& intervals) {
//        sort(intervals.begin(), intervals.end());
//        for (int i = 0; i < intervals.size(); i++)
//        {
//            if (heap.empty() || heap.top() >= intervals[i][0])
//                heap.push(intervals[i][1]);
//            else
//            {
//                heap.pop();
//                heap.push(intervals[i][1]);
//            }
//        }
//        return heap.size();
//    }
//};


//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//typedef pair<int, int> PII;
//vector<PII> q;
//bool flag;
//
//bool cmp(PII x, PII y)
//{
//    return x.first < y.first;
//}
//
//int main()
//{
//    int st, ed;
//    cin >> st >> ed;
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b;
//        cin >> a >> b;
//        q.push_back({ a, b });
//    }
//    sort(q.begin(), q.end(), cmp);
//    int count = 0;
//    for (int i = 0; i < q.size(); i++)
//    {
//        int j = i;
//        int r = -2e9;
//        while (j < q.size() && q[j].first <= st)
//        {
//            r = max(r, q[j].second);
//            j++;
//        }
//        if (r < st) break;
//        count++;
//        if (r >= ed)
//        {
//            flag = true;
//            break;
//        }
//        st = r;
//        i = j - 1;
//    }
//    if (!flag) cout << "-1" << endl;
//    else cout << count << endl;
//    return 0;
//}


//#include <iostream>
//#include <queue>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    priority_queue<int, vector<int>, greater<int>> heap;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        heap.push(x);
//    }
//    int res = 0;
//    while (heap.size() > 1)
//    {
//        int a = heap.top(); heap.pop();
//        int b = heap.top(); heap.pop();
//        res = res + (a + b);
//        heap.push(a + b);
//    }
//    cout << res << endl;
//    return 0;
//}


//class Solution {
//    vector<int> res;
//    int hash[27] = { 0 };
//public:
//    vector<int> partitionLabels(string s) {
//        for (int i = 0; i < s.size(); i++)
//            hash[s[i] - 'a'] = i;
//        int l = 0, r = 0;
//        for (int i = 0; i < s.size(); i++)
//        {
//            r = max(r, hash[s[i] - 'a']);
//            if (i == r)
//            {
//                res.push_back(r - l + 1);
//                l = i + 1;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int monotoneIncreasingDigits(int n) {
//        string s = to_string(n);
//        int k = s.size();
//        for (int i = s.size() - 1; i > 0; i--)
//        {
//            if (s[i - 1] > s[i])
//            {
//                s[i - 1]--;
//                k = i;
//            }
//        }
//        for (int i = k; i < s.size(); i++)
//            s[i] = '9';
//        return stoi(s);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//    int res = 0;
//    int interval(TreeNode* cur)
//    {
//        if (cur == NULL) return 2; //有覆盖
//        //后序遍历(左右中)
//        int left = interval(cur->left);
//        int right = interval(cur->right);
//        if (left == 2 && right == 2) return 0; //无覆盖
//        if (left == 0 || right == 0)
//        {
//            res++;
//            return 1; //安摄像头
//        }
//        if (left == 1 || right == 1) return 2; //有覆盖
//        return -1;
//    }
//public:
//    int minCameraCover(TreeNode* root) {
//        if (interval(root) == 0) //增加一个摄像头
//            res++;
//        return res;
//    }
//};


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 1e5 + 10;
//int t[N];
//
//typedef long long LL;
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//        cin >> t[i];
//    sort(t, t + n);
//    LL res = 0;
//    for (int i = 0; i < n; i++)
//        res += t[i] * (n - i - 1);
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//#include <vector>
//using namespace std;
//
//vector<vector<int>> t;
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        int x;
//        cin >> x;
//        t.push_back({ x, i });
//    }
//    sort(t.begin(), t.end());
//    for (int i = 0; i < n; i++)
//        cout << t[i][1]+1 << ' ';
//    cout << endl;
//    double res = 0;
//    for (int i = 0; i < n; i++)
//        res += t[i][0] * (n - i - 1);
//    res = 1.0 * res / n;
//    printf("%.2lf\n", res);
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//#include <stdlib.h>
//using namespace std;
//
//const int N = 100010;
//int a[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//        cin >> a[i];
//    sort(a, a + n);
//    int res = 0;
//    for (int i = 0; i < n; i++)
//        res += abs(a[i] - a[n / 2]);
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//typedef pair<int, int> PII;
//vector<PII> cow;
//
//bool cmp(PII x, PII y)
//{
//    return x.first < y.first;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    int w, s;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> w >> s;
//        cow.push_back({ w + s, w });
//    }
//    sort(cow.begin(), cow.end(), cmp);
//    int danger = -2e9, sum = 0;
//    for (int i = 0; i < cow.size(); i++)
//    {
//        w = cow[i].second;
//        s = cow[i].first - w;
//        danger = max(danger, sum - s);
//        sum += w;
//    }
//    cout << danger << endl;
//    return 0;
//}


#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

const int N = 50010;
typedef pair<int, int> PII;
PII cow[N];

int main()
{
    int n;
    cin >> n;
    int w, s;
    for (int i = 0; i < n; i++)
    {
        cin >> w >> s;
        cow[i] = { w + s, w };
    }
    sort(cow, cow + n);
    int danger = -2e9;
    int sum = 0;
    for (int i = 0; i < n; i++)
    {
        w = cow[i].second;
        s = cow[i].first - w;
        danger = max(danger, sum - s);
        sum += w;
    }
    cout << danger << endl;
    return 0;
}