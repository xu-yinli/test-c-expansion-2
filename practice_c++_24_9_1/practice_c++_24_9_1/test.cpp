#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @param target int整型
//     * @return int整型
//     */
//    int search(vector<int>& nums, int target) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left <= right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > target) right = mid - 1;
//            else if (nums[mid] < target) left = mid + 1;
//            else return mid;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @param target int整型
//     * @return int整型
//     */
//    int search(vector<int>& nums, int target) {
//        int n = nums.size();
//        if (n == 0) return -1;
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] > target) right = mid - 1;
//            else left = mid;
//        }
//        if (nums[left] == target) return left;
//        return -1;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @return int整型
//     */
//    int findPeakElement(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid - 1] < nums[mid]) left = mid;
//            else if (nums[mid - 1] > nums[mid]) right = mid - 1;
//        }
//        return left;
//    }
//};


////class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @return int整型
//     */
//    int minNumberInRotateArray(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (right > 0 && nums[right] == nums[right - 1])
//            right--;
//        int x = nums[right];
//        while (left < right && nums[left] == x)
//            left++;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > x) left = mid + 1;
//            else right = mid;
//        }
//        return nums[left];
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @return int整型
//     */
//    int minNumberInRotateArray(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right && nums[0] == nums[right])
//            right--;
//        int x = nums[right];
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > x) left = mid + 1;
//            else right = mid;
//        }
//        return nums[left];
//    }
//};