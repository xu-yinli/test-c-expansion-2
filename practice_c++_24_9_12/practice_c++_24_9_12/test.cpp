#define _CRT_SECURE_NO_WARNINGS 1

///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param preOrder int整型vector
//     * @param vinOrder int整型vector
//     * @return TreeNode类
//     */
//    TreeNode* rebuildTree(vector<int>& preOrder, int pre_st, int pre_ed, vector<int>& vinOrder, int in_st, int in_ed)
//    {
//        if (pre_st > pre_ed || in_st > in_ed) return nullptr;
//        TreeNode* root = new TreeNode(preOrder[pre_st]);
//        for (int i = 0; i < vinOrder.size(); i++)
//        {
//            if (root->val == vinOrder[i])
//            {
//                root->left = rebuildTree(preOrder, pre_st + 1, pre_st + (i - in_st), vinOrder, in_st, i - 1);
//                root->right = rebuildTree(preOrder, pre_st + (i - in_st) + 1, pre_ed, vinOrder, i + 1, in_ed);
//                break;
//            }
//        }
//        return root;
//    }
//    TreeNode* reConstructBinaryTree(vector<int>& preOrder, vector<int>& vinOrder) {
//        int n = preOrder.size(), m = vinOrder.size();
//        return rebuildTree(preOrder, 0, n - 1, vinOrder, 0, m - 1);
//    }
//};


///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param pRoot TreeNode类
//     * @return int整型vector<vector<>>
//     */
//    vector<vector<int> > Print(TreeNode* pRoot) {
//        if (pRoot == nullptr) return res;
//        TreeNode* root = pRoot;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> tmp;
//            int size = q.size();
//            while (size--)
//            {
//                TreeNode* k = q.front();
//                q.pop();
//                tmp.push_back(k->val);
//                if (k->left) q.push(k->left);
//                if (k->right) q.push(k->right);
//            }
//            res.push_back(tmp);
//        }
//        return res;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int pmi(int x)
//{
//    int res = x;
//    for (int i = 2; i <= x / i; i++)
//    {
//        if (x % i == 0)
//        {
//            res = res / i * (i - 1);
//            while (x % i == 0) x /= i;
//        }
//    }
//    if (x > 1) res = res / x * (x - 1);
//    return res;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a;
//        cin >> a;
//        cout << pmi(a) << endl;
//    }
//    return 0;
//}


//class Solution {
//private:
//    typedef long long LL;
//public:
//    int getPrims(int n)
//    {
//        vector<bool> st(n);
//        int cnt = 0;
//        for (int i = 2; i < n; i++)
//        {
//            if (st[i]) continue;
//            cnt++;
//            if ((LL)i * i < n)
//            {
//                for (int j = i * i; j < n; j += i)
//                    st[j] = true;
//            }
//        }
//        return cnt;
//    }
//    int countPrimes(int n) {
//        return getPrims(n);
//    }
//};


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//const int N = 1e6 + 10;
//bool st[N];
//
//int getPrimes(int n)
//{
//    int cnt = 0;
//    for (int i = 2; i <= n; i++)
//    {
//        if (st[i]) continue;
//        cnt++;
//        if ((LL)i * i <= n)
//        {
//            for (int j = i * i; j <= n; j += i)
//                st[j] = true;
//        }
//    }
//    return cnt;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    cout << getPrimes(n) << endl;
//    return 0;
//}


//class Solution {
//public:
//    int getPrimes(int n)
//    {
//        int cnt = 0;
//        vector<bool> st(n);
//        vector<int> primes(n);
//        for (int i = 2; i < n; i++)
//        {
//            if (!st[i]) primes[cnt++] = i;
//            for (int j = 0; j < cnt && i * primes[j] < n; j++)
//            {
//                st[i * primes[j]] = true;
//                if (i % primes[j] == 0) break;
//            }
//        }
//        return cnt;
//    }
//    int countPrimes(int n) {
//        return getPrimes(n);
//    }
//};