#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//private:
//    stack<int> st;
//public:
//    int trap(vector<int>& height) {
//        int n = height.size();
//        if (n <= 2) return 0;
//        int sum = 0;
//        st.push(0);
//        for (int i = 1; i < n; i++)
//        {
//            if (height[i] < height[st.top()])
//                st.push(i);
//            else if (height[i] == height[st.top()])
//            {
//                st.pop();
//                st.push(i);
//            }
//            else
//            {
//                while (st.size() && height[i] > height[st.top()])
//                {
//                    int mid = st.top();
//                    st.pop();
//                    if (!st.empty())
//                    {
//                        int h = min(height[st.top()], height[i]) - height[mid];
//                        int w = i - st.top() - 1;
//                        sum += h * w;
//                    }
//                }
//                st.push(i);
//            }
//        }
//        return sum;
//    }
//};


//#include <iostream>
//#include <vector>
//using namespace std;
//
//int res;
//int num[5];
//bool same[15][15];
//vector<int> path;
//
//bool check(int pos, int cur)
//{
//    for (int i = 1; i < pos; i++)
//        if (same[pos][i] && path[i] != cur)
//            return false;
//    return true;
//}
//
//void dfs(int pos)
//{
//    if (pos > 12)
//    {
//        res++;
//        return;
//    }
//    for (int i = 1; i <= 4; i++)
//    {
//        if (num[i] == 0) continue;
//        if (!check(pos, i)) continue;
//        num[i]--;
//        path.push_back(i);
//        dfs(pos + 1);
//        path.pop_back();
//        num[i]++;
//    }
//}
//
//int main()
//{
//    int m;
//    cin >> num[1] >> num[2] >> num[3] >> num[4];
//    cin >> m;
//    while (m--)
//    {
//        int x, y;
//        cin >> x >> y;
//        same[x][y] = same[y][x] = true;
//    }
//    path.push_back(0);
//    dfs(1);
//    cout << res << endl;
//    return 0;
//}


//class Solution {
//public:
//    int longestCommonSubsequence(string text1, string text2) {
//        int n = text1.size(), m = text2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (text1[i - 1] == text2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int longestCommonSubsequence(string text1, string text2) {
//        int n = text1.size(), m = text2.size();
//        text1 = " " + text1;
//        text2 = " " + text2;
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (text1[i] == text2[j]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int maxUncrossedLines(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size(), m = nums2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (nums1[i - 1] == nums2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int numDistinct(string s, string t) {
//        int n = s.size(), m = t.size();
//        vector<vector<double>> dp(n + 1, vector<double>(m + 1));
//        for (int i = 0; i <= n; i++) dp[i][0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (s[i - 1] == t[j - 1]) dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];
//                else dp[i][j] = dp[i - 1][j];
//            }
//        }
//        return dp[n][m];
//    }
//};