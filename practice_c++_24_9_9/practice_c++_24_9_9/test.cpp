#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//private:
//    stack<int> st;
//public:
//    vector<int> dailyTemperatures(vector<int>& temperatures) {
//        int n = temperatures.size();
//        vector<int> res(n, 0);
//        st.push(0);
//        for (int i = 0; i < n; i++)
//        {
//            if (temperatures[i] <= temperatures[st.top()]) st.push(i);
//            else
//            {
//                while (!st.empty() && temperatures[i] > temperatures[st.top()])
//                {
//                    res[st.top()] = i - st.top();
//                    st.pop();
//                }
//                st.push(i);
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    stack<int> st;
//    unordered_map<int, int> hash;
//public:
//    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size(), m = nums2.size();
//        vector<int> res(n, -1);
//        if (n == 0) return res;
//        for (int i = 0; i < n; i++)
//            hash[nums1[i]] = i;
//        st.push(0);
//        for (int i = 1; i < m; i++)
//        {
//            if (nums2[i] <= nums2[st.top()]) st.push(i);
//            else
//            {
//                while (!st.empty() && nums2[i] > nums2[st.top()])
//                {
//                    if (hash.count(nums2[st.top()]))
//                    {
//                        int index = hash[nums2[st.top()]];
//                        res[index] = nums2[i];
//                    }
//                    st.pop();
//                }
//                st.push(i);
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    stack<int> st;
//public:
//    vector<int> nextGreaterElements(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> res(n, -1);
//        st.push(0);
//        for (int i = 1; i < 2 * n; i++)
//        {
//            if (nums[i % n] <= nums[st.top()]) st.push(i % n);
//            else
//            {
//                while (!st.empty() && nums[i % n] > nums[st.top()])
//                {
//                    res[st.top()] = nums[i % n];
//                    st.pop();
//                }
//                st.push(i % n);
//            }
//        }
//        return res;
//    }
//};