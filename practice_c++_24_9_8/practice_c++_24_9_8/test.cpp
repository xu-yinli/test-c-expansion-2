#define _CRT_SECURE_NO_WARNINGS 1

//class Solution
//{
//public:
//    void push(int node) {
//        stack1.push(node);
//    }
//
//    int pop() {
//        while (stack1.size() > 1)
//        {
//            stack2.push(stack1.top());
//            stack1.pop();
//        }
//        int res = stack1.top();
//        stack1.pop();
//        while (stack2.size())
//        {
//            stack1.push(stack2.top());
//            stack2.pop();
//        }
//        return res;
//    }
//
//private:
//    stack<int> stack1;
//    stack<int> stack2;
//};


//class Solution {
//private:
//    stack<int> st;
//    stack<int> mi;
//public:
//    void push(int value) {
//        st.push(value);
//        if (mi.empty())
//            mi.push(value);
//        else
//        {
//            if (value <= mi.top())
//                mi.push(value);
//        }
//    }
//    void pop() {
//        if (st.top() == mi.top()) mi.pop();
//        st.pop();
//    }
//    int top() {
//        return st.top();
//    }
//    int min() {
//        return mi.top();
//    }
//};


//class Solution {
//private:
//    stack<char> st;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param s string字符串
//     * @return bool布尔型
//     */
//    bool isValid(string s) {
//        int n = s.size();
//        if (n % 2 != 0) return false;
//        for (auto ch : s)
//        {
//            if (ch == '(') st.push(')');
//            else if (ch == '{') st.push('}');
//            else if (ch == '[') st.push(']');
//            else if (st.empty()) return false;
//            else if (ch != st.top()) return false;
//            else if (ch == st.top()) st.pop();
//        }
//        if (st.empty()) return true;
//        return false;
//    }
//};