#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//
//int v[N], w[N];
//int dp1[N][N], dp2[N][N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            dp1[i][j] = dp1[i - 1][j];
//            if (j >= v[i]) dp1[i][j] = max(dp1[i][j], dp1[i - 1][j - v[i]] + w[i]);
//        }
//    }
//    cout << dp1[n][m] << endl;
//
//    for (int j = 1; j <= m; j++) dp2[0][j] = -1;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            dp2[i][j] = dp2[i - 1][j];
//            if (j >= v[i] && dp2[i - 1][j - v[i]] != -1) dp2[i][j] = max(dp2[i][j], dp2[i - 1][j - v[i]] + w[i]);
//        }
//    }
//    if (dp2[n][m] == -1) cout << 0 << endl;
//    else cout << dp2[n][m] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//
//int v[N], w[N];
//int dp1[N], dp2[N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//        for (int j = m; j >= v[i]; j--)
//            dp1[j] = max(dp1[j], dp1[j - v[i]] + w[i]);
//    cout << dp1[m] << endl;
//
//    for (int j = 1; j <= m; j++) dp2[j] = -1;
//    for (int i = 1; i <= n; i++)
//        for (int j = m; j >= v[i]; j--)
//            if (dp2[j - v[i]] != -1) dp2[j] = max(dp2[j], dp2[j - v[i]] + w[i]);
//    if (dp2[m] == -1) cout << 0 << endl;
//    else cout << dp2[m] << endl;
//    return 0;
//}


//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        if (sum % 2 == 1) return false;
//        int target = sum / 2;
//        vector<vector<bool>> dp(n + 1, vector<bool>(target + 1));
//        for (int i = 0; i <= n; i++) dp[i][0] = true;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= target; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1]) dp[i][j] = dp[i][j] || dp[i - 1][j - nums[i - 1]];
//            }
//        }
//        return dp[n][target];
//    }
//};


//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        if (sum % 2 == 1) return false;
//        int target = sum / 2;
//        vector<bool> dp(target + 1);
//        dp[0] = true;
//        for (int i = 1; i <= n; i++)
//            for (int j = target; j >= nums[i - 1]; j--)
//                dp[j] = dp[j] || dp[j - nums[i - 1]];
//        return dp[target];
//    }
//};


//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        int aim = (target + sum) / 2;
//        if (aim < 0 || (sum + target) % 2 == 1) return 0;
//        vector<vector<int>> dp(n + 1, vector<int>(aim + 1));
//        dp[0][0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 0; j <= aim; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i - 1]) dp[i][j] += dp[i - 1][j - nums[i - 1]];
//            }
//        }
//        return dp[n][aim];
//    }
//};


//class Solution {
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        int aim = (target + sum) / 2;
//        if (aim < 0 || (sum + target) % 2 == 1) return 0;
//        vector<int> dp(aim + 1);
//        dp[0] = 1;
//        for (int i = 1; i <= n; i++)
//            for (int j = aim; j >= nums[i - 1]; j--)
//                dp[j] += dp[j - nums[i - 1]];
//        return dp[aim];
//    }
//};


//class Solution {
//public:
//    int lastStoneWeightII(vector<int>& stones) {
//        int n = stones.size();
//        int sum = 0;
//        for (auto x : stones)
//            sum += x;
//        int target = sum / 2;
//        vector<vector<int>> dp(n + 1, vector<int>(target + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 0; j <= target; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= stones[i - 1]) dp[i][j] = max(dp[i][j], dp[i - 1][j - stones[i - 1]] + stones[i - 1]);
//            }
//        }
//        return sum - dp[n][target] - dp[n][target];
//    }
//};


//class Solution {
//public:
//	int lastStoneWeightII(vector<int>& stones) {
//		int n = stones.size();
//		int sum = 0;
//		for (auto x : stones)
//			sum += x;
//		int target = sum / 2;
//		vector<int> dp(target + 1);
//		for (int i = 1; i <= n; i++)
//			for (int j = target; j >= stones[i - 1]; j--)
//				dp[j] = max(dp[j], dp[j - stones[i - 1]] + stones[i - 1]);
//		return sum - dp[target] - dp[target];
//	}
//};