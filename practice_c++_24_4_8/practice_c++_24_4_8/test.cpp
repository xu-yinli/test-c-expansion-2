#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
////权重分配
//int w[17] = { 7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2 };
////对应校验码
//char m[12] = "10X98765432";
//bool flag[110];
//
//int main()
//{
//    int n;
//    cin >> n;
//    int k = 0;
//    while (n--)
//    {
//        string id;
//        cin >> id;
//        int sum = 0;
//        for (int i = 0; i < 17; i++)
//        {
//            if (id[i] >= '0' && id[i] <= '9')
//                sum += (id[i] - '0') * w[i];
//            else
//            {
//                cout << id << endl;
//                flag[k] = true;
//                break;
//            }
//        }
//        if (flag[k])
//        {
//            k++;
//            continue;
//        }
//        int z = sum % 11;
//        if (id[id.size() - 1] != m[z])
//        {
//            cout << id << endl;
//            flag[k] = true;
//        }
//        k++;
//    }
//    for (int i = 0; i < k; i++)
//    {
//        if (flag[i]) return 0;
//    }
//    cout << "All passed" << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//struct Stu
//{
//    char id[17]; //准考证号
//    int com_id; //试机座位号
//    int test_id; //考试座位号
//}s[1010];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//        cin >> s[i].id >> s[i].com_id >> s[i].test_id;
//    int m;
//    cin >> m;
//    while (m--)
//    {
//        int x; //待查询的试机座位号码
//        cin >> x;
//        for (int i = 0; i < n; i++)
//        {
//            if (x == s[i].com_id) //输出对应考生的准考证号和考试座位号码
//                cout << s[i].id << ' ' << s[i].test_id << endl;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    int max = 0;
//    int st = 0;
//    for (int i = 2; i <= n / i; i++)
//    {
//        if (n % i != 0) continue;
//        int k = n;
//        int j = i;
//        int len = 0;
//        while (k % j == 0)
//        {
//            k /= j;
//            j++;
//            len++;
//        }
//        if (len > max)
//        {
//            max = len;
//            st = i;
//        }
//    }
//    if (max == 0)
//        cout << 1 << endl << n << endl;
//    else
//    {
//        cout << max << endl;
//        cout << st;
//        for (int i = 1; i < max; i++)
//        {
//            cout << "*";
//            cout << st + i;
//        }
//        cout << endl;
//    }
//    return 0;
//}


//#include<iostream>
//using namespace std;
//
//bool f[100010]; //记录是否有朋友
//
//int main()
//{
//    int n; //已知朋友圈的个数
//    cin >> n;
//    while (n--)
//    {
//        int k; //朋友圈中的人数
//        cin >> k;
//        for (int i = 0; i < k; i++)
//        {
//            int id; //每人对应一个ID号
//            cin >> id;
//            if (k > 1) //朋友圈人数>1说明有朋友
//                f[id] = true;
//        }
//    }
//    int m; //待查询的人数
//    cin >> m;
//    int no_f = 0; //没朋友的人的人数 
//    while (m--)
//    {
//        int check_id; //待查询人的ID
//        cin >> check_id;
//        if (!f[check_id])
//        {
//            if (no_f == 0)
//                printf("%05d", check_id);
//            else
//                printf(" %05d", check_id);
//            no_f++;
//        }
//        f[check_id] = true;
//    }
//    if (no_f == 0)
//        cout << "No one is handsome" << endl;
//    return 0;
//}


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int n = nums.size();
//        sort(nums.begin(), nums.end());
//        for (int i = 0; i < n; i++)
//            if (i != nums[i])
//                return i;
//        return n;
//    }
//};


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int n = nums.size();
//        unordered_set<int> hash;
//        for (int i = 0; i < n; i++)
//            hash.insert(nums[i]);
//        for (int i = 0; i <= n; i++)
//            if (hash.count(i) == 0)
//                return i;
//        return -1;
//    }
//};


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int n = nums.size();
//        int sum = (n * (1 + n)) / 2;
//        int total = 0;
//        for (int i = 0; i < n; i++)
//            total += nums[i];
//        return sum - total;
//    }
//};


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        int res = 0;
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//            res ^= nums[i];
//        for (int i = 0; i <= n; i++)
//            res ^= i;
//        return res;
//    }
//};


//class Solution {
//public:
//    int getSum(int a, int b) {
//        return a + b;
//    }
//};


//class Solution {
//public:
//    int getSum(int a, int b) {
//        while (b)
//        {
//            int x = a ^ b; //无进位加法
//            unsigned int carry = (unsigned int)(a & b) << 1; //进位后结果
//            a = x;
//            b = carry;
//        }
//        return a;
//    }
//};


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int res = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            int sum = 0;
//            for (int x : nums)
//                sum += ((x >> i) & 1);
//            sum %= 3;
//            if (sum == 1) res |= (1 << i);
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> map;
//public:
//    int singleNumber(vector<int>& nums) {
//        for (int x : nums)
//            map[x]++;
//        int res = 0;
//        for (auto x : map)
//            if (x.second == 1)
//                res = x.first;
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<int> missingTwo(vector<int>& nums) {
//        int n = nums.size();
//        vector<bool> used(n + 2, false);
//        vector<int> res;
//        for (int x : nums)
//            used[x - 1] = true;
//        for (int i = 0; i < n + 2; i++)
//            if (used[i] == false)
//                res.push_back(i + 1);
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<int> missingTwo(vector<int>& nums) {
//        int res = 0;
//        int n = nums.size();
//        for (int x : nums)
//            res ^= x;
//        for (int i = 1; i <= n + 2; i++)
//            res ^= i;
//        int diff = 0;
//        while (1)
//        {
//            if (((res >> diff) & 1) == 1) break;
//            else diff++;
//        }
//        int a = 0, b = 0;
//
//        for (int x : nums)
//        {
//            if (((x >> diff) & 1) == 1) a ^= x;
//            else b ^= x;
//        }
//        for (int i = 1; i <= n + 2; i++)
//        {
//            if (((i >> diff) & 1) == 1) a ^= i;
//            else b ^= i;
//        }
//        return { a, b };
//    }
//};


//class Solution {
//public:
//    int findLengthOfLCIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n, 1);
//        int res = 1;
//        for (int i = 1; i < n; i++)
//        {
//            if (nums[i] > nums[i - 1])
//                dp[i] = dp[i - 1] + 1;
//            if (dp[i] > res)
//                res = dp[i];
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int findLengthOfLCIS(vector<int>& nums) {
//        int n = nums.size();
//        int res = 1;
//        int len = 1;
//        for (int i = 1; i < n; i++)
//        {
//            if (nums[i] > nums[i - 1])
//                len++;
//            else
//                len = 1;
//            if (len > res) res = len;
//        }
//        return res;
//    }
//};


//class Solution {
//    //以下标i-1为结尾的nums1和以下标j-1为结尾的nums2的最长重复子数组长度
//public:
//    int findLength(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size(), m = nums2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        int res = 0;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (nums1[i - 1] == nums2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                if (dp[i][j] > res)
//                    res = dp[i][j];
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int findLength(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size(), m = nums2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        int res = 0;
//        for (int i = 0; i < n; i++)
//            if (nums1[i] == nums2[0])
//                dp[i][0] = 1;
//        for (int j = 0; j < m; j++)
//            if (nums1[0] == nums2[j])
//                dp[0][j] = 1;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (nums1[i] == nums2[j] && i > 0 && j > 0)
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                if (dp[i][j] > res)
//                    res = dp[i][j];
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int minFallingPathSum(vector<vector<int>>& matrix) {
//        int n = matrix.size();
//        vector<vector<int>> dp(n + 1, vector<int>(n + 2, INT_MAX));
//        for (int j = 0; j < n + 2; j++)
//            dp[0][j] = 0;
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= n; j++)
//                dp[i][j] = min(dp[i - 1][j - 1], min(dp[i - 1][j], dp[i - 1][j + 1])) + matrix[i - 1][j - 1];
//        int res = INT_MAX;
//        for (int j = 1; j <= n; j++)
//            res = min(res, dp[n][j]);
//        return res;
//    }
//};