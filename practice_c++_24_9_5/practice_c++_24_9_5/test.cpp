#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    vector<vector<int>> generateMatrix(int n) {
//        vector<vector<int>> res(n, vector<int>(n));
//        int stX = 0, stY = 0;
//        int count = 1;
//        int offset = 1;
//        int loop = n / 2;
//        while (loop--)
//        {
//            int i = stX, j = stY;
//            for (j = stY; j < n - offset; j++)
//                res[i][j] = count++;
//            for (i = stX; i < n - offset; i++)
//                res[i][j] = count++;
//            for (; j > stY; j--)
//                res[i][j] = count++;
//            for (; i > stX; i--)
//                res[i][j] = count++;
//            offset++;
//            stX++; stY++;
//        }
//        if (n % 2 == 1) res[n / 2][n / 2] = count;
//        return res;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        ListNode* newHead = new ListNode(0);
//        newHead->next = head;
//        ListNode* cur = newHead;
//        while (cur->next)
//        {
//            if (cur->next->val == val) cur->next = cur->next->next;
//            else cur = cur->next;
//        }
//        return newHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        while (head && head->val == val)
//            head = head->next;
//        ListNode* cur = head;
//        while (cur && cur->next)
//        {
//            if (cur->next->val == val) cur->next = cur->next->next;
//            else cur = cur->next;
//        }
//        return head;
//    }
//};


//class MyLinkedList {
//private:
//    int size;
//    ListNode* dummyHead;
//public:
//    MyLinkedList() {
//        this->size = 0;
//        this->dummyHead = new ListNode(0);
//    }
//
//    int get(int index) {
//        if (index < 0 || index >= size) return -1;
//        ListNode* cur = dummyHead->next;
//        while (index--)
//            cur = cur->next;
//        return cur->val;
//    }
//
//    void addAtHead(int val) {
//        ListNode* newNode = new ListNode(val);
//        newNode->next = dummyHead->next;
//        dummyHead->next = newNode;
//        size++;
//    }
//
//    void addAtTail(int val) {
//        ListNode* newNode = new ListNode(val);
//        ListNode* cur = dummyHead;
//        while (cur->next)
//            cur = cur->next;
//        cur->next = newNode;
//        size++;
//    }
//
//    void addAtIndex(int index, int val) {
//        if (index<0 || index>size) return;
//        ListNode* newNode = new ListNode(val);
//        ListNode* cur = dummyHead;
//        while (index--)
//            cur = cur->next;
//        newNode->next = cur->next;
//        cur->next = newNode;
//        size++;
//    }
//
//    void deleteAtIndex(int index) {
//        if (index < 0 || index >= size) return;
//        ListNode* cur = dummyHead;
//        while (index--)
//            cur = cur->next;
//        ListNode* tmp = cur->next;
//        cur->next = cur->next->next;
//        delete tmp;
//        size--;
//    }
//};
//
///**
// * Your MyLinkedList object will be instantiated and called as such:
// * MyLinkedList* obj = new MyLinkedList();
// * int param_1 = obj->get(index);
// * obj->addAtHead(val);
// * obj->addAtTail(val);
// * obj->addAtIndex(index,val);
// * obj->deleteAtIndex(index);
// */


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        ListNode* newHead = new ListNode(0);
//        newHead->next = head;
//        ListNode* cur = newHead;
//        while (cur->next && cur->next->next)
//        {
//            ListNode* tmp = cur->next;
//            cur->next = cur->next->next;
//            tmp->next = tmp->next->next;
//            cur->next->next = tmp;
//            cur = cur->next->next;
//        }
//        return newHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* removeNthFromEnd(ListNode* head, int n) {
//        ListNode* newHead = new ListNode(0);
//        newHead->next = head;
//        ListNode* fast = newHead;
//        ListNode* slow = newHead;
//        n++;
//        while (n--)
//            fast = fast->next;
//        while (fast)
//        {
//            fast = fast->next;
//            slow = slow->next;
//        }
//        ListNode* tmp = slow->next;
//        slow->next = slow->next->next;
//        delete tmp;
//        return newHead->next;
//    }
//};


//class Solution {
//private:
//    stack<int> st;
//public:
//    bool isValid(string s) {
//        int n = s.size();
//        if (n % 2 != 0) return false;
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] == '(') st.push(')');
//            else if (s[i] == '[') st.push(']');
//            else if (s[i] == '{') st.push('}');
//            else if (st.empty()) return false;
//            else if (st.top() != s[i]) return false;
//            else if (st.top() == s[i]) st.pop();
//        }
//        if (st.empty()) return true;
//        return false;
//    }
//};


//class MyStack {
//private:
//    queue<int> q1;
//    queue<int> q2;
//public:
//    MyStack() {
//
//    }
//
//    void push(int x) {
//        q1.push(x);
//    }
//
//    int pop() {
//        while (q1.size() > 1)
//        {
//            q2.push(q1.front());
//            q1.pop();
//        }
//        int res = q1.front();
//        q1.pop();
//        q1 = q2;
//        while (q2.size())
//            q2.pop();
//        return res;
//    }
//
//    int top() {
//        return q1.back();
//    }
//
//    bool empty() {
//        return (q1.empty() && q2.empty());
//    }
//};
//
///**
// * Your MyStack object will be instantiated and called as such:
// * MyStack* obj = new MyStack();
// * obj->push(x);
// * int param_2 = obj->pop();
// * int param_3 = obj->top();
// * bool param_4 = obj->empty();
// */


//class MyQueue {
//private:
//    stack<int> in;
//    stack<int> out;
//public:
//    MyQueue() {
//
//    }
//
//    void push(int x) {
//        in.push(x);
//    }
//
//    int pop() {
//        while (in.size() > 1)
//        {
//            out.push(in.top());
//            in.pop();
//        }
//        int res = in.top();
//        in.pop();
//        while (out.size())
//        {
//            in.push(out.top());
//            out.pop();
//        }
//        return res;
//    }
//
//    int peek() {
//        while (in.size() > 1)
//        {
//            out.push(in.top());
//            in.pop();
//        }
//        int res = in.top();
//        while (out.size())
//        {
//            in.push(out.top());
//            out.pop();
//        }
//        return res;
//    }
//
//    bool empty() {
//        return (in.empty() && out.empty());
//    }
//};
//
///**
// * Your MyQueue object will be instantiated and called as such:
// * MyQueue* obj = new MyQueue();
// * obj->push(x);
// * int param_2 = obj->pop();
// * int param_3 = obj->peek();
// * bool param_4 = obj->empty();
// */


//class MyCircularQueue {
//private:
//    int front;
//    int rear;
//    int capacity;
//    vector<int> q;
//public:
//    MyCircularQueue(int k) {
//        this->front = 0;
//        this->rear = 0;
//        this->capacity = k + 1;
//        this->q = vector<int>(capacity);
//    }
//
//    bool enQueue(int value) {
//        if (isFull()) return false;
//        q[rear] = value;
//        rear = (rear + 1) % capacity;
//        return true;
//    }
//
//    bool deQueue() {
//        if (isEmpty()) return false;
//        front = (front + 1) % capacity;
//        return true;
//    }
//
//    int Front() {
//        if (isEmpty()) return -1;
//        return q[front];
//    }
//
//    int Rear() {
//        if (isEmpty()) return -1;
//        return q[(rear - 1 + capacity) % capacity];
//    }
//
//    bool isEmpty() {
//        if (front == rear) return true;
//        return false;
//    }
//
//    bool isFull() {
//        if ((rear + 1) % capacity == front) return true;
//        return false;
//    }
//};
//
///**
// * Your MyCircularQueue object will be instantiated and called as such:
// * MyCircularQueue* obj = new MyCircularQueue(k);
// * bool param_1 = obj->enQueue(value);
// * bool param_2 = obj->deQueue();
// * int param_3 = obj->Front();
// * int param_4 = obj->Rear();
// * bool param_5 = obj->isEmpty();
// * bool param_6 = obj->isFull();
// */


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* newHead = new ListNode(0);
//        ListNode* cur = head;
//        while (cur)
//        {
//            ListNode* tmp = cur->next;
//            cur->next = newHead->next;
//            newHead->next = cur;
//            cur = tmp;
//        }
//        return newHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* num1 = l1;
//        ListNode* num2 = l2;
//        int t = 0;
//        ListNode* head = new ListNode(0);
//        ListNode* cur = head;
//        while (num1 || num2 || t)
//        {
//            if (num1)
//            {
//                t += num1->val;
//                num1 = num1->next;
//            }
//            if (num2)
//            {
//                t += num2->val;
//                num2 = num2->next;
//            }
//            cur->next = new ListNode(t % 10);
//            cur = cur->next;
//            t /= 10;
//        }
//        cur = head->next;
//        delete head;
//        return cur;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        ListNode* newHead = new ListNode(0);
//        newHead->next = head;
//        ListNode* cur = newHead;
//        while (cur->next && cur->next->next)
//        {
//            ListNode* tmp = cur->next;
//            cur->next = cur->next->next;
//            tmp->next = tmp->next->next;
//            cur->next->next = tmp;
//            cur = cur->next->next;
//        }
//        cur = newHead->next;
//        delete newHead;
//        return cur;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    void reorderList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr || head->next->next == nullptr) return;
//        ListNode* fast = head;
//        ListNode* slow = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        ListNode* newHead = nullptr;
//        ListNode* cur = slow->next;
//        slow->next = nullptr;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = newHead;
//            newHead = cur;
//            cur = next;
//        }
//        ListNode* cur1 = head;
//        ListNode* cur2 = newHead;
//        ListNode* res = new ListNode(0);
//        while (cur1)
//        {
//            res->next = cur1;
//            cur1 = cur1->next;
//            res = res->next;
//            if (cur2)
//            {
//                res->next = cur2;
//                cur2 = cur2->next;
//                res = res->next;
//            }
//        }
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    void reorderList(ListNode* head) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* fast = dummyHead;
//        ListNode* slow = dummyHead;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        ListNode* prev = nullptr;
//        ListNode* cur = slow->next;
//        slow->next = nullptr;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = next;
//        }
//        ListNode* cur1 = head;
//        ListNode* cur2 = prev;
//        ListNode* res = new ListNode(0);
//        while (cur1)
//        {
//            res->next = cur1;
//            cur1 = cur1->next;
//            res = res->next;
//            if (cur2)
//            {
//                res->next = cur2;
//                cur2 = cur2->next;
//                res = res->next;
//            }
//        }
//    }
//};