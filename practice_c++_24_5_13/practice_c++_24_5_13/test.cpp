#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param schedule int整型vector<vector<>>
//     * @return bool布尔型
//     */
//    bool hostschedule(vector<vector<int> >& schedule) {
//        vector<int> t;
//        sort(schedule.begin(), schedule.end());
//        int n = schedule.size();
//        for (int i = 0; i < n; i++)
//        {
//            t.push_back(schedule[i][0]);
//            t.push_back(schedule[i][1]);
//        }
//        for (int i = 0; i < n - 1; i++)
//            if (t[i] > t[i + 1])
//                return false;
//        return true;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param schedule int整型vector<vector<>>
//     * @return bool布尔型
//     */
//    bool hostschedule(vector<vector<int> >& schedule) {
//        sort(schedule.begin(), schedule.end());
//        int n = schedule.size();
//        for (int i = 1; i < n; i++)
//            if (schedule[i][0] < schedule[i - 1][1])
//                return false;
//        return true;
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 510, M = 510 * 110 / 2;
//int nums[N];
//int dp[N][M];
//
//int main()
//{
//    int n;
//    cin >> n;
//    int sum = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> nums[i];
//        sum += nums[i];
//    }
//    if (sum % 2 == 1) cout << "false" << endl;
//    else
//    {
//        int target = sum /= 2;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 0; j <= target; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i]) dp[i][j] = max(dp[i][j], dp[i - 1][j - nums[i]] + nums[i]);
//                if (dp[i][j] == target)
//                {
//                    cout << "true" << endl;
//                    return 0;
//                }
//            }
//        }
//        cout << "false" << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 510, M = 510 * 110 / 2;
//int nums[N];
//int dp[N][M];
//
//int main()
//{
//    int n;
//    cin >> n;
//    int sum = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> nums[i];
//        sum += nums[i];
//    }
//    if (sum % 2 == 1) cout << "false" << endl;
//    else
//    {
//        int target = sum /= 2;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = target; j >= nums[i]; j--)
//            {
//                dp[i][j] = max(dp[i][j], dp[i - 1][j - nums[i]] + nums[i]);
//                if (dp[i][j] == target)
//                {
//                    cout << "true" << endl;
//                    return 0;
//                }
//            }
//        }
//        cout << "false" << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 510, M = 510 * 110 / 2;
//int nums[N], dp[M];
//
//int main()
//{
//    int n;
//    cin >> n;
//    int sum = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> nums[i];
//        sum += nums[i];
//    }
//    if (sum % 2 == 1) cout << "false" << endl;
//    else
//    {
//        int target = sum /= 2;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = target; j >= nums[i]; j--)
//            {
//                dp[j] = max(dp[j], dp[j - nums[i]] + nums[i]);
//                if (dp[j] == target)
//                {
//                    cout << "true" << endl;
//                    return 0;
//                }
//            }
//        }
//        cout << "false" << endl;
//    }
//    return 0;
//}


//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto x : nums)
//            sum += x;
//        if (sum % 2 == 1) return false;
//        int target = sum / 2;
//        vector<vector<int>> dp(n, vector<int>(target + 1));
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j <= target; j++)
//            {
//                dp[i][j] = dp[i - 1][j];
//                if (j >= nums[i]) dp[i][j] = max(dp[i][j], dp[i - 1][j - nums[i]] + nums[i]);
//                if (dp[i][j] == target) return true;
//            }
//        }
//        return false;
//    }
//};