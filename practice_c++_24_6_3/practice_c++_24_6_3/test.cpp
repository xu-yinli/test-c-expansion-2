#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//private:
//    string res;
//public:
//    static bool cmp(int x, int y)
//    {
//        string sx = to_string(x);
//        string sy = to_string(y);
//        string A = sx + sy;
//        string B = sy + sx;
//        return A < B;
//    }
//    string crackPassword(vector<int>& password) {
//        sort(password.begin(), password.end(), cmp);
//        for (int i = 0; i < password.size(); i++)
//            res += to_string(password[i]);
//        return res;
//    }
//};


//class Solution {
//private:
//    string res;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param numbers int整型vector
//     * @return string字符串
//     */
//    static bool cmp(int x, int y)
//    {
//        string sx = to_string(x);
//        string sy = to_string(y);
//        string A = sx + sy;
//        string B = sy + sx;
//        return A < B;
//    }
//    string PrintMinNumber(vector<int>& numbers) {
//        sort(numbers.begin(), numbers.end(), cmp);
//        for (int i = 0; i < numbers.size(); i++)
//            res += to_string(numbers[i]);
//        return res;
//    }
//};


///*
//struct ListNode {
//	int val;
//	struct ListNode *next;
//	ListNode(int x) :
//			val(x), next(NULL) {
//	}
//};*/
//class Solution {
//public:
//	int GetListLength(ListNode* list)
//	{
//		if (list == nullptr) return 0;
//		int len = 0;
//		while (list)
//		{
//			len++;
//			list = list->next;
//		}
//		return len;
//	}
//	ListNode* FindFirstCommonNode(ListNode* pHead1, ListNode* pHead2) {
//		if (pHead1 == nullptr || pHead2 == nullptr) return nullptr;
//		int length1 = GetListLength(pHead1);
//		int length2 = GetListLength(pHead2);
//		if (length1 >= length2)
//		{
//			int step = length1 - length2;
//			while (step--)
//			{
//				pHead1 = pHead1->next;
//			}
//		}
//		else
//		{
//			int step = length2 - length1;
//			while (step--)
//			{
//				pHead2 = pHead2->next;
//			}
//		}
//		while (pHead1 && pHead2)
//		{
//			if (pHead1 == pHead2)
//				return pHead1;
//			pHead1 = pHead1->next;
//			pHead2 = pHead2->next;
//		}
//		return nullptr;
//	}
//};


///*
//struct TreeNode {
//	int val;
//	struct TreeNode *left;
//	struct TreeNode *right;
//	TreeNode(int x) :
//			val(x), left(NULL), right(NULL) {
//	}
//};*/
//class Solution {
//public:
//	int TreeDepth(TreeNode* pRoot) {
//		if (pRoot == nullptr) return 0;
//		return max(TreeDepth(pRoot->left), TreeDepth(pRoot->right)) + 1;
//	}
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int calculateDepth(TreeNode* root) {
//        if (root == nullptr) return 0;
//        return max(calculateDepth(root->left), calculateDepth(root->right)) + 1;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    void treeDepth(TreeNode* root, int depth, int& maxi)
//    {
//        if (root == nullptr)
//        {
//            if (depth > maxi)
//                maxi = depth;
//            return;
//        }
//        treeDepth(root->left, depth + 1, maxi);
//        treeDepth(root->right, depth + 1, maxi);
//    }
//    int maxDepth(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        int depth = 0, maxi = 0;
//        treeDepth(root, depth, maxi);
//        return maxi;
//    }
//};


//#include <iostream>
//using namespace std;
//
//struct TreeNode
//{
//    int val;
//    struct TreeNode* left;
//    struct TreeNode* right;
//    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//};
//
//void createTree(TreeNode* root, int n)
//{
//    if (n == 0) return;
//    int rootVal, leftVal, rightVal;
//    cin >> rootVal >> leftVal >> rightVal;
//    root->val = rootVal;
//    if (leftVal != 0)
//    {
//        root->left = new TreeNode(leftVal);
//        createTree(root->left, n--);
//    }
//    if (rightVal != 0)
//    {
//        root->right = new TreeNode(rightVal);
//        createTree(root->right, n--);
//    }
//}
//
//int height(TreeNode* root)
//{
//    if (root == nullptr) return 0;
//    return max(height(root->left), height(root->right)) + 1;
//}
//
//bool isBalanced(TreeNode* root)
//{
//    if (root == nullptr) return true;
//    if (isBalanced(root->left) && isBalanced(root->right))
//    {
//        if (abs(height(root->left) - height(root->right)) <= 1)
//        {
//            return true;
//        }
//    }
//    return false;
//}
//
//int main()
//{
//    int n, rootVal;
//    cin >> n >> rootVal;
//    TreeNode* root = new TreeNode(rootVal);
//    createTree(root, n);
//    if (isBalanced(root)) cout << "true" << endl;
//    else cout << "false" << endl;
//    return 0;
//}


//class Solution {
//public:
//    vector<int> singleNumber(vector<int>& nums) {
//        int res = 0;
//        for (int a : nums)
//            res ^= a;
//        int k = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            if (((res >> i) & 1) == 1)
//            {
//                k = i;
//                break;
//            }
//        }
//        int x = 0, y = 0;
//        for (int a : nums)
//        {
//            if (((a >> k) & 1) == 1) x ^= a;
//            else y ^= a;
//        }
//        return { x, y };
//    }
//};


//class solution {
//public:
//    vector<int> findnumberswithsum(vector<int> array, int sum) {
//        int n = array.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int total = array[left] + array[right];
//            if (total > sum) right--;
//            else if (total < sum) left++;
//            else return { array[left], array[right] };
//        }
//        return {};
//    }
//};


//class Solution {
//private:
//    vector<vector<int> >res;
//public:
//    vector<vector<int>> fileCombination(int target) {
//        if (target < 3) return res;
//        int left = 1, right = 2;
//        while (left < right)
//        {
//            int sum = (left + right) * (right - left + 1) / 2; //求和公式
//            if (sum < target) right++;
//            else if (sum > target) left++;
//            else
//            {
//                vector<int> v;
//                for (int i = left; i <= right; i++)
//                    v.push_back(i);
//                res.push_back(v);
//                left++;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param sum int整型
//     * @return int整型vector<vector<>>
//     */
//    vector<vector<int> > FindContinuousSequence(int sum) {
//        if (sum < 3) return res;
//        int left = 1, right = 2;
//        while (left < right)
//        {
//            int total = (left + right) * (right - left + 1) / 2; //求和公式
//            if (total < sum) right++;
//            else if (total > sum) left++;
//            else
//            {
//                vector<int> v;
//                for (int i = left; i <= right; i++)
//                    v.push_back(i);
//                res.push_back(v);
//                left++;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    void removeExtraSpaces(string& s)
//    {
//        int slow = 0;
//        for (int fast = 0; fast < s.size(); fast++)
//        {
//            if (s[fast] != ' ')
//            {
//                if (slow != 0)
//                    s[slow++] = ' ';
//                while (fast < s.size() && s[fast] != ' ')
//                    s[slow++] = s[fast++];
//            }
//        }
//        s.resize(slow);
//    }
//
//    string reverseWords(string s) {
//        removeExtraSpaces(s);
//        reverse(s.begin(), s.end());
//        int st = 0;
//        for (int ed = 0; ed <= s.size(); ed++)
//        {
//            if (ed == s.size() || s[ed] == ' ')
//            {
//                reverse(s.begin() + st, s.begin() + ed);
//                st = ed + 1;
//            }
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    string ReverseSentence(string str) {
//        reverse(str.begin(), str.end());
//        int st = 0;
//        for (int ed = 0; ed <= str.size(); ed++)
//        {
//            if (ed == str.size() || str[ed] == ' ')
//            {
//                reverse(str.begin() + st, str.begin() + ed);
//                st = ed + 1;
//            }
//        }
//        return str;
//    }
//};


//class Solution {
//public:
//    string dynamicPassword(string password, int target) {
//        int n = password.size();
//        password += password;
//        return password.substr(target, n);
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param str string字符串
//     * @param n int整型
//     * @return string字符串
//     */
//    void ReverseString(string& str, int st, int ed)
//    {
//        while (st < ed)
//        {
//            char tmp = str[st];
//            str[st] = str[ed];
//            str[ed] = tmp;
//            st++;
//            ed--;
//        }
//    }
//    string LeftRotateString(string str, int n) {
//        if (str.size() == 0 || n == 0) return str;
//        n %= str.size();
//        ReverseString(str, 0, n - 1);
//        ReverseString(str, n, str.size() - 1);
//        ReverseString(str, 0, str.size() - 1);
//        return str;
//    }
//};


//class Solution {
//public:
//    void ReverseString(string& password, int st, int ed)
//    {
//        while (st < ed)
//        {
//            char tmp = password[st];
//            password[st] = password[ed];
//            password[ed] = tmp;
//            st++;
//            ed--;
//        }
//    }
//    string dynamicPassword(string password, int target) {
//        if (password.size() == 0 || target == 0) return password;
//        target %= password.size();
//        ReverseString(password, 0, target - 1);
//        ReverseString(password, target, password.size() - 1);
//        ReverseString(password, 0, password.size() - 1);
//        return password;
//    }
//};


//class Solution {
//public:
//    void leftRotate(string& password)
//    {
//        char tmp = password[0];
//        int len = password.size();
//        for (int i = 0; i < len - 1; i++)
//            password[i] = password[i + 1];
//        password[len - 1] = tmp;
//    }
//    string dynamicPassword(string password, int target) {
//        if (password.size() == 0 || target == 0) return password;
//        target %= password.size();
//        while (target--)
//            leftRotate(password);
//        return password;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//    stack<TreeNode*> st;
//    queue<TreeNode*> q;
//public:
//    vector<vector<int>> decorateRecord(TreeNode* root) {
//        if (root == nullptr) return res;
//        int dir = 1; // 1：左->右  2：右->左
//        st.push(root);
//        while (st.size())
//        {
//            vector<int> v;
//            int n = st.size();
//            while (n--)
//            {
//                TreeNode* node = st.top();
//                st.pop();
//                v.push_back(node->val);
//                TreeNode* first = nullptr;
//                TreeNode* second = nullptr;
//                if (dir == 1)
//                {
//                    first = node->left;
//                    second = node->right;
//                }
//                else
//                {
//                    first = node->right;
//                    second = node->left;
//                }
//                if (first) q.push(first);
//                if (second) q.push(second);
//            }
//            res.push_back(v);
//            while (q.size())
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                st.push(node);
//            }
//            if (dir == 1) dir = 2;
//            else dir = 1;
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    stack<TreeNode*> st;
//public:
//    int kthSmallest(TreeNode* root, int k) {
//        TreeNode* cur = root;
//        do
//        {
//            while (cur)
//            {
//                st.push(cur);
//                cur = cur->left;
//            }
//            if (st.size())
//            {
//                cur = st.top();
//                st.pop();
//                k--;
//                if (k == 0) return cur->val;
//                cur = cur->right;
//            }
//        } while (cur || st.size());
//        return -1;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    int res;
//public:
//    void dfs(TreeNode* root, int& cnt)
//    {
//        if (root == nullptr || cnt == 0) return;
//        dfs(root->right, cnt);
//        cnt--;
//        if (cnt == 0) res = root->val;
//        dfs(root->left, cnt);
//    }
//    int findTargetNode(TreeNode* root, int cnt) {
//        dfs(root, cnt);
//        return res;
//    }
//};