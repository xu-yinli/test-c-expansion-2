#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n + 1);
//        int res = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i] = max(nums[i - 1], dp[i - 1] + nums[i - 1]);
//            if (dp[i] > res) res = dp[i];
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxSubarraySumCircular(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1);
//        vector<int> g(n + 1);
//        int fmax = INT_MIN, g_min = INT_MAX;
//        int sum = 0;
//        for (int i = 1; i <= n; i++)
//        {
//            sum += nums[i - 1];
//            f[i] = max(nums[i - 1], f[i - 1] + nums[i - 1]);
//            g[i] = min(nums[i - 1], g[i - 1] + nums[i - 1]);
//            fmax = max(fmax, f[i]);
//            g_min = min(g_min, g[i]);
//        }
//        if (sum == g_min) return fmax;;
//        return max(fmax, sum - g_min);
//    }
//};


//class Solution {
//private:
//    unordered_map<int, vector<int>> edges;
//public:
//    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
//        vector<int> in(numCourses);
//        for (auto& e : prerequisites)
//        {
//            int a = e[0], b = e[1]; //b->a
//            edges[b].push_back(a);
//            in[a]++;
//        }
//        queue<int> q;
//        for (int i = 0; i < numCourses; i++)
//            if (in[i] == 0)
//                q.push(i);
//        while (q.size())
//        {
//            int t = q.front();
//            q.pop();
//            for (int a : edges[t])
//            {
//                in[a]--;
//                if (in[a] == 0)
//                    q.push(a);
//            }
//        }
//        for (int i = 0; i < numCourses; i++)
//            if (in[i] != 0) return false;
//        return true;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, vector<int>> edges;
//    vector<int> res;
//public:
//    vector<int> findOrder(int numCourses, vector<vector<int>>& prerequisites) {
//        vector<int> in(numCourses);
//        for (auto& e : prerequisites)
//        {
//            int a = e[0], b = e[1];
//            edges[b].push_back(a);
//            in[a]++;
//        }
//        queue<int> q;
//        for (int i = 0; i < numCourses; i++)
//            if (in[i] == 0) q.push(i);
//        while (q.size())
//        {
//            int t = q.front();
//            q.pop();
//            res.push_back(t);
//            for (int a : edges[t])
//            {
//                in[a]--;
//                if (in[a] == 0) q.push(a);
//            }
//        }
//        for (int i = 0; i < numCourses; i++)
//            if (in[i] != 0) return {};
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> findOrder(int numCourses, vector<vector<int>>& prerequisites) {
//        vector<vector<int>> edges(numCourses);
//        vector<int> in(numCourses);
//        for (auto& e : prerequisites)
//        {
//            int a = e[0], b = e[1];
//            edges[b].push_back(a);
//            in[a]++;
//        }
//        queue<int> q;
//        for (int i = 0; i < numCourses; i++)
//            if (in[i] == 0) q.push(i);
//        while (q.size())
//        {
//            int t = q.front();
//            q.pop();
//            res.push_back(t);
//            for (int a : edges[t])
//            {
//                in[a]--;
//                if (in[a] == 0) q.push(a);
//            }
//        }
//        if (res.size() == numCourses) return res;
//        return {};
//    }
//};


//class Solution {
//private:
//    unordered_map<char, unordered_set<char>> edges;
//    unordered_map<char, int> in;
//    bool check;
//public:
//    void add(string& s1, string& s2)
//    {
//        int n = min(s1.size(), s2.size());
//        int i = 0;
//        for (; i < n; i++)
//        {
//            if (s1[i] != s2[i])
//            {
//                char a = s1[i], b = s2[i]; //a->b
//                if (!edges.count(a) || !edges[a].count(b))
//                {
//                    edges[a].insert(b);
//                    in[b]++;
//                }
//                break;
//            }
//        }
//        if (i == s2.size() && i < s1.size())
//            check = true;
//    }
//    string alienOrder(vector<string>& words) {
//        for (auto& s : words)
//            for (auto ch : s)
//                in[ch] = 0;
//        int n = words.size();
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = i + 1; j < n; j++)
//            {
//                add(words[i], words[j]);
//                if (check) return "";
//            }
//        }
//        queue<char> q;
//        for (auto& [a, b] : in)
//            if (b == 0) q.push(a);
//        string res;
//        while (q.size())
//        {
//            char t = q.front();
//            q.pop();
//            res += t;
//            for (char ch : edges[t])
//            {
//                in[ch]--;
//                if (in[ch] == 0) q.push(ch);
//            }
//        }
//        for (auto& [a, b] : in)
//            if (b != 0) return "";
//        return res;
//    }
//};