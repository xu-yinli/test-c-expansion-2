#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 15;
//int a[N];
//bool vis[N];
//int n, res;
//
//void dfs(int pos)
//{
//    if (pos == n + 1)
//    {
//        res++;
//        return;
//    }
//    for (int i = 1; i <= n; i++)
//    {
//        if (vis[i]) continue;
//        if (vis[a[i]]) return;
//        vis[i] = true;
//        dfs(pos + 1);
//        vis[i] = false;
//    }
//}
//
//int main()
//{
//    cin >> n;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    dfs(1);
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 1e6 + 10;
//int a[N];
//
//int main()
//{
//    int n, p;
//    cin >> n >> p;
//    for (int i = 0; i < n; i++)
//        cin >> a[i];
//    sort(a, a + n);
//    int res = 0;
//    int mini = a[0], maxi = a[n - 1];
//    for (int k = mini; k <= maxi; k++)
//    {
//        int target = max(k - p, 1);
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (a[mid] >= target) right = mid;
//            else left = mid + 1;
//        }
//        int l = left;
//
//        left = 0, right = n - 1;
//        target = k + p;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (a[mid] <= target) left = mid;
//            else right = mid - 1;
//        }
//        res = max(res, right - l + 1);
//    }
//    cout << res << endl;
//    return 0;
//}


///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//private:
//    int maxSum = -1010;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param root TreeNode类
//     * @return int整型
//     */
//    int dfs(TreeNode* node)
//    {
//        if (node == nullptr) return 0;
//        int leftSum = max(0, dfs(node->left));
//        int rightSum = max(0, dfs(node->right));
//        maxSum = max(maxSum, node->val + leftSum + rightSum);
//        return node->val + max(leftSum, rightSum);
//    }
//    int maxPathSum(TreeNode* root) {
//        dfs(root);
//        return maxSum;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    int maxSum = -1010;
//public:
//    int dfs(TreeNode* node)
//    {
//        if (node == nullptr) return 0;
//        int leftSum = max(0, dfs(node->left));
//        int rightSum = max(0, dfs(node->right));
//        maxSum = max(maxSum, node->val + leftSum + rightSum);
//        return node->val + max(leftSum, rightSum);
//    }
//    int maxPathSum(TreeNode* root) {
//        dfs(root);
//        return maxSum;
//    }
//};


//#include <iostream>
//#include <cstring>
//using namespace std;
//
//const int N = 10010;
//bool s[N];
//
//int main()
//{
//    int t;
//    cin >> t;
//    while (t--)
//    {
//        memset(s, false, sizeof(s));
//        int y1, y2, n;
//        cin >> y1 >> n;
//        for (int i = 1; i <= n; i++)
//        {
//            int x;
//            cin >> x;
//            s[x] = true;
//        }
//        cin >> y2;
//        int res = 0;
//        for (int i = y1; i <= y2; i++)
//            if (!s[i])
//                res++;
//        cout << res << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <cstring>
//using namespace std;
//
//const int N = 10;
//int n, m, k, res;
//int map[N][N];
//
//void dfs(int t)
//{
//    res = min(res, t);
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            if (map[i][j]==1)
//            {
//                
//                if (i > 1 && i < n)
//                { 
//                    if (map[i - 1][j] == 1 && map[i + 1][j] == 0) //向下跳
//                    {
//                        map[i - 1][j] = map[i][j] = 0;
//                        map[i + 1][j] = 1;
//                        dfs(t - 1);
//                        map[i - 1][j] = map[i][j] = 1;
//                        map[i + 1][j] = 0;
//                    }
//                    if (map[i - 1][j] == 0 && map[i + 1][j] == 1) //向上跳
//                    {
//                        map[i + 1][j] = map[i][j] = 0;
//                        map[i - 1][j] = 1;
//                        dfs(t - 1);
//                        map[i + 1][j] = map[i][j] = 1;
//                        map[i - 1][j] = 0;
//                    }
//                }
//
//                if (j > 1 && j < m)
//                {
//                    if (map[i][j - 1] == 1 && map[i][j + 1] == 0) // 向右跳
//                    {
//                        map[i][j - 1] = map[i][j] = 0;
//                        map[i][j + 1] = 1;
//                        dfs(t - 1);
//                        map[i][j - 1] = map[i][j] = 1;
//                        map[i][j + 1] = 0;
//                    }
//                    if (map[i][j - 1] == 0 && map[i][j + 1] == 1) // 向左跳
//                    {
//                        map[i][j + 1] = map[i][j] = 0;
//                        map[i][j - 1] = 1;
//                        dfs(t - 1);
//                        map[i][j + 1] = map[i][j] = 1;
//                        map[i][j - 1] = 0;
//                    }
//                }
//            }
//        }
//    }
//}
//
//int main()
//{
//    int t;
//    cin >> t;
//    while (t--)
//    {
//        cin >> n >> m >> k;
//        memset(map, 0, sizeof(map));
//        for(int i=0; i<k; i++)
//        {
//            int x, y;
//            cin >> x >> y;
//            map[x][y] = 1;
//        }
//        res = k;
//        dfs(k);
//        cout << res << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//typedef long long LL;
//typedef pair<int, int> PII;
//const int N = 1e5 + 10;
//PII sale[N];
//
//int main()
//{
//	int t;
//    scanf("%d", &t);
//	while (t--)
//	{
//		int n;
//        scanf("%d", &n);
//		for (int i = 0; i < n; i++)
//            scanf("%d %d", &sale[i].first, &sale[i].second);
//        sort(sale, sale + n);
//		LL sum = 0;
//        int left = 0, right = n - 1;
//        while(left<right)
//        {
//            LL cnt = min(sale[left].second, sale[right].second); //交易次数
//            LL profit = (LL)(sale[right].first - sale[left].first) * cnt; //一次买卖的盈利
//            sum += profit;
//            sale[left].second -= cnt;
//            sale[right].second -= cnt;
//            if (sale[left].second == 0)
//                left++;
//            if (sale[right].second == 0)
//                right--;
//        }
//        printf("%lld\n", sum);
//	}
//	return 0;
//}