#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    string addStrings(string num1, string num2) {
//        string sum;
//        int cur1 = num1.size() - 1, cur2 = num2.size() - 1;
//        int t = 0;
//        while (cur1 >= 0 || cur2 >= 0 || t)
//        {
//            if (cur1 >= 0) t += num1[cur1--] - '0';
//            if (cur2 >= 0) t += num2[cur2--] - '0';
//            sum += t % 10 + '0';
//            t /= 10;
//        }
//        reverse(sum.begin(), sum.end());
//        return sum;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string s;
//    getline(cin, s);
//    size_t pos = s.rfind(' ');
//    cout << s.size() - 1 - pos << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int cnt[256];
//
//int main()
//{
//    string s;
//    cin >> s;
//    int n = s.size();
//    for (int i = 0; i < n; i++)
//        cnt[s[i]]++;
//    for (int i = 0; i < n; i++)
//    {
//        if (cnt[s[i]] == 1)
//        {
//            cout << s[i] << endl;
//            return 0;
//        }
//    }
//    cout << -1 << endl;
//    return 0;
//}


//class Solution {
//private:
//    int cnt[256];
//public:
//    int firstUniqChar(string s) {
//        int n = s.size();
//        for (int i = 0; i < n; i++)
//            cnt[s[i]]++;
//        for (int i = 0; i < n; i++)
//            if (cnt[s[i]] == 1)
//                return i;
//        return -1;
//    }
//};


//class Solution {
//public:
//    bool isLetterOrNumber(char ch)
//    {
//        return (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9');
//    }
//
//    bool isPalindrome(string s) {
//        int n = s.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] >= 'A' && s[i] <= 'Z')
//                s[i] = tolower(s[i]);
//        }
//        int st = 0, ed = n - 1;
//        while (st < ed)
//        {
//            while (st < ed && !isLetterOrNumber(s[st]))
//                st++;
//            while (st < ed && !isLetterOrNumber(s[ed]))
//                ed--;
//            if (s[st] == s[ed])
//            {
//                st++;
//                ed--;
//            }
//            else return false;
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    string reverseStr(string s, int k) {
//        int n = s.size();
//        for (int i = 0; i < n; i += 2 * k)
//        {
//            if (i + k <= s.size()) reverse(s.begin() + i, s.begin() + i + k);
//            else reverse(s.begin() + i, s.end());
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    string reverseWords(string s) {
//        int n = s.size();
//        int left = 0, right = 0;
//        while (right <= n)
//        {
//            if (s[right] == ' ' || right == n)
//            {
//                reverse(s.begin() + left, s.begin() + right);
//                right++;
//                left = right;
//            }
//            else right++;
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    string reverseOnlyLetters(string s) {
//        int left = 0, right = s.size() - 1;
//        while (left < right)
//        {
//            while (left < right && !isalpha(s[left])) left++;
//            while (left < right && !isalpha(s[right])) right--;
//            if (left < right)
//            {
//                swap(s[left], s[right]);
//                left++; right--;
//            }
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    string multiply(string num1, string num2) {
//        int n = num1.size(), m = num2.size();
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        vector<int> sum(n + m - 1);
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                sum[i + j] += (num1[i] - '0') * (num2[j] - '0');
//        string res;
//        int t = 0;
//        int cur = 0;
//        while (cur < n + m - 1 || t)
//        {
//            if (cur < n + m - 1) t += sum[cur++];
//            res += t % 10 + '0';
//            t /= 10;
//        }
//        while (res.size() > 1 && res.back() == '0') res.pop_back();
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};