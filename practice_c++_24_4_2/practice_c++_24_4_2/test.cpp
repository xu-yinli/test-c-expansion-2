#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    bool isAnagram(string s, string t) {
//        if (s.size() != t.size()) return false;
//        sort(s.begin(), s.end());
//        sort(t.begin(), t.end());
//        return s == t;
//    }
//};


//class Solution {
//private:
//    int a[27];
//public:
//    bool isAnagram(string s, string t) {
//        for (int i = 0; i < s.size(); i++)
//            a[s[i] - 'a']++;
//        for (int i = 0; i < t.size(); i++)
//            a[t[i] - 'a']--;
//        for (int i = 0; i < 26; i++)
//            if (a[i] != 0) return false;
//        return true;
//    }
//};


//class Solution {
//public:
//    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
//        unordered_set<int> res;
//        unordered_set<int> tmp(nums1.begin(), nums1.end());
//        for (auto e : nums2)
//        {
//            if (tmp.find(e) != tmp.end())
//                res.insert(e);
//        }
//        return vector<int>(res.begin(), res.end());
//    }
//};


//class Solution {
//private:
//    int hash[1010];
//    unordered_set<int> res;
//public:
//    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
//        for (auto e : nums1)
//            hash[e] = 1;
//        for (auto e : nums2)
//        {
//            if (hash[e] == 1)
//                res.insert(e);
//        }
//        return vector<int>(res.begin(), res.end());
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> map;
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        for (int i = 0; i < nums.size(); i++)
//        {
//            auto k = map.find(target - nums[i]);
//            if (k != map.end())
//                return { k->second, i };
//            map[nums[i]] = i;
//        }
//        return {};
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> map;
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        for (int i = 0; i < nums.size(); i++)
//        {
//            auto k = map.find(target - nums[i]);
//            if (k != map.end())
//                return { k->second, i };
//            map.insert(pair<int, int>(nums[i], i));
//        }
//        return {};
//    }
//};


//class Solution {
//private:
//    int res;
//    unordered_map<int, int> map;
//public:
//    int fourSumCount(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3, vector<int>& nums4) {
//        for (int i = 0; i < nums1.size(); i++)
//        {
//            for (int j = 0; j < nums2.size(); j++)
//            {
//                int sum1 = nums1[i] + nums2[j];
//                map[sum1]++;
//            }
//        }
//        for (int i = 0; i < nums3.size(); i++)
//        {
//            for (int j = 0; j < nums4.size(); j++)
//            {
//                int sum2 = nums3[i] + nums4[j];
//                if (map.find(-sum2) != map.end())
//                    res += map[-sum2];
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    int res;
//    unordered_map<int, int> map;
//public:
//    int fourSumCount(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3, vector<int>& nums4) {
//        for (auto a : nums1)
//        {
//            for (auto b : nums2)
//            {
//                map[a + b]++;
//            }
//        }
//        for (auto c : nums3)
//        {
//            for (auto d : nums4)
//            {
//                if (map.find(-(c + d)) != map.end())
//                    res += map[-(c + d)];
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int res = 0;
//        for (int i = 0; i < prices.size(); i++)
//        {
//            for (int j = i + 1; j < prices.size(); j++)
//            {
//                res = max(res, prices[j] - prices[i]);
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//    //dp[i][0] -- 第i天持有股票所得最多现金
//    //dp[i][1] -- 第i天不持有股票所得最多现金
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(2));
//        dp[0][0] = -prices[0];
//        dp[0][1] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], 0 - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i]);
//        }
//        return dp[n - 1][1];
//    }
//};


//class Solution {
//    //dp[i][0] -- 第i天持有股票所得最多现金
//    //dp[i][1] -- 第i天不持有股票所得最多现金
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(2, vector<int>(2));
//        dp[0][0] = -prices[0];
//        dp[0][1] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            dp[i % 2][0] = max(dp[(i - 1) % 2][0], -prices[i]);
//            dp[i % 2][1] = max(dp[(i - 1) % 2][1], dp[(i - 1) % 2][0] + prices[i]);
//        }
//        return dp[(n - 1) % 2][1];
//    }
//};


//class Solution {
//    //dp[i][0] -- 第i天持有股票所得现金
//    //dp[i][1] -- 第i天不持有股票所得最多现金
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(2));
//        dp[0][0] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i]);
//        }
//        return dp[n - 1][1];
//    }
//};


//class Solution {
//    //dp[i][0] -- 第i天持有股票所得现金
//    //dp[i][1] -- 第i天不持有股票所得最多现金
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(2, vector<int>(2));
//        dp[0][0] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i % 2][0] = max(dp[(i - 1) % 2][0], dp[(i - 1) % 2][1] - prices[i]);
//            dp[i % 2][1] = max(dp[(i - 1) % 2][1], dp[(i - 1) % 2][0] + prices[i]);
//        }
//        return dp[(n - 1) % 2][1];
//    }
//};


//class Solution {
//    //dp[i][0] -- 没有操作
//    //dp[i][1] -- 第一次持有股票
//    //dp[i][2] -- 第一次不持有股票
//    //dp[i][3] -- 第二次持有股票
//    //dp[i][4] -- 第二次不持有股票
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(5));
//        dp[0][0] = 0;
//        dp[0][1] = -prices[0];
//        dp[0][2] = 0;
//        dp[0][3] = -prices[0];
//        dp[0][4] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = dp[i - 1][0];
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] - prices[i]);
//            dp[i][2] = max(dp[i - 1][2], dp[i - 1][1] + prices[i]);
//            dp[i][3] = max(dp[i - 1][3], dp[i - 1][2] - prices[i]);
//            dp[i][4] = max(dp[i - 1][4], dp[i - 1][3] + prices[i]);
//        }
//        return dp[n - 1][4];
//    }
//};


//class Solution {
//    //dp[0] -- 没有操作
//    //dp[1] -- 第一次持有股票
//    //dp[2] -- 第一次不持有股票
//    //dp[3] -- 第二次持有股票
//    //dp[4] -- 第二次不持有股票
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<int> dp(5);
//        dp[1] = -prices[0];
//        dp[3] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[1] = max(dp[1], dp[0] - prices[i]);
//            dp[2] = max(dp[2], dp[1] + prices[i]);
//            dp[3] = max(dp[3], dp[2] - prices[i]);
//            dp[4] = max(dp[4], dp[3] + prices[i]);
//        }
//        return dp[4];
//    }
//};


//class Solution {
//    //dp[i][0] -- 没有操作
//    //下面j为奇数：买入；j为偶数：卖出 (j的范围：1~2k-1)
//    //dp[i][j] -- 第1~k次买入
//    //dp[i][j+1] -- 第1~k次卖出
//public:
//    int maxProfit(int k, vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(2 * k + 1));
//        for (int j = 1; j < 2 * k; j += 2)
//            dp[0][j] = -prices[0];
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < 2 * k; j += 2)
//            {
//                dp[i][j + 1] = max(dp[i - 1][j + 1], dp[i - 1][j] - prices[i]);
//                dp[i][j + 2] = max(dp[i - 1][j + 2], dp[i - 1][j + 1] + prices[i]);
//            }
//        }
//        return dp[n - 1][2 * k];
//    }
//};


//class Solution {
//    //dp[i][0] -- 保持持有股票状态
//    //dp[i][1] -- 保持卖出股票状态（不包含状态三当天）
//    //dp[i][2] -- 今天卖出股票（下一天不是状态二，而是状态四冷冻期）
//    //dp[i][3] -- 冷冻期期间（这里冷冻期只有一天）
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(4));
//        dp[0][0] = -prices[0];
//        dp[0][1] = dp[0][2] = dp[0][3] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(max(dp[i - 1][0], dp[i - 1][1] - prices[i]), dp[i - 1][3] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][3]);
//            dp[i][2] = dp[i - 1][0] + prices[i];
//            dp[i][3] = dp[i - 1][2];
//        }
//        return max(max(dp[n - 1][1], dp[n - 1][2]), dp[n - 1][3]);
//    }
//};


//class Solution {
//    //dp[i][0] -- 第i天持有股票所剩最多现金
//    //dp[i][1] -- 第i天不持有股票所得最多现金
//public:
//    int maxProfit(vector<int>& prices, int fee) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(2));
//        dp[0][0] = -prices[0];
//        dp[0][1] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i] - fee);
//        }
//        return dp[n - 1][1];
//    }
//};


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int n=2021;
//	for (int i = n; i >= 1; i -= 2)
//	{
//		res *= i;
//		res %= 100000;
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//int main()
//{
//	int target = 2021;
//	LL res = 0;
//	for (int i = 1; i <= target; i++)
//	{
//		for (int j = 1; j <= target; j++)
//		{
//			if (i * j <= target)
//				res++;
//		}
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//int main()
//{
//	int n = 2021;
//	LL res = 0;
//	for (int i = 1; i < n; i++)
//	{
//		for (int j = 1; j < n; j++)
//		{
//			for(int k = 1; k < n; k++)
//			{
//				for (int p = 1; p < n; p++)
//				{
//					for (int q = 1; q < n; q++)
//					{
//						if (i + j + k + p + q == n)
//							res++;
//					}
//				}
//			}
//		}
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//int main()
//{
//	//求C(2020, 4) 从2020个物品中选出4个物品的组合数
//	int n = 2020;
//	LL res = 1;
//	int k = 4;
//	while (k--)
//	{
//		res *= n;
//		n--;
//	}
//	for (int i = 4; i >= 1; i--)
//	{
//		res /= i;
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
////7*10+9*10      //7*2+8*3+10*4+9*3+11*8
////一只青蛙一张嘴，两只眼睛四条腿。
////两只青蛙两张嘴，四只眼睛八条腿。
////三只青蛙三张嘴，六只眼睛十二条腿。
////四只青蛙四张嘴，八只眼睛十六条腿。
////五只青蛙五张嘴，十只眼睛二十条腿。
////六只青蛙六张嘴，十二只眼睛二十四条腿。
////七只青蛙七张嘴，十四只眼睛二十八条腿。
////八只青蛙八张嘴，十六只眼睛三十二条腿。
////九只青蛙九张嘴，十八只眼睛三十六条腿。
////十只青蛙十张嘴，二十只眼睛四十条腿。
////十一只青蛙十一张嘴，二十二只眼睛四十四条腿。
////十二只青蛙十二张嘴，二十四只眼睛四十八条腿。
////十三只青蛙十三张嘴，二十六只眼睛五十二条腿。
////十四只青蛙十四张嘴，二十八只眼睛五十六条腿。
////十五只青蛙十五张嘴，三十只眼睛六十条腿。
////十六只青蛙十六张嘴，三十二只眼睛六十四条腿。
////十七只青蛙十七张嘴，三十四只眼睛六十八条腿。
////十八只青蛙十八张嘴，三十六只眼睛七十二条腿。
////十九只青蛙十九张嘴，三十八只眼睛七十六条腿。
////二十只青蛙二十张嘴，四十只眼睛八十条腿。
//
//int number(int x)
//{
//	if (x <= 10) return 1; //个位数和十
//	if (x % 10 == 0 || (x >= 11 && x < 20))
//		return 2; //整十数(这里不包括十)和十几位数
//	return 3; //其他数
//}
//
//int main()
//{
//	int res = 0;
//	//统计第一个只前面的数字的字数
//	for (int i = 1; i <= 20; i++)
//		res += number(i);
//
//	//统计张前面的数字的字数(只需要将前面的结果*2)
//	res *= 2;
//
//	for (int i = 2; i <= 40; i += 2)
//		res += number(i);
//	for (int i = 4; i <= 80; i += 4)
//		res += number(i);
//
//	//其它文字(只青蛙张嘴，只眼睛条腿)
//	res += 10 * 20;
//
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int gcd(int a, int b)
//{
//	return b ? gcd(b, a % b) : a;
//}
//
//int main()
//{
//	int n = 1018;
//	int res = 0;
//	for (int i = 1; i <= 2020; i++)
//	{
//		if (gcd(i, n) == 1)
//			res++;
//	}
//	cout << res << endl;
//	return 0;
//}


#include <iostream>
#include <cstring>
using namespace std;

const int N = 1010;
int n, m;
int f[N][N];
char g[N][N];

int dx[4] = { -1, 0, 1, 0 }, dy[4] = { 0, 1, 0, -1 };

int dfs(int x, int y) //从(x, y)这个位置开始的上升子串的数量
{
	if (f[x][y] != -1)
		return f[x][y];
	f[x][y] = 1; //至少有一个上升子串
	for (int i = 0; i < 4; i++)
	{
		int a = x + dx[i], b = y + dy[i];
		if (a >= 1 && a <= n && b >= 1  && b <= m && g[a][b] > g[x][y])
			f[x][y] += dfs(a, b);
	}
	return f[x][y];
}

int main()
{
	cin >> n >> m;
	memset(f, -1, sizeof f);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			cin >> g[i][j];
	int res = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			res += dfs(i, j);
	cout << res << endl;
	return 0;
}