#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int countDigitOne(int n) {
//        int cnt = 0;
//        int back = 0;
//        for (int i = 0; n != 0; i++)
//        {
//            int front = n / 10;
//            if (front * 10 + 1 < n) front++;
//            else if (front * 10 + 1 == n) cnt += back + 1;
//            cnt += front * pow(10, i);
//            back += n % 10 * pow(10, i);
//            n /= 10;
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    int digitOneInNumber(int num) {
//        int cnt = 0;
//        int back = 0;
//        for (int i = 0; num != 0; i++)
//        {
//            int front = num / 10;
//            if (front * 10 + 1 < num) front++;
//            else if (front * 10 + 1 == num) cnt += back + 1;
//            cnt += front * pow(10, i);
//            back += num % 10 * pow(10, i);
//            num /= 10;
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    int NumberOf1Between1AndN_Solution(int n) {
//        int cnt = 0;
//        int back = 0;
//        for (int i = 0; n != 0; i++)
//        {
//            int front = n / 10;
//            if (front * 10 + 1 < n) front++;
//            else if (front * 10 + 1 == n) cnt += back + 1;
//            cnt += front * pow(10, i);
//            back += n % 10 * pow(10, i);
//            n /= 10;
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    int nthUglyNumber(int n) {
//        vector<int> dp(n + 1);
//        dp[1] = 1;
//        int p2 = 1, p3 = 1, p5 = 1;
//        for (int i = 2; i <= n; i++)
//        {
//            int num2 = dp[p2] * 2;
//            int num3 = dp[p3] * 3;
//            int num5 = dp[p5] * 5;
//            dp[i] = min(num2, min(num3, num5));
//            if (dp[i] == num2) p2++;
//            if (dp[i] == num3) p3++;
//            if (dp[i] == num5) p5++;
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int nthUglyNumber(int n) {
//        vector<int> dp(n + 1);
//        dp[1] = 1;
//        int a = 1, b = 1, c = 1;
//        for (int i = 2; i <= n; i++)
//        {
//            int x = dp[a] * 2;
//            int y = dp[b] * 3;
//            int z = dp[c] * 5;
//            dp[i] = min(x, min(y, z));
//            if (dp[i] == x) a++;
//            if (dp[i] == y) b++;
//            if (dp[i] == z) c++;
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param index int整型
//     * @return int整型
//     */
//    int GetUglyNumber_Solution(int index) {
//        if (index == 0) return 0;
//        vector<int> dp(index + 1);
//        dp[1] = 1;
//        int a = 1, b = 1, c = 1;
//        for (int i = 2; i <= index; i++)
//        {
//            int x = dp[a] * 2;
//            int y = dp[b] * 3;
//            int z = dp[c] * 5;
//            dp[i] = min(x, min(y, z));
//            if (dp[i] == x) a++;
//            if (dp[i] == y) b++;
//            if (dp[i] == z) c++;
//        }
//        return dp[index];
//    }
//};


//class Solution {
//private:
//    unordered_map<char, int> hash;
//public:
//    char dismantlingAction(string arr) {
//        for (auto ch : arr)
//            hash[ch]++;
//        for (auto ch : arr)
//            if (hash[ch] == 1)
//                return ch;
//        return ' ';
//    }
//};


//class Solution {
//private:
//    unordered_map<char, int> hash;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param str string字符串
//     * @return int整型
//     */
//    int FirstNotRepeatingChar(string str) {
//        int n = str.size();
//        for (int i = 0; i < n; i++)
//            hash[str[i]]++;
//        for (int i = 0; i < n; i++)
//            if (hash[str[i]] == 1)
//                return i;
//        return -1;
//    }
//};


//class Solution {
//private:
//    int a[27];
//public:
//    bool isAnagram(string s, string t) {
//        for (auto ch : s)
//            a[ch - 'a']++;
//        for (auto ch : t)
//            a[ch - 'a']--;
//        for (int i = 0; i < 26; i++)
//            if (a[i] != 0) return false;
//        return true;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int a[27];
//
//int main()
//{
//    string s1, s2, res;
//    getline(cin, s1);
//    getline(cin, s2);
//    for (auto ch : s2)
//        a[ch]++;
//    for (auto ch : s1)
//        if (a[ch] == 0)
//            res += ch;
//    cout << res << endl;
//    return 0;
//}


//class Solution {
//public:
//    string removeDuplicateLetters(string s) {
//        string res;
//        vector<int> cnt(26), used(26);
//        for (auto ch : s)
//            cnt[ch - 'a']++;
//        for (auto ch : s)
//        {
//            cnt[ch - 'a']--;
//            if (used[ch - 'a'] != 0) continue;
//            while (!res.empty() && ch < res.back() && cnt[res.back() - 'a']>0)
//            {
//                used[res.back() - 'a'] = false;
//                res.pop_back();
//            }
//            res += ch;
//            used[ch - 'a'] = true;
//        }
//        return res;
//    }
//};