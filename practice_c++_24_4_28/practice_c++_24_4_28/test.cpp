#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    long long prev = LONG_LONG_MIN;
//public:
//    bool isValidBST(TreeNode* root) {
//        if (root == nullptr) return true;
//        bool left = isValidBST(root->left);
//        bool cur = false;
//        if (root->val > prev)
//            cur = true;
//        prev = root->val;
//        bool right = isValidBST(root->right);
//        return left && right && cur;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    long long prev = LONG_LONG_MIN;
//public:
//    bool isValidBST(TreeNode* root) {
//        if (root == nullptr) return true;
//        bool left = isValidBST(root->left);
//        if (left == false) return false; //剪枝
//
//        bool cur = false;
//        if (root->val > prev)
//            cur = true;
//        prev = root->val;
//        if (cur == false) return false; //剪枝
//
//        bool right = isValidBST(root->right);
//        return left && right && cur;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    int res;
//public:
//    void dfs(TreeNode* root, int& k)
//    {
//        if (root == nullptr || k == 0) return;
//        dfs(root->left, k);
//        k--;
//        if (k == 0) res = root->val;
//        dfs(root->right, k);
//    }
//    int kthSmallest(TreeNode* root, int k) {
//        dfs(root, k);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<string> res;
//public:
//    void traversal(TreeNode* node, string path)
//    {
//        path += to_string(node->val);
//        if (node->left == nullptr && node->right == nullptr)
//        {
//            res.push_back(path);
//            return;
//        }
//        path += "->";
//        if (node->left) traversal(node->left, path);
//        if (node->right) traversal(node->right, path);
//    }
//    vector<string> binaryTreePaths(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        string path;
//        traversal(root, path);
//        return res;
//    }
//};


//class Solution {
//    //dp[i][0]:第i天结束，处于"买入"状态，此时的最大利润
//    //dp[i][1]:第i天结束，处于"可交易"状态，此时的最大利润
//    //dp[i][2]:第i天结束，处于"冷冻期"状态，此时的最大利润
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> dp(n, vector<int>(3));
//        dp[0][0] = -prices[0];
//        dp[0][1] = dp[0][2] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
//            dp[i][1] = max(dp[i - 1][1], dp[i - 1][2]);
//            dp[i][2] = dp[i - 1][0] + prices[i];
//        }
//        return max(dp[n - 1][1], dp[n - 1][2]);
//    }
//};


//class Solution {
//    //f[i]:第i天结束之后，处于"持有股票"状态，此时所剩最多现金
//    //g[i]:第i天结束之后，处于"可交易"状态，此时所得最多现金
//public:
//    int maxProfit(vector<int>& prices, int fee) {
//        int n = prices.size();
//        vector<int> f(n);
//        vector<int> g(n);
//        f[0] = -prices[0];
//        g[0] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            f[i] = max(f[i - 1], g[i - 1] - prices[i]);
//            g[i] = max(g[i - 1], f[i - 1] + prices[i] - fee);
//        }
//        return g[n - 1];
//    }
//};


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int dx[4] = { 1,-1,0,0 }, dy[4] = { 0,0,1,-1 };
//public:
//    int nearestExit(vector<vector<char>>& maze, vector<int>& entrance) {
//        int n = maze.size(), m = maze[0].size();
//        vector<vector<bool>> vis(n, vector<bool>(m));
//        int step = 0;
//        queue<PII> q;
//        q.push({ entrance[0], entrance[1] });
//        vis[entrance[0]][entrance[1]] = true;
//        while (q.size())
//        {
//            step++;
//            int size = q.size();
//            while (size--)
//            {
//                auto [a, b] = q.front();
//                q.pop();
//                for (int k = 0; k < 4; k++)
//                {
//                    int x = a + dx[k], y = b + dy[k];
//                    if (x >= 0 && x < n && y >= 0 && y < m && maze[x][y] == '.' && !vis[x][y])
//                    {
//                        if (x == 0 || x == n - 1 || y == 0 || y == m - 1) return step;
//                        q.push({ x, y });
//                        vis[x][y] = true;
//                    }
//                }
//            }
//        }
//        return -1;
//    }
//};


//class Solution {
//private:
//    unordered_set<string> vis; //表示已经搜索过的状态
//    string change = "ACGT";
//public:
//    int minMutation(string startGene, string endGene, vector<string>& bank) {
//        unordered_set<string> hash(bank.begin(), bank.end());
//        if (startGene == endGene) return 0;
//        if (!hash.count(endGene)) return -1;
//        queue<string> q;
//        q.push(startGene);
//        vis.insert(startGene);
//        int cnt = 0;
//        while (q.size())
//        {
//            cnt++;
//            int size = q.size();
//            while (size--)
//            {
//                string t = q.front();
//                q.pop();
//                for (int i = 0; i < 8; i++) //遍历t字符串
//                {
//                    string tmp = t;
//                    for (int j = 0; j < 4; j++) //遍历change字符串
//                    {
//                        tmp[i] = change[j];
//                        if (hash.count(tmp) && !vis.count(tmp))
//                        {
//                            if (tmp == endGene) return cnt;
//                            q.push(tmp);
//                            vis.insert(tmp);
//                        }
//                    }
//                }
//            }
//        }
//        return -1;
//    }
//};