#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    while (cin >> n)
//    {
//        int res = 1;
//        while (res - 1 <= n)
//            res *= 2;
//        cout << res / 2 - 1 << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//int n, m;
//int sum;
//vector<int> res;
//
//void dfs(int x)
//{
//    if (sum == m)
//    {
//        for (auto e : res)
//            cout << e << ' ';
//        cout << endl;
//        return;
//    }
//    if (sum > m || x > n) return;
//    sum += x;
//    res.push_back(x);
//    dfs(x + 1);
//    res.pop_back();
//    sum -= x;
//
//    dfs(x + 1);
//}
//
//int main()
//{
//    cin >> n >> m;
//    dfs(1);
//    return 0;
//}


//#include <iostream>
//#include <queue>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    priority_queue<int, vector<int>, greater<int>> heap;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        heap.push(x);
//    }
//    int res = 0;
//    while (heap.size() > 1)
//    {
//        int t1 = heap.top(); heap.pop();
//        int t2 = heap.top(); heap.pop();
//        res += (t1 + t2);
//        heap.push(t1 + t2);
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//const int N = 1e5 + 10;
//char s[N];
//LL dp[N];
//LL f[26], g[26];
//
//int main()
//{
//    int n;
//    cin >> n >> s;
//    LL res = 0;
//    for (int i = 0; i < n; i++)
//    {
//        int x = s[i] - 'a';
//        dp[i] = f[x];
//        res += dp[i];
//        f[x] = f[x] + (i - g[x]);
//        g[x] = g[x] + 1;
//    }
//    cout << res << endl;
//    return 0;
//}


#include <iostream>
using namespace std;

typedef long long LL;
const int N = 1e5 + 10;
char s[N];
LL f[26], g[26];

int main()
{
    int n;
    cin >> n >> s;
    LL res = 0;
    for (int i = 0; i < n; i++)
    {
        int x = s[i] - 'a';
        res += f[x];
        f[x] = f[x] + (i - g[x]);
        g[x] = g[x] + 1;
    }
    cout << res << endl;
    return 0;
}