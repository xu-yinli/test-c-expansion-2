#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//private:
//    unordered_map<char, int> hash;
//public:
//    int lengthOfLongestSubstring(string s) {
//        int n = s.size();
//        int len = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash[s[right]]++;
//            while (hash[s[right]] > 1)
//            {
//                hash[s[left]]--;
//                left++;
//            }
//            if (right - left + 1 > len) len = right - left + 1;
//            right++;
//        }
//        return len;
//    }
//};


//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    string s;
//    cin >> s;
//    string res = s;
//    char maxValue = 0;
//    int maxCount = 0;
//    unordered_map<char, int> hash;
//    for (auto x : s)
//    {
//        hash[x]++;
//        if (hash[x] > maxCount)
//        {
//            maxCount = hash[x];
//            maxValue = x;
//        }
//    }
//    int k = 0;
//    while (maxCount--)
//    {
//        res[k] = maxValue;
//        if (k >= n)
//        {
//            cout << "no" << endl;
//            return 0;
//        }
//        k += 2;
//    }
//    hash[maxValue] = 0;
//    for (auto& [a, b] : hash)
//    {
//        while (b > 0)
//        {
//            if (k >= n) k = 1;
//            res[k] = a;
//            k += 2;
//            b--;
//        }
//    }
//    cout << "yes" << endl;
//    cout << res << endl;
//    return 0;
//}