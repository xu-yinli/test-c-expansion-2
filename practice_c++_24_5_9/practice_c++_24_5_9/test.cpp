#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

int main()
{
    string a, b;
    cin >> a >> b;
    int n = a.size(), m = b.size();
    int res = n;
    for (int i = 0; i <= m - n; i++)
    {
        int diff = 0;
        for (int j = 0; j < n; j++)
        {
            if (a[j] != b[i + j])
                diff++;
        }
        res = min(res, diff);
    }
    cout << res << endl;
    return 0;
}