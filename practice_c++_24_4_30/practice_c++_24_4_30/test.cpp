#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <algorithm>
//#include <vector>
//using namespace std;
//
//typedef long long LL;
//
//const int N = 1e5 + 10;
//int a[3 * N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < 3 * n; i++)
//        cin >> a[i];
//    sort(a, a + 3 * n);
//    vector<int> dp(n);
//    int k = 3 * n - 2;
//    for (int i = 0; i < n; i++)
//    {
//        dp[i] = a[k];
//        k -= 2;
//    }
//    LL sum = 0;
//    for (int i = 0; i < n; i++)
//        sum += dp[i];
//    cout << sum << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e4 + 10;
//int sum[N], f[N], g[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        int x;
//        cin >> x;
//        sum[x] += x;
//    }
//    for (int i = 1; i < N; i++)
//    {
//        f[i] = sum[i] + g[i - 1];
//        g[i] = max(f[i - 1], g[i - 1]);
//    }
//    cout << max(f[N - 1], g[N - 1]) << endl;
//    return 0;
//}


//class Solution {
//public:
//    double pow(double x, long long n)
//    {
//        if (n == 0) return 1.0;
//        double k = pow(x, n / 2);
//        if (n % 2 == 0) return k * k;
//        else return x * k * k;
//    }
//    double myPow(double x, int n) {
//        if (n < 0) return 1.0 / pow(x, (long long)n);
//        else return pow(x, n);
//    }
//};


//class Solution {
//public:
//    double myPow(double x, int n)
//    {
//        if (n == 0) return 1.0;
//        double k = myPow(x, n / 2);
//        if (n % 2 == 0) return k * k;
//        else return x * k * k;
//    }
//    double Power(double base, int exponent) {
//        if (exponent < 0) return 1.0 / myPow(base, exponent);
//        else return myPow(base, exponent);
//    }
//};


//class Solution {
//public:
//    double pow(double x, long long n)
//    {
//        if (n == 0) return 1.0;
//        double k = pow(x, n / 2);
//        if (n % 2 == 0) return k * k;
//        else return x * k * k;
//    }
//    double myPow(double x, int n) {
//        if (n < 0) return 1.0 / pow(x, (long long)n);
//        else return pow(x, n);
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> countNumbers(int cnt) {
//        int n = 1;
//        while (cnt--)
//            n *= 10;
//        for (int i = 1; i < n; i++)
//            res.push_back(i);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> countNumbers(int cnt) {
//        for (int i = 1; i < pow(10, cnt); i++)
//            res.push_back(i);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> countNumbers(int cnt) {
//        string maxValue;
//        while (cnt--)
//            maxValue += '9';
//        for (int i = 1; i <= stoi(maxValue); i++)
//            res.push_back(i);
//        return res;
//    }
//};


//class Solution {
//private:
//    unordered_set<string> vis;
//public:
//    int ladderLength(string beginWord, string endWord, vector<string>& wordList) {
//        unordered_set<string> hash(wordList.begin(), wordList.end());
//        if (hash.count(endWord) == 0) return 0;
//        queue<string> q;
//        q.push(beginWord);
//        vis.insert(beginWord);
//        int cnt = 1;
//        while (q.size())
//        {
//            cnt++;
//            int n = q.size();
//            while (n--)
//            {
//                string t = q.front();
//                q.pop();
//                for (int i = 0; i < t.size(); i++)
//                {
//                    string tmp = t;
//                    for (char ch = 'a'; ch <= 'z'; ch++)
//                    {
//                        tmp[i] = ch;
//                        if (vis.count(tmp) == 0 && hash.count(tmp))
//                        {
//                            if (tmp == endWord) return cnt;
//                            q.push(tmp);
//                            vis.insert(tmp);
//                        }
//                    }
//                }
//            }
//        }
//        return 0;
//    }
//};


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int n, m;
//    bool vis[51][51];
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    int dfs(vector<vector<int>>& forest, int begin_x, int begin_y, int end_x, int end_y)
//    {
//        if (begin_x == end_x && begin_y == end_y) return 0;
//        memset(vis, false, sizeof vis);
//        int cnt = 0;
//        queue<PII> q;
//        q.push({ begin_x, begin_y });
//        while (q.size())
//        {
//            cnt++;
//            int size = q.size();
//            while (size--)
//            {
//                auto [a, b] = q.front();
//                q.pop();
//                for (int k = 0; k < 4; k++)
//                {
//                    int x = a + dx[k], y = b + dy[k];
//                    if (x >= 0 && x < n && y >= 0 && y < m && forest[x][y] != 0 && !vis[x][y])
//                    {
//                        if (x == end_x && y == end_y) return cnt;
//                        q.push({ x, y });
//                        vis[x][y] = true;
//                    }
//                }
//            }
//        }
//        return -1;
//    }
//    int cutOffTree(vector<vector<int>>& forest) {
//        n = forest.size(), m = forest[0].size();
//        vector<PII> trees;
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (forest[i][j] > 1)
//                    trees.push_back({ i, j });
//        sort(trees.begin(), trees.end(), [&](const pair<int, int>& p1, const pair<int, int>& p2)
//            {
//                return forest[p1.first][p1.second] < forest[p2.first][p2.second];
//            });
//        int begin_x = 0, begin_y = 0;
//        int res = 0;
//        for (auto& [a, b] : trees)
//        {
//            int step = dfs(forest, begin_x, begin_y, a, b);
//            if (step == -1) return -1;
//            res += step;
//            begin_x = a, begin_y = b;
//        }
//        return res;
//    }
//};