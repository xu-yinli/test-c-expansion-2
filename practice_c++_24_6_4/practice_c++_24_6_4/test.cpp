#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n, 1);
//        int res = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[j] < nums[i])
//                    dp[i] = max(dp[i], dp[j] + 1);
//            }
//            res = max(res, dp[i]);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    //f[i]:表示以i位置元素为结尾的所有子序列中，最后一个位置呈现"上升"趋势的最长摆动序列的长度
//    //g[i]:表示以i位置元素为结尾的所有子序列中，最后一个位置呈现"下降"趋势的最长摆动序列的长度
//    int wiggleMaxLength(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n, 1);
//        vector<int> g(n, 1);
//        int res = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[j] < nums[i]) f[i] = max(f[i], g[j] + 1);
//                else if (nums[j] > nums[i]) g[i] = max(g[i], f[j] + 1);
//            }
//            res = max(res, max(f[i], g[i]));
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    //len[i]:表示以i位置元素为结尾的所有子序列中，最长递增子序列的"长度"
//    //count[i]:表示以i位置元素为结尾的所有子序列中，最长递增子序列的"个数"
//    int findNumberOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> len(n, 1);
//        vector<int> count(n, 1);
//        int reslen = 1, rescount = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[j] < nums[i])
//                {
//                    if (len[j] + 1 == len[i]) count[i] += count[j];
//                    else if (len[j] + 1 > len[i])
//                    {
//                        len[i] = len[j] + 1;
//                        count[i] = count[j];
//                    }
//                }
//            }
//            if (reslen == len[i]) rescount += count[i];
//            else if (reslen < len[i])
//            {
//                reslen = len[i];
//                rescount = count[i];
//            }
//        }
//        return rescount;
//    }
//};


//class Solution {
//public:
//    //dp[i]:表示以i位置元素为结尾的所有数对链中，最长的数对链的长度 
//    int findLongestChain(vector<vector<int>>& pairs) {
//        sort(pairs.begin(), pairs.end());
//        int n = pairs.size();
//        vector<int> dp(n, 1);
//        int res = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (pairs[j][1] < pairs[i][0])
//                {
//                    dp[i] = max(dp[i], dp[j] + 1);
//                }
//            }
//            res = max(res, dp[i]);
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    bool row[9][10], col[9][10];
//    bool grid[3][3][10];
//public:
//    bool isValidSudoku(vector<vector<char>>& board) {
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    if (row[i][num] || col[j][num] || grid[i / 3][j / 3][num])
//                        return false;
//                    row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
//                }
//            }
//        }
//        return true;
//    }
//};


//class Solution {
//private:
//    bool row[9][10], col[9][10];
//    bool grid[3][3][10];
//public:
//    bool dfs(vector<vector<char>>& board)
//    {
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] == '.')
//                {
//                    for (int num = 1; num <= 9; num++)
//                    {
//                        if (!row[i][num] && !col[j][num] && !grid[i / 3][j / 3][num])
//                        {
//                            board[i][j] = num + '0';
//                            row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
//                            if (dfs(board) == true) return true;
//                            board[i][j] = '.';
//                            row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = false;
//                        }
//                    }
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//
//    void solveSudoku(vector<vector<char>>& board) {
//        for (int i = 0; i < 9; i++)
//        {
//            for (int j = 0; j < 9; j++)
//            {
//                if (board[i][j] != '.')
//                {
//                    int num = board[i][j] - '0';
//                    row[i][num] = col[j][num] = grid[i / 3][j / 3][num] = true;
//                }
//            }
//        }
//        dfs(board);
//    }
//};


//class Solution {
//private:
//    int n, m;
//    bool vis[7][7];
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    bool dfs(vector<vector<char>>& board, int i, int j, string& word, int pos)
//    {
//        if (pos == word.size()) return true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && !vis[x][y] && board[x][y] == word[pos])
//            {
//                vis[x][y] = true;
//                if (dfs(board, x, y, word, pos + 1)) return true;
//                vis[x][y] = false;
//            }
//        }
//        return false;
//    }
//    bool exist(vector<vector<char>>& board, string word) {
//        n = board.size(), m = board[0].size();
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (board[i][j] == word[0])
//                {
//                    vis[i][j] = true;
//                    if (dfs(board, i, j, word, 1)) return true;
//                    vis[i][j] = false;
//                }
//            }
//        }
//        return false;
//    }
//};


//class Solution {
//private:
//    int n, m;
//    int res;
//    bool vis[16][16];
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    void dfs(vector<vector<int>>& grid, int i, int j, int path)
//    {
//        res = max(res, path);
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && !vis[x][y] && grid[x][y] != 0)
//            {
//                vis[x][y] = true;
//                dfs(grid, x, y, path + grid[x][y]);
//                vis[x][y] = false;
//            }
//        }
//    }
//    int getMaximumGold(vector<vector<int>>& grid) {
//        n = grid.size(), m = grid[0].size();
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (grid[i][j] != 0)
//                {
//                    vis[i][j] = true;
//                    dfs(grid, i, j, grid[i][j]);
//                    vis[i][j] = false;
//                }
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    int n, m, step;
//    int res;
//    bool vis[21][21];
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    void dfs(vector<vector<int>>& grid, int i, int j, int cnt)
//    {
//        if (grid[i][j] == 2)
//        {
//            if (cnt == step)
//            {
//                res++;
//                return;
//            }
//        }
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && !vis[x][y] && grid[x][y] != -1)
//            {
//                vis[x][y] = true;
//                dfs(grid, x, y, cnt + 1);
//                vis[x][y] = false;
//            }
//        }
//    }
//    int uniquePathsIII(vector<vector<int>>& grid) {
//        n = grid.size(), m = grid[0].size();
//        int bx = 0, by = 0;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (grid[i][j] == 0) step++;
//                else if (grid[i][j] == 1)
//                {
//                    bx = i;
//                    by = j;
//                }
//            }
//        }
//        step += 2;
//        vis[bx][by] = true;
//        dfs(grid, bx, by, 1);
//        return res;
//    }
//};