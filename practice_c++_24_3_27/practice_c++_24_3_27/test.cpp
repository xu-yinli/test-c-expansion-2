#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int eraseOverlapIntervals(vector<vector<int>>& intervals) {
//        int res = 0;
//        sort(intervals.begin(), intervals.end());
//        int left = intervals[0][0], right = intervals[0][1];
//        for (int i = 1; i < intervals.size(); i++)
//        {
//            int a = intervals[i][0], b = intervals[i][1];
//            if (a < right)
//            {
//                res++;
//                right = min(right, b);
//            }
//            else
//            {
//                left = a;
//                right = b;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int findMinArrowShots(vector<vector<int>>& points) {
//        sort(points.begin(), points.end());
//        int res = 1;
//        int right = points[0][1];
//        for (int i = 1; i < points.size(); i++)
//        {
//            int a = points[i][0], b = points[i][1];
//            if (a <= right)
//            {
//                right = min(right, b);
//            }
//            else
//            {
//                res++;
//                right = b;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxSumDivThree(vector<int>& nums) {
//        int INF = 0x3f3f3f3f;
//        int x1 = INF, x2 = INF, y1 = INF, y2 = INF;
//        int sum = 0;
//        for (auto e : nums)
//        {
//            sum += e;
//            if (e % 3 == 1)
//            {
//                if (e <= x1)
//                {
//                    x2 = x1;
//                    x1 = e;
//                }
//                else if (x1 < e && e <= x2) x2 = e;
//            }
//            else if (e % 3 == 2)
//            {
//                if (e <= y1)
//                {
//                    y2 = y1;
//                    y1 = e;
//                }
//                else if (y1 < e && e <= y2) y2 = e;
//            }
//        }
//        if (sum % 3 == 0) return sum;
//        else if (sum % 3 == 1) return max(sum - x1, sum - y1 - y2);
//        else return max(sum - y1, sum - x1 - x2);
//    }
//};


//class Solution {
//public:
//    vector<int> rearrangeBarcodes(vector<int>& barcodes) {
//        unordered_map<int, int> hash;
//        int n = barcodes.size();
//        int maxValue = 0, maxCount = 0;
//        for (int x : barcodes)
//            hash[x]++;
//        for (int x : barcodes)
//        {
//            if (hash[x] > maxCount)
//            {
//                maxCount = hash[x];
//                maxValue = x;
//            }
//        }
//        vector<int> res(n);
//        int k = 0;
//        for (int i = 0; i < maxCount; i++)
//        {
//            res[k] = maxValue;
//            k += 2;
//        }
//        hash.erase(maxValue);
//        for (auto& [x, y] : hash)
//        {
//            for (int j = 0; j < y; j++)
//            {
//                if (k >= n) k = 1;
//                res[k] = x;
//                k += 2;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<int> rearrangeBarcodes(vector<int>& barcodes) {
//        unordered_map<int, int> hash;
//        int n = barcodes.size();
//        int maxValue = 0, maxCount = 0;
//        for (int x : barcodes)
//        {
//            if (++hash[x] > maxCount)
//            {
//                maxCount = hash[x];
//                maxValue = x;
//            }
//        }
//        vector<int> res(n);
//        int k = 0;
//        for (int i = 0; i < maxCount; i++)
//        {
//            res[k] = maxValue;
//            k += 2;
//        }
//        hash.erase(maxValue);
//        for (auto& [x, y] : hash)
//        {
//            for (int j = 0; j < y; j++)
//            {
//                if (k >= n) k = 1;
//                res[k] = x;
//                k += 2;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    string reorganizeString(string s) {
//        int n = s.size();
//        int hash[26] = { 0 };
//        char maxChar = ' ';
//        int maxCount = 0;
//        for (auto c : s)
//        {
//            if (maxCount < ++hash[c - 'a'])
//            {
//                maxChar = c;
//                maxCount = hash[c - 'a'];
//            }
//        }
//        if (maxCount > (n + 1) / 2) return "";
//        string res(n, ' ');
//        int k = 0;
//        for (int i = 0; i < maxCount; i++)
//        {
//            res[k] = maxChar;
//            k += 2;
//        }
//        hash[maxChar - 'a'] = 0;
//        for (int i = 0; i < 26; i++)
//        {
//            for (int j = 0; j < hash[i]; j++)
//            {
//                if (k >= n) k = 1;
//                res[k] = i + 'a';
//                k += 2;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int tribonacci(int n) {
//        if (n == 0) return 0;
//        if (n == 1 || n == 2) return 1;
//        vector<int> dp(n + 1);
//        dp[0] = 0, dp[1] = 1, dp[2] = 1;
//        for (int i = 3; i <= n; i++)
//        {
//            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int waysToStep(int n) {
//        const int MOD = 1e9 + 7;
//        if (n == 1 || n == 2) return n;
//        if (n == 3) return 4;
//        int a = 1, b = 2, c = 4;
//        int sum = 0;
//        for (int i = 4; i <= n; i++)
//        {
//            sum = ((a + b) % MOD + c) % MOD;
//            a = b;
//            b = c;
//            c = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        vector<int> dp(n + 1);
//        for (int i = 2; i <= n; i++)
//            dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        vector<int> dp(n); //从i位置出发到达楼顶所需的最小花费
//        dp[n - 1] = cost[n - 1], dp[n - 2] = cost[n - 2];
//        for (int i = n - 3; i >= 0; i--)
//            dp[i] = min(dp[i + 1] + cost[i], dp[i + 2] + cost[i]);
//        return min(dp[0], dp[1]);
//    }
//};


//class Solution {
//public:
//    int waysToStep(int n) {
//        const int MOD = 1e9 + 7;
//        if (n == 1 || n == 2) return n;
//        if (n == 3) return 4;
//        int a = 1, b = 2, c = 4;
//        int sum = 0;
//        for (int i = 4; i <= n; i++)
//        {
//            sum = ((a % MOD + b) % MOD + c) % MOD;
//            a = b;
//            b = c;
//            c = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//public:
//    int numDecodings(string s) {
//        int n = s.size();
//        vector<int> dp(n);
//        if (s[0] != '0')
//            dp[0] = 1;
//        if (n == 1)
//            return dp[0];
//        if (s[0] != '0' && s[1] != '0')
//            dp[1]++;
//        int k = (s[0] - '0') * 10 + (s[1] - '0');
//        if (k >= 10 && k <= 26)
//            dp[1]++;
//        for (int i = 2; i < n; i++)
//        {
//            //单独编码
//            if (s[i] != '0')
//                dp[i] += dp[i - 1];
//            //两位数编码
//            int t = (s[i - 1] - '0') * 10 + (s[i] - '0');
//            if (t >= 10 && t <= 26)
//                dp[i] += dp[i - 2];
//        }
//        return dp[n - 1];
//    }
//};


//class Solution {
//public:
//    int numDecodings(string s) {
//        int n = s.size();
//        vector<int> dp(n + 1);
//        dp[0] = 1;
//        if (s[1 - 1] != '0')
//            dp[1] = 1;
//        for (int i = 2; i <= n; i++)
//        {
//            //单独编码
//            if (s[i - 1] != '0')
//                dp[i] += dp[i - 1];
//            //两位数编码
//            int t = (s[i - 1 - 1] - '0') * 10 + (s[i - 1] - '0');
//            if (t >= 10 && t <= 26)
//                dp[i] += dp[i - 2];
//        }
//        return dp[n];
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 510, INF = 1e9 + 7;
//int a[N][N];
//int f[N][N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= i; j++)
//            cin >> a[i][j];
//    for (int i = 0; i <= n; i++)
//        for (int j = 0; j <= i + 1; j++)
//            f[i][j] = -INF;
//    f[1][1] = a[1][1];
//    for (int i = 2; i <= n; i++)
//        for (int j = 1; j <= i; j++)
//            f[i][j] = max(f[i - 1][j - 1] + a[i][j], f[i - 1][j] + a[i][j]);
//    int res = -INF;
//    for (int j = 1; j <= n; j++)
//        res = max(res, f[n][j]);
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int a[N], f[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//        cin >> a[i];
//    for (int i = 1; i <= n; i++)
//    {
//        f[i] = 1;
//        for (int j = 1; j < i; j++)
//        {
//            if (a[j] < a[i])
//                f[i] = max(f[i], f[j] + 1);
//        }
//    }
//    int res = 0;
//    for (int i = 1; i <= n; i++)
//        res = max(res, f[i]);
//    cout << res << endl;
//    return 0;
//}


//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n + 1);
//        for (int i = 0; i < n; i++)
//        {
//            dp[i] = 1;
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[j] < nums[i])
//                    dp[i] = max(dp[i], dp[j] + 1);
//            }
//        }
//        int res = 0;
//        for (int i = 0; i < n; i++)
//            res = max(res, dp[i]);
//        return res;
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//char a[N], b[N];
//int f[N][N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    scanf("%s%s", a + 1, b + 1);
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            f[i][j] = max(f[i - 1][j], f[i][j - 1]);
//            if (a[i] == b[j])
//                f[i][j] = f[i - 1][j - 1] + 1;
//        }
//    }
//    cout << f[n][m] << endl;
//    return 0;
//}


class Solution {
public:
    int longestCommonSubsequence(string text1, string text2) {
        int n = text1.size(), m = text2.size();
        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
                if (text1[i - 1] == text2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
            }
        }
        return dp[n][m];
    }
};