#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <cstring>
//#include <queue>
//using namespace std;
//
//typedef pair<int, int> PII;
//const int N = 35;
//char g[N][N];
//int s[N][N];
//int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    int st = 0, ed = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            cin >> g[i][j];
//            if (g[i][j] == 'k')
//            {
//                st = i;
//                ed = j;
//            }
//        }
//    }
//    memset(s, -1, sizeof s);
//    s[st][ed] = 0;
//    queue<PII> q;
//    q.push({ st, ed });
//    while (q.size())
//    {
//        auto& [a, b] = q.front();
//        q.pop();
//        for (int k = 0; k < 4; k++)
//        {
//            int x = a + dx[k], y = b + dy[k];
//            if (x >= 1 && x <= n && y >= 1 && y <= m && g[a][b] != '*' && s[x][y] == -1)
//            {
//                s[x][y] = s[a][b] + 1;
//                if (g[x][y] == '.')
//                    q.push({ x, y });
//            }
//        }
//    }
//    int cnt = 0, len = 1e9;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            if (g[i][j] == 'e' && s[i][j] != -1)
//            {
//                cnt++;
//                len = min(len, s[i][j]);
//            }
//        }
//    }
//    if (cnt == 0) cout << -1 << endl;
//    else cout << cnt << ' ' << len << endl;
//    return 0;
//}


#include <iostream>
using namespace std;

typedef long long LL;
const int N = 1e5 + 10;
int a[N];

int main()
{
	int n, k;
	cin >> n >> k;
	for (int i = 1; i <= n; i++) cin >> a[i];
    LL sum = 0;
    int st = 1;
    for (int i = 1; i < k; ++i) sum += a[i];
    for (int i = k; i <= n; ++i)
    {
        sum += a[i];
        double res = 1.0 * sum / k;
        if (res >= 1024)
        {
            res /= 1024.0;
            printf("%.6lf MiBps\n", res);
        }
        else printf("%.6lf KiBps\n", res);
        sum -= a[i - k + 1];
    }
	return 0;
}