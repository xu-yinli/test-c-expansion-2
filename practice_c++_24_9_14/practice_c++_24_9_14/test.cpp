#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int res = 0;
//        for (int x : nums)
//            res ^= x;
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> generate(int numRows) {
//        vector<vector<int>> vv(numRows);
//        for (int i = 0; i < numRows; i++)
//        {
//            vv[i].resize(i + 1);
//            vv[i][0] = vv[i][vv[i].size() - 1] = 1;
//        }
//        for (int i = 2; i < numRows; i++)
//        {
//            for (int j = 1; j < vv[i].size() - 1; j++)
//            {
//                if (vv[i][j] == 0)
//                    vv[i][j] = vv[i - 1][j - 1] + vv[i - 1][j];
//            }
//        }
//        return vv;
//    }
//};


//class Solution {
//public:
//    int removeDuplicates(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            if (nums[left] == nums[right]) right++;
//            else nums[++left] = nums[right++];
//        }
//        return left + 1;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> hash;
//public:
//    int singleNumber(vector<int>& nums) {
//        for (int x : nums)
//            hash[x]++;
//        int res = 0;
//        for (auto x : hash)
//            if (x.second == 1)
//                return x.first;
//        return -1;
//    }
//};


//class Solution {
//public:
//    vector<int> singleNumber(vector<int>& nums) {
//        int res = 0;
//        for (int x : nums)
//            res ^= x;
//        int k = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            if (((res >> i) & 1) == 1)
//            {
//                k = i;
//                break;
//            }
//        }
//        int a = 0, b = 0;
//        for (int x : nums)
//        {
//            if (((x >> k) & 1) == 1) a ^= x;
//            else b ^= x;
//        }
//        return { a, b };
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param numbers int整型vector
//     * @return int整型
//     */
//    int MoreThanHalfNum_Solution(vector<int>& numbers) {
//        unordered_map<int, int> hash;
//        for (int x : numbers)
//            hash[x]++;
//        int n = numbers.size();
//        for (auto e : hash)
//            if (e.second > n / 2)
//                return e.first;
//        return -1;
//    }
//};


//class Solution {
//private:
//    string str[10] = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
//    vector<string> res;
//    string path;
//public:
//    void dfs(const string& digits, int pos)
//    {
//        if (pos == digits.size())
//        {
//            res.push_back(path);
//            return;
//        }
//        string s = str[digits[pos] - '0'];
//        for (int i = 0; i < s.size(); i++)
//        {
//            path.push_back(s[i]);
//            dfs(digits, pos + 1);
//            path.pop_back();
//        }
//    }
//    vector<string> letterCombinations(string digits) {
//        if (digits.empty()) return res;
//        dfs(digits, 0);
//        return res;
//    }
//};


//class MyQueue {
//private:
//    stack<int> in;
//    stack<int> out;
//public:
//    MyQueue() {
//
//    }
//
//    void push(int x) {
//        in.push(x);
//    }
//
//    int pop() {
//        while (in.size() > 1)
//        {
//            out.push(in.top());
//            in.pop();
//        }
//        int res = in.top();
//        in.pop();
//        while (out.size())
//        {
//            in.push(out.top());
//            out.pop();
//        }
//        return res;
//    }
//
//    int peek() {
//        while (in.size() > 1)
//        {
//            out.push(in.top());
//            in.pop();
//        }
//        int res = in.top();
//        while (out.size())
//        {
//            in.push(out.top());
//            out.pop();
//        }
//        return res;
//    }
//
//    bool empty() {
//        return in.empty() && out.empty();
//    }
//};
//
///**
// * Your MyQueue object will be instantiated and called as such:
// * MyQueue* obj = new MyQueue();
// * obj->push(x);
// * int param_2 = obj->pop();
// * int param_3 = obj->peek();
// * bool param_4 = obj->empty();
// */


//class MyStack {
//private:
//    queue<int> q;
//public:
//    MyStack() {
//
//    }
//
//    void push(int x) {
//        q.push(x);
//    }
//
//    int pop() {
//        int n = q.size() - 1;
//        while (n--)
//        {
//            q.push(q.front());
//            q.pop();
//        }
//        int res = q.front();
//        q.pop();
//        return res;
//    }
//
//    int top() {
//        return q.back();
//    }
//
//    bool empty() {
//        return q.empty();
//    }
//};
//
///**
// * Your MyStack object will be instantiated and called as such:
// * MyStack* obj = new MyStack();
// * obj->push(x);
// * int param_2 = obj->pop();
// * int param_3 = obj->top();
// * bool param_4 = obj->empty();
// */


//class Solution {
//private:
//    stack<int> st;
//public:
//    int evalRPN(vector<string>& tokens) {
//        for (int i = 0; i < tokens.size(); i++)
//        {
//            if (tokens[i] == "+" || tokens[i] == "-" || tokens[i] == "*" || tokens[i] == "/")
//            {
//                int b = st.top(); st.pop();
//                int a = st.top(); st.pop();
//                if (tokens[i] == "+") st.push(a + b);
//                else if (tokens[i] == "-") st.push(a - b);
//                else if (tokens[i] == "*") st.push(a * b);
//                else if (tokens[i] == "/") st.push(a / b);
//            }
//            else st.push(stoi(tokens[i]));
//        }
//        int sum = 0;
//        while (st.size())
//        {
//            sum += st.top();
//            st.pop();
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    stack<int> st;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param pushV int整型vector
//     * @param popV int整型vector
//     * @return bool布尔型
//     */
//    bool IsPopOrder(vector<int>& pushV, vector<int>& popV) {
//        if (pushV.empty() || popV.empty() || pushV.size() != popV.size()) return false;
//        int j = 0;
//        for (int i = 0; i < pushV.size(); i++)
//        {
//            st.push(pushV[i]);
//            while (!st.empty() && st.top() == popV[j])
//            {
//                st.pop();
//                j++;
//            }
//        }
//        if (j == popV.size() && st.empty()) return true;
//        return false;
//    }
//};


//class MinStack {
//private:
//    stack<int> st;
//    stack<int> mini;
//public:
//    MinStack() {
//
//    }
//
//    void push(int val) {
//        st.push(val);
//        if (mini.empty() || val <= mini.top()) mini.push(val);
//    }
//
//    void pop() {
//        if (st.empty()) return;
//        int res = st.top();
//        st.pop();
//        if (res == mini.top()) mini.pop();
//    }
//
//    int top() {
//        return st.top();
//    }
//
//    int getMin() {
//        return mini.top();
//    }
//};
//
///**
// * Your MinStack object will be instantiated and called as such:
// * MinStack* obj = new MinStack();
// * obj->push(val);
// * obj->pop();
// * int param_3 = obj->top();
// * int param_4 = obj->getMin();
// */