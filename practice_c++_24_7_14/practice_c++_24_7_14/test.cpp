#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    bool isMatch(string s, string p) {
//        int n = s.size(), m = p.size();
//        vector<vector<bool>> dp(n + 1, vector<bool>(m + 1));
//        dp[0][0] = true;
//        for (int j = 1; j <= m; j++)
//        {
//            if (p[j - 1] == '*') dp[0][j] = true;
//            else break;
//        }
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (p[j - 1] == '*')
//                    dp[i][j] = dp[i - 1][j] || dp[i][j - 1];
//                else
//                    if ((p[j - 1] == '?' || s[i - 1] == p[j - 1]) && (dp[i - 1][j - 1]))
//                        dp[i][j] = true;
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    bool isMatch(string s, string p) {
//        int n = s.size(), m = p.size();
//        vector<vector<bool>> dp(n + 1, vector<bool>(m + 1));
//        dp[0][0] = true;
//        for (int j = 2; j <= m; j += 2)
//            if (p[j - 1] == '*') dp[0][j] = true;
//            else break;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (p[j - 1] == '*')
//                {
//                    if (dp[i][j - 2] || (p[j - 2] == '.' || s[i - 1] == p[j - 2]) && dp[i - 1][j])
//                        dp[i][j] = true;
//                }
//                else
//                    if ((s[i - 1] == p[j - 1] || p[j - 1] == '.') && (dp[i - 1][j - 1]))
//                        dp[i][j] = true;
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    bool isInterleave(string s1, string s2, string s3) {
//        int n = s1.size(), m = s2.size();
//        if (n + m != s3.size()) return false;
//        vector<vector<bool>> dp(n + 1, vector<bool>(m + 1));
//        dp[0][0] = true;
//        for (int i = 1; i <= n; i++)
//        {
//            if (s1[i - 1] == s3[i - 1]) dp[i][0] = true;
//            else break;
//        }
//        for (int j = 1; j <= m; j++)
//        {
//            if (s2[j - 1] == s3[j - 1]) dp[0][j] = true;
//            else break;
//        }
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if ((s1[i - 1] == s3[i + j - 1] && dp[i - 1][j]) || (s2[j - 1] == s3[i + j - 1] && dp[i][j - 1]))
//                    dp[i][j] = true;
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int minimumDeleteSum(string s1, string s2) {
//        int n = s1.size(), m = s2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                dp[i][j] = max(dp[i][j - 1], dp[i - 1][j]);
//                if (s1[i - 1] == s2[j - 1])
//                    dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + s1[i - 1]);
//            }
//        }
//        int sum = 0;
//        for (int i = 0; i < n; i++) sum += s1[i];
//        for (int j = 0; j < m; j++) sum += s2[j];
//        return sum - 2 * dp[n][m];
//    }
//};


//class Solution {
//public:
//    int findLength(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size(), m = nums2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        int res = 0;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (nums1[i - 1] == nums2[j - 1])
//                {
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                    res = max(res, dp[i][j]);
//                }
//            }
//        }
//        return res;
//    }
//};


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//typedef long long LL;
//int n, l, r;
//const int N = 2e5 + 10;
//int a[N];
//
//LL s(int x)
//{
//    LL res = 0;
//    int left = 0, right = 0;
//    while (right < n)
//    {
//        while (right<n && a[right] - a[left]>x) left++;
//        res += right - left;
//        right++;
//    }
//    return res;
//}
//
//int main()
//{
//    cin >> n >> l >> r;
//    for (int i = 0; i < n; i++)
//        cin >> a[i];
//    sort(a, a + n);
//    cout << s(r) - s(l - 1) << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int MOD = 1e9 + 7;
//
//int main()
//{
//    int n;
//    cin >> n;
//    int x = 1, y = 2;
//    for (int i = 2; i <= n; i++)
//    {
//        int xx = x;
//        int yy = y;
//        x = (2 * yy + 1) % MOD;
//        y = ((2 * yy) % MOD + 2 + xx) % MOD;
//    }
//    cout << x << ' ' << y << endl;
//    return 0;
//}


//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    unordered_map<string, string> map;
//    for (int i = 0; i < n; i++)
//    {
//        string s1, level;
//        cin >> s1 >> level;
//        map[s1] = level;
//    }
//    while (m--)
//    {
//        string s2;
//        cin >> s2;
//        if (map.count(s2))
//            cout << map[s2] << endl;
//        else
//        {
//            int way = 0;
//            string res;
//            for (auto s : map)
//            {
//                if (s.first.size() < s2.size() && s.first == s2.substr(0, s.first.size()) && map.count(s2.substr(s.first.size(), s2.size())))
//                {
//                    way++;
//                    res = s.second + map[s2.substr(s.first.size(), s2.size())];
//                }
//            }
//            if (way != 1) cout << 'D' << endl;
//            else cout << res << endl;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e3 + 10;
//int p[N];
//
//int main()
//{
//    int n, m; //击杀的怪物数量 允许拥有金币数量的上限
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> p[i]; //击杀第i个怪物能获得的金币数量
//    int cnt = 0;
//    int sum = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        if (sum + p[i] > m)
//        {
//            cnt++;
//            sum = 0;
//        }
//        sum += p[i];
//    }
//    cout << cnt << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e3 + 10;
//int gap_time[N];    //编号为i的药服用间隔时间 
//int last[N]; //编号为i的药上一次被服用的时间 
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//    {
//        cin >> gap_time[i];
//        last[i] = -200;
//    }
//    while (m--)
//    {
//        int t, k; //服药的时刻 服用药的种类
//        cin >> t >> k;
//        while (k--)
//        {
//            int id; //对应t时刻要吃的药物种类的编号
//            cin >> id;
//            if (gap_time[id] == -1) continue;
//            if (t - gap_time[id] >= last[id])
//                last[id] = t; //更新编号为id的药物的上一次服用时间为t 
//            else
//                printf("Don't take %d at %d!\n", id, t);
//        }
//    }
//    return 0;
//}


#include <iostream>
#include <map>
using namespace std;

int main()
{
    string s;
    cin >> s;
    int n = s.size() - 1;
    int mini = 0, maxi = 0;
    map<int, int> num;
    bool flag = true;
    int i = 0;
    while (i <= n)
    {
        int x = 0, y = 0;
        while (i <= n && s[i] != 'd' && s[i] != '+' && s[i] != '-')
        {
            x = x * 10 + (s[i] - '0');
            i++;
        }
        if (s[i] == 'd')
        {
            if (x == 0) x = 1;
            i++;
            while (i <= n && s[i] != '+' && s[i] != '-')
            {
                y = y * 10 + (s[i] - '0');
                i++;
            }
            num[y] += x;
            if (flag)
            {
                maxi += x * y;
                mini += x;
            }
            else
            {
                maxi -= x;
                mini -= x * y;
            }
        }
        else
        {
            if (flag)
            {
                maxi += x;
                mini += x;
            }
            else
            {
                maxi -= x;
                mini -= x;
            }
        }
        if (i <= n)
        {
            if (s[i] == '+') flag = true;
            else flag = false;
            i++;
        }
    }
    for (auto t : num)
        cout << t.first << ' ' << t.second << endl;
    cout << mini << ' ' << maxi << endl;
    return 0;
}