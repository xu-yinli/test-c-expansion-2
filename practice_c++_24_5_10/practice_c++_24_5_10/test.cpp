#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param m int整型vector<vector<>>
//     * @return int整型
//     */
//    int find(int x, vector<int>& p)
//    {
//        if (p[x] != x) p[x] = find(p[x], p);
//        return p[x];
//    }
//    int citys(vector<vector<int> >& m) {
//        int n = m.size();
//        vector<int> p(n);
//        for (int i = 0; i < n; i++)
//            p[i] = i;
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < n; j++)
//                if (i != j && m[i][j] == 1)
//                    p[find(i, p)] = find(j, p);
//        int res = 0;
//        for (int i = 0; i < n; i++)
//            if (i == p[i]) res++;
//        return res;
//    }
//};


//class Solution {
//private:
//    bool vis[201] = { false };
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param m int整型vector<vector<>>
//     * @return int整型
//     */
//    void dfs(vector<vector<int> >& m, int i)
//    {
//        vis[i] = true;
//        for (int j = 0; j < m[i].size(); j++)
//        {
//            if (!vis[j] && m[i][j] == 1)
//                dfs(m, j);
//        }
//    }
//    int citys(vector<vector<int> >& m) {
//        int n = m.size();
//        int res = 0;
//        for (int i = 0; i < n; i++)
//        {
//            if (!vis[i])
//            {
//                res++;
//                dfs(m, i);
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    int n;
//    queue<int> q;
//    bool vis[201] = { false };
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param m int整型vector<vector<>>
//     * @return int整型
//     */
//    void bfs(vector<vector<int>>& m, int st)
//    {
//        q.push(st);
//        vis[st] = true;
//        while (q.size())
//        {
//            int t = q.front();
//            q.pop();
//            for (int i = 0; i < n; i++)
//            {
//                if (!vis[i] && t != i && m[t][i] == 1)
//                {
//                    q.push(i);
//                    vis[i] = true;
//                }
//            }
//        }
//    }
//    int citys(vector<vector<int> >& m) {
//        n = m.size();
//        int res = 0;
//        for (int i = 0; i < n; i++)
//        {
//            if (!vis[i])
//            {
//                res++;
//                bfs(m, i);
//            }
//        }
//        return res;
//    }
//};