#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string s;
//    cin >> s;
//    int res = 1e9;
//    for (char ch = 'a'; ch <= 'z'; ch++)
//    {
//        int sum = 0;
//        for (auto x : s)
//        {
//            sum += min(abs(x - ch), 26 - abs(x - ch));
//        }
//        res = min(res, sum);
//    }
//    cout << res << endl;
//    return 0;
//}


#include <iostream>
using namespace std;

const int N = 1010;
int t[N];
int f[N], g[N];

int main()
{
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++) cin >> t[i];

    //求最长上升子序列
    for (int i = 1; i <= n; i++)
    {
        f[i] = 1;
        for (int j = 1; j < i; j++)
        {
            if (t[j] < t[i])
            {
                f[i] = max(f[i], f[j] + 1);
            }
        }
    }
    //求最长递减子序列
    for (int i = n; i >= 1; i--)
    {
        g[i] = 1;
        for (int j = i + 1; j <= n; j++)
        {
            if (t[i] > t[j])
            {
                g[i] = max(g[i], g[j] + 1);
            }
        }
    }
    int res = N;
    for (int i = 1; i <= n; i++)
        res = min(res, n - (f[i] + g[i] - 1));
    cout << res << endl;
    return 0;
}