#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int getHeight(TreeNode* node)
//    {
//        if (node == nullptr) return 0;
//        int leftHeight = getHeight(node->left);
//        int rightHeight = getHeight(node->right);
//        if (leftHeight == -1 || rightHeight == -1)
//            return -1;
//        if (abs(leftHeight - rightHeight) > 1)
//            return -1;
//        else return max(leftHeight, rightHeight) + 1;
//    }
//    bool isBalanced(TreeNode* root) {
//        if (root == nullptr) return true;
//        if (getHeight(root) == -1) return false;
//        return true;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int getHeight(TreeNode* node)
//    {
//        if (node == nullptr) return 0;
//        int leftHeight = getHeight(node->left);
//        int rightHeight = getHeight(node->right);
//        return max(leftHeight, rightHeight) + 1;
//    }
//    bool isBalanced(TreeNode* root) {
//        if (root == nullptr) return true;
//        if (isBalanced(root->left) && isBalanced(root->right))
//            if (abs(getHeight(root->left) - getHeight(root->right)) <= 1)
//                return true;
//        return false;
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 110;
//int a[N][N], s[N][N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= n; j++)
//        {
//            cin >> a[i][j];
//            s[i][j] = s[i - 1][j] + s[i][j - 1] - s[i - 1][j - 1] + a[i][j];
//        }
//    }
//    int res = -127 * N;
//    for (int a = 1; a <= n; a++)
//    {
//        for (int b = 1; b <= n; b++)
//        {
//            for (int c = a; c <= n; c++)
//            {
//                for (int d = b; d <= n; d++)
//                {
//                    res = max(res, s[c][d] - s[a - 1][d] - s[c][b - 1] + s[a - 1][b - 1]);
//                }
//            }
//        }
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    string s;
//    cin >> s;
//    int zero = 0, one = 0;
//    for (auto ch : s)
//    {
//        if (ch == '0') zero++;
//        if (ch == '1') one++;
//    }
//    int res = 0;
//    int half = n / 2;
//    int zero_cnt = 0, one_cnt = 0;
//    int left = 0, right = 0;
//    while (right < n - 1) //细节：防止重复计数
//    {
//        if (s[right] == '0') zero_cnt++;
//        if (s[right] == '1') one_cnt++;
//        while (right - left + 1 > half)
//        {
//            if (s[left] == '0') zero_cnt--;
//            if (s[left] == '1') one_cnt--;
//            left++;
//        }
//        if (right - left + 1 == half)
//        {
//            if (zero_cnt * 2 == zero && one_cnt * 2 == one)
//                res += 2;
//        }
//        right++;
//    }
//    cout << res << endl;
//    return 0;
//}


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
// * };
// */
//class Solution {
//public:
//    bool hasSubtree(TreeNode* a, TreeNode* b)
//    {
//        if (b == NULL) return true;
//        if (a == NULL) return false;
//        if (a->val != b->val) return false;
//        return hasSubtree(a->left, b->left) && hasSubtree(a->right, b->right);
//    }
//    bool isSubStructure(TreeNode* A, TreeNode* B) {
//        if (A == NULL || B == NULL) return false;
//        bool result = false;
//        if (A->val == B->val)
//            result = hasSubtree(A, B);
//        if (!result)
//            result = isSubStructure(A->left, B);
//        if (!result)
//            result = isSubStructure(A->right, B);
//        return result;
//    }
//};


///*
//struct TreeNode {
//	int val;
//	struct TreeNode *left;
//	struct TreeNode *right;
//	TreeNode(int x) :
//			val(x), left(NULL), right(NULL) {
//	}
//};*/
//class Solution {
//public:
//	bool isSubStructure(TreeNode* a, TreeNode* b)
//	{
//		if (b == nullptr) return true;
//		if (a == nullptr) return false;
//		if (a->val != b->val) return false;
//		return isSubStructure(a->left, b->left) && isSubStructure(a->right, b->right);
//	}
//	bool HasSubtree(TreeNode* pRoot1, TreeNode* pRoot2) {
//		if (pRoot1 == nullptr || pRoot2 == nullptr) return false;
//		bool result = false;
//		if (pRoot1->val == pRoot2->val)
//			result = isSubStructure(pRoot1, pRoot2);
//		if (!result)
//			result = HasSubtree(pRoot1->left, pRoot2);
//		if (!result)
//			result = HasSubtree(pRoot1->right, pRoot2);
//		return result;
//	}
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* invertTree(TreeNode* root) {
//        if (root == nullptr) return nullptr;
//        swap(root->left, root->right);
//        invertTree(root->left);
//        invertTree(root->right);
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* mirrorTree(TreeNode* root) {
//        if (root == NULL) return NULL;
//        swap(root->left, root->right);
//        mirrorTree(root->left);
//        mirrorTree(root->right);
//        return root;
//    }
//};



///*
//struct ListNode {
//    int val;
//    struct ListNode *next;
//    ListNode(int x) :
//        val(x), next(NULL) {
//    }
//};
//*/
//class Solution {
//public:
//    ListNode* deleteDuplication(ListNode* pHead) {
//        if (pHead == nullptr) return nullptr;
//        ListNode* newHead = new ListNode(0); //考虑到链表中的元素可能全部相同，所以构造一个头结点
//        newHead->next = pHead;
//
//        //prev 永远在last的前面
//        ListNode* prev = newHead;
//        ListNode* last = prev->next;
//        while (last)
//        {
//            // 1.如果last和last->next不相等，就一直让prev和last往后走
//            while (last->next && last->val != last->next->val)
//            {
//                prev = prev->next;
//                last = last->next;
//            }
//            // 2. 如果如果last和last->next相等，就让last一直往后走
//            while (last->next && last->val == last->next->val)
//                last = last->next;
//
//            // 走到这里结果一共有三种,注意：prev永远指向的是前驱有效节点 
//            // 1. last->next != nullptr 且 (prev, last] 限定了一段重复范围
//            // 2. last->next == nullptr 且 (prev, last] 限定了一段重复范围
//            // 最后相当于 prev->next = nullptr
//            // 3. last->next == nullptr && prev->next == last 说明从本次循环开始，
//            // 整个链表中的元素都不相同，此时就不需要进行去重(特殊情况)
//            if (prev->next != last)
//                prev->next = last->next;
//            last = last->next;
//        }
//        return newHead->next;
//    }
//};