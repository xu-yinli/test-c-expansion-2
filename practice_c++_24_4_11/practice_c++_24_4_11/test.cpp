#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int mySqrt(int x) {
//        long long res = 0;
//        for (long long i = 0; i * i <= x; i++)
//            res = i;
//        return (int)res;
//    }
//};


//class Solution {
//public:
//    int mySqrt(int x) {
//        if (x == 0) return 0;
//        int left = 1, right = x;
//        while (left < right)
//        {
//            long long mid = left + (right - left + 1) / 2;
//            if (mid * mid <= x) left = mid;
//            else right = mid - 1;
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    int searchInsert(vector<int>& nums, int target) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] >= target) right = mid;
//            else left = mid + 1;
//        }
//        if (nums[left] < target) return left + 1;
//        return left;
//    }
//};


//class Solution {
//public:
//    int peakIndexInMountainArray(vector<int>& arr) {
//        int n = arr.size();
//        int res = -1;
//        int k = 0;
//        for (int i = 0; i < n; i++)
//        {
//            if (arr[i] > res)
//            {
//                res = arr[i];
//                k = i;
//            }
//        }
//        return k;
//    }
//};


//class Solution {
//public:
//    int peakIndexInMountainArray(vector<int>& arr) {
//        int n = arr.size();
//        int res = -1;
//        for (int i = 1; i < n - 1; i++)
//        {
//            if (arr[i] > arr[i + 1])
//            {
//                res = i;
//                break;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int peakIndexInMountainArray(vector<int>& arr) {
//        int n = arr.size();
//        int left = 1, right = n - 2;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (arr[mid - 1] < arr[mid]) left = mid;
//            else if (arr[mid - 1] > arr[mid]) right = mid - 1;
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    int findPeakElement(vector<int>& nums) {
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (i == 0 && ((n >= 2 && nums[i] > nums[i + 1]) || (n == 1)))
//                return 0;
//            if (i == n - 1 && nums[i] > nums[i - 1])
//                return n - 1;
//            if ((i >= 1 && nums[i] > nums[i - 1]) && (i <= n - 2 && nums[i] > nums[i + 1]))
//                return i;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    int findPeakElement(vector<int>& nums) {
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//        {
//            bool flag = true;
//            if (i - 1 >= 0 && nums[i] <= nums[i - 1]) flag = false;
//            if (i + 1 < n && nums[i] <= nums[i + 1]) flag = false;
//            if (flag) return i;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    int findPeakElement(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < nums[mid + 1]) left = mid + 1;
//            else if (nums[mid] > nums[mid + 1]) right = mid;
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int n = nums.size();
//        int res = INT_MAX;
//        for (int i = 0; i < n; i++)
//            if (res > nums[i])
//                res = nums[i];
//        return res;
//    }
//};


//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        int x = nums[n - 1];
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > x) left = mid + 1;
//            else right = mid;
//        }
//        return nums[left];
//    }
//};


//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1 || nums[0] < nums[n - 1]) return nums[0];
//        int left = 0, right = n - 1;
//        int x = nums[0];
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] >= x) left = mid + 1;
//            else right = mid;
//        }
//        return nums[left];
//    }
//};


//class Solution {
//public:
//    int missingNumber(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] == mid) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] == left) return left + 1;
//        return left;
//    }
//};


//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        int n = records.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (records[mid] == mid) left = mid + 1;
//            else right = mid;
//        }
//        if (records[left] == left) return left + 1;
//        return left;
//    }
//};


//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        int n = records.size();
//        for (int i = 0; i < n; i++)
//            if (records[i] != i)
//                return i;
//        return n;
//    }
//};


//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        int n = records.size();
//        unordered_map<int, int> hash;
//        for (int x : records)
//            hash[x]++;
//        for (int i = 0; i <= n; i++)
//            if (hash.count(i) == 0)
//                return i;
//        return n;
//    }
//};


//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        int n = records.size();
//        unordered_set<int> set;
//        for (int x : records)
//            set.insert(x);
//        for (int i = 0; i <= n; i++)
//            if (set.count(i) == 0)
//                return i;
//        return n;
//    }
//};


//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        int n = records.size();
//        int res = 0;
//        for (int i = 0; i <= n; i++)
//            res ^= i;
//        for (int x : records)
//            res ^= x;
//        return res;
//    }
//};


//class Solution {
//public:
//    int takeAttendance(vector<int>& records) {
//        int n = records.size();
//        int sum = (n * (1 + n)) / 2;
//        int total = 0;
//        for (int x : records)
//            total += x;
//        return sum - total;
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 100010;
//int a[N], s[N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    for (int i = 1; i <= n; i++) s[i] = s[i - 1] + a[i];
//    while (m--)
//    {
//        int l, r;
//        cin >> l >> r;
//        cout << s[r] - s[l - 1] << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int a[N][N], s[N][N];
//
//int main()
//{
//    int n, m, q;
//    cin >> n >> m >> q;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= m; j++)
//            cin >> a[i][j];
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= m; j++)
//            s[i][j] = s[i - 1][j] + s[i][j - 1] - s[i - 1][j - 1] + a[i][j];
//    while (q--)
//    {
//        int x1, y1, x2, y2;
//        cin >> x1 >> y1 >> x2 >> y2;
//        cout << s[x2][y2] - s[x1 - 1][y2] - s[x2][y1 - 1] + s[x1 - 1][y1 - 1] << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int a[N];
//long long dp[N];
//
//int main()
//{
//    int n, q;
//    cin >> n >> q;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    for (int i = 1; i <= n; i++) dp[i] = dp[i - 1] + a[i];
//    while (q--)
//    {
//        int l, r;
//        cin >> l >> r;
//        cout << dp[r] - dp[l - 1] << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int n, q;
//    cin >> n >> q;
//    vector<int> a(n + 1);
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    vector<long long> dp(n + 1);
//    for (int i = 1; i <= n; i++) dp[i] = dp[i - 1] + a[i];
//    while (q--)
//    {
//        int l, r;
//        cin >> l >> r;
//        cout << dp[r] - dp[l - 1] << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int n, m, q;
//    cin >> n >> m >> q;
//    vector<vector<int>> a(n + 1, vector<int>(m + 1));
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= m; j++)
//            cin >> a[i][j];
//    vector<vector<long long>> dp(n + 1, vector<long long>(m + 1));
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= m; j++)
//            dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + a[i][j];
//    while (q--)
//    {
//        int x1, y1, x2, y2;
//        cin >> x1 >> y1 >> x2 >> y2;
//        cout << dp[x2][y2] - dp[x1 - 1][y2] - dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1] << endl;
//    }
//    return 0;
//}


//class Solution {
//public:
//    int pivotIndex(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n), g(n);
//        for (int i = 1; i < n; i++)
//            f[i] = f[i - 1] + nums[i - 1];
//        for (int i = n - 2; i >= 0; i--)
//            g[i] = g[i + 1] + nums[i + 1];
//        for (int i = 0; i < n; i++)
//            if (f[i] == g[i])
//                return i;
//        return -1;
//    }
//};


//class Solution {
//public:
//    vector<int> productExceptSelf(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n), g(n);
//        f[0] = 1, g[n - 1] = 1;
//        for (int i = 1; i < n; i++)
//            f[i] = f[i - 1] * nums[i - 1];
//        for (int i = n - 2; i >= 0; i--)
//            g[i] = g[i + 1] * nums[i + 1];
//        vector<int> res(n);
//        for (int i = 0; i < n; i++)
//            res[i] = f[i] * g[i];
//        return res;
//    }
//};


//class Solution {
//public:
//    int subarraySum(vector<int>& nums, int k) {
//        int res = 0;
//        unordered_map<int, int> hash;
//        hash[0] = 1;
//        int sum = 0;
//        for (auto x : nums)
//        {
//            sum += x;
//            if (hash.count(sum - k)) res += hash[sum - k];
//            hash[sum]++;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int subarraysDivByK(vector<int>& nums, int k) {
//        int res = 0;
//        unordered_map<int, int> hash;
//        hash[0 % k] = 1;
//        int sum = 0;
//        for (auto x : nums)
//        {
//            sum += x;
//            int r = (sum % k + k) % k;
//            if (hash.count(r)) res += hash[r];
//            hash[r]++;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int findMaxLength(vector<int>& nums) {
//        int n = nums.size();
//        int res = 0;
//        unordered_map<int, int> hash;
//        hash[0] = -1;
//        int sum = 0;
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[i] == 0) sum += -1;
//            else sum += 1;
//            if (hash.count(sum)) res = max(res, i - hash[sum]);
//            else hash[sum] = i;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k) {
//        int n = mat.size(), m = mat[0].size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        vector<vector<int>> ans(n, vector<int>(m));
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= m; j++)
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + mat[i - 1][j - 1];
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                int x1 = max(0, i - k) + 1;
//                int y1 = max(0, j - k) + 1;
//                int x2 = min(n - 1, i + k) + 1;
//                int y2 = min(m - 1, j + k) + 1;
//                ans[i][j] = dp[x2][y2] - dp[x1 - 1][y2] - dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1];
//            }
//        }
//        return ans;
//    }
//};


//class Solution {
//public:
//    string findCommon(string& s1, string& s2)
//    {
//        int i = 0;
//        while (i < s1.size() && i < s2.size() && s1[i] == s2[i]) i++;
//        return s1.substr(0, i);
//    }
//    string longestCommonPrefix(vector<string>& strs) {
//        string res = strs[0];
//        for (int i = 0; i < strs.size(); i++)
//            res = findCommon(res, strs[i]);
//        return res;
//    }
//};


//class Solution {
//public:
//    string findCommon(string& s1, string& s2)
//    {
//        int i = 0;
//        string s = "";
//        while (i < s1.size() && i < s2.size() && s1[i] == s2[i]) s += s1[i++];
//        return s;
//    }
//    string longestCommonPrefix(vector<string>& strs) {
//        string res = strs[0];
//        for (int i = 0; i < strs.size(); i++)
//            res = findCommon(res, strs[i]);
//        return res;
//    }
//};


//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& strs) {
//        int n = strs[0].size();
//        for (int i = 0; i < n; i++)
//        {
//            char tmp = strs[0][i];
//            for (int j = 1; j < strs.size(); j++)
//                if (i == strs[j].size() || tmp != strs[j][i])
//                    return strs[0].substr(0, i);
//        }
//        return strs[0];
//    }
//};