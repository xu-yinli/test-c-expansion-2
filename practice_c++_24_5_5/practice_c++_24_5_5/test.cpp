#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//typedef long long LL;
//const int N = 200010;
//
//struct fruits
//{
//    LL a;
//    LL b;
//    static bool cmp(const fruits& x, const fruits& y)
//    {
//        if (x.b == y.b) return x.a < y.a;
//        return x.b > y.b;
//    }
//}f[N];
//
//int main()
//{
//    int n, k;
//    cin >> n >> k;
//    for (int i = 0; i < n; i++) cin >> f[i].a;
//    for (int i = 0; i < n; i++) cin >> f[i].b;
//    sort(f, f + n, fruits::cmp);
//    LL sum_a = 0, sum_b = 0;
//    for (int i = 0; i < k; i++)
//    {
//        sum_a += f[i].a;
//        sum_b += f[i].b;
//    }
//    cout << sum_a << ' ' << sum_b << endl;
//
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//const int N = 1e5 + 10, INF = 1e9 + 7;
//int happy[N], shame[N];
//
//int main()
//{
//    LL n, k;
//    cin >> n >> k;
//    for (int i = 0; i < n; i++) cin >> happy[i];
//    for (int i = 0; i < n; i++) cin >> shame[i];
//    int left = 0, right = 0;
//    LL sum_happy = 0, sum_shame = 0;
//    LL max_happy = 0, min_shame = INF;
//    LL st = 0;
//    while (right < n)
//    {
//        sum_happy += happy[right];
//        sum_shame += shame[right];
//        while (right - left + 1 > k)
//        {
//            sum_happy -= happy[left];
//            sum_shame -= shame[left];
//            left++;
//        }
//        if (right - left + 1 == k)
//        {
//            if (sum_happy >= max_happy)
//            {
//                if (sum_happy == max_happy && sum_shame < min_shame)
//                {
//                    st = left;
//                    max_happy = sum_happy;
//                    min_shame = sum_shame;
//                }
//                else if (sum_happy > max_happy)
//                {
//                    st = left;
//                    max_happy = sum_happy;
//                    min_shame = sum_shame;
//                }
//            }
//        }
//        right++;
//    }
//    cout << st + 1 << endl;
//    return 0;
//}


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     * 计算01背包问题的结果
//     * @param V int整型 背包的体积
//     * @param n int整型 物品的个数
//     * @param vw int整型vector<vector<>> 第一维度为n,第二维度为2的二维数组,vw[i][0],vw[i][1]分别描述i+1个物品的vi,wi
//     * @return int整型
//     */
//    int knapsack(int V, int n, vector<vector<int> >& vw) {
//        int dp[1010][1010] = { 0 };
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 0; j <= V; j++)
//            {
//                if (j >= vw[i - 1][0])
//                    dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - vw[i - 1][0]] + vw[i - 1][1]);
//                else
//                    dp[i][j] = dp[i - 1][j];
//            }
//        }
//        return dp[n][V];
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     * 计算01背包问题的结果
//     * @param V int整型 背包的体积
//     * @param n int整型 物品的个数
//     * @param vw int整型vector<vector<>> 第一维度为n,第二维度为2的二维数组,vw[i][0],vw[i][1]分别描述i+1个物品的vi,wi
//     * @return int整型
//     */
//    int knapsack(int V, int n, vector<vector<int> >& vw) {
//        int dp[1010] = { 0 };
//        for (int i = 1; i <= n; i++)
//            for (int j = V; j >= vw[i - 1][0]; j--)
//                dp[j] = max(dp[j], dp[j - vw[i - 1][0]] + vw[i - 1][1]);
//        return dp[V];
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    bool increment(string& s)
//    {
//        bool isOverflow = false;
//        int carry = 0;
//        for (int i = s.size() - 1; i >= 0; i--)
//        {
//            int cur = s[i] - '0' + carry;
//            if (i == s.size() - 1)
//                cur++;
//            if (cur >= 10)
//            {
//                if (i == 0)
//                    isOverflow = true;
//                else
//                {
//                    carry = 1;
//                    s[i] = cur - 10 + '0';
//                }
//            }
//            else
//            {
//                s[i] = cur + '0';
//                break;
//            }
//        }
//        return isOverflow;
//    }
//    void printNumbers(string& s)
//    {
//        bool isBeginningZero = true;
//        string tmp;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (isBeginningZero && s[i] != '0')
//                isBeginningZero = false;
//            if (!isBeginningZero)
//                tmp += s[i];
//        }
//        res.push_back(stoi(tmp));
//    }
//    vector<int> countNumbers(int cnt) {
//        if (cnt <= 0) return res;
//        string s(cnt, '0');
//        while (!increment(s))
//            printNumbers(s);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    void printNumbers(string& s)
//    {
//        bool isBeginningZero = true;
//        string tmp;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (isBeginningZero && s[i] != '0')
//                isBeginningZero = false;
//            if (!isBeginningZero)
//                tmp += s[i];
//        }
//        if (tmp != "") res.push_back(stoi(tmp));
//    }
//    void permutation(string& s, int length, int index)
//    {
//        if (index == length - 1)
//        {
//            printNumbers(s);
//            return;
//        }
//        for (int i = 0; i < 10; i++)
//        {
//            s[index + 1] = i + '0';
//            permutation(s, length, index + 1);
//        }
//    }
//    vector<int> countNumbers(int cnt) {
//        if (cnt <= 0) return res;
//        string s(cnt, '0');
//        for (int i = 0; i < 10; i++)
//        {
//            s[0] = i + '0';
//            permutation(s, cnt, 0);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型 最大位数
//     * @return int整型vector
//     */
//    vector<int> printNumbers(int n) {
//        vector<int> res;
//        int num = 1;
//        while (n--)
//            num *= 10;
//        for (int i = 1; i < num; i++)
//            res.push_back(i);
//        return res;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        ListNode* newHead = nullptr;
//        ListNode* end = nullptr;
//        while (list1 && list2)
//        {
//            ListNode* cur = nullptr;
//            if (list1->val < list2->val) cur = list1;
//            else cur = list2;
//            if (cur == list1) list1 = list1->next;
//            else list2 = list2->next;
//            if (newHead == nullptr)
//            {
//                newHead = cur;
//                end = cur;
//            }
//            else
//            {
//                end->next = cur;
//                end = end->next;
//            }
//        }
//        if (list1) end->next = list1;
//        if (list2) end->next = list2;
//        return newHead;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        ListNode* newHead = nullptr;
//        if (list1->val <= list2->val)
//        {
//            newHead = list1;
//            list1 = list1->next;
//        }
//        else
//        {
//            newHead = list2;
//            list2 = list2->next;
//        }
//        newHead->next = mergeTwoLists(list1, list2);
//        return newHead;
//    }
//};


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int n, m;
//    int dx[4] = { 1,-1,0,0 }, dy[4] = { 0,0,1,-1 };
//public:
//    vector<vector<int>> updateMatrix(vector<vector<int>>& mat) {
//        n = mat.size(), m = mat[0].size();
//        vector<vector<int>> dist(n, vector<int>(m, -1));
//        queue<PII> q;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (mat[i][j] == 0)
//                {
//                    q.push({ i, j });
//                    dist[i][j] = 0;
//                }
//            }
//        }
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < n && y >= 0 && y < m && dist[x][y] == -1)
//                {
//                    dist[x][y] = dist[a][b] + 1;
//                    q.push({ x, y });
//                }
//            }
//        }
//        return dist;
//    }
//};


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int n, m;
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    int numEnclaves(vector<vector<int>>& grid) {
//        int n = grid.size(), m = grid[0].size();
//        vector<vector<bool>> vis(n, vector<bool>(m));
//        queue<PII> q;
//        for (int j = 0; j < m; j++)
//        {
//            if (grid[0][j] == 1)
//            {
//                q.push({ 0, j });
//                vis[0][j] = true;
//            }
//            if (grid[n - 1][j] == 1)
//            {
//                q.push({ n - 1, j });
//                vis[n - 1][j] = true;
//            }
//        }
//        for (int i = 1; i < n - 1; i++)
//        {
//            if (grid[i][0] == 1)
//            {
//                q.push({ i, 0 });
//                vis[i][0] = true;
//            }
//            if (grid[i][m - 1] == 1)
//            {
//                q.push({ i, m - 1 });
//                vis[i][m - 1] = true;
//            }
//        }
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < n && y>0 && y < m && !vis[x][y] && grid[x][y] == 1)
//                {
//                    q.push({ x, y });
//                    vis[x][y] = true;
//                }
//            }
//        }
//        int cnt = 0;
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (grid[i][j] == 1 && !vis[i][j])
//                    cnt++;
//        return cnt;
//    }
//};


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int n, m;
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    vector<vector<int>> highestPeak(vector<vector<int>>& isWater) {
//        n = isWater.size(), m = isWater[0].size();
//        vector<vector<int>> dist(n, vector<int>(m, -1));
//        queue<PII> q;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (isWater[i][j] == 1)
//                {
//                    dist[i][j] = 0;
//                    q.push({ i, j });
//                }
//            }
//        }
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < n && y >= 0 && y < m && dist[x][y] == -1)
//                {
//                    dist[x][y] = dist[a][b] + 1;
//                    q.push({ x, y });
//                }
//            }
//        }
//        return dist;
//    }
//};


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int n, m;
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    int maxDistance(vector<vector<int>>& grid) {
//        n = grid.size(), m = grid[0].size();
//        vector<vector<int>> dist(n, vector<int>(m, -1));
//        queue<PII> q;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (grid[i][j] == 1)
//                {
//                    q.push({ i, j });
//                    dist[i][j] = 0;
//                }
//            }
//        }
//        int res = -1;
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < n && y >= 0 && y < m && dist[x][y] == -1)
//                {
//                    dist[x][y] = dist[a][b] + 1;
//                    q.push({ x, y });
//                    res = max(res, dist[x][y]);
//                }
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//    //f[i][j]:第i天结束后，完成了j次交易，此时处于“买入”状态下的最大利润
//    //g[i][j]:第i天结束后，完成了j次交易，此时处于“卖出”状态下的最大利润
//private:
//    const int INF = 0x3f3f3f3f;
//public:
//    int maxProfit(vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> f(n, vector<int>(3, -INF));
//        vector<vector<int>> g(n, vector<int>(3, -INF));
//        f[0][0] = -prices[0], g[0][0] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < 3; j++)
//            {
//                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//                g[i][j] = g[i - 1][j];
//                if (j >= 1) g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//            }
//        }
//        int res = 0;
//        for (int j = 0; j < 3; j++)
//            res = max(res, g[n - 1][j]);
//        return res;
//    }
//};


//class Solution {
//    //f[i][j]:第i天结束后，完成了j次交易，此时处于“买入”状态下的最大利润
//    //g[i][j]:第i天结束后，完成了j次交易，此时处于“卖出”状态下的最大利润
//private:
//    const int INF = 0x3f3f3f3f;
//public:
//    int maxProfit(int k, vector<int>& prices) {
//        int n = prices.size();
//        k = min(k, n / 2); //优化:处理最多交易次数
//        vector<vector<int>> f(n, vector<int>(k + 1, -INF));
//        vector<vector<int>> g(n, vector<int>(k + 1, -INF));
//        f[0][0] = -prices[0], g[0][0] = 0;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j <= k; j++)
//            {
//                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//                g[i][j] = g[i - 1][j];
//                if (j >= 1) g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//            }
//        }
//        int res = 0;
//        for (int j = 0; j <= k; j++)
//            res = max(res, g[n - 1][j]);
//        return res;
//    }
//};