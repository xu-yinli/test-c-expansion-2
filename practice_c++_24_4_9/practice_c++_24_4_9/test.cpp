#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    void moveZeroes(vector<int>& nums) {
//        int n = nums.size();
//        for (int cur = 0, dest = -1; cur < n; cur++)
//        {
//            if (nums[cur])
//                swap(nums[++dest], nums[cur]);
//        }
//    }
//};


//class Solution {
//public:
//    void moveZeroes(vector<int>& nums) {
//        int n = nums.size();
//        int cur = 0, dest = -1;
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[i] == 0) cur++;
//            else
//            {
//                dest++;
//                swap(nums[dest], nums[cur]);
//                cur++;
//            }
//        }
//    }
//};


//class Solution {
//    unordered_map<int, int> hash;
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//        {
//            int x = target - nums[i];
//            if (hash.count(x)) return { hash[x], i };
//            hash[nums[i]] = i;
//        }
//        return { -1, -1 };
//    }
//};


//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target) {
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//            for (int j = i + 1; j < n; j++)
//                if (nums[i] + nums[j] == target)
//                    return { i, j };
//        return { -1, -1 };
//    }
//};


//class Solution {
//private:
//    int hash[26];
//public:
//    bool CheckPermutation(string s1, string s2) {
//        int n = s1.size(), m = s2.size();
//        if (n != m) return false;
//        for (int i = 0; i < n; i++)
//            hash[s1[i] - 'a']++;
//        for (int i = 0; i < m; i++)
//        {
//            hash[s2[i] - 'a']--;
//            if (hash[s2[i] - 'a'] < 0)
//                return false;
//        }
//        return true;
//    }
//};


//class Solution {
//private:
//    int hash[26];
//public:
//    bool CheckPermutation(string s1, string s2) {
//        int n = s1.size(), m = s2.size();
//        if (n != m) return false;
//        for (auto ch : s1)
//            hash[ch - 'a']++;
//        for (auto ch : s2)
//        {
//            hash[ch - 'a']--;
//            if (hash[ch - 'a'] < 0)
//                return false;
//        }
//        return true;
//    }
//};


//class Solution {
//private:
//    unordered_set<int> hash;
//public:
//    bool containsDuplicate(vector<int>& nums) {
//        for (int x : nums)
//        {
//            if (hash.count(x)) return true;
//            else hash.insert(x);
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    bool containsDuplicate(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        int n = nums.size();
//        for (int i = 0; i < n - 1; i++)
//            if (nums[i] == nums[i + 1])
//                return true;
//        return false;
//    }
//};


//class Solution {
//private:
//    unordered_set<int> hash;
//public:
//    bool containsDuplicate(vector<int>& nums) {
//        for (int x : nums)
//        {
//            if (hash.find(x) != hash.end()) return true;
//            else hash.insert(x);
//        }
//        return false;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> hash;
//public:
//    bool containsNearbyDuplicate(vector<int>& nums, int k) {
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (hash.count(nums[i]))
//            {
//                if (i - hash[nums[i]] <= k)
//                    return true;
//            }
//            hash[nums[i]] = i;
//        }
//        return false;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> hash;
//public:
//    bool containsNearbyDuplicate(vector<int>& nums, int k) {
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (hash.find(nums[i]) != hash.end())
//            {
//                if (i - hash[nums[i]] <= k)
//                    return true;
//            }
//            hash[nums[i]] = i;
//        }
//        return false;
//    }
//};


//class Solution {
//private:
//    unordered_map<string, vector<string>> hash;
//public:
//    vector<vector<string>> groupAnagrams(vector<string>& strs) {
//        for (auto s : strs)
//        {
//            string tmp = s;
//            sort(tmp.begin(), tmp.end());
//            hash[tmp].push_back(s);
//        }
//        vector<vector<string>> res;
//        for (auto [x, y] : hash)
//            res.push_back(y);
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 0) return 0;
//        vector<int> dp(n);
//        dp[0] = nums[0];
//        int res = dp[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i] = max(dp[i - 1] + nums[i], nums[i]);
//            if (dp[i] > res) res = dp[i];
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int longestCommonSubsequence(string text1, string text2) {
//        int n = text1.size(), m = text2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (text1[i - 1] == text2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                else
//                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int maxUncrossedLines(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size(), m = nums2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (nums1[i - 1] == nums2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                else
//                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[n][m];
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 1010, M = 10010;
//int a[N], b[M];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    for (int i = 0; i < n; i++)
//    {
//        int x = 0;
//        for (int j = i; j < i + 4 && j < n; j++)
//        {
//            x = x * 10 + a[j];
//            b[x]++;
//        }
//    }
//    for (int i = 0; i < M; i++)
//    {
//        if (b[i] == 0)
//        {
//            cout << i << endl;
//            break;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//int main()
//{
//	int t;
//	cin >> t;
//	while(t--)
//	{
//		LL n, m;
//		cin >> n >> m;
//		if (n == m)
//		{
//			cout << 0 << endl;
//			continue;
//		}
//		int h = 0, y = 0;
//		for (int i = 0; (n >> i) || (m >> i); i++)
//		{
//			//0->1 需要按位或上一个1
//			if ((n >> i & 1) == 0 && (m >> i & 1) == 1)
//				h = 1;
//			//1->0 需要按位与上一个0
//			else if ((n >> i & 1) == 1 && (m >> i & 1) == 0)
//				y = 1;
//		}
//		cout << h + y << endl;
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//bool is_prime(int x)
//{
//    bool flag = true;
//    for (int i = 2; i <= x / i; i++)
//    {
//        if (x % i == 0)
//        {
//            flag = false;
//            return false;
//        }
//    }
//    if (flag) return true;
//    return false;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    int count = 0;
//    for (int i = 2; i < n; i++)
//    {
//        if (is_prime(i))
//        {
//            cout << i << ' ';
//            count++;
//        }
//    }
//
//    cout << endl << count << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int gcd(int a, int b)
//{
//	return b ? gcd(b, a % b) : a;
//}
//
//int main()
//{
//	int a, b, c;
//	cin >> a >> b >> c;
//	int x = (a * b) / gcd(a, b);
//	int y = (a * c) / gcd(a, c);
//	int z = (b * c) / gcd(b, c);
//	cout << max(x, max(y, z)) << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int gcd(int a, int b)
//{
//	return b ? gcd(b, a % b) : a;
//}
//
//int main()
//{
//	int a, b, c;
//	cin >> a >> b >> c;
//	int x = (a * b) / gcd(a, b);
//	cout << (c * x) / gcd(c, x) << endl;
//	return 0;
//}


//class Solution {
//public:
//    bool isSubsequence(string s, string t) {
//        int n = s.size(), m = t.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (s[i - 1] == t[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = dp[i][j - 1];
//            }
//        }
//        if (dp[n][m] == n) return true;
//        else return false;
//    }
//};


//class Solution {
//public:
//    int minDistance(string word1, string word2) {
//        int n = word1.size(), m = word2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 0; i <= n; i++) dp[i][0] = i;
//        for (int j = 0; j <= m; j++) dp[0][j] = j;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (word1[i - 1] == word2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1];
//                else
//                    dp[i][j] = min(dp[i - 1][j] + 1, dp[i][j - 1] + 1);
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int minDistance(string word1, string word2) {
//        int n = word1.size(), m = word2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (word1[i - 1] == word2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + 1;
//                else
//                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return n + m - 2 * dp[n][m];
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 10010;
//char name[N][11];
//
//int main()
//{
//    int k = 0;
//    while (cin >> name[k])
//    {
//        if (name[k][0] == '.')
//            break;
//        k++;
//    }
//    if (k >= 14)
//        cout << name[1] << " and " << name[13] << " are inviting you to dinner..." << endl;
//    else if (k >= 2 && k < 14)
//        cout << name[1] << " is the only one for you..." << endl;
//    else
//        cout << "Momo... No one is for you ..." << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int a[4];
//
//int main()
//{
//    int y, n;
//    cin >> y >> n;
//    for (int i = y; ; i++)
//    {
//        int cnt = 0;
//        int st[10] = { 0 };
//        st[i / 1000]++;
//        st[i / 100 % 10]++;
//        st[i / 10 % 10]++;
//        st[i % 10]++;
//        for (int i = 0; i < 10; i++)
//        {
//            //记录一共有多少个不同的数字
//            if (st[i] != 0)
//                cnt++;
//        }
//        if (cnt == n) //不同的数字正好是n个
//        {
//            printf("%d %04d\n", i - y, i);
//            break;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n; //填充结果字符串的长度
//    char c; //用于填充的字符
//    cin >> n >> c;
//    getchar();
//    string s;
//    getline(cin, s);
//    int len = s.length();
//    if (n > len)
//    {
//        for (int i = 0; i < n - len; i++)
//            cout << c;
//        cout << s << endl;
//    }
//    else
//    {
//        for (int i = len - n; i < len; i++)
//            cout << s[i];
//        cout << endl;
//    }
//    return 0;
//}


#include <iostream>
using namespace std;

const int N = 1010;
int a[N];

int main()
{
    int n; //用户点赞的博文数量
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        int k;
        cin >> k;
        for (int j = 0; j < k; j++)
        {
            int x; //特性标签的编号
            cin >> x;
            a[x]++;
        }
    }
    int id = 0, cnt = 0; //编号和出现次数
    for (int i = 0; i < N; i++)
    {
        if (a[i] >= cnt)
        {
            cnt = a[i];
            id = i;
        }
    }
    cout << id << ' ' << cnt << endl;
    return 0;
}