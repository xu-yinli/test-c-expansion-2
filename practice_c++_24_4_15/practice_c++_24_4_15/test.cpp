#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    vector<int> trainingPlan(vector<int>& actions) {
//        int n = actions.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            while (left < right && actions[left] % 2 == 1)
//                left++;
//            while (left < right && actions[right] % 2 == 0)
//                right--;
//            if (left < right)
//                swap(actions[left], actions[right]);
//        }
//        return actions;
//    }
//};


//class Solution {
//public:
//    vector<int> trainingPlan(vector<int>& actions) {
//        int n = actions.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            while (left < right && (actions[left] & 1))
//                left++;
//            while (left < right && !(actions[right] & 1))
//                right--;
//            if (left < right)
//                swap(actions[left], actions[right]);
//        }
//        return actions;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param array int整型vector
//     * @return int整型vector
//     */
//    vector<int> reOrderArrayTwo(vector<int>& array) {
//        int n = array.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            while (left < right && array[left] % 2 == 1)
//                left++;
//            while (left < right && array[right] % 2 == 0)
//                right--;
//            if (left < right)
//                swap(array[left], array[right]);
//        }
//        return array;
//    }
//};


//class Solution {
//public:
//    vector<int> trainingPlan(vector<int>& actions) {
//        int n = actions.size();
//        int k = 0; //记录已排好序的奇数个数
//        for (int i = 0; i < n; i++)
//        {
//            if (actions[i] % 2 == 1)
//            {
//                int tmp = actions[i];
//                int j = i;
//                while (j > k)
//                {
//                    actions[j] = actions[j - 1];
//                    j--;
//                }
//                actions[k++] = tmp;
//            }
//        }
//        return actions;
//    }
//};


//class Solution {
//public:
//    int majorityElement(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        int n = nums.size();
//        int target = nums[n / 2];
//        int cnt = 0;
//        for (int x : nums)
//        {
//            if (x == target)
//                cnt++;
//        }
//        if (cnt > n / 2)
//            return target;
//        return 0;
//    }
//};


//class Solution {
//public:
//    int majorityElement(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        int n = nums.size();
//        return nums[n / 2];
//    }
//};


//class Solution {
//public:
//    int majorityElement(vector<int>& nums) {
//        int n = nums.size();
//        int target = nums[0];
//        int times = 1;
//        for (int i = 1; i < n; i++)
//        {
//            if (times == 0)
//            {
//                target = nums[i];
//                times = 1;
//            }
//            else if (target == nums[i])
//                times++;
//            else
//                times--;
//        }
//        int cnt = 0;
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[i] == target)
//                cnt++;
//        }
//        return cnt > n / 2 ? target : 0;
//    }
//};


//class Solution {
//public:
//    int inventoryManagement(vector<int>& stock) {
//        sort(stock.begin(), stock.end());
//        int n = stock.size();
//        return stock[n / 2];
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> path;
//    vector<string> res;
//public:
//    void traversal(TreeNode* node)
//    {
//        path.push_back(node->val);
//        if (node->left == nullptr && node->right == nullptr)
//        {
//            string str;
//            for (int i = 0; i < path.size() - 1; i++)
//            {
//                str += to_string(path[i]);
//                str += "->";
//            }
//            str += to_string(path[path.size() - 1]);
//            res.push_back(str);
//            return;
//        }
//        if (node->left)
//        {
//            traversal(node->left);
//            path.pop_back();
//        }
//        if (node->right)
//        {
//            traversal(node->right);
//            path.pop_back();
//        }
//    }
//    vector<string> binaryTreePaths(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        traversal(root);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<string> res;
//public:
//    void traversal(TreeNode* node, string path)
//    {
//        path += to_string(node->val);
//        if (node->left == nullptr && node->right == nullptr)
//        {
//            res.push_back(path);
//            return;
//        }
//        if (node->left)
//            traversal(node->left, path + "->");
//        if (node->right)
//            traversal(node->right, path + "->");
//    }
//    vector<string> binaryTreePaths(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        string path;
//        traversal(root, path);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int sumOfLeftLeaves(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        int leftNum = 0;
//        if (root->left != nullptr && root->left->left == nullptr && root->left->right == nullptr)
//            leftNum = root->left->val;
//        return leftNum + sumOfLeftLeaves(root->left) + sumOfLeftLeaves(root->right);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    int sum;
//public:
//    int sumLeaves(TreeNode* node)
//    {
//        if (node == nullptr)
//            return 0;
//        sumLeaves(node->left);
//        if (node->left && node->left->left == nullptr && node->left->right == nullptr)
//            sum += node->left->val;
//        sumLeaves(node->right);
//        return sum;
//    }
//    int sumOfLeftLeaves(TreeNode* root) {
//        return sumLeaves(root);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    queue<TreeNode*> q;
//public:
//    int findBottomLeftValue(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        int res = 0;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (node->right) q.push(node->right);
//                if (node->left) q.push(node->left);
//                res = node->val;
//            }
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    int res;
//    int maxDepth = INT_MIN;
//public:
//    void traversal(TreeNode* node, int depth)
//    {
//        if (node->left == nullptr && node->right == nullptr)
//        {
//            if (depth > maxDepth)
//            {
//                maxDepth = depth;
//                res = node->val;
//            }
//        }
//        if (node->left)traversal(node->left, depth + 1);
//        if (node->right) traversal(node->right, depth + 1);
//        return;
//    }
//    int findBottomLeftValue(TreeNode* root) {
//        traversal(root, 0);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    bool traversal(TreeNode* node, int count)
//    {
//        if (node->left == nullptr && node->right == nullptr)
//        {
//            if (count == 0) return true;
//            else return false;
//        }
//        if (node->left)
//            if (traversal(node->left, count - node->left->val))
//                return true;
//        if (node->right)
//            if (traversal(node->right, count - node->right->val))
//                return true;
//        return false;
//    }
//    bool hasPathSum(TreeNode* root, int targetSum) {
//        if (root == nullptr)
//            return false;
//        return traversal(root, targetSum - root->val);
//    }
//};


//class Solution {
//private:
//    int n, m;
//    int prev;
//    int dx[4] = { -1, 0, 1, 0 }, dy[4] = { 0, 1, 0, -1 };
//public:
//    void dfs(vector<vector<int>>& image, int i, int j, int color)
//    {
//        image[i][j] = color;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && image[x][y] == prev)
//                dfs(image, x, y, color);
//        }
//    }
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        if (image[sr][sc] == color)
//            return image;
//        n = image.size(), m = image[0].size();
//        prev = image[sr][sc];
//        dfs(image, sr, sc, color);
//        return image;
//    }
//};


//class Solution {
//private:
//    int n, m;
//    int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//    vector<vector<bool>> vis;
//public:
//    void dfs(vector<vector<char>>& grid, int i, int j)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && !vis[x][y] && grid[x][y] == '1')
//                dfs(grid, x, y);
//        }
//    }
//    int numIslands(vector<vector<char>>& grid) {
//        n = grid.size(), m = grid[0].size();
//        vis = vector<vector<bool>>(n, vector<bool>(m));
//        int res = 0;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (!vis[i][j] && grid[i][j] == '1')
//                {
//                    res++;
//                    dfs(grid, i, j);
//                }
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    int n, m;
//    int count;
//    vector<vector<bool>> vis;
//    int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//public:
//    void dfs(vector<vector<int>>& grid, int i, int j)
//    {
//        count++;
//        vis[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && !vis[x][y] && grid[x][y] == 1)
//                dfs(grid, x, y);
//        }
//    }
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        n = grid.size(), m = grid[0].size();
//        vis = vector<vector<bool>>(n, vector<bool>(m));
//        int res = 0;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (!vis[i][j] && grid[i][j] == 1)
//                {
//                    count = 0;
//                    dfs(grid, i, j);
//                    res = max(res, count);
//                }
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    int n, m;
//    int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//public:
//    void dfs(vector<vector<char>>& board, int i, int j)
//    {
//        board[i][j] = '.';
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && board[x][y] == 'O')
//                dfs(board, x, y);
//        }
//    }
//    void solve(vector<vector<char>>& board) {
//        n = board.size(), m = board[0].size();
//        for (int j = 0; j < m; j++)
//        {
//            if (board[0][j] == 'O') dfs(board, 0, j);
//            if (board[n - 1][j] == 'O') dfs(board, n - 1, j);
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (board[i][0] == 'O') dfs(board, i, 0);
//            if (board[i][m - 1] == 'O') dfs(board, i, m - 1);
//        }
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (board[i][j] == '.') board[i][j] = 'O';
//                else if (board[i][j] == 'O') board[i][j] = 'X';
//            }
//        }
//    }
//};


//class Solution {
//private:
//    int n, m;
//    int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//    vector<vector<int>> res;
//public:
//    void dfs(vector<vector<int>>& heights, int i, int j, vector<vector<bool>>& ocean)
//    {
//        ocean[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && !ocean[x][y] && heights[x][y] >= heights[i][j])
//                dfs(heights, x, y, ocean);
//        }
//    }
//    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
//        n = heights.size(), m = heights[0].size();
//        vector<vector<bool>> pac(n, vector<bool>(m));
//        vector<vector<bool>> atl(n, vector<bool>(m));
//
//        for (int j = 0; j < m; j++) dfs(heights, 0, j, pac);
//        for (int i = 0; i < n; i++) dfs(heights, i, 0, pac);
//
//        for (int i = 0; i < n; i++) dfs(heights, i, m - 1, atl);
//        for (int j = 0; j < m; j++) dfs(heights, n - 1, j, atl);
//
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (pac[i][j] && atl[i][j])
//                    res.push_back({ i, j });
//        return res;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    cout << "This is a simple problem." << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int h, m;
//    scanf("%d:%d", &h, &m);
//    if ((h >= 0 && h < 12) || (h == 12 && m == 0))
//        printf("Only %02d:%02d.  Too early to Dang.\n", h, m);
//    else
//    {
//        if (m > 0)
//            for (int i = 0; i <= h - 12; i++) cout << "Dang";
//        else
//            for (int i = 0; i < h - 12; i++) cout << "Dang";
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int a[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    int odd = 0, even = 0;
//    for (int i = 0; i < n; i++)
//    {
//        if (a[i] % 2 == 1) odd++;
//        else even++;
//    }
//    cout << odd << ' ' << even << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int p, q; //甲的酒量 乙的酒量
//    cin >> p >> q;
//    int n1 = 0, n2 = 0;
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b, c, d;
//        cin >> a >> b >> c >> d;
//        if (b == a + c && d != a + c)
//        {
//            n1++;
//            p--;
//        }
//        if (b != a + c && d == a + c)
//        {
//            n2++;
//            q--;
//        }
//        if (p < 0)
//        {
//            cout << 'A' << endl;
//            cout << n2 << endl;
//            break;
//        }
//        if (q < 0)
//        {
//            cout << 'B' << endl;
//            cout << n1 << endl;
//            break;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    char c;
//    cin >> n >> c;
//    for (int i = 0; i < (n + 1) / 2; i++)
//    {
//        for (int j = 0; j < n; j++)
//            cout << c;
//        cout << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string a, b;
//    getline(cin, a);
//    getline(cin, b);
//    for (int i = 0; i < a.size(); i++)
//    {
//        bool flag = true;
//        for (int j = 0; j < b.size(); j++)
//        {
//            if (a[i] == b[j])
//            {
//                flag = false;
//                break;
//            }
//        }
//        if (flag) cout << a[i];
//    }
//    cout << endl;
//    return 0;
//}


#include <iostream>
#include <vector>
#include <set>
using namespace std;

const int N = 1e4 + 10;
int indep[N];
bool flag, noIndep[N];

bool isPrime(int x)
{
    if (x == 1) return false;
    for (int i = 2; i <= x / i; i++)
        if (x % i == 0)
            return false;
    return true;
}

bool isIndependent(int x)
{
    set<int> set;
    int k = x;
    while (k != 1)
    {
        set.insert(k);
        int sum = 0;
        while (k)
        {
            int t = k % 10;
            sum += t * t;
            k /= 10;
        }
        indep[x]++;
        noIndep[sum] = true;
        k = sum;
        if (set.find(k) != set.end())
            return false;
    }
    return true;
}

int main()
{
    int a, b;
    cin >> a >> b;
    vector<int> happy;
    for (int i = a; i <= b; i++)
    {
        if (isIndependent(i))
            happy.push_back(i);
    }
    for (int i = 0; i < happy.size(); i++)
    {
        if (isPrime(happy[i]))
            indep[happy[i]] *= 2;
        if (!noIndep[happy[i]])
        {
            cout << happy[i] << ' ' << indep[happy[i]] << endl;
            flag = true;
        }
    }
    if (!flag) cout << "SAD" << endl;
    return 0;
}