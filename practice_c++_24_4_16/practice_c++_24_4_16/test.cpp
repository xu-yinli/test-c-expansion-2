#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    cout << "Good code is its own best documentation." << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int a, b;
//    cin >> a >> b;
//    int sum = a + b;
//    cout << sum - 16 << endl;
//    cout << sum - 3 << endl;
//    cout << sum - 1 << endl;
//    cout << sum << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n, m, k;
//    string x;
//    cin >> n >> x >> m >> k;
//    if (k == n) cout << "mei you mai " << x << " de" << endl;
//    else if (k == m) cout << "kan dao le mai " << x << " de" << endl;
//    else cout << "wang le zhao mai " << x << " de" << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b, c;
//        cin >> a >> b >> c;
//        if (c == a * b) cout << "Lv Yan" << endl;
//        else if (c == a + b) cout << "Tu Dou" << endl;
//        else cout << "zhe du shi sha ya!" << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 110;
//int color[N], guess[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//        cin >> color[i];
//    int k;
//    cin >> k;
//    while (k--)
//    {
//        bool flag1 = false, flag2 = true; //有没有人猜 是否有人猜错
//        for (int i = 0; i < n; i++)
//            cin >> guess[i];
//        for (int i = 0; i < n; i++)
//        {
//            if (guess[i] != 0)
//                flag1 = true;
//            else
//                continue;
//            if (guess[i] != color[i])
//                flag2 = false;
//        }
//        if (flag1 && flag2) cout << "Da Jiang!!!" << endl;
//        else cout << "Ai Ya" << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string s;
//    cin >> s;
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b;
//        string s1, s2;
//        cin >> a >> b >> s1 >> s2;
//        string tmp = s.substr(a - 1, b - a + 1); //剪贴板
//        s.erase(a - 1, b - a + 1); //删除剪贴板内容
//        string k = s1 + s2;
//        int index = s.find(k);
//        if (index != -1) s.insert(index + s1.size(), tmp);
//        else s += tmp;
//    }
//    cout << s << endl;
//    return 0;
//}


//#include <iostream>
//#include <stack>
//#include <algorithm>
//using namespace std;
//
//stack<int> A; //A柱串宝塔
//stack<int> B; //B柱用于临时叠放
//
//int main()
//{
//    int n; //彩虹圈的个数
//    cin >> n;
//    int product = 0; //堆出的宝塔个数
//    int max_num = 0; //最高的宝塔的层数
//    while (n--)
//    {
//        int C; //彩虹圈的直径
//        cin >> C;
//        if (A.empty())
//            A.push(C);
//        else //跟当前A柱宝塔最上面的彩虹圈作比较
//        {
//            if (C < A.top()) //比最上面的小，直接放上去
//                A.push(C);
//            else //把C跟B柱最上面的彩虹圈比一下
//            {
//                if (B.empty() || C > B.top()) //B柱是空的或者C大，就在B柱上放好
//                    B.push(C);
//                else //把A柱上串好的宝塔取下来作为一件成品
//                {
//                    product++; //宝塔数+1
//                    int len_a = A.size();
//                    max_num = max(max_num, len_a);
//                    while (!A.empty())
//                        A.pop();
//                    while (!B.empty() && B.top() > C)
//                    {
//                        A.push(B.top());
//                        B.pop();
//                    }
//                    A.push(C);
//                }
//            }
//        }
//    }
//    if (!A.empty())
//    {
//        product++; //宝塔数+1
//        int len_a = A.size();
//        max_num = max(max_num, len_a);
//    }
//    while (!A.empty())
//        A.pop();
//    while (!B.empty())
//    {
//        A.push(B.top());
//        B.pop();
//    }
//    if (!A.empty())
//    {
//        product++; //宝塔数+1
//        int len_a = A.size();
//        max_num = max(max_num, len_a);
//    }
//    while (!A.empty())
//        A.pop();
//    cout << product << ' ' << max_num << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//const int N = 1e5 + 10;
//
//int n, m;
//string g[N]; //存储地图信息
//bool flag; //标记当前岛屿是否有宝藏
//int dx[4] = { 1,0,-1,0 }, dy[4] = { 0,1,0,-1 };
//
//void dfs(int i, int j)
//{
//    if (i < 0 || i >= n || j < 0 || j >= m || g[i][j] == '0')
//        return;
//    if (g[i][j] > '1') //有宝藏
//        flag = true;
//    g[i][j] = '0'; //表明该点已被搜索
//    for (int k = 0; k < 4; k++)
//    {
//        int x = i + dx[k], y = j + dy[k];
//        dfs(x, y);
//    }
//    return;
//}
//
//int main()
//{
//    cin >> n >> m;
//    for (int i = 0; i < n; i++)
//        cin >> g[i];
//    int sum = 0, gift = 0; //岛屿的总数量 有宝藏的岛屿的数量
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            if (g[i][j] > '0') //非水域(岛屿)
//            {
//                sum++; //岛屿数量+1
//                flag = false; //重置为false
//                dfs(i, j); //标记与之连通的岛屿块
//                if (flag) gift++;
//            }
//        }
//    }
//    cout << sum << ' ' << gift << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//const int N = 110;
//int g[N], id[11][N], t[N];
//
//int main()
//{
//    int n, m; //小偷的原始标记个数 新标记对照矩阵的行数
//    cin >> n >> m;
//    getchar();
//    vector<string> mean(n + 1); //对应的解释
//    for (int i = 1; i <= n; i++)
//        getline(cin, mean[i]);
//    for (int i = 1; i <= m; i++) //第i个数对应矩阵第i行
//        cin >> g[i];
//    for (int i = 1; i <= m; i++)
//        for (int j = 1; j <= 10; j++)
//            cin >> id[i][j];
//    int k;
//    cin >> k;
//    for (int i = 1; i <= k; i++)
//    {
//        cin >> t[i];
//        int p = t[i] / 10; //行标记
//        int q = t[i] % 10; //列标记
//        int ans = -1;
//        bool flag = false;
//        for (int j = 1; j <= m; j++)
//        {
//            if (p == g[j])
//            {
//                ans = id[j][q + 1];
//                flag = true;
//                break;
//            }
//        }
//        if (flag && ans != -1) cout << mean[ans] << endl;
//        else cout << "?" << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <map>
//#include <algorithm>
//using namespace std;
//
//const int N = 110;
//
//struct User
//{
//    string name;
//    int k;
//    map<int, int> hash;
//    double ave;
//    bool operator<(const User& t)
//    {
//        if (hash.size() == t.hash.size())
//            return ave < t.ave; //平均分降序
//        return hash.size() > t.hash.size(); //数量升序
//    }
//}u[N];
//
//int main()
//{
//    int n; //待统计的用户数
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> u[i].name >> u[i].k;
//        for (int j = 1; j <= u[i].k; j++)
//        {
//            int x;
//            cin >> x;
//            u[i].hash[x]++;
//        }
//    }
//    for (int i = 0; i < n; i++)
//        u[i].ave = 1.0 * u[i].k / u[i].hash.size();
//    sort(u, u + n);
//    if (n < 3)
//    {
//        for (int i = 0; i < n; i++)
//            cout << u[i].name << ' ';
//        if (n == 1) cout << "- -" << endl;
//        else if (n == 2) cout << "-" << endl;
//    }
//    else
//        cout << u[0].name << ' ' << u[1].name << ' ' << u[2].name << endl;
//    return 0;
//}


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
//        TreeNode* node = new TreeNode(0);
//        int maxValue = nums[0];
//        int maxValueIndex = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (nums[i] > maxValue)
//            {
//                maxValue = nums[i];
//                maxValueIndex = i;
//            }
//        }
//        node->val = maxValue;
//        if (maxValueIndex > 0)
//        {
//            vector<int> newVec(nums.begin(), nums.begin() + maxValueIndex);
//            node->left = constructMaximumBinaryTree(newVec);
//        }
//        if (maxValueIndex < nums.size() - 1)
//        {
//            vector<int> newVec(nums.begin() + maxValueIndex + 1, nums.end());
//            node->right = constructMaximumBinaryTree(newVec);
//        }
//        return node;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* traversal(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return nullptr;
//        int maxValueIndex = left;
//        for (int i = left + 1; i < right; i++)
//        {
//            if (nums[i] > nums[maxValueIndex])
//                maxValueIndex = i;
//        }
//        TreeNode* node = new TreeNode(nums[maxValueIndex]);
//        node->left = traversal(nums, left, maxValueIndex);
//        node->right = traversal(nums, maxValueIndex + 1, right);
//        return node;
//    }
//    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
//        int n = nums.size();
//        return traversal(nums, 0, n);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
//        if (root1 == nullptr) return root2;
//        if (root2 == nullptr) return root1;
//        root1->val += root2->val;
//        root1->left = mergeTrees(root1->left, root2->left);
//        root1->right = mergeTrees(root1->right, root2->right);
//        return root1;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
//        if (root1 == nullptr) return root2;
//        if (root2 == nullptr) return root1;
//        root1->left = mergeTrees(root1->left, root2->left);
//        root1->val += root2->val;
//        root1->right = mergeTrees(root1->right, root2->right);
//        return root1;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
//        if (root1 == nullptr) return root2;
//        if (root2 == nullptr) return root1;
//        root1->left = mergeTrees(root1->left, root2->left);
//        root1->right = mergeTrees(root1->right, root2->right);
//        root1->val += root2->val;
//        return root1;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
//        if (root1 == nullptr) return root2;
//        if (root2 == nullptr) return root1;
//        TreeNode* root = new TreeNode(0);
//        root->val = root1->val + root2->val;
//        root->left = mergeTrees(root1->left, root2->left);
//        root->right = mergeTrees(root1->right, root2->right);
//        return root;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        //删除头结点
//        while (head != nullptr && head->val == val)
//            head = head->next;
//        ListNode* cur = head;
//        while (cur != nullptr && cur->next != nullptr)
//        {
//            if (cur->next->val == val)
//                cur->next = cur->next->next;
//            else
//                cur = cur->next;
//        }
//        return head;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* cur = dummyHead;
//        while (cur->next != nullptr)
//        {
//            if (cur->next->val == val)
//                cur->next = cur->next->next;
//            else
//                cur = cur->next;
//        }
//        return dummyHead->next;
//    }
//};


//class MyLinkedList {
//private:
//    int size;
//    ListNode* dummyHead;
//public:
//    MyLinkedList() {
//        this->size = 0;
//        this->dummyHead = new ListNode(0);
//    }
//
//    int get(int index) {
//        if (index < 0 || index >= size) return -1;
//        ListNode* cur = dummyHead->next;
//        while (index--)
//            cur = cur->next;
//        return cur->val;
//    }
//
//    void addAtHead(int val) {
//        ListNode* newNode = new ListNode(val);
//        newNode->next = dummyHead->next;
//        dummyHead->next = newNode;
//        size++;
//    }
//
//    void addAtTail(int val) {
//        ListNode* newNode = new ListNode(val);
//        ListNode* cur = dummyHead;
//        while (cur->next != nullptr)
//            cur = cur->next;
//        cur->next = newNode;
//        size++;
//    }
//
//    void addAtIndex(int index, int val) {
//        if (index<0 || index>size) return;
//        ListNode* newNode = new ListNode(val);
//        ListNode* cur = dummyHead;
//        while (index--)
//            cur = cur->next;
//        newNode->next = cur->next;
//        cur->next = newNode;
//        size++;
//    }
//
//    void deleteAtIndex(int index) {
//        if (index < 0 || index >= size) return;
//        ListNode* cur = dummyHead;
//        while (index--)
//            cur = cur->next;
//        cur->next = cur->next->next;
//        size--;
//    }
//};
//
///**
// * Your MyLinkedList object will be instantiated and called as such:
// * MyLinkedList* obj = new MyLinkedList();
// * int param_1 = obj->get(index);
// * obj->addAtHead(val);
// * obj->addAtTail(val);
// * obj->addAtIndex(index,val);
// * obj->deleteAtIndex(index);
// */


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* cur = head;
//        ListNode* prev = nullptr;
//        while (cur)
//        {
//            ListNode* tmp = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = tmp;
//        }
//        return prev;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverse(ListNode* prev, ListNode* cur)
//    {
//        if (cur == nullptr) return prev;
//        ListNode* tmp = cur->next;
//        cur->next = prev;
//        return reverse(cur, tmp);
//    }
//    ListNode* reverseList(ListNode* head) {
//        return reverse(nullptr, head);
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* cur = dummyHead;
//        while (cur->next != nullptr && cur->next->next != nullptr)
//        {
//            ListNode* tmp = cur->next;
//            cur->next = cur->next->next;
//            tmp->next = tmp->next->next;
//            cur->next->next = tmp;
//
//            cur = cur->next->next;
//        }
//        return dummyHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* removeNthFromEnd(ListNode* head, int n) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* slow = dummyHead;
//        ListNode* fast = dummyHead;
//        while (n--)
//            fast = fast->next;
//        fast = fast->next;
//        while (fast != nullptr)
//        {
//            slow = slow->next;
//            fast = fast->next;
//        }
//        slow->next = slow->next->next;
//        return dummyHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    ListNode* detectCycle(ListNode* head) {
//        ListNode* slow = head;
//        ListNode* fast = head;
//        while (fast != NULL && fast->next != NULL)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//            if (slow == fast)
//            {
//                ListNode* k1 = fast;
//                ListNode* k2 = head;
//                while (k1 != k2)
//                {
//                    k1 = k1->next;
//                    k2 = k2->next;
//                }
//                return k1;
//            }
//        }
//        return NULL;
//    }
//};


//class Solution {
//private:
//    int n, m;
//    int dx[8] = { -1,0,1,0,-1,-1,1,1 }, dy[8] = { 0,1,0,-1,-1,1,1,-1 };
//public:
//    void dfs(vector<vector<char>>& board, int i, int j)
//    {
//        int cnt = 0;
//        for (int k = 0; k < 8; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && board[x][y] == 'M')
//                cnt++;
//        }
//        if (cnt != 0)
//        {
//            board[i][j] = cnt + '0';
//            return;
//        }
//        else
//        {
//            board[i][j] = 'B';
//            for (int k = 0; k < 8; k++)
//            {
//                int x = i + dx[k], y = j + dy[k];
//                if (x >= 0 && x < n && y >= 0 && y < m && board[x][y] == 'E')
//                    dfs(board, x, y);
//            }
//        }
//        return;
//    }
//    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
//        n = board.size(), m = board[0].size();
//        int x = click[0], y = click[1];
//        if (board[x][y] == 'M')
//        {
//            board[x][y] = 'X';
//            return board;
//        }
//        dfs(board, x, y);
//        return board;
//    }
//};


//class Solution {
//private:
//    int _m, _n, _cnt;
//    int res;
//    bool vis[110][110];
//    int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//public:
//    bool check(int x, int y)
//    {
//        int p;
//        while (x)
//        {
//            p += x % 10;
//            x /= 10;
//        }
//        while (y)
//        {
//            p += y % 10;
//            y /= 10;
//        }
//        return p <= _cnt;
//    }
//    void dfs(int i, int j)
//    {
//        res++;
//        vis[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < _n && y >= 0 && y < _m && !vis[x][y] && check(x, y))
//                dfs(x, y);
//
//        }
//    }
//    int wardrobeFinishing(int m, int n, int cnt) {
//        _m = m, _n = n, _cnt = cnt;
//        dfs(0, 0);
//        return res;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int a, b, c;
//    cin >> a >> b >> c;
//    if (a > b) swap(a, b);
//    if (a > c) swap(a, c);
//    if (b > c) swap(b, c);
//    printf("%d->%d->%d\n", a, b, c);
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string a, b;
//    getline(cin, a);
//    getline(cin, b);
//    bool flag = true;
//    for (int i = 0; i < a.size(); i++)
//    {
//        flag = true;
//        for (int j = 0; j < b.size(); j++)
//        {
//            if (a[i] == b[j])
//            {
//                flag = false;
//                break;
//            }
//        }
//        if (flag) cout << a[i];
//    }
//    cout << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    int k = n;
//    int res = 1;
//    while (k--)
//        res *= 2;
//    printf("2^%d = %d\n", n, res);
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//string pinyin[10] = { "ling","yi","er","san","si","wu","liu","qi","ba","jiu" };
//
//int main()
//{
//    string n;
//    cin >> n;
//    int i = 0;
//    if (n[0] == '-')
//    {
//        cout << "fu ";
//        i++;
//    }
//    while (n[i] != '\0')
//    {
//        cout << pinyin[n[i] - '0'];
//        i++;
//        if (n[i] != '\0')
//            cout << ' ';
//    }
//    return 0;
//}


#include <iostream>
using namespace std;

typedef long long LL;

LL max_gcd(LL a, LL b)
{
    return b ? max_gcd(b, a % b) : a;
}

int main()
{
    int n;
    cin >> n;
    LL fenzi = 0, fenmu = 1, gcd;
    for (int i = 0; i < n; i++)
    {
        LL a, b;
        scanf("%lld/%lld", &a, &b);
        fenzi = fenzi * b + fenmu * a;
        fenmu = fenmu * b;
        if (fenzi == 0)
        {
            fenmu = 1;
            continue;
        }
        gcd = max_gcd(fenzi, fenmu);
        fenzi = fenzi / gcd;
        fenmu = fenmu / gcd;
    }
    if (fenmu == 1) printf("%lld\n", fenzi);
    else if (fenzi < fenmu) printf("%lld/%lld\n", fenzi, fenmu);
    else printf("%lld %lld/%lld\n", fenzi / fenmu, fenzi % fenmu, fenmu);
    return 0;
}