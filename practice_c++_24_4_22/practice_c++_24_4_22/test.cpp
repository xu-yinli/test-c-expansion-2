#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    int cnt, maxCount;
//    TreeNode* prev = nullptr;
//    vector<int> res;
//public:
//    void searchBST(TreeNode* cur)
//    {
//        if (cur == nullptr) return;
//        searchBST(cur->left);
//        if (prev == nullptr) cnt = 1;
//        else if (prev->val == cur->val) cnt++;
//        else cnt = 1;
//        prev = cur;
//        if (cnt == maxCount) res.push_back(cur->val);
//        if (cnt > maxCount)
//        {
//            maxCount = cnt;
//            res.clear();
//            res.push_back(cur->val);
//        }
//        searchBST(cur->right);
//        return;
//    }
//    vector<int> findMode(TreeNode* root) {
//        searchBST(root);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        if (root == NULL) return root;
//        if (root == p || root == q) return root;
//        TreeNode* left = lowestCommonAncestor(root->left, p, q);
//        TreeNode* right = lowestCommonAncestor(root->right, p, q);
//        if (left != NULL && right != NULL) return root;
//        else if (left == NULL && right != NULL) return right;
//        else if (left != NULL && right == NULL) return left;
//        else return NULL;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
// * };
// */
//
//class Solution {
//public:
//    TreeNode* traversal(TreeNode* cur, TreeNode* p, TreeNode* q)
//    {
//        if (cur == NULL) return cur;
//        if (cur->val > p->val && cur->val > q->val)
//        {
//            TreeNode* left = traversal(cur->left, p, q);
//            if (left != NULL) return left;
//        }
//        if (cur->val < p->val && cur->val < q->val)
//        {
//            TreeNode* right = traversal(cur->right, p, q);
//            if (right != NULL) return right;
//        }
//        return cur;
//    }
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        return traversal(root, p, q);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
// * };
// */
//
//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        if (root->val > p->val && root->val > q->val)
//            return lowestCommonAncestor(root->left, p, q);
//        else if (root->val < p->val && root->val < q->val)
//            return lowestCommonAncestor(root->right, p, q);
//        else return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
// * };
// */
//
//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        while (root)
//        {
//            if (root->val > p->val && root->val > q->val)
//                root = root->left;
//            else if (root->val < p->val && root->val < q->val)
//                root = root->right;
//            else return root;
//        }
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* insertIntoBST(TreeNode* root, int val) {
//        if (root == nullptr)
//        {
//            TreeNode* node = new TreeNode(val);
//            return node;
//        }
//        if (val < root->val) root->left = insertIntoBST(root->left, val);
//        if (val > root->val) root->right = insertIntoBST(root->right, val);
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* deleteNode(TreeNode* root, int key) {
//        if (root == nullptr) return root;
//        if (key < root->val) root->left = deleteNode(root->left, key);
//        else if (key > root->val) root->right = deleteNode(root->right, key);
//        else
//        {
//            if (root->left == nullptr && root->right == nullptr)
//            {
//                delete root;
//                return nullptr;
//            }
//            else if (root->left == nullptr && root->right != nullptr)
//            {
//                auto node = root->right;
//                delete root;
//                return node;
//            }
//            else if (root->left != nullptr && root->right == nullptr)
//            {
//                auto node = root->left;
//                delete root;
//                return node;
//            }
//            else
//            {
//                TreeNode* cur = root->right;
//                while (cur->left)
//                    cur = cur->left;
//                cur->left = root->left;
//                auto tmp = root;
//                root = root->right;
//                delete tmp;
//                return root;
//            }
//        }
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* trimBST(TreeNode* root, int low, int high) {
//        if (root == nullptr) return root;
//        if (root->val < low) return trimBST(root->right, low, high);
//        if (root->val > high) return trimBST(root->left, low, high);
//        root->left = trimBST(root->left, low, high);
//        root->right = trimBST(root->right, low, high);
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* traversal(vector<int>& nums, int left, int right)
//    {
//        if (left > right) return nullptr;
//        int mid = left + (right - left) / 2;
//        TreeNode* root = new TreeNode(nums[mid]);
//        root->left = traversal(nums, left, mid - 1);
//        root->right = traversal(nums, mid + 1, right);
//        return root;
//    }
//    TreeNode* sortedArrayToBST(vector<int>& nums) {
//        int n = nums.size();
//        return traversal(nums, 0, n - 1);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    int prev = 0;
//public:
//    void traversal(TreeNode* cur)
//    {
//        if (cur == nullptr) return;
//        traversal(cur->right);
//        cur->val += prev;
//        prev = cur->val;
//        traversal(cur->left);
//    }
//    TreeNode* convertBST(TreeNode* root) {
//        traversal(root);
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    int prev = 0;
//public:
//    void traversal(TreeNode* cur)
//    {
//        if (cur == nullptr) return;
//        traversal(cur->right);
//        cur->val += prev;
//        prev = cur->val;
//        traversal(cur->left);
//    }
//    TreeNode* bstToGst(TreeNode* root) {
//        traversal(root);
//        return root;
//    }
//};


///**
//*  struct ListNode {
//*        int val;
//*        struct ListNode *next;
//*        ListNode(int x) :
//*              val(x), next(NULL) {
//*        }
//*  };
//*/
//class Solution {
//private:
//    stack<int> st;
//    vector<int> res;
//public:
//    vector<int> printListFromTailToHead(ListNode* head) {
//        while (head)
//        {
//            st.push(head->val);
//            head = head->next;
//        }
//        while (st.size())
//        {
//            res.push_back(st.top());
//            st.pop();
//        }
//        return res;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//private:
//    stack<int> st;
//    vector<int> res;
//public:
//    vector<int> reverseBookList(ListNode* head) {
//        while (head)
//        {
//            st.push(head->val);
//            head = head->next;
//        }
//        while (st.size())
//        {
//            res.push_back(st.top());
//            st.pop();
//        }
//        return res;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> reverseBookList(ListNode* head) {
//        while (head)
//        {
//            res.push_back(head->val);
//            head = head->next;
//        }
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


///**
//*  struct ListNode {
//*        int val;
//*        struct ListNode *next;
//*        ListNode(int x) :
//*              val(x), next(NULL) {
//*        }
//*  };
//*/
//class Solution {
//private:
//    vector<int> res;
//public:
//    void reversePrint(ListNode* head)
//    {
//        if (head == nullptr) return;
//        reversePrint(head->next);
//        res.push_back(head->val);
//    }
//    vector<int> printListFromTailToHead(ListNode* head) {
//        reversePrint(head);
//        return res;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void reversePrint(ListNode* head)
//    {
//        if (head == nullptr) return;
//        reversePrint(head->next);
//        res.push_back(head->val);
//    }
//    vector<int> reverseBookList(ListNode* head) {
//        reversePrint(head);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* traversal(vector<int>& preorder, int pre_st, int pre_ed, vector<int>& inorder, int in_st, int in_ed)
//    {
//        if (pre_st > pre_ed || in_st > in_ed) return nullptr;
//        TreeNode* root = new TreeNode(preorder[pre_st]);
//        for (int i = 0; i < inorder.size(); i++)
//        {
//            if (inorder[i] == preorder[pre_st])
//            {
//                root->left = traversal(preorder, pre_st + 1, (pre_st + 1) + (i - in_st - 1), inorder, in_st, i - 1);
//                root->right = traversal(preorder, (pre_st + 1) + (i - in_st - 1) + 1, pre_ed, inorder, i + 1, in_ed);
//                break;
//            }
//        }
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
//        int n = preorder.size(), m = inorder.size();
//        return traversal(preorder, 0, n - 1, inorder, 0, m - 1);
//    }
//};


///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param preOrder int整型vector
//     * @param vinOrder int整型vector
//     * @return TreeNode类
//     */
//    TreeNode* traversal(vector<int>& preOrder, int pre_st, int pre_ed, vector<int>& vinOrder, int in_st, int in_ed)
//    {
//        if (pre_st > pre_ed || in_st > in_ed) return nullptr;
//        TreeNode* root = new TreeNode(preOrder[pre_st]);
//        for (int i = 0; i < vinOrder.size(); i++)
//        {
//            if (vinOrder[i] == preOrder[pre_st])
//            {
//                root->left = traversal(preOrder, pre_st + 1, (pre_st + 1) + (i - in_st - 1), vinOrder, in_st, i - 1);
//                root->right = traversal(preOrder, (pre_st + 1) + (i - in_st - 1) + 1, pre_ed, vinOrder, i + 1, in_ed);
//                break;
//            }
//        }
//        return root;
//    }
//    TreeNode* reConstructBinaryTree(vector<int>& preOrder, vector<int>& vinOrder) {
//        int n = preOrder.size(), m = vinOrder.size();
//        return traversal(preOrder, 0, n - 1, vinOrder, 0, m - 1);
//    }
//};


///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param preOrder int整型vector
//     * @param vinOrder int整型vector
//     * @return TreeNode类
//     */
//    TreeNode* traversal(vector<int>& preOrder, int pre_st, int pre_ed, vector<int>& vinOrder, int in_st, int in_ed)
//    {
//        if (pre_st > pre_ed || in_st > in_ed) return nullptr;
//        TreeNode* root = new TreeNode(preOrder[pre_st]);
//        for (int i = 0; i < vinOrder.size(); i++)
//        {
//            if (vinOrder[i] == preOrder[pre_st])
//            {
//                root->left = traversal(preOrder, pre_st + 1, pre_st + i - in_st, vinOrder, in_st, i - 1);
//                root->right = traversal(preOrder, pre_st + i - in_st + 1, pre_ed, vinOrder, i + 1, in_ed);
//                break;
//            }
//        }
//        return root;
//    }
//    TreeNode* reConstructBinaryTree(vector<int>& preOrder, vector<int>& vinOrder) {
//        int n = preOrder.size(), m = vinOrder.size();
//        return traversal(preOrder, 0, n - 1, vinOrder, 0, m - 1);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* traversal(vector<int>& inorder, int in_st, int in_ed, vector<int>& postorder, int post_st, int post_ed)
//    {
//        if (in_st > in_ed || post_st > post_ed) return nullptr;
//        TreeNode* root = new TreeNode(postorder[post_ed]);
//        for (int i = 0; i < inorder.size(); i++)
//        {
//            if (inorder[i] == postorder[post_ed])
//            {
//                root->left = traversal(inorder, in_st, i - 1, postorder, post_st, post_st + (i - in_st) - 1);
//                root->right = traversal(inorder, i + 1, in_ed, postorder, post_st + (i - in_st), post_ed - 1);
//                break;
//            }
//        }
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
//        int n = inorder.size(), m = postorder.size();
//        return traversal(inorder, 0, n - 1, postorder, 0, m - 1);
//    }
//};


//class Solution {
//private:
//    int MOD = 1e9 + 7;
//    unordered_map<int, int> filter;
//public:
//    int fib(int n) {
//        if (n == 0 || n == 1) return n;
//        if (n == 2) return 1;
//        int ppre = 0;
//        if (filter.find(n - 2) == filter.end())
//        {
//            ppre = fib(n - 2);
//            filter.insert({ n - 2, ppre });
//        }
//        else
//            ppre = filter[n - 2];
//        int pre = 0;
//        if (filter.find(n - 1) == filter.end())
//        {
//            pre = fib(n - 1);
//            filter.insert({ n - 1, pre });
//        }
//        else
//            pre = filter[n - 1];
//        return (ppre + pre) % MOD;
//    }
//};


//#include <unordered_map>
//class Solution {
//private:
//    unordered_map<int, int> filter;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @return int整型
//     */
//    int Fibonacci(int n) {
//        if (n == 0 || n == 1) return n;
//        if (n == 2) return 1;
//        int ppre = 0;
//        if (filter.find(n - 2) == filter.end())
//        {
//            ppre = Fibonacci(n - 2);
//            filter.insert({ n - 2, ppre });
//        }
//        else ppre = filter[n - 2];
//        int pre = 0;
//        if (filter.find(n - 1) == filter.end())
//        {
//            pre = Fibonacci(n - 1);
//            filter.insert({ n - 1, pre });
//        }
//        else pre = filter[n - 1];
//        return ppre + pre;
//    }
//};