#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//vector<int> a;
//
//int gcd(int a, int b)
//{
//	return b ? gcd(b, a % b) : a;
//}
//
//int main()
//{
//	int n;
//	cin >> n;
//	for (int i = 0; i < n; i++)
//	{
//		int x;
//		cin >> x;
//		a.push_back(x);
//	}
//	sort(a.begin(), a.end());
//	int diff = 0;
//	for (int i = 1; i < n; i++)
//	{
//		int tmp = a[i] - a[i - 1];
//		diff = gcd(diff, tmp);
//	}
//	int res = 1;
//	int st = a[0], ed = a[n - 1];
//	if (diff == 0)
//	{
//		cout << n << endl;
//		return 0;
//	}
//	while (1)
//	{
//		res++;
//		st += diff;
//		if (st == ed) break;
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 40;
//int f[N][N];
//
//int main()
//{
//	int n, m;
//	cin >> n >> m;
//	for (int i = 1; i <= n; i++)
//		f[i][1] = 1;
//	for (int j = 1; j <= m; j++)
//		f[1][j] = 1; 
//	for (int i = 2; i <= n; i++)
//	{
//		for (int j = 2; j <= m; j++)
//		{
//			if (i % 2 == 0 && j % 2 == 0) //行号和列数都是偶数，不能走入这一格中
//				f[i][j] = 0;
//			else
//				f[i][j] = f[i - 1][j] + f[i][j - 1]; //只能向右/向下走
//		}
//	}
//	cout << f[n][m] << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 22, M = 7;
//int max_sum;
//int team[N][M];
//bool used[N];
//
//void dfs(int id, int sum)
//{
//    if (id > 5)
//    {
//        max_sum = max(max_sum, sum);
//        return;
//    }
//    for (int i = 1; i <= 20; i++)
//    {
//        if (!used[i])
//        {
//            used[i] = true;
//            dfs(id + 1, sum + team[i][id]);
//            used[i] = false;
//        }
//    }
//}
//
//int main()
//{
//    for (int i = 1; i <= 20; i++)
//    {
//        for (int j = 0; j <= 5; j++)
//        {
//            cin >> team[i][j];
//        }
//    }
//    dfs(1, 0);
//    cout << max_sum << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//char ch[26];
//vector<char> res;
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < 26; i++)
//        ch[i] = i + 'A';
//    int k = 0;
//    while (n)
//    {
//        int t = n % 26;  
//        if (t == 0) t += 26;
//        res.push_back(ch[t - 1]);
//        n = n / 26;
//    }
//    reverse(res.begin(), res.end());
//    for (char c : res)
//        cout << c;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//const int N = 20200000;
//LL dp[N];
//
//int main()
//{
//    LL n;
//    cin >> n;
//    dp[0] = dp[1] = dp[2] = 1;
//    for (int i = 3; i < n; i++)
//        dp[i] = (dp[i - 1] + dp[i - 2] + dp[i - 3]) % 10000;
//    cout << dp[n - 1] << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//bool check_num[2020];
//
//bool check(int x)
//{
//    while (x)
//    {
//        int t = x % 10;
//        if (t == 2 || t == 4)
//            return false;
//        x /= 10;
//    }
//    return true;
//}
//
//int main()
//{
//    int n = 2019;
//    int res = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        if (check(i)) check_num[i] = true;
//    }
//    for (int i = 1; i < n; i++)
//    {
//        for (int j = i + 1; j < n - i; j++)
//        {
//            int k = n - i - j;
//            if (i<j && j<k && k>0 && check(i) && check(j) && check(k))
//                res++;
//        }
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int n, cnt, ans; //表示地图的行和列 当前岛屿周围的陆地数量 没有被淹没的岛屿数量
//bool flag; //标记是否找到了一个不会被淹没的岛屿
//char map[1010][1010];
//int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//
//void dfs(int x, int y)
//{
//	if (!flag)
//	{
//		cnt = 0;
//		for (int i = 0; i < 4; i++)
//		{
//			int a = x + dx[i], b = y + dy[i];
//			if (map[a][b] != '.')
//				cnt++;
//			if (cnt == 4) //不会被淹没
//			{
//				ans++;
//				flag = true;
//			}
//		}
//	}
//	map[x][y] = '*'; //表示被访问过了
//	for (int i = 0; i < 4; i++)
//	{
//		int a = x + dx[i], b = y + dy[i];
//		if (a < 0 || a >= n || b < 0 || b >= n || map[a][b] != '#')
//			continue;
//		dfs(a, b);
//	}
//	return;
//}
//
//int main()
//{
//	cin >> n;
//	for (int i = 0; i < n; i++)
//		for (int j = 0; j < n; j++)
//			cin >> map[i][j];
//	int sum = 0;
//	for (int i = 1; i < n - 1; i++)
//	{
//		for (int j = 1; j < n - 1; j++)
//		{
//			if (map[i][j] == '#')
//			{
//				sum++;
//				flag = false;
//				dfs(i, j);
//			}
//		}
//	}
//	cout << sum - ans << endl; //岛屿总数量-没被淹没的岛屿
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	string a, b;
//	cin >> a >> b;
//	int res = 0;
//	for (int i = 0; i < a.size(); i++)
//	{
//		if (a[i] != b[i])
//		{
//			if (a[i] == 'o') a[i] = '*';
//			else a[i] = 'o';
//			if (a[i + 1] == 'o') a[i + 1] = '*';
//			else a[i + 1] = 'o';
//			res++;
//		}
//	}
//	cout << res << endl;
//	return 0;
//}


//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    vector<vector<int>> threeSum(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (nums[i] > 0)
//                return res;
//            if (i > 0 && nums[i - 1] == nums[i])
//                continue;
//            int left = i + 1, right = nums.size() - 1;
//            while (left < right)
//            {
//                if (nums[i] + nums[left] + nums[right] > 0)
//                    right--;
//                else if (nums[i] + nums[left] + nums[right] < 0)
//                    left++;
//                else
//                {
//                    res.push_back({ nums[i], nums[left], nums[right] });
//                    while (left < right && nums[left] == nums[left + 1])
//                        left++;
//                    while (left < right && nums[right] == nums[right - 1])
//                        right--;
//                    left++;
//                    right--;
//                }
//            }
//        }
//        return res;
//    }
//};