#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseKGroup(ListNode* head, int k) {
//        int count = 0;
//        ListNode* cur = head;
//        while (cur)
//        {
//            count++;
//            cur = cur->next;
//        }
//        int n = count / k;
//        ListNode* newHead = new ListNode(0);
//        ListNode* prev = newHead;
//        cur = head;
//        for (int i = 0; i < n; i++)
//        {
//            ListNode* tmp = cur;
//            for (int j = 0; j < k; j++)
//            {
//                ListNode* next = cur->next;
//                cur->next = prev->next;
//                prev->next = cur;
//                cur = next;
//            }
//            prev = tmp;
//        }
//        if (count % k != 0) prev->next = cur;
//        return newHead->next;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* ReverseList(ListNode* head) {
//        ListNode* prev = nullptr;
//        ListNode* cur = head;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = next;
//        }
//        return prev;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param pHead1 ListNode类
//     * @param pHead2 ListNode类
//     * @return ListNode类
//     */
//    ListNode* Merge(ListNode* pHead1, ListNode* pHead2) {
//        ListNode* newHead = new ListNode(0);
//        ListNode* cur = newHead;
//        ListNode* p1 = pHead1;
//        ListNode* p2 = pHead2;
//        while (p1 && p2)
//        {
//            if (p1->val <= p2->val)
//            {
//                cur->next = p1;
//                p1 = p1->next;
//            }
//            else
//            {
//                cur->next = p2;
//                p2 = p2->next;
//            }
//            cur = cur->next;
//        }
//        if (p1) cur->next = p1;
//        if (p2) cur->next = p2;
//        return newHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        ListNode* newHead = new ListNode(0);
//        ListNode* cur = newHead;
//        while (list1 && list2)
//        {
//            if (list1->val <= list2->val)
//            {
//                cur->next = list1;
//                list1 = list1->next;
//            }
//            else
//            {
//                cur->next = list2;
//                list2 = list2->next;
//            }
//            cur = cur->next;
//        }
//        if (list1) cur->next = list1;
//        if (list2) cur->next = list2;
//        return newHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    bool hasCycle(ListNode* head) {
//        ListNode* fast = head;
//        ListNode* slow = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//            if (fast == slow) return true;
//        }
//        return false;
//    }
//};



///*
//struct ListNode {
//    int val;
//    struct ListNode *next;
//    ListNode(int x) :
//        val(x), next(NULL) {
//    }
//};
//*/
//class Solution {
//public:
//    ListNode* EntryNodeOfLoop(ListNode* pHead) {
//        ListNode* fast = pHead;
//        ListNode* slow = pHead;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//            if (fast == slow)
//            {
//                ListNode* prev = pHead;
//                ListNode* cur = fast;
//                while (prev != cur)
//                {
//                    prev = prev->next;
//                    cur = cur->next;
//                }
//                return prev;
//            }
//        }
//        return nullptr;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param pHead ListNode类
//     * @param k int整型
//     * @return ListNode类
//     */
//    ListNode* FindKthToTail(ListNode* pHead, int k) {
//        ListNode* fast = pHead;
//        ListNode* slow = pHead;
//        while (k--)
//        {
//            if (fast == nullptr) return nullptr;
//            fast = fast->next;
//        }
//        while (fast)
//        {
//            fast = fast->next;
//            slow = slow->next;
//        }
//        return slow;
//    }
//};


///*
//struct ListNode {
//	int val;
//	struct ListNode *next;
//	ListNode(int x) :
//			val(x), next(NULL) {
//	}
//};*/
//class Solution {
//public:
//	ListNode* FindFirstCommonNode(ListNode* pHead1, ListNode* pHead2) {
//		int count1 = 0, count2 = 0;
//		ListNode* cur1 = pHead1;
//		ListNode* cur2 = pHead2;
//		while (cur1)
//		{
//			count1++;
//			cur1 = cur1->next;
//		}
//		while (cur2)
//		{
//			count2++;
//			cur2 = cur2->next;
//		}
//		int step = abs(count1 - count2);
//		cur1 = pHead1;
//		cur2 = pHead2;
//		if (count1 > count2)
//		{
//			while (step--)
//				cur1 = cur1->next;
//		}
//		else if (count1 < count2)
//		{
//			while (step--)
//				cur2 = cur2->next;
//		}
//		while (cur1 && cur2)
//		{
//			if (cur1 == cur2) return cur1;
//			cur1 = cur1->next;
//			cur2 = cur2->next;
//		}
//		return nullptr;
//	}
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head1 ListNode类
//     * @param head2 ListNode类
//     * @return ListNode类
//     */
//    ListNode* reverseList(ListNode* head)
//    {
//        ListNode* prev = nullptr;
//        ListNode* cur = head;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = next;
//        }
//        return prev;
//    }
//    ListNode* addInList(ListNode* head1, ListNode* head2) {
//        head1 = reverseList(head1);
//        head2 = reverseList(head2);
//        ListNode* num1 = head1;
//        ListNode* num2 = head2;
//        ListNode* newHead = new ListNode(0);
//        ListNode* cur = newHead;
//        int t = 0;
//        while (num1 || num2 || t)
//        {
//            if (num1)
//            {
//                t += num1->val;
//                num1 = num1->next;
//            }
//            if (num2)
//            {
//                t += num2->val;
//                num2 = num2->next;
//            }
//            cur->next = new ListNode(t % 10);
//            cur = cur->next;
//            t /= 10;
//        }
//        cur = reverseList(newHead->next);
//        return cur;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类 the head node
//     * @return ListNode类
//     */
//    ListNode* sortInList(ListNode* head) {
//        vector<int> res;
//        ListNode* cur = head;
//        while (cur)
//        {
//            res.push_back(cur->val);
//            cur = cur->next;
//        }
//        sort(res.begin(), res.end());
//        cur = head;
//        for (int x : res)
//        {
//            cur->val = x;
//            cur = cur->next;
//        }
//        return head;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类 the head
//     * @return bool布尔型
//     */
//    bool isPail(ListNode* head) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* fast = dummyHead;
//        ListNode* slow = dummyHead;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        ListNode* cur = slow->next;
//        slow->next = nullptr;
//        ListNode* prev = nullptr;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = next;
//        }
//        while (head && prev)
//        {
//            if (head->val != prev->val) return false;
//            head = head->next;
//            prev = prev->next;
//        }
//        return true;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* oddEvenList(ListNode* head) {
//        ListNode* newHead1 = new ListNode(0);
//        ListNode* prev1 = newHead1;
//        ListNode* cur1 = head;
//        ListNode* newHead2 = new ListNode(0);
//        ListNode* prev2 = newHead2;
//        ListNode* cur2 = head->next;
//        while (cur1)
//        {
//            prev1->next = cur1;
//            prev1 = prev1->next;
//            if (cur1->next == nullptr || cur1->next->next == nullptr)
//            {
//                prev1->next = nullptr;
//                cur1 = nullptr;
//            }
//            else cur1 = cur1->next->next;
//            if (cur2)
//            {
//                prev2->next = cur2;
//                prev2 = prev2->next;
//                if (cur2->next == nullptr || cur2->next->next == nullptr)
//                {
//                    prev2->next = nullptr;
//                    cur2 = nullptr;
//                }
//                else cur2 = cur2->next->next;
//            }
//        }
//        prev1->next = newHead2->next;
//        return newHead1->next;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* oddEvenList(ListNode* head) {
//        if (head == nullptr) return head;
//        ListNode* odd = head;
//        ListNode* even = head->next;
//        ListNode* oddHead = odd;
//        ListNode* evenHead = even;
//        while (even && even->next)
//        {
//            odd->next = even->next;
//            odd = odd->next;
//            even->next = odd->next;
//            even = even->next;
//        }
//        odd->next = evenHead;
//        return head;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* deleteDuplicates(ListNode* head) {
//        if (head == nullptr) return head;
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* cur = head;
//        ListNode* prev = dummyHead;
//        while (cur->next)
//        {
//            ListNode* next = cur->next;
//            if (cur->val == next->val) prev->next = next;
//            else prev = cur;
//            cur = next;
//        }
//        return dummyHead->next;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* deleteDuplicates(ListNode* head) {
//        if (head == nullptr) return head;
//        ListNode* cur = head;
//        while (cur && cur->next)
//        {
//            if (cur->val == cur->next->val) cur->next = cur->next->next;
//            else cur = cur->next;
//        }
//        return head;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* deleteDuplicates(ListNode* head) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* cur = dummyHead;
//        while (cur->next && cur->next->next)
//        {
//            if (cur->next->val == cur->next->next->val)
//            {
//                int num = cur->next->val;
//                while (cur->next && cur->next->val == num)
//                {
//                    cur->next = cur->next->next;
//                }
//            }
//            else cur = cur->next;
//        }
//        return dummyHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* prev = nullptr;
//        ListNode* cur = head;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = next;
//        }
//        return prev;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    ListNode* sortList(ListNode* head) {
//        ListNode* cur = head;
//        while (cur)
//        {
//            res.push_back(cur->val);
//            cur = cur->next;
//        }
//        sort(res.begin(), res.end());
//        cur = head;
//        for (int x : res)
//        {
//            cur->val = x;
//            cur = cur->next;
//        }
//        return head;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    void deleteNode(ListNode* node) {
//        node->val = node->next->val;
//        node->next = node->next->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseBetween(ListNode* head, int left, int right) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* prev = dummyHead;
//        ListNode* cur = head;
//        for (int i = 1; i < left; i++)
//        {
//            prev = cur;
//            cur = cur->next;
//        }
//        ListNode* tmp = cur;
//        prev->next = nullptr;
//        int len = right - left + 1;
//        while (len--)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev->next;
//            prev->next = cur;
//            cur = next;
//        }
//        tmp->next = cur;
//        return dummyHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head)
//    {
//        ListNode* prev = nullptr;
//        ListNode* cur = head;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = next;
//        }
//        return prev;
//    }
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        l1 = reverseList(l1);
//        l2 = reverseList(l2);
//        ListNode* newHead = new ListNode(0);
//        ListNode* cur = newHead;
//        ListNode* num1 = l1;
//        ListNode* num2 = l2;
//        int t = 0;
//        while (num1 || num2 || t)
//        {
//            if (num1)
//            {
//                t += num1->val;
//                num1 = num1->next;
//            }
//            if (num2)
//            {
//                t += num2->val;
//                num2 = num2->next;
//            }
//            cur->next = new ListNode(t % 10);
//            cur = cur->next;
//            t /= 10;
//        }
//        cur = reverseList(newHead->next);
//        return cur;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* deleteMiddle(ListNode* head) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* fast = head;
//        ListNode* slow = dummyHead;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        slow->next = slow->next->next;
//        return dummyHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* rotateRight(ListNode* head, int k) {
//        if (head == nullptr || head->next == nullptr || k == 0) return head;
//        int count = 0;
//        ListNode* cur = head;
//        while (cur)
//        {
//            count++;
//            cur = cur->next;
//        }
//        if (k % count == 0) return head;
//        k %= count;
//        ListNode* fast = head;
//        ListNode* slow = head;
//        while (k--)
//            fast = fast->next;
//        while (fast->next)
//        {
//            fast = fast->next;
//            slow = slow->next;
//        }
//        cur = slow->next;
//        slow->next = nullptr;
//        fast->next = head;
//        return cur;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* deleteDuplicates(ListNode* head) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* cur = dummyHead;
//        while (cur->next && cur->next->next)
//        {
//            if (cur->next->val == cur->next->next->val)
//            {
//                int num = cur->next->val;
//                while (cur->next && cur->next->val == num)
//                {
//                    cur->next = cur->next->next;
//                }
//            }
//            else cur = cur->next;
//        }
//        return dummyHead->next;
//    }
//};