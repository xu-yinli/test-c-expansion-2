#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//private:
//    const int MOD = 1000000007;
//    vector<int> tmp;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @return int整型
//     */
//    int mergeSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return 0;
//        int mid = left + (right - left) / 2;
//        int res = 0;
//        res += mergeSort(nums, left, mid);
//        res += mergeSort(nums, mid + 1, right);
//        int i = 0, cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] <= nums[cur2])
//            {
//                tmp[i++] = nums[cur2++];
//            }
//            else
//            {
//                tmp[i++] = nums[cur1++];
//                res += right - cur2 + 1;
//                res %= MOD;
//            }
//        }
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//        for (int j = left; j <= right; j++)
//            nums[j] = tmp[j - left];
//        return res % MOD;
//    }
//    int InversePairs(vector<int>& nums) {
//        int n = nums.size();
//        tmp.resize(n);
//        return mergeSort(nums, 0, n - 1);
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @param k int整型
//     * @return int整型
//     */
//    int GetNumberOfK(vector<int>& nums, int k) {
//        int n = nums.size();
//        if (n == 0) return 0;
//        int st = 0;
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < k) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] != k) return 0;
//        else st = left;
//        right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] <= k) left = mid;
//            else right = mid - 1;
//        }
//        return right - st + 1;
//    }
//};


//class Solution {
//public:
//    //dp[i][j]:投掷i个骰子，i个朝上的面的点数之和为j的事件出现的次数
//    vector<double> statisticsProbability(int num) {
//        vector<double> res(6 * num - num + 1);
//        vector<vector<int> > dp(num + 1, vector<int>(6 * num + 1));
//        for (int i = 1; i <= 6; i++)
//            dp[1][i] = 1;
//        for (int i = 2; i <= num; i++)
//        {
//            for (int j = i; j <= 6 * i; j++)
//            {
//                for (int k = 1; k <= 6; k++)
//                {
//                    if (j - k > 0) dp[i][j] += dp[i - 1][j - k];
//                    else break;
//                }
//            }
//        }
//        double deno = pow(6.0, num);
//        int i = 0;
//        for (int k = num; k <= 6 * num; k++)
//            res[i++] = dp[num][k] / deno;
//        return res;
//    }
//};


//class Solution {
//public:
//    bool checkDynasty(vector<int>& places) {
//        sort(places.begin(), places.end());
//        int useful = 0;
//        for (int i = 0; i < 4; i++)
//        {
//            if (places[i] == 0) useful++;
//            else if (places[i] == places[i + 1]) return false;
//        }
//        if (places[4] - places[useful] < 5) return true;
//        else return false;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param numbers int整型vector
//     * @return bool布尔型
//     */
//    bool IsContinuous(vector<int>& numbers) {
//        sort(numbers.begin(), numbers.end());
//        int n = numbers.size();
//        int useful = 0;
//        for (int i = 0; i < 4; i++)
//        {
//            if (numbers[i] == 0) useful++;
//            else if (numbers[i] == numbers[i + 1]) return false;
//        }
//        if (numbers[4] - numbers[useful] < 5) return true;
//        else return false;
//    }
//};


//class Solution {
//public:
//    int iceBreakingGame(int num, int target) {
//        int last = 0;
//        for (int i = 2; i <= num; i++)
//            last = (last + target) % i;
//        return last;
//    }
//};


//class Solution {
//private:
//    vector<int> circle;
//public:
//    int iceBreakingGame(int num, int target) {
//        for (int i = 0; i < num; i++)
//            circle.push_back(i);
//        int start = 0;
//        while (num > 1)
//        {
//            start = (start + target - 1) % num;
//            circle.erase(circle.begin() + start);
//            num--;
//        }
//        return circle[0];
//    }
//};


//class A;
//A* Array[2];
//
//class A {
//public:
//    virtual int sum(int target)
//    {
//        return 0;
//    }
//};
//
//class B : public A {
//public:
//    virtual int sum(int target)
//    {
//        return Array[!!target]->sum(target - 1) + target;
//    }
//};
//
//class Solution {
//public:
//    int mechanicalAccumulator(int target) {
//        A a;
//        B b;
//        Array[0] = &a;
//        Array[1] = &b;
//        return Array[1]->sum(target);
//    }
//};


//class Temp {
//public:
//    Temp()
//    {
//        N++;
//        sum += N;
//    }
//    static int getSum()
//    {
//        return sum;
//    }
//    static void Reset()
//    {
//        N = 0;
//        sum = 0;
//    }
//private:
//    static int N;
//    static int sum;
//};
//
//int Temp::N = 0;
//int Temp::sum = 0;
//
//class Solution {
//public:
//    int mechanicalAccumulator(int target) {
//        Temp::Reset();
//        Temp* a = new Temp[target];
//        delete[] a;
//        a = nullptr;
//        return Temp::getSum();
//    }
//};


//class Solution {
//public:
//    int encryptionCalculate(int dataA, int dataB) {
//        while (dataB)
//        {
//            int sum = dataA ^ dataB;
//            int carry = (dataA & dataB) << 1;
//            dataA = sum;
//            dataB = carry;
//        }
//        return dataA;
//    }
//};


//class Solution {
//public:
//    int Add(int num1, int num2) {
//        while (num2)
//        {
//            int sum = num1 ^ num2;
//            int carry = (num1 & num2) << 1;
//            num1 = sum;
//            num2 = carry;
//        }
//        return num1;
//    }
//};


//class Solution {
//public:
//    int myAtoi(string s) {
//        int n = s.size();
//        if (n == 0) return 0;
//        int res = 0;
//        int sign = 1;
//        int i = 0;
//        while (s[i] == ' ')
//        {
//            i++;
//            if (i == n) return 0;
//        }
//        if (s[i] == '-')
//        {
//            sign = -1;
//            i++;
//        }
//        else if (s[i] == '+') i++;
//        for (int j = i; j < n; j++)
//        {
//            if (s[j] < '0' || s[j]>'9') break;
//            if (res > INT_MAX / 10 || (res == INT_MAX / 10 && s[j] > '7'))
//            {
//                if (sign == 1) return INT_MAX;
//                else return INT_MIN;
//            }
//            res = res * 10 + (s[j] - '0');
//        }
//        return sign * res;
//    }
//};