#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int son[N][26], cnt[N], idx;
//
//void insert(char str[])
//{
//    int p = 0;
//    for (int i = 0; str[i]; i++)
//    {
//        int s = str[i] - 'a';
//        if (!son[p][s]) son[p][s] = ++idx;
//        p = son[p][s];
//    }
//    cnt[p]++;
//}
//
//int query(char str[])
//{
//    int p = 0;
//    for (int i = 0; str[i]; i++)
//    {
//        int s = str[i] - 'a';
//        if (!son[p][s]) return 0;
//        p = son[p][s];
//    }
//    return cnt[p];
//}
//
//
//int main()
//{
//    int n;
//    cin >> n;
//    char op[2];
//    char str[N];
//    while (n--)
//    {
//        scanf("%s%s", op, str);
//        if (op[0] == 'I') insert(str);
//        else if (op[0] == 'Q') cout << query(str) << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int p[N];
//
//int find(int x)
//{
//    if (p[x] != x) p[x] = find(p[x]);
//    return p[x];
//}
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++) p[i] = i;
//    while (m--)
//    {
//        char op;
//        int a, b;
//        cin >> op >> a >> b;
//        if (op == 'M') p[find(a)] = find(b);
//        else if (op == 'Q')
//        {
//            if (find(a) == find(b)) cout << "Yes" << endl;
//            else cout << "No" << endl;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int p[N], s[N];
//
//int find(int x)
//{
//    if (p[x] != x) p[x] = find(p[x]);
//    return p[x];
//}
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//    {
//        p[i] = i;
//        s[i] = 1;
//    }
//    while (m--)
//    {
//        char op[5];
//        scanf("%s", op);
//        int a, b;
//        if (op[0] == 'C')
//        {
//            cin >> a >> b;
//            if (find(a) == find(b)) continue;
//            s[find(a)] += s[find(b)];
//            p[find(b)] = find(a);
//        }
//        else if (op[0] == 'Q' && op[1] == '1')
//        {
//            cin >> a >> b;
//            if (find(a) == find(b)) cout << "Yes" << endl;
//            else cout << "No" << endl;
//        }
//        else
//        {
//            cin >> a;
//            cout << s[find(a)] << endl;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    unordered_map<int, bool> hash;
//    while (n--)
//    {
//        char op[2];
//        int x;
//        scanf("%s%d", op, &x);
//        if (op[0] == 'I')
//            hash[x] = true;
//        else if (op[0] == 'Q')
//        {
//            if (hash[x]) cout << "Yes" << endl;
//            else cout << "No" << endl;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10, M = 31 * N;
//int a[N];
//int son[M][2], idx;
//
//void insert(int x)
//{
//    int p = 0;
//    for (int i = 30; i >= 0; i--)
//    {
//        int s = x >> i & 1;
//        if (!son[p][s]) son[p][s] = ++idx;
//        p = son[p][s];
//    }
//}
//
//int query(int x)
//{
//    int p = 0, res = 0;
//    for (int i = 30; i >= 0; i--)
//    {
//        int s = x >> i & 1;
//        if (son[p][!s])
//        {
//            p = son[p][!s];
//            res = res * 2 + !s;
//        }
//        else
//        {
//            p = son[p][s];
//            res = res * 2 + s;
//        }
//    }
//    return res;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    int res = 0;
//    for (int i = 0; i < n; i++)
//    {
//        insert(a[i]);
//        int t = query(a[i]);
//        res = max(res, a[i] ^ t);
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int a[N], s[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    int res = 0;
//    int k = 0;
//    for (int i = 0; i < n; i++)
//    {
//        s[a[i]]++;
//        while (k < i && s[a[i]]>1)
//        {
//            s[a[k]]--;
//            k++;
//        }
//        res = max(res, i - k + 1);
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int a[N], b[N];
//
//int main()
//{
//    int n, m, x;
//    cin >> n >> m >> x;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    for (int i = 0; i < m; i++) cin >> b[i];
//    for (int i = 0, j = m - 1; i < n; i++)
//    {
//        while (j >= 0 && a[i] + b[j] > x)
//            j--;
//        if (j >= 0 && a[i] + b[j] == x)
//            cout << i << ' ' << j << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 100010;
//int a[N], b[N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    for (int i = 0; i < m; i++) cin >> b[i];
//    int i = 0, j = 0;
//    while (i < n && j < m)
//    {
//        if (a[i] == b[j])
//        {
//            i++;
//            j++;
//        }
//        else j++;
//    }
//    if (i == n) cout << "Yes" << endl;
//    else cout << "No" << endl;
//    return 0;
//}


//#include <iostream>
//#include <bitset>
//using namespace std;
//
//const int N = 100010;
//int a[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> a[i];
//        bitset<32> res(a[i]);
//        cout << res.count() << ' ';
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        int res = 0;
//        while (x)
//        {
//            if (x & 1)
//                res++;
//            x >>= 1;
//        }
//        cout << res << ' ';
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        int res = 0;
//        for (int i = 30; i >= 0; i--)
//        {
//            if (x >> i & 1)
//                res++;
//        }
//        cout << res << ' ';
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        int res = 0;
//        while (x)
//        {
//            int t = x & -x; //-x�൱��~x+1
//            x -= t;
//            res++;
//        }
//        cout << res << ' ';
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//typedef pair<int, int> PII;
//vector<PII> q;
//
//void merge(vector<PII>& q)
//{
//    vector<PII> res;
//    sort(q.begin(), q.end());
//    int st = -2e9, ed = -2e9;
//    for (auto a : q)
//    {
//        if (ed < a.first)
//        {
//            if (st != -2e9)
//                res.push_back({ st, ed });
//            st = a.first, ed = a.second;
//        }
//        else
//            ed = max(ed, a.second);
//    }
//    if (st != -2e9)
//        res.push_back({ st, ed });
//    q = res;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        int l, r;
//        cin >> l >> r;
//        q.push_back({ l, r });
//    }
//    merge(q);
//    cout << q.size() << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//int main()
//{
//	int a[3];
//	a[0] = 1; a[1] = 2; a[2] = 3;
//	do
//	{
//		cout << a[0] << " " << a[1] << " " << a[2] << endl;
//	}while (next_permutation(a, a + 3));
//	return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//vector<int> mul(vector<int>& A, int b)
//{
//    vector<int> C;
//    int t = 0;
//    for (int i = 0; i < A.size() || t; i++)
//    {
//        if (i < A.size()) t += A[i] * b;
//        C.push_back(t % 10);
//        t /= 10;
//    }
//    while (C.back() == 0 && C.size() > 1) C.pop_back();
//    return C;
//}
//
//int main()
//{
//    string a;
//    int b;
//    cin >> a >> b;
//    vector<int> A;
//    for (int i = a.size() - 1; i >= 0; i--) A.push_back(a[i] - '0');
//    vector<int> C = mul(A, b);
//    for (int i = C.size() - 1; i >= 0; i--) cout << C[i];
//    cout << endl;
//    return 0;
//}


//class Solution {
//private:
//    vector<int> path;
//    vector<vector<int>> res;
//    bool used[11];
//public:
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == nums.size())
//        {
//            res.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (!used[i])
//            {
//                path.push_back(nums[i]);
//                used[i] = true;
//                dfs(nums);
//                used[i] = false;
//                path.pop_back();
//            }
//        }
//        return;
//    }
//    vector<vector<int>> permute(vector<int>& nums) {
//        dfs(nums);
//        return res;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int n = 2020;
//	int res = 0;
//	int g = 0, s = 0, b = 0, q = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		g = i % 10;
//		s = i / 10 % 10;
//		b = i / 100 % 10;
//		q = i / 1000 % 10;
//		if (g == 2) res++;
//		if (s == 2) res++;
//		if (b == 2) res++;
//		if (q == 2) res++;
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int n;
//	cin >> n;
//	int g = 0, s = 0, b = 0, q = 0;
//	int res = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		g = i % 10;
//		s = i / 10 % 10;
//		b = i / 100 % 10;
//		q = i / 1000 % 10;
//		if (g == 2 || g == 0 || g == 1 || g == 9) res += i;
//		else if (i>=10 && (s == 2 || s == 0 || s == 1 || s == 9)) res += i;
//		else if (i>=100 && (b == 2 || b == 0 || b == 1 || b == 9)) res += i;
//		else if (i>=1000 && (q == 2 || q == 0 || q == 1 || q== 9)) res += i;
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//
//bool check(int n)
//{
//	int d = n % 100;
//	int m = n / 100 % 100;
//	int y = n / 10000;
//	if (d <= 0) return false;
//	if (m <= 0 || m>12) return false;
//	if (m != 2 && d > days[m]) return false;
//	if (m == 2)
//	{
//		if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0)
//		{
//			if (d > 29) return false;
//		}
//		else
//			if (d > 28) return false;
//	}
//	return true;
//}
//
//int main()
//{
//	int n;
//	cin >> n;
//	n /= 10000;
//	int res1, res2;
//	bool flag = false;
//	for (int i = n + 1; ; i++)
//	{
//		int k = 4;
//		int date = i, x = i;
//		while (k--)
//		{
//			date = date * 10 + x % 10;
//			x /= 10;
//		}
//		if (n <= date && check(date))
//		{
//			if (!flag)
//			{
//				res1 = date;
//				flag = true;
//				continue;
//			}
//			if (flag)
//			{
//				if (date / 10000 % 100 == date / 1000000 && date / 100 % 100 == date % 100)
//				{
//					if (date / 10 % 100 == date / 10000 % 100 && date % 10 != date / 10000 % 10)
//					{
//						res2 = date;
//						cout << res1 << endl << res2 << endl;
//						return 0;
//					}
//				}
//			}
//		}
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//
//bool is_leapyear(int y)
//{
//	if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0)
//		return true;
//	else
//		return false;
//}
//
//int main()
//{
//	int a, b, c;
//	scanf("%d/%d/%d", &a, &b, &c);
//	for (int y = 1960; y <= 2059; y++)
//	{
//		if (is_leapyear(y))
//			days[2] = 29;
//		else
//			days[2] = 28;
//		for (int m = 1; m <= 12; m++)
//		{
//			for (int d = 1; d <= days[m]; d++)
//			{
//				if ((a == y % 100 && b == m && c == d) ||
//					(a == m && b == d && c == y % 100) ||
//					(a == d && b == m && c == y % 100))
//						printf("%d-%02d-%02d\n", y, m, d);
//			}
//		}
//	}
//	return 0;
//}


//class Solution {
//private:
//    vector<int> path;
//    vector<vector<int>> res;
//public:
//    void dfs(vector<int>& nums, vector<bool> used)
//    {
//        if (path.size() == nums.size())
//        {
//            res.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (i > 0 && nums[i] == nums[i - 1] && used[i - 1] == false)
//                continue;
//            if (!used[i])
//            {
//                path.push_back(nums[i]);
//                used[i] = true;
//                dfs(nums, used);
//                used[i] = false;
//                path.pop_back();
//            }
//        }
//    }
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        vector<bool> used(nums.size(), false);
//        dfs(nums, used);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> path;
//    vector<vector<int>> res;
//public:
//    void dfs(vector<int>& nums, int pos)
//    {
//        res.push_back(path);
//        for (int i = pos; i < nums.size(); i++)
//        {
//            path.push_back(nums[i]);
//            dfs(nums, i + 1);
//            path.pop_back();
//        }
//        return;
//    }
//    vector<vector<int>> subsets(vector<int>& nums) {
//        dfs(nums, 0);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> path;
//    vector<vector<int>> res;
//public:
//    void dfs(vector<int>& nums, int pos)
//    {
//        res.push_back(path);
//        for (int i = pos; i < nums.size(); i++)
//        {
//            if (i > pos && nums[i] == nums[i - 1])
//                continue;
//            path.push_back(nums[i]);
//            dfs(nums, i + 1);
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        dfs(nums, 0);
//        return res;
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int a[N];
//
//int main()
//{
//    int n, q;
//    cin >> n >> q;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    while (q--)
//    {
//        int x;
//        cin >> x;
//        int l = 0, r = n - 1;
//        while (l < r)
//        {
//            int mid = l + r >> 1;
//            if (a[mid] >= x) r = mid;
//            else l = mid + 1;
//        }
//        if (a[l] != x) cout << "-1 -1" << endl;
//        else
//        {
//            cout << l << ' ';
//            l = 0, r = n - 1;
//            while (l < r)
//            {
//                int mid = l + r + 1 >> 1;
//                if (a[mid] <= x) l = mid;
//                else r = mid - 1;
//            }
//            cout << l << endl;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int n, k;
//int h[N], w[N];
//
//bool check(int mid)
//{
//	int res = 0;
//	for (int i = 0; i < n; i++)
//		res += (h[i] / mid) * (w[i] / mid);
//	if (res >= k)
//		return true;
//	else
//		return false;
//}
//
//int main()
//{
//	cin >> n >> k;
//	for (int i = 0; i < n; i++) cin >> h[i] >> w[i];
//	int l = 0, r = N;
//	while (l < r)
//	{
//		int mid = l + r + 1 >> 1;
//		if (check(mid)) l = mid;
//		else r = mid - 1;
//	}
//	cout << l << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	long long a, b, n;
//	cin >> a >> b >> n;
//	int day = 0;
//	while (n > 0)
//	{
//		if (n - 5 * a - 2 * b > 0)
//		{
//			n -= 5 * a;
//			n -= 2 * b;
//			day += 7;
//		}
//		else
//		{
//			int d = day % 7;
//			if (d >= 0 && d < 5)
//			{
//				n -= a;
//				day ++;
//			}
//			else
//			{
//				n -= b;
//				day ++;
//			}
//		}
//	}
//	cout << day << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int res = 0;
//	int k = 5;
//	while (k--)
//	{
//		int year;
//		cin >> year;
//		int q = year / 1000;
//		int b = year / 100 % 10;
//		int s = year / 10 % 10;
//		int g = year % 10;
//		if (q == s && g - b == 1)
//			res++;
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int n;
//	cin >> n;
//	bool flag = true;
//	if (n % 2 == 1) flag = false;
//	int res = 0;
//	for (int i = 1; i < n; i++)
//	{
//		if (flag)
//		{
//			if (i * i % n < n / 2)
//				res++;
//		}
//		else
//		{
//			if (i * i % n < n / 2.0)
//				res++;
//		}
//		
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//typedef long long LL;
//
//int main()
//{
//	LL n;
//	cin >> n;
//	vector<LL> res;
//	res.push_back(n);
//	while (n > 0)
//	{
//		if(n / 2 > 0)
//			res.push_back(n / 2);
//		n /= 2;
//	}
//	for (auto a : res) cout << a << ' ';
//	cout << endl;
//	return 0;
//}


//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//int main()
//{
//	int n;
//	cin >> n;
//	vector<int> score;
//	while (n--)
//	{
//		int x;
//		cin >> x;
//		score.push_back(x);
//	}
//	sort(score.begin(), score.end());
//	int min = score[0], max = score[score.size() - 1];
//	int sum = 0;
//	for (auto a : score)
//		sum += a;
//	double avg = 1.0 * sum / score.size();
//	cout << max << endl << min << endl;
//	printf("%.2lf\n", avg);
//	return 0;
//}


//#include <iostream>
//#include <string>
//using namespace std;
//
//string month[13] = { "", "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec" };
//
//int main()
//{
//	string s;
//	cin >> s;
//	string m = s.substr(0, 3);
//	int k = 0;
//	for (int i = 1; i <= 12; i++)
//	{
//		if (m == month[i])
//		{
//			k = i;
//			break;
//		}
//	}
//	int d = stoi(s.substr(3, 2));
//	cout << k << ' ' << d << endl;
//	return 0;
//}


//#include <iostream>
//#include <unordered_map>
//#include <string>
//using namespace std;
//
//unordered_map<string, int> map
//{
//	{"Jan", 1}, {"Feb", 2}, {"Mar", 3}, {"Apr", 4}, {"May", 5},
//	{"Jun", 6}, {"Jul", 7}, {"Aug", 8}, {"Sep", 9}, {"Oct", 10},
//	{"Nov", 11}, {"Dec", 12}
//};
//
//int main()
//{
//	string s;
//	cin >> s;
//	string m = s.substr(0, 3);
//	int month = map[m];
//	string d = s.substr(3, 2);
//	int day = stoi(d);
//	cout << month << " " << day << endl;
//	return 0;
//}


#include <iostream>
using namespace std;

int main()
{
	string s;
	cin >> s;
	for (int i = 0; i < s.size(); i++)
	{
		if (s[i] < '0' || s[i]>'9')
		{
			if (s[i + 1] < '0' || s[i + 1]>'9')
			{
				cout << s[i];
				continue;
			}
			for (int k = 1; k <= s[i + 1] - '0'; k++)
				cout << s[i];
		}

	}
	cout << endl;
	return 0;
}