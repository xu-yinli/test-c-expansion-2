#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int change(int amount, vector<int>& coins) {
//        int n = coins.size();
//        vector<int> dp(amount + 1);
//        dp[0] = 1;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = coins[i]; j <= amount; j++)
//            {
//                dp[j] += dp[j - coins[i]];
//            }
//        }
//        return dp[amount];
//    }
//};


//class Solution {
//public:
//    int combinationSum4(vector<int>& nums, int target) {
//        int n = nums.size();
//        vector<int> dp(target + 1);
//        dp[0] = 1;
//        for (int i = 1; i <= target; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (i >= nums[j] && dp[i] < INT_MAX - dp[i - nums[j]])
//                    dp[i] += dp[i - nums[j]];
//            }
//        }
//        return dp[target];
//    }
//};


//class Solution {
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<int> dp(amount + 1, INT_MAX);
//        dp[0] = 0;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = coins[i]; j <= amount; j++)
//            {
//                if (dp[j - coins[i]] != INT_MAX)
//                    dp[j] = min(dp[j], dp[j - coins[i]] + 1);
//            }
//        }
//        if (dp[amount] == INT_MAX) return -1;
//        return dp[amount];
//    }
//};


//class Solution {
//public:
//    int numSquares(int n) {
//        vector<int> dp(n + 1, INT_MAX);
//        dp[0] = 0;
//        for (int i = 0; i <= n; i++)
//        {
//            for (int j = 1; j * j <= i; j++)
//            {
//                dp[i] = min(dp[i], dp[i - j * j] + 1);
//            }
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    bool wordBreak(string s, vector<string>& wordDict) {
//        unordered_set<string> wordSet(wordDict.begin(), wordDict.end());
//        int n = s.size();
//        vector<bool> dp(n + 1, false);
//        dp[0] = true;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                string word = s.substr(j, i - j);
//                if (wordSet.find(word) != wordSet.end() && dp[j])
//                    dp[i] = true;
//            }
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int numSquares(int n) {
//        vector<int> dp(n + 1, INT_MAX);
//        dp[0] = 0;
//        for (int i = 1; i * i <= n; i++)
//        {
//            for (int j = i * i; j <= n; j++)
//            {
//                dp[j] = min(dp[j], dp[j - i * i] + 1);
//            }
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<int> dp(amount + 1, INT_MAX);
//        dp[0] = 0;
//        for (int i = 1; i <= amount; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (i - coins[j] >= 0 && dp[i - coins[j]] != INT_MAX)
//                    dp[i] = min(dp[i], dp[i - coins[j]] + 1);
//            }
//        }
//        if (dp[amount] == INT_MAX) return -1;
//        return dp[amount];
//    }
//};


//class Solution {
//public:
//    string optimalDivision(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 0) return "";
//        if (n == 1) return to_string(nums[0]);
//        if (n == 2) return to_string(nums[0]) + "/" + to_string(nums[1]);
//        string res = to_string(nums[0]) + "/(" + to_string(nums[1]);
//        for (int i = 2; i < n; i++)
//        {
//            res += "/" + to_string(nums[i]);
//        }
//        res += ")";
//        return res;
//    }
//};


//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        int left = 0, right = 0, maxPos = 0;
//        int res = 0;
//        while (left <= right)
//        {
//            if (maxPos >= nums.size() - 1)
//                return res;
//            for (int i = left; i <= right; i++)
//            {
//                maxPos = max(maxPos, i + nums[i]);
//            }
//            res++;
//            left = right + 1;
//            right = maxPos;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    bool canJump(vector<int>& nums) {
//        int left = 0, right = 0, maxPos = 0;
//        while (left <= right)
//        {
//            if (maxPos >= nums.size() - 1) return true;
//            for (int i = left; i <= right; i++)
//            {
//                maxPos = max(maxPos, i + nums[i]);
//            }
//            left = right + 1;
//            right = maxPos;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    int monotoneIncreasingDigits(int n) {
//        string s = to_string(n);
//        int m = s.size();
//        int i = 0;
//        while (i + 1 < m && s[i] <= s[i + 1])
//            i++;
//        if (i == m - 1) return n;
//        while (i - 1 >= 0 && s[i - 1] == s[i])
//            i--;
//        s[i]--;
//        i++;
//        while (i < m)
//            s[i++] = '9';
//        return stoi(s);
//    }
//};


//class Solution {
//public:
//    int brokenCalc(int startValue, int target) {
//        int res = 0;
//        // 1.target>startValue(奇数：+1 偶数：/2) -- 先除更优
//        while (target > startValue)
//        {
//            if (target % 2 == 0) target /= 2;
//            else target++;
//            res++;
//        }
//
//        // 2.target<=startValue只能加(奇数：+1 偶数：+1) ==> target-startValue
//        return res + (startValue - target);
//    }
//};


class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        sort(intervals.begin(), intervals.end());
        vector<vector<int>> res;
        int left = intervals[0][0], right = intervals[0][1];
        for (int i = 1; i < intervals.size(); i++)
        {
            int a = intervals[i][0], b = intervals[i][1];
            if (a <= right)
            {
                right = max(right, b);
            }
            else
            {
                res.push_back({ left, right });
                left = a;
                right = b;
            }
        }
        res.push_back({ left, right });
        return res;
    }
};