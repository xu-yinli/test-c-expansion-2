#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    void sortColors(vector<int>& nums) {
//        int n = nums.size();
//        int left = -1, right = n;
//        int i = 0;
//        while (i < right)
//        {
//            if (nums[i] == 0) swap(nums[++left], nums[i++]);
//            else if (nums[i] == 1) i++;
//            else if (nums[i] == 2) swap(nums[--right], nums[i]);
//        }
//    }
//};


//class Solution {
//public:
//    void qsort(vector<int>& nums, int st, int ed)
//    {
//        if (st >= ed) return;
//        int key = nums[st + (rand() % (ed - st + 1))];
//        int left = st - 1, right = ed + 1;
//        int i = st;
//        while (i < right)
//        {
//            if (nums[i] < key) swap(nums[++left], nums[i++]);
//            else if (nums[i] == key) i++;
//            else swap(nums[--right], nums[i]);
//        }
//        qsort(nums, st, left);
//        qsort(nums, right, ed);
//    }
//    vector<int> sortArray(vector<int>& nums) {
//        srand(time(NULL));
//        int n = nums.size();
//        qsort(nums, 0, n - 1);
//        return nums;
//    }
//};


//class Solution {
//public:
//    int qsort(vector<int>& nums, int st, int ed, int k)
//    {
//        if (st >= ed) return nums[st];
//        int key = nums[st + (rand() % (ed - st + 1))];
//        int left = st - 1, right = ed + 1;
//        int i = st;
//        while (i < right)
//        {
//            if (nums[i] < key) swap(nums[++left], nums[i++]);
//            else if (nums[i] == key) i++;
//            else swap(nums[--right], nums[i]);
//        }
//        int c = ed - right + 1;
//        int b = (right - 1) - (left + 1) + 1;
//        if (c >= k) return qsort(nums, right, ed, k);
//        else if (b + c >= k) return key;
//        else return qsort(nums, st, left, k - b - c);
//    }
//    int findKthLargest(vector<int>& nums, int k) {
//        srand(time(NULL));
//        int n = nums.size();
//        return qsort(nums, 0, n - 1, k);
//    }
//};


//class Solution {
//public:
//    void qsort(vector<int>& arr, int st, int ed, int k)
//    {
//        if (st >= ed) return;
//        int key = arr[st + (rand() % (ed - st + 1))];
//        int left = st - 1, right = ed + 1;
//        int i = st;
//        while (i < right)
//        {
//            if (arr[i] < key) swap(arr[++left], arr[i++]);
//            else if (arr[i] == key) i++;
//            else swap(arr[--right], arr[i]);
//        }
//        int a = left - st + 1;
//        int b = (right - 1) - (left + 1) + 1;
//        if (k <= a) qsort(arr, st, left, k);
//        else if (k <= a + b) return;
//        else qsort(arr, right, ed, k - a - b);
//    }
//    vector<int> smallestK(vector<int>& arr, int k) {
//        srand(time(NULL));
//        int n = arr.size();
//        qsort(arr, 0, n - 1, k);
//        return { arr.begin(), arr.begin() + k };
//    }
//};


//class Solution {
//private:
//    vector<int> tmp;
//public:
//    void mergesort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return;
//        int mid = left + (right - left) / 2;
//        mergesort(nums, left, mid);
//        mergesort(nums, mid + 1, right);
//        int cur1 = left, cur2 = mid + 1;
//        int k = 0;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] <= nums[cur2]) tmp[k++] = nums[cur1++];
//            else tmp[k++] = nums[cur2++];
//        }
//        while (cur1 <= mid) tmp[k++] = nums[cur1++];
//        while (cur2 <= right) tmp[k++] = nums[cur2++];
//        for (int i = left; i <= right; i++)
//            nums[i] = tmp[i - left];
//    }
//    vector<int> sortArray(vector<int>& nums) {
//        int n = nums.size();
//        tmp.resize(n);
//        mergesort(nums, 0, n - 1);
//        return nums;
//    }
//};


//class Solution {
//private:
//    vector<int> tmp;
//public:
//    int mergesort(vector<int>& record, int left, int right)
//    {
//        if (left >= right) return 0;
//        int mid = left + (right - left) / 2;
//        int res = 0;
//        res += mergesort(record, left, mid);
//        res += mergesort(record, mid + 1, right);
//        int cur1 = left, cur2 = mid + 1;
//        int k = 0;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (record[cur1] <= record[cur2]) tmp[k++] = record[cur1++];
//            else
//            {
//                res += mid - cur1 + 1;
//                tmp[k++] = record[cur2++];
//            }
//        }
//        while (cur1 <= mid) tmp[k++] = record[cur1++];
//        while (cur2 <= right) tmp[k++] = record[cur2++];
//        for (int i = left; i <= right; i++)
//            record[i] = tmp[i - left];
//        return res;
//    }
//    int reversePairs(vector<int>& record) {
//        int n = record.size();
//        tmp.resize(n);
//        return mergesort(record, 0, n - 1);
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//    vector<int> index;
//    int tmpNum[100010];
//    int tmpIndex[100010];
//public:
//    void mergesort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return;
//        int mid = left + (right - left) / 2;
//        mergesort(nums, left, mid);
//        mergesort(nums, mid + 1, right);
//        int cur1 = left, cur2 = mid + 1;
//        int k = 0;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] <= nums[cur2])
//            {
//                tmpNum[k] = nums[cur2];
//                tmpIndex[k++] = index[cur2++];
//            }
//            else
//            {
//                res[index[cur1]] += right - cur2 + 1;
//                tmpNum[k] = nums[cur1];
//                tmpIndex[k++] = index[cur1++];
//            }
//        }
//        while (cur1 <= mid)
//        {
//            tmpNum[k] = nums[cur1];
//            tmpIndex[k++] = index[cur1++];
//        }
//        while (cur2 <= right)
//        {
//            tmpNum[k] = nums[cur2];
//            tmpIndex[k++] = index[cur2++];
//        }
//        for (int i = left; i <= right; i++)
//        {
//            nums[i] = tmpNum[i - left];
//            index[i] = tmpIndex[i - left];
//        }
//    }
//    vector<int> countSmaller(vector<int>& nums) {
//        int n = nums.size();
//        res.resize(n);
//        index.resize(n);
//        for (int i = 0; i < n; i++)
//            index[i] = i;
//        mergesort(nums, 0, n - 1);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> tmp;
//public:
//    int mergesort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return 0;
//        int mid = left + (right - left) / 2;
//        int res = 0;
//        res += mergesort(nums, left, mid);
//        res += mergesort(nums, mid + 1, right);
//        int cur1 = left, cur2 = mid + 1;
//        int k = 0;
//        while (cur1 <= mid)
//        {
//            while (cur2 <= right && nums[cur1] / 2.0 <= nums[cur2]) cur2++;
//            if (cur2 > right) break;
//            res += right - cur2 + 1;
//            cur1++;
//        }
//        cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] <= nums[cur2]) tmp[k++] = nums[cur2++];
//            else tmp[k++] = nums[cur1++];
//        }
//        while (cur1 <= mid) tmp[k++] = nums[cur1++];
//        while (cur2 <= right) tmp[k++] = nums[cur2++];
//        for (int i = left; i <= right; i++)
//            nums[i] = tmp[i - left];
//        return res;
//    }
//    int reversePairs(vector<int>& nums) {
//        int n = nums.size();
//        tmp.resize(n);
//        return mergesort(nums, 0, n - 1);
//    }
//};


//class Solution {
//private:
//    const int MOD = 1000000007;
//    vector<int> tmp;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @return int整型
//     */
//    int mergesort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return 0;
//        int mid = left + (right - left) / 2;
//        int res = 0;
//        res += mergesort(nums, left, mid);
//        res += mergesort(nums, mid + 1, right);
//        int cur1 = left, cur2 = mid + 1;
//        int k = 0;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] <= nums[cur2]) tmp[k++] = nums[cur2++];
//            else
//            {
//                res += (right - cur2 + 1);
//                res %= MOD;
//                tmp[k++] = nums[cur1++];
//            }
//        }
//        while (cur1 <= mid) tmp[k++] = nums[cur1++];
//        while (cur2 <= right) tmp[k++] = nums[cur2++];
//        for (int i = left; i <= right; i++)
//            nums[i] = tmp[i - left];
//        return res % MOD;
//    }
//    int InversePairs(vector<int>& nums) {
//        int n = nums.size();
//        tmp.resize(n);
//        return mergesort(nums, 0, n - 1);
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param lists ListNode类vector
//     * @return ListNode类
//     */
//    ListNode* mergeTwoList(ListNode* l1, ListNode* l2)
//    {
//        if (l1 == nullptr) return l2;
//        if (l2 == nullptr) return l1;
//        ListNode* cur1 = l1;
//        ListNode* cur2 = l2;
//        ListNode* newHead = new ListNode(0);
//        ListNode* prev = newHead;
//        while (cur1 && cur2)
//        {
//            if (cur1->val <= cur2->val)
//            {
//                prev->next = cur1;
//                prev = cur1;
//                cur1 = cur1->next;
//            }
//            else
//            {
//                prev->next = cur2;
//                prev = cur2;
//                cur2 = cur2->next;
//            }
//        }
//        if (cur1) prev->next = cur1;
//        if (cur2) prev->next = cur2;
//        return newHead->next;
//
//    }
//    ListNode* mergesort(vector<ListNode*>& lists, int left, int right)
//    {
//        if (left > right) return nullptr;
//        if (left == right) return lists[left];
//        int mid = left + (right - left) / 2;
//        ListNode* l1 = mergesort(lists, left, mid);
//        ListNode* l2 = mergesort(lists, mid + 1, right);
//        return mergeTwoList(l1, l2);
//    }
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        int n = lists.size();
//        return mergesort(lists, 0, n - 1);
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    struct cmp
//    {
//        bool operator()(const ListNode* a, const ListNode* b)
//        {
//            return a->val > b->val;
//        }
//    };
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        priority_queue<ListNode*, vector<ListNode*>, cmp> heap;
//        for (auto l : lists)
//            if (l) heap.push(l);
//        ListNode* newHead = new ListNode(0);
//        ListNode* prev = newHead;
//        while (heap.size())
//        {
//            ListNode* tmp = heap.top();
//            heap.pop();
//            prev->next = tmp;
//            prev = prev->next;
//            if (tmp->next) heap.push(tmp->next);
//        }
//        return newHead->next;
//    }
//};


//class Solution {
//private:
//    vector<string> res;
//public:
//    struct cmp
//    {
//        bool operator()(const pair<string, int>& a, const pair<string, int>& b)
//        {
//            if (a.second > b.second) return true;
//            if (a.second == b.second && a.first < b.first) return true;
//            return false;
//        }
//    };
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        map<string, int> Map;
//        for (auto& s : words)
//            Map[s]++;
//        vector<pair<string, int>> tmp(Map.begin(), Map.end());
//        sort(tmp.begin(), tmp.end(), cmp());
//        for (int i = 0; i < k; i++)
//            res.push_back(tmp[i].first);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
//        set<int> s;
//        for (auto x : nums1)
//            s.insert(x);
//        for (auto x : nums2)
//        {
//            if (s.find(x) != s.end())
//            {
//                res.push_back(x);
//                s.erase(x);
//            }
//        }
//        return res;
//    }
//};