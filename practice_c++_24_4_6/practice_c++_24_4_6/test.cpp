#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//bool check(int x)
//{
//	while (x)
//	{
//		int t = x % 10;
//		if (t == 2 || t == 4) return true;
//		x /= 10;
//	}
//	return false;
//}
//
//int main()
//{
//	int n = 2019;
//	int res = 0;
//	for (int i = 1; i < n; i++)
//	{
//		if (check(i)) continue;
//		int left = i + 1, right = n;
//		while (left < right)
//		{
//			if (i + left + right > n || check(right))
//				right--;
//			else if (i + left + right < n || check(left))
//				left++;
//			else
//			{
//				res++;
//				left++;
//				right--;
//			}
//		}
//	}
//	cout << res << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int a[N];
//
//int main()
//{
//	int n;
//	cin >> n;
//	int cnt = 0;
//	for(int i=0; i<n; i++)
//	{
//		int x;
//		while (cin >> x)
//		{
//			a[x]++;
//			cnt++;
//			if (cin.get() == '\n')
//				break;
//		}
//	}
//	int res1 = 0, res2 = 0;
//	for (int i = 1; i < N; i++)
//	{
//		if (a[i] == 0 && a[i - 1] != 0 && a[i + 1] != 0)
//			res1 = i;
//		if (a[i] > 1)
//			res2 = i;
//		if (res1 && res2) break;
//	}
//	cout << res1 << ' ' << res2 << endl;
//	return 0;
//}


//#include <iostream>
//#include <cmath>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int a = 0; a * a <= n; a++)
//    {
//        for (int b = a; a * a + b * b <= n; b++)
//        {
//            for (int c = b; a * a + b * b + c * c <= n; c++)
//            {
//                int t = n - a * a - b * b - c * c;
//                int d = sqrt(t);
//                if (c <= d && d * d == t)
//                {
//                    cout << a << ' ' << b << ' ' << c << ' ' << d << endl;
//                    return 0;
//                }
//            }
//        }
//    }
//	return 0;
//}


//#include <iostream>
//#include <cmath>
//using namespace std;
//
//int Max = -1e9;
//
//int main()
//{
//	int n;
//	cin >> n;
//	int sum = 0, res = 0, k = 0;
//	int depth = 1;
//	for (int i = 1; i <= n; i++)
//	{
//		int x;
//		cin >> x;
//		sum += x;
//		k++;
//		if (i==n || k==pow(2, depth-1))
//		{
//			if (sum > Max)
//			{
//				Max = sum;
//				res = depth;
//			}
//			depth++;
//			sum = 0;
//			k = 0;
//		}
//	}
//	cout << res << endl;
//	return 0;
//}


//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    vector<vector<int>> fourSum(vector<int>& nums, int target) {
//        sort(nums.begin(), nums.end());
//        for (int k = 0; k < nums.size(); k++)
//        {
//            if (nums[k] > target && nums[k] >= 0)
//                break;
//            if (k > 0 && nums[k] == nums[k - 1])
//                continue;
//            for (int i = k + 1; i < nums.size(); i++)
//            {
//                if (nums[k] + nums[i] > target && nums[k] + nums[i] >= 0)
//                    break;
//                if (i > k + 1 && nums[i] == nums[i - 1])
//                    continue;
//                int left = i + 1, right = nums.size() - 1;
//                while (left < right)
//                {
//                    if ((long long)nums[k] + nums[i] + nums[left] + nums[right] > target)
//                        right--;
//                    else if ((long long)nums[k] + nums[i] + nums[left] + nums[right] < target)
//                        left++;
//                    else
//                    {
//                        res.push_back({ nums[k], nums[i], nums[left], nums[right] });
//                        while (left < right && nums[left] == nums[left + 1]) left++;
//                        while (left < right && nums[right] == nums[right - 1]) right--;
//                        left++;
//                        right--;
//                    }
//                }
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    void reverseString(vector<char>& s) {
//        int left = 0, right = s.size() - 1;
//        while (left < right)
//        {
//            swap(s[left], s[right]);
//            left++;
//            right--;
//        }
//    }
//};


//class Solution {
//public:
//    void reverseString(vector<char>& s) {
//        int n = s.size();
//        for (int i = 0, j = n - 1; i < n / 2; i++, j--)
//            swap(s[i], s[j]);
//    }
//};


//class Solution {
//public:
//    string reverseStr(string s, int k) {
//        int n = s.size();
//        for (int i = 0; i < n; i += 2 * k)
//        {
//            if (i + k <= n) reverse(s.begin() + i, s.begin() + i + k);
//            else reverse(s.begin() + i, s.begin() + n);
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    void removeExtraSpaces(string& s)
//    {
//        int slow = 0;
//        for (int fast = 0; fast < s.size(); fast++)
//        {
//            if (s[fast] != ' ')
//            {
//                if (slow != 0)
//                    s[slow++] = ' ';
//                while (fast < s.size() && s[fast] != ' ')
//                    s[slow++] = s[fast++];
//            }
//        }
//        s.resize(slow);
//    }
//
//    string reverseWords(string s) {
//        removeExtraSpaces(s);
//        reverse(s.begin(), s.end());
//        int left = 0;
//        for (int right = 0; right <= s.size(); right++)
//        {
//            if (right == s.size() || s[right] == ' ')
//            {
//                reverse(s.begin() + left, s.begin() + right);
//                left = right + 1;
//            }
//        }
//        return s;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//    stack<TreeNode*> st;
//public:
//    vector<int> preorderTraversal(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        st.push(root);
//        while (st.size())
//        {
//            TreeNode* node = st.top();
//            st.pop();
//            res.push_back(node->val);
//            if (node->right != nullptr) st.push(node->right);
//            if (node->left != nullptr) st.push(node->left);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void preorder(TreeNode* root, vector<int>& res)
//    {
//        if (root == nullptr)
//            return;
//        res.push_back(root->val);
//        preorder(root->left, res);
//        preorder(root->right, res);
//    }
//    vector<int> preorderTraversal(TreeNode* root) {
//        preorder(root, res);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void postor(TreeNode* root, vector<int>& res)
//    {
//        if (root == nullptr)
//            return;
//        postor(root->left, res);
//        postor(root->right, res);
//        res.push_back(root->val);
//    }
//    vector<int> postorderTraversal(TreeNode* root) {
//        postor(root, res);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//    stack<TreeNode*> st;
//public:
//    vector<int> postorderTraversal(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        st.push(root);
//        while (st.size())
//        {
//            TreeNode* node = st.top();
//            st.pop();
//            res.push_back(node->val);
//            if (node->left) st.push(node->left);
//            if (node->right) st.push(node->right);
//        }
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void inorder(TreeNode* root, vector<int>& res)
//    {
//        if (root == nullptr)
//            return;
//        inorder(root->left, res);
//        res.push_back(root->val);
//        inorder(root->right, res);
//    }
//    vector<int> inorderTraversal(TreeNode* root) {
//        inorder(root, res);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//    stack<TreeNode*> st;
//public:
//    vector<int> inorderTraversal(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        TreeNode* cur = root;
//        while (cur != nullptr || st.size())
//        {
//            if (cur != nullptr)
//            {
//                st.push(cur);
//                cur = cur->left;
//            }
//            else
//            {
//                cur = st.top();
//                st.pop();
//                res.push_back(cur->val);
//                cur = cur->right;
//            }
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//    queue<TreeNode*> q;
//public:
//    vector<vector<int>> levelOrder(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> v;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                v.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            res.push_back(v);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//    queue<TreeNode*> q;
//public:
//    vector<vector<int>> levelOrderBottom(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> v;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                v.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            res.push_back(v);
//        }
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//    vector<int> res;
//    queue<TreeNode*> q;
//public:
//    vector<int> rightSideView(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> v;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                v.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            res.push_back(v[v.size() - 1]);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//    vector<int> res;
//    queue<TreeNode*> q;
//public:
//    vector<int> rightSideView(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            for (int i = 0; i < n; i++)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (i == n - 1) res.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//    vector<double> res;
//    queue<TreeNode*> q;
//public:
//    vector<double> averageOfLevels(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            double sum = 0;
//            for (int i = 0; i < n; i++)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                sum += node->val;
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            res.push_back(sum / n);
//        }
//        return res;
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    vector<Node*> children;
//
//    Node() {}
//
//    Node(int _val) {
//        val = _val;
//    }
//
//    Node(int _val, vector<Node*> _children) {
//        val = _val;
//        children = _children;
//    }
//};
//*/
//
//class Solution {
//    vector<vector<int>> res;
//    queue<Node*> q;
//public:
//    vector<vector<int>> levelOrder(Node* root) {
//        if (root == nullptr)
//            return res;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            vector<int> v;
//            while (n--)
//            {
//                Node* node = q.front();
//                q.pop();
//                v.push_back(node->val);
//                for (int i = 0; i < node->children.size(); i++)
//                    if (node->children[i])
//                        q.push(node->children[i]);
//            }
//            res.push_back(v);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//    queue<TreeNode*> q;
//public:
//    vector<int> largestValues(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            int max = q.front()->val;
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (node->val > max) max = node->val;
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            res.push_back(max);
//        }
//        return res;
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    Node* left;
//    Node* right;
//    Node* next;
//
//    Node() : val(0), left(NULL), right(NULL), next(NULL) {}
//
//    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}
//
//    Node(int _val, Node* _left, Node* _right, Node* _next)
//        : val(_val), left(_left), right(_right), next(_next) {}
//};
//*/
//
//class Solution {
//private:
//    queue<Node*> q;
//public:
//    Node* connect(Node* root) {
//        if (root == NULL)
//            return root;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            Node* prev;
//            Node* node;
//            for (int i = 0; i < n; i++)
//            {
//                if (i == 0)
//                {
//                    prev = q.front();
//                    q.pop();
//                    node = prev;
//                }
//                else
//                {
//                    node = q.front();
//                    q.pop();
//                    prev->next = node;
//                    prev = prev->next;
//                }
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            node->next = NULL;
//        }
//        return root;
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    Node* left;
//    Node* right;
//    Node* next;
//
//    Node() : val(0), left(NULL), right(NULL), next(NULL) {}
//
//    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}
//
//    Node(int _val, Node* _left, Node* _right, Node* _next)
//        : val(_val), left(_left), right(_right), next(_next) {}
//};
//*/
//
//class Solution {
//private:
//    queue<Node*> q;
//public:
//    Node* connect(Node* root) {
//        if (root == NULL)
//            return root;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            Node* prev;
//            Node* node;
//            for (int i = 0; i < n; i++)
//            {
//                if (i == 0)
//                {
//                    prev = q.front();
//                    q.pop();
//                    node = prev;
//                }
//                else
//                {
//                    node = q.front();
//                    q.pop();
//                    prev->next = node;
//                    prev = prev->next;
//                }
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            node->next = NULL;
//        }
//        return root;
//    }
//};


//
///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    queue<TreeNode*> q;
//public:
//    int maxDepth(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        q.push(root);
//        int depth = 0;
//        while (q.size())
//        {
//            depth++;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return depth;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    queue<TreeNode*> q;
//public:
//    int minDepth(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        q.push(root);
//        int depth = 0;
//        while (q.size())
//        {
//            depth++;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//                if (node->left == nullptr && node->right == nullptr)
//                    return depth;
//            }
//        }
//        return depth;
//    }
//};


//class Solution {
//public:
//    int hammingWeight(int n) {
//        int res = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            if (n & (1 << i))
//                res++;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int hammingWeight(int n) {
//        int res = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            if ((n >> i) & 1 == 1) res++;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int hammingWeight(int n) {
//        int res = 0;
//        while (n)
//        {
//            res++;
//            n &= (n - 1);
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> countBits(int n) {
//        for (int i = 0; i <= n; i++)
//        {
//            int k = i;
//            int cnt = 0;
//            while (k)
//            {
//                cnt++;
//                k &= (k - 1);
//            }
//            res.push_back(cnt);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int hammingDistance(int x, int y) {
//        int res = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            if (((x >> i) & 1) != ((y >> i) & 1))
//                res++;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int hammingDistance(int x, int y) {
//        int res = 0;
//        int z = x ^ y;
//        for (int i = 0; i < 32; i++)
//        {
//            if ((z >> i) & 1)
//                res++;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int hammingDistance(int x, int y) {
//        int res = 0;
//        int z = x ^ y;
//        while (z)
//        {
//            if (z & 1) res++;
//            z >>= 1;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int hammingDistance(int x, int y) {
//        int res = 0;
//        int z = x ^ y;
//        while (z)
//        {
//            res++;
//            z &= (z - 1);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        int res = 0;
//        for (int a : nums)
//            res ^= a;
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<int> singleNumber(vector<int>& nums) {
//        int res = 0;
//        for (int a : nums)
//            res ^= a;
//        int k = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            if (((res >> i) & 1) == 1)
//            {
//                k = i;
//                break;
//            }
//        }
//        int x = 0, y = 0;
//        for (int a : nums)
//        {
//            if (((a >> k) & 1) == 1) x ^= a;
//            else y ^= a;
//        }
//        return { x, y };
//    }
//};


#include <iostream>
#include <unordered_map>
using namespace std;

int main()
{
    string s;
    cin >> s;
    unordered_map<char, int> heap;
    for (int i = 0; i < s.size(); i++)
        heap[s[i]]++;
    char ch;
    int max = 0;
    for (auto e : heap)
    {
        if (e.second > max)
        {
            ch = e.first;
            max = e.second;
        }
    }
    cout << ch << endl << max << endl;
    return 0;
}