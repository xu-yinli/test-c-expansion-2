#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    bool preOrder(TreeNode* root, int num)
//    {
//        if (root == nullptr) return true;
//        if (root->val == num)
//            return preOrder(root->left, num) && preOrder(root->right, num);
//        else return false;
//    }
//    bool isUnivalTree(TreeNode* root) {
//		  if(root==nullptr) return true;
//        int num = root->val;
//        if (preOrder(root, num)) return true;
//        return false;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    bool isSameTree(TreeNode* p, TreeNode* q) {
//        if (p == nullptr && q == nullptr) return true;
//        if (p == nullptr || q == nullptr) return false;
//        if (p->val == q->val)
//            return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
//        return false;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    bool cmp(TreeNode* left, TreeNode* right)
//    {
//        if (left == nullptr && right == nullptr) return true;
//        if (left == nullptr || right == nullptr) return false;
//        if (left->val == right->val)
//            return cmp(left->left, right->right) && cmp(left->right, right->left);
//        return false;
//    }
//    bool isSymmetric(TreeNode* root) {
//        if (root == nullptr) return true;
//        if (cmp(root->left, root->right)) return true;
//        return false;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void preOrder(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        res.push_back(root->val);
//        preOrder(root->left);
//        preOrder(root->right);
//    }
//    vector<int> preorderTraversal(TreeNode* root) {
//        if (root == nullptr) return res;
//        preOrder(root);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void inOrder(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        inOrder(root->left);
//        res.push_back(root->val);
//        inOrder(root->right);
//    }
//    vector<int> inorderTraversal(TreeNode* root) {
//        if (root == nullptr) return res;
//        inOrder(root);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void postOrder(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        postOrder(root->left);
//        postOrder(root->right);
//        res.push_back(root->val);
//    }
//    vector<int> postorderTraversal(TreeNode* root) {
//        if (root == nullptr) return res;
//        postOrder(root);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    bool isSame(TreeNode* root, TreeNode* subRoot)
//    {
//        if (root == nullptr && subRoot == nullptr) return true;
//        if (root == nullptr || subRoot == nullptr) return false;
//        if (root->val == subRoot->val)
//            return isSame(root->left, subRoot->left) && isSame(root->right, subRoot->right);
//        return false;
//    }
//    bool isSubtree(TreeNode* root, TreeNode* subRoot) {
//        if (root == nullptr) return false;
//        if (isSame(root, subRoot)) return true;
//        return isSubtree(root->left, subRoot) || isSubtree(root->right, subRoot);
//    }
//};


//#include <iostream>
//using namespace std;
//
//struct TreeNode
//{
//    char val;
//    TreeNode* left;
//    TreeNode* right;
//    TreeNode(char x) : val(x), left(nullptr), right(nullptr) {}
//};
//
//TreeNode* buyNode(char x)
//{
//    TreeNode* newNode = new TreeNode(x);
//    return newNode;
//}
//
//TreeNode* createTree(char* s, int* pi)
//{
//    if (s[(*pi)] == '#')
//    {
//        (*pi)++;
//        return nullptr;
//    }
//    TreeNode* root = buyNode(s[(*pi)++]);
//    root->left = createTree(s, pi);
//    root->right = createTree(s, pi);
//    return root;
//}
//
//void inOrder(TreeNode* root)
//{
//    if (root == nullptr) return;
//    inOrder(root->left);
//    cout << root->val << ' ';
//    inOrder(root->right);
//}
//
//int main()
//{
//    char s[101];
//    cin >> s;
//    int i = 0;
//    TreeNode* root = createTree(s, &i);
//    inOrder(root);
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//struct TreeNode
//{
//    char val;
//    TreeNode* left;
//    TreeNode* right;
//    TreeNode(char x) : val(x), left(nullptr), right(nullptr) {}
//};
//
//TreeNode* createTree(char* s, int& pi)
//{
//    if (s[pi] == '#')
//    {
//        pi++;
//        return nullptr;
//    }
//    TreeNode* root = new TreeNode(s[pi++]);
//    root->left = createTree(s, pi);
//    root->right = createTree(s, pi);
//    return root;
//}
//
//void inOrder(TreeNode* root)
//{
//    if (root == nullptr) return;
//    inOrder(root->left);
//    cout << root->val << ' ';
//    inOrder(root->right);
//}
//
//int main()
//{
//    char s[101];
//    cin >> s;
//    int i = 0;
//    TreeNode* root = createTree(s, i);
//    inOrder(root);
//    return 0;
//}


//class MyStack {
//private:
//    queue<int> q;
//public:
//    MyStack() {
//
//    }
//
//    void push(int x) {
//        q.push(x);
//    }
//
//    int pop() {
//        int n = q.size() - 1;
//        while (n--)
//        {
//            q.push(q.front());
//            q.pop();
//        }
//        int res = q.front();
//        q.pop();
//        return res;
//    }
//
//    int top() {
//        return q.back();
//    }
//
//    bool empty() {
//        return q.empty();
//    }
//};
//
///**
// * Your MyStack object will be instantiated and called as such:
// * MyStack* obj = new MyStack();
// * obj->push(x);
// * int param_2 = obj->pop();
// * int param_3 = obj->top();
// * bool param_4 = obj->empty();
// */


//class Solution {
//public:
//    string removeDuplicates(string s) {
//        string res;
//        for (auto ch : s)
//        {
//            if (res.empty() || res.back() != ch) res.push_back(ch);
//            else res.pop_back();
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    string removeDuplicates(string s) {
//        string res;
//        for (auto ch : s)
//        {
//            if (res.empty() || res.back() != ch) res += ch;
//            else res.pop_back();
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    stack<int> st;
//public:
//    int evalRPN(vector<string>& tokens) {
//        for (int i = 0; i < tokens.size(); i++)
//        {
//            if (tokens[i] == "+" || tokens[i] == "-" || tokens[i] == "*" || tokens[i] == "/")
//            {
//                int num2 = st.top(); st.pop();
//                int num1 = st.top(); st.pop();
//                if (tokens[i] == "+") st.push(num1 + num2);
//                if (tokens[i] == "-") st.push(num1 - num2);
//                if (tokens[i] == "*") st.push(num1 * num2);
//                if (tokens[i] == "/") st.push(num1 / num2);
//            }
//            else st.push(stoi(tokens[i]));
//        }
//        return st.top();
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    class myDeque
//    {
//    public:
//        deque<int> q;
//        void pop(int x)
//        {
//            if (!q.empty() && q.front() == x)
//                q.pop_front();
//        }
//        void push(int x)
//        {
//            while (!q.empty() && x > q.back())
//                q.pop_back();
//            q.push_back(x);
//        }
//        int maxValue()
//        {
//            return q.front();
//        }
//    };
//    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
//        myDeque q;
//        for (int i = 0; i < k; i++)
//            q.push(nums[i]);
//        res.push_back(q.maxValue());
//        for (int i = k; i < nums.size(); i++)
//        {
//            q.pop(nums[i - k]);
//            q.push(nums[i]);
//            res.push_back(q.maxValue());
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> hash;
//public:
//    struct cmp
//    {
//        bool operator()(const pair<int, int>& v, const pair<int, int>& t)
//        {
//            return v.second > t.second;
//        }
//    };
//    vector<int> topKFrequent(vector<int>& nums, int k) {
//        for (int x : nums)
//            hash[x]++;
//        priority_queue<pair<int, int>, vector<pair<int, int>>, cmp> q;
//        for (auto it : hash)
//        {
//            q.push(it);
//            if (q.size() > k)
//                q.pop();
//        }
//        vector<int> res(k);
//        for (int i = k - 1; i >= 0; i--)
//        {
//            res[i] = q.top().first;
//            q.pop();
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    bool backspaceCompare(string s, string t) {
//        string s1, s2;
//        for (auto ch : s)
//        {
//            if (ch != '#') s1 += ch;
//            else if (!s1.empty()) s1.pop_back();
//        }
//        for (auto ch : t)
//        {
//            if (ch != '#') s2 += ch;
//            else if (!s2.empty()) s2.pop_back();
//        }
//        if (s1 == s2) return true;
//        return false;
//    }
//};


//class Solution {
//private:
//    stack<int> st;
//public:
//    int calculate(string s) {
//        char op = '+';
//        int n = s.size();
//        int i = 0;
//        while (i < n)
//        {
//            if (s[i] == ' ')
//            {
//                i++;
//                continue;
//            }
//            else if (s[i] >= '0' && s[i] <= '9')
//            {
//                int num = 0;
//                while (i < n && s[i] >= '0' && s[i] <= '9')
//                {
//                    num = num * 10 + (s[i] - '0');
//                    i++;
//                }
//                if (op == '+') st.push(num);
//                else if (op == '-') st.push(-num);
//                else if (op == '*') st.top() *= num;
//                else if (op == '/') st.top() /= num;
//            }
//            else
//            {
//                op = s[i];
//                i++;
//            }
//        }
//        int sum = 0;
//        while (st.size())
//        {
//            sum += (st.top());
//            st.pop();
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    stack<int> nums;
//    stack<string> str;
//public:
//    string decodeString(string s) {
//        str.push("");
//        int n = s.size();
//        int i = 0;
//        while (i < n)
//        {
//            if (s[i] >= '0' && s[i] <= '9')
//            {
//                int num = 0;
//                while (i < n && s[i] >= '0' && s[i] <= '9')
//                {
//                    num = num * 10 + (s[i] - '0');
//                    i++;
//                }
//                nums.push(num);
//            }
//            else if (s[i] == '[')
//            {
//                i++;
//                string tmp;
//                while (i < n && s[i] >= 'a' && s[i] <= 'z')
//                {
//                    tmp += s[i];
//                    i++;
//                }
//                str.push(tmp);
//            }
//            else if (s[i] == ']')
//            {
//                i++;
//                int count = nums.top(); nums.pop();
//                string tmp = str.top(); str.pop();
//                while (count--)
//                    str.top() += tmp;
//            }
//            else
//            {
//                string tmp;
//                while (i < n && s[i] >= 'a' && s[i] <= 'z')
//                {
//                    tmp += s[i];
//                    i++;
//                }
//                str.top() += tmp;
//            }
//        }
//        return str.top();
//    }
//};


//class Solution {
//private:
//    stack<int> st;
//public:
//    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
//        if (pushed.size() != popped.size()) return false;
//        int i = 0;
//        for (int x : pushed)
//        {
//            st.push(x);
//            while (!st.empty() && st.top() == popped[i])
//            {
//                st.pop();
//                i++;
//            }
//        }
//        if (st.empty() && i == popped.size()) return true;
//        return false;
//    }
//};