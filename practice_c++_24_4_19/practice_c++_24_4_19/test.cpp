#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//private:
//    int n, m;
//    bool vis[1010][1010];
//    int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param grid int整型vector<vector<>>
//     * @return int整型
//     */
//    int rotApple(vector<vector<int> >& grid) {
//        n = grid.size(), m = grid[0].size();
//        queue<pair<int, int>> q;
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (grid[i][j] == 2)
//                    q.push({ i, j });
//        int t = 0;
//        while (q.size())
//        {
//            t++;
//            int size = q.size();
//            while (size--)
//            {
//                auto [a, b] = q.front();
//                q.pop();
//                for (int k = 0; k < 4; k++)
//                {
//                    int x = a + dx[k], y = b + dy[k];
//                    if (x >= 0 && x < n && y >= 0 && y < m && !vis[x][y] && grid[x][y] == 1)
//                    {
//                        vis[x][y] = true;
//                        q.push({ x, y });
//                    }
//                }
//            }
//        }
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (!vis[i][j] && grid[i][j] == 1)
//                    return -1;
//        return t - 1;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @param m int整型
//     * @return int整型
//     */
//    int LastRemaining_Solution(int n, int m) {
//        vector<int> dp(n + 1);
//        dp[0] = 1;
//        for (int i = 1; i <= n; i++) dp[i] = (dp[i - 1] + m) % i;
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @param m int整型
//     * @return int整型
//     */
//    int LastRemaining_Solution(int n, int m) {
//        int f = 0;
//        for (int i = 2; i <= n; i++) f = (f + m) % i;
//        return f;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @param m int整型
//     * @return int整型
//     */
//    int f(int n, int m) {
//        if (n == 1)
//            return 0;
//        int x = f(n - 1, m);
//        return (m + x) % n;
//    }
//    int LastRemaining_Solution(int n, int m) {
//        return f(n, m);
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @param m int整型
//     * @return int整型
//     */
//    int LastRemaining_Solution(int n, int m) {
//        int x = 0;
//        for (int i = 2; i <= n; i++)
//            x = (m + x) % i;
//        return x;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @param m int整型
//     * @return int整型
//     */
//    int LastRemaining_Solution(int n, int m)
//    {
//        vector<int> circle;
//        for (int i = 0; i < n; i++)
//            circle.push_back(i);
//
//        int start = 0;
//        while (n > 1)
//        {
//            start = (start + m - 1) % n;
//            circle.erase(circle.begin() + start);
//            n--;
//        }
//        return circle[0];
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> transpose(vector<vector<int>>& matrix) {
//        int n = matrix.size(), m = matrix[0].size();
//        vector<vector<int>> res(m, vector<int>(n));
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                res[j][i] = matrix[i][j];
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int vowelStrings(vector<string>& words, int left, int right) {
//        int res = 0;
//        for (int i = left; i <= right; i++)
//        {
//            string str = words[i];
//            char a = str[0], b = str[str.size() - 1];
//            if ((a == 'a' || a == 'e' || a == 'i' || a == 'o' || a == 'u') &&
//                (b == 'a' || b == 'e' || b == 'i' || b == 'o' || b == 'u'))
//                res++;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    bool isUgly(int n) {
//        if (n <= 0) return false;
//        vector<int> a = { 2,3,5 };
//        for (int x : a)
//        {
//            while (n % x == 0)
//            {
//                n /= x;
//            }
//        }
//        if (n == 1) return true;
//        else return false;
//    }
//};


//class Solution {
//public:
//    bool isPowerOfThree(int n) {
//        while (n != 0 && n % 3 == 0)
//        {
//            n /= 3;
//        }
//        if (n == 1) return true;
//        else return false;
//    }
//};