#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size();
//        int begin = 0, len = 0;
//        for (int i = 0; i < n; i++)
//        {
//            //奇数长度的扩展
//            int left = i, right = i;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                left--;
//                right++;
//            }
//            if (right - left - 1 > len)
//            {
//                len = right - left - 1;
//                begin = left + 1;
//            }
//
//            //偶数长度的扩展
//            left = i, right = i + 1;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                left--;
//                right++;
//            }
//            if (right - left - 1 > len)
//            {
//                len = right - left - 1;
//                begin = left + 1;
//            }
//        }
//        return s.substr(begin, len);
//    }
//};


//class Solution {
//public:
//    string addBinary(string a, string b) {
//        string res;
//        int cur1 = a.size() - 1, cur2 = b.size() - 1;
//        int t = 0;
//        while (cur1 >= 0 || cur2 >= 0 || t)
//        {
//            if (cur1 >= 0) t += a[cur1--] - '0';
//            if (cur2 >= 0) t += b[cur2--] - '0';
//            res += t % 2 + '0';
//            t /= 2;
//        }
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


//class Solution {
//public:
//    string multiply(string num1, string num2) {
//        int n = num1.size(), m = num2.size();
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        vector<int> str(n + m - 1);
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                str[i + j] += (num1[i] - '0') * (num2[j] - '0');
//        int cur = 0, t = 0;
//        string res;
//        while (cur < n + m - 1 || t)
//        {
//            if (cur < n + m - 1) t += str[cur++];
//            res += t % 10 + '0';
//            t /= 10;
//        }
//        while (res.size() > 1 && res.back() == '0') res.pop_back();
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


//class Solution {
//public:
//    void sortColors(vector<int>& nums) {
//        int n = nums.size();
//        int left = -1, right = n;
//        int i = 0;
//        while (i < right)
//        {
//            if (nums[i] == 0)
//            {
//                swap(nums[left + 1], nums[i]);
//                left++;
//                i++;
//            }
//            else if (nums[i] == 1) i++;
//            else
//            {
//                swap(nums[right - 1], nums[i]);
//                right--;
//            }
//        }
//    }
//};


//class Solution {
//    //[0, left]：全都是0
//    //[left+1, i-1]：全都是1
//    //[i, right-1]：待扫描的元素
//    //[right, n-1]：全都是2
//public:
//    void sortColors(vector<int>& nums) {
//        int n = nums.size();
//        int left = -1, right = n;
//        int i = 0;
//        while (i < right)
//        {
//            if (nums[i] == 0) swap(nums[++left], nums[i++]);
//            else if (nums[i] == 1) i++;
//            else swap(nums[--right], nums[i]);
//        }
//    }
//};


//class Solution {
//public:
//    int getRandom(vector<int>& nums, int left, int right)
//    {
//        int r = rand();
//        return nums[r % (right - left + 1) + left];
//    }
//    void qsort(vector<int>& nums, int l, int r)
//    {
//        if (l >= r) return;
//        int key = getRandom(nums, l, r);
//        int i = l, left = l - 1, right = r + 1;
//        while (i < right)
//        {
//            if (nums[i] < key) swap(nums[++left], nums[i++]);
//            else if (nums[i] == key) i++;
//            else swap(nums[--right], nums[i]);
//        }
//
//        //[l, left] [left+1, right-1] [right, r]
//        qsort(nums, l, left);
//        qsort(nums, right, r);
//    }
//    vector<int> sortArray(vector<int>& nums) {
//        srand(time(NULL));
//        int n = nums.size();
//        qsort(nums, 0, n - 1);
//        return nums;
//    }
//};


//class Solution {
//public:
//    int getRandom(vector<int>& nums, int left, int right)
//    {
//        return nums[rand() % (right - left + 1) + left];
//    }
//    int qsort(vector<int>& nums, int l, int r, int k)
//    {
//        if (l == r) return nums[l];
//        int key = getRandom(nums, l, r);
//        int i = l, left = l - 1, right = r + 1;
//        while (i < right)
//        {
//            if (nums[i] < key) swap(nums[++left], nums[i++]);
//            else if (nums[i] == key) i++;
//            else swap(nums[--right], nums[i]);
//        }
//        //     a           b            c
//        //[l, left] [left+1, right-1] [right, r]
//        int c = r - right + 1;
//        int b = (right - 1) - (left + 1) + 1;
//        if (c >= k) return qsort(nums, right, r, k);
//        else if (b + c >= k) return key;
//        else return qsort(nums, l, left, k - b - c);
//    }
//    int findKthLargest(vector<int>& nums, int k) {
//        srand(time(NULL));
//        int n = nums.size();
//        return qsort(nums, 0, n - 1, k);
//    }
//};


//class Solution {
//public:
//    vector<int> inventoryManagement(vector<int>& stock, int cnt) {
//        sort(stock.begin(), stock.end());
//        vector<int> res(cnt);
//        for (int i = 0; i < cnt; i++)
//            res[i] = stock[i];
//        return res;
//    }
//};


//class Solution {
//public:
//    int getRandom(vector<int>& stock, int left, int right)
//    {
//        return stock[rand() % (right - left + 1) + left];
//    }
//    void qsort(vector<int>& stock, int l, int r, int cnt)
//    {
//        if (l >= r) return;
//        int key = getRandom(stock, l, r);
//        int i = l, left = l - 1, right = r + 1;
//        while (i < right)
//        {
//            if (stock[i] < key) swap(stock[++left], stock[i++]);
//            else if (stock[i] == key) i++;
//            else swap(stock[--right], stock[i]);
//        }
//        //    a             b             c
//        //[l, left] [left+1, right-1] [right, r]
//        int a = left - l + 1;
//        int b = (right - 1) - (left + 1) + 1;
//        if (a >= cnt) qsort(stock, l, left, cnt);
//        else if (a + b >= cnt) return;
//        else qsort(stock, right, r, cnt - a - b);
//    }
//    vector<int> inventoryManagement(vector<int>& stock, int cnt) {
//        srand(time(NULL));
//        int n = stock.size();
//        qsort(stock, 0, n - 1, cnt);
//        return { stock.begin(), stock.begin() + cnt };
//    }
//};


//class Solution {
//public:
//    void mergeSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return;
//
//        int mid = left + (right - left) / 2;
//
//        mergeSort(nums, left, mid);
//        mergeSort(nums, mid + 1, right);
//
//        vector<int> tmp(right - left + 1);
//        int i = 0, cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//            tmp[i++] = nums[cur1] <= nums[cur2] ? nums[cur1++] : nums[cur2++];
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//
//        for (int i = left; i <= right; i++)
//            nums[i] = tmp[i - left];
//    }
//    vector<int> sortArray(vector<int>& nums) {
//        int n = nums.size();
//        mergeSort(nums, 0, n - 1);
//        return nums;
//    }
//};


//class Solution {
//private:
//    vector<int> tmp;
//public:
//    void mergeSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return;
//
//        int mid = left + (right - left) / 2;
//
//        mergeSort(nums, left, mid);
//        mergeSort(nums, mid + 1, right);
//
//        int i = 0, cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//            tmp[i++] = nums[cur1] <= nums[cur2] ? nums[cur1++] : nums[cur2++];
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//
//        for (int i = left; i <= right; i++)
//            nums[i] = tmp[i - left];
//    }
//    vector<int> sortArray(vector<int>& nums) {
//        int n = nums.size();
//        tmp.resize(n);
//        mergeSort(nums, 0, n - 1);
//        return nums;
//    }
//};


//class Solution {
//private:
//    vector<int> tmp;
//public:
//    int mergeSort(vector<int>& record, int left, int right)
//    {
//        if (left >= right) return 0;
//
//        int mid = left + (right - left) / 2;
//        //[left, mid] [mid+1, right]
//
//        int res = 0;
//        res += mergeSort(record, left, mid);
//        res += mergeSort(record, mid + 1, right);
//
//        int i = 0, cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (record[cur1] <= record[cur2])
//                tmp[i++] = record[cur1++];
//            else
//            {
//                res += mid - cur1 + 1;
//                tmp[i++] = record[cur2++];
//            }
//        }
//        while (cur1 <= mid) tmp[i++] = record[cur1++];
//        while (cur2 <= right) tmp[i++] = record[cur2++];
//        for (int i = left; i <= right; i++)
//            record[i] = tmp[i - left];
//        return res;
//    }
//    int reversePairs(vector<int>& record) {
//        int n = record.size();
//        tmp.resize(n);
//        return mergeSort(record, 0, n - 1);
//    }
//};


//class Solution {
//private:
//    vector<int> tmp;
//public:
//    int mergeSort(vector<int>& record, int left, int right)
//    {
//        if (left >= right) return 0;
//
//        int mid = left + (right - left) / 2;
//        //[left, mid] [mid+1, right]
//
//        int res = 0;
//        res += mergeSort(record, left, mid);
//        res += mergeSort(record, mid + 1, right);
//
//        int i = 0, cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (record[cur1] <= record[cur2])
//                tmp[i++] = record[cur2++];
//            else
//            {
//                res += right - cur2 + 1;
//                tmp[i++] = record[cur1++];
//            }
//        }
//        while (cur1 <= mid) tmp[i++] = record[cur1++];
//        while (cur2 <= right) tmp[i++] = record[cur2++];
//        for (int i = left; i <= right; i++)
//            record[i] = tmp[i - left];
//        return res;
//    }
//    int reversePairs(vector<int>& record) {
//        int n = record.size();
//        tmp.resize(n);
//        return mergeSort(record, 0, n - 1);
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//    vector<int> index;
//    int tmpNums[100010];
//    int tmpIndex[100010];
//public:
//    void mergeSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return;
//
//        int mid = left + (right - left) / 2;
//        //[left, mid] [mid+1, right]
//
//        mergeSort(nums, left, mid);
//        mergeSort(nums, mid + 1, right);
//
//        int i = 0, cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] <= nums[cur2])
//            {
//                tmpNums[i] = nums[cur2];
//                tmpIndex[i++] = index[cur2++];
//            }
//            else
//            {
//                res[index[cur1]] += right - cur2 + 1;
//                tmpNums[i] = nums[cur1];
//                tmpIndex[i++] = index[cur1++];
//            }
//        }
//        while (cur1 <= mid)
//        {
//            tmpNums[i] = nums[cur1];
//            tmpIndex[i++] = index[cur1++];
//        }
//        while (cur2 <= right)
//        {
//            tmpNums[i] = nums[cur2];
//            tmpIndex[i++] = index[cur2++];
//        }
//        for (int i = left; i <= right; i++)
//        {
//            nums[i] = tmpNums[i - left];
//            index[i] = tmpIndex[i - left];
//        }
//    }
//    vector<int> countSmaller(vector<int>& nums) {
//        int n = nums.size();
//        res.resize(n);
//        index.resize(n);
//        for (int i = 0; i < n; i++)
//            index[i] = i;
//        mergeSort(nums, 0, n - 1);
//        return res;
//    }
//};


//class Solution {
//private:
//    int tmp[50010];
//public:
//    int mergeSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return 0;
//
//        int mid = left + (right - left) / 2;
//
//        int res = 0;
//        res += mergeSort(nums, left, mid);
//        res += mergeSort(nums, mid + 1, right);
//
//        int i = 0, cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid)
//        {
//            while (cur2 <= right && nums[cur2] >= nums[cur1] / 2.0) cur2++;
//            if (cur2 > right)
//                break;
//            res += right - cur2 + 1;
//            cur1++;
//        }
//        cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//            tmp[i++] = nums[cur1] <= nums[cur2] ? nums[cur2++] : nums[cur1++];
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//        for (int i = left; i <= right; i++)
//            nums[i] = tmp[i - left];
//        return res;
//    }
//    int reversePairs(vector<int>& nums) {
//        int n = nums.size();
//        return mergeSort(nums, 0, n - 1);
//    }
//};


//class Solution {
//private:
//    int tmp[50010];
//public:
//    int mergeSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right) return 0;
//
//        int mid = left + (right - left) / 2;
//
//        int res = 0;
//        res += mergeSort(nums, left, mid);
//        res += mergeSort(nums, mid + 1, right);
//
//        int i = 0, cur1 = left, cur2 = mid + 1;
//        while (cur2 <= right)
//        {
//            while (cur1 <= mid && nums[cur2] >= nums[cur1] / 2.0) cur1++;
//            if (cur1 > mid)
//                break;
//            res += mid - cur1 + 1;
//            cur2++;
//        }
//        cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//            tmp[i++] = nums[cur1] <= nums[cur2] ? nums[cur1++] : nums[cur2++];
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//        for (int i = left; i <= right; i++)
//            nums[i] = tmp[i - left];
//        return res;
//    }
//    int reversePairs(vector<int>& nums) {
//        int n = nums.size();
//        return mergeSort(nums, 0, n - 1);
//    }
//};


//class Solution {
//public:
//    void dfs(vector<int>& A, vector<int>& B, vector<int>& C, int n)
//    {
//        if (n == 1)
//        {
//            C.push_back(A.back());
//            A.pop_back();
//            return;
//        }
//        dfs(A, C, B, n - 1);
//        C.push_back(A.back());
//        A.pop_back();
//        dfs(B, A, C, n - 1);
//    }
//    void hanota(vector<int>& A, vector<int>& B, vector<int>& C) {
//        dfs(A, B, C, A.size());
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        if (list1->val <= list2->val)
//        {
//            list1->next = mergeTwoLists(list1->next, list2);
//            return list1;
//        }
//        else
//        {
//            list2->next = mergeTwoLists(list1, list2->next);
//            return list2;
//        }
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newHead = reverseList(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return newHead;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        auto tmp = swapPairs(head->next->next);
//        auto res = head->next;
//        head->next->next = head;
//        head->next = tmp;
//        return res;
//    }
//};


//class Solution {
//public:
//    double pow(double x, long long n)
//    {
//        if (n == 0) return 1.0;
//        double tmp = pow(x, n / 2);
//        return n % 2 == 0 ? tmp * tmp : tmp * tmp * x;
//    }
//    double myPow(double x, int n) {
//        return n < 0 ? 1.0 / pow(x, -(long long)n) : pow(x, n);
//    }
//};