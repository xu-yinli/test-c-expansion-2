#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//private:
//    unordered_map<int, int> hash; //arr[i] - dp[i]
//public:
//    int longestSubsequence(vector<int>& arr, int difference) {
//        hash[arr[0]] = 1;
//        int res = 1;
//        int n = arr.size();
//        for (int i = 1; i < n; i++)
//        {
//            hash[arr[i]] = hash[arr[i] - difference] + 1;
//            res = max(res, hash[arr[i]]);
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> hash;
//public:
//    int lenLongestFibSubseq(vector<int>& arr) {
//        int res = 2;
//        int n = arr.size();
//        for (int i = 0; i < n; i++) hash[arr[i]] = i;
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        for (int j = 2; j < n; j++) //固定最后一个位置
//        {
//            for (int i = 1; i < j; i++) //固定倒数第二个位置
//            {
//                int a = arr[j] - arr[i];
//                if (a < arr[i] && hash.count(a)) dp[i][j] = dp[hash[a]][i] + 1;
//                res = max(res, dp[i][j]);
//            }
//        }
//        if (res < 3) return 0;
//        return res;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> hash; //nums[i] = i
//public:
//    int longestArithSeqLength(vector<int>& nums) {
//        hash[nums[0]] = 0;
//        int n = nums.size();
//        vector<vector<int>> dp(n, vector<int>(n, 2));
//        int res = 2;
//        for (int i = 1; i < n; i++) //固定倒数第二个数
//        {
//            for (int j = i + 1; j < n; j++) //枚举倒数第一个数
//            {
//                int a = 2 * nums[i] - nums[j];
//                if (hash.count(a))
//                    dp[i][j] = dp[hash[a]][i] + 1;
//                res = max(res, dp[i][j]);
//            }
//            hash[nums[i]] = i;
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    typedef long long LL;
//    unordered_map<LL, vector<int>> hash;
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        for (int i = 0; i < n; i++) hash[nums[i]].push_back(i);
//        vector<vector<int>>dp(n, vector<int>(n));
//        int sum = 0;
//        for (int j = 2; j < n; j++) //固定倒数第一个数
//        {
//            for (int i = 1; i < j; i++) //枚举倒数第二个数
//            {
//                LL a = (LL)2 * nums[i] - nums[j];
//                if (hash.count(a))
//                {
//                    for (auto k : hash[a])
//                    {
//                        if (k < i) dp[i][j] += dp[k][i] + 1;
//                    }
//                }
//                sum += dp[i][j];
//            }
//        }
//        return sum;
//    }
//};