#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* searchBST(TreeNode* root, int val) {
//        if (root == nullptr || root->val == val) return root;
//        if (val < root->val) return searchBST(root->left, val);
//        if (val > root->val) return searchBST(root->right, val);
//        return nullptr;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* searchBST(TreeNode* root, int val) {
//        while (root)
//        {
//            if (val < root->val) root = root->left;
//            else if (val > root->val) root = root->right;
//            else return root;
//        }
//        return nullptr;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> vec;
//public:
//    void traversal(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        traversal(root->left);
//        vec.push_back(root->val);
//        traversal(root->right);
//    }
//    bool isValidBST(TreeNode* root) {
//        traversal(root);
//        for (int i = 0; i < vec.size() - 1; i++)
//        {
//            if (vec[i] >= vec[i + 1])
//                return false;
//        }
//        return true;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    long long maxVal = LONG_MIN;
//public:
//    bool isValidBST(TreeNode* root) {
//        if (root == nullptr) return true;
//        bool left = isValidBST(root->left);
//        if (root->val > maxVal) maxVal = root->val;
//        else return false;
//        bool right = isValidBST(root->right);
//        return left && right;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    TreeNode* prev = nullptr;
//public:
//    bool isValidBST(TreeNode* root) {
//        if (root == nullptr) return true;
//        bool left = isValidBST(root->left);
//        if (prev && prev->val >= root->val)
//            return false;
//        prev = root;
//        bool right = isValidBST(root->right);
//        return left && right;
//    }
//};


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
private:
    int res = INT_MAX;
    TreeNode* prev = nullptr;
public:
    void traversal(TreeNode* cur)
    {
        if (cur == nullptr) return;
        traversal(cur->left);
        if (prev)
            res = min(res, cur->val - prev->val);
        prev = cur;
        traversal(cur->right);
    }
    int getMinimumDifference(TreeNode* root) {
        traversal(root);
        return res;
    }
};