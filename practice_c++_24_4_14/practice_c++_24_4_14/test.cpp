#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    bool findTargetIn2DPlants(vector<vector<int>>& plants, int target) {
//        if (plants.size() == 0 || plants[0].size() == 0) return false;
//        int i = 0, j = plants[0].size() - 1;
//        while (i < plants.size() && j >= 0)
//        {
//            if (plants[i][j] > target)
//                j--;
//            else if (plants[i][j] < target)
//                i++;
//            else return true;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    bool searchMatrix(vector<vector<int>>& matrix, int target) {
//        int i = 0, j = matrix[0].size() - 1;
//        while (i < matrix.size() && j >= 0)
//        {
//            if (matrix[i][j] > target)
//                j--;
//            else if (matrix[i][j] < target)
//                i++;
//            else return true;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    bool searchMatrix(vector<vector<int>>& matrix, int target) {
//        int i = matrix.size() - 1, j = 0;
//        while (i >= 0 && j < matrix[0].size())
//        {
//            if (matrix[i][j] > target)
//                i--;
//            else if (matrix[i][j] < target)
//                j++;
//            else return true;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int res = nums[0];
//        int n = nums.size();
//        for (int i = 1; i < n; i++)
//        {
//            if (nums[i - 1] > nums[i])
//            {
//                res = nums[i];
//                break;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int findMin(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right && nums[0] == nums[right])
//            right--;
//        int x = nums[right];
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > x) left = mid + 1;
//            else right = mid;
//        }
//        return nums[left];
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param nums int整型vector
//     * @return int整型
//     */
//    int minNumberInRotateArray(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left < right && nums[0] == nums[right])
//            right--;
//        int x = nums[right];
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > x) left = mid + 1;
//            else right = mid;
//        }
//        return nums[left];
//    }
//};


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    double a, c;
//    int b; //小天体的属性
//    cin >> a >> b >> c;
//    if (b == 0) //流体
//    {
//        double distance = a * 2.455;
//        if (c < distance)
//            printf("%.2lf T_T\n", distance);
//        else
//            printf("%.2lf ^_^\n", distance);
//    }
//    else if (b == 1) //刚体
//    {
//        double distance = a * 1.26;
//        if (c < distance)
//            printf("%.2lf T_T\n", distance);
//        else
//            printf("%.2lf ^_^\n", distance);
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//double a[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//        cin >> a[i];
//    double res = 0;
//    for (int i = 0; i < n; i++)
//        res += 1.0 / a[i];
//    res /= n;
//    printf("%.2lf\n", 1.0 / res);
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int wheel[5]; //轮胎的胎压
//bool index[5]; //记录需要报警的轮胎
//
//int main()
//{
//    int min_alarm, threshold; //最低报警胎压 胎压差的阈值
//    cin >> wheel[1] >> wheel[2] >> wheel[3] >> wheel[4] >> min_alarm >> threshold;
//    int max_wheel = wheel[1] < wheel[2] ? wheel[2] : wheel[1];
//    max_wheel = max_wheel < wheel[3] ? wheel[3] : max_wheel;
//    max_wheel = max_wheel < wheel[4] ? wheel[4] : max_wheel;
//    bool flag = true;
//    for (int i = 1; i <= 4; i++)
//    {
//        if (max_wheel - wheel[i] > threshold || wheel[i] < min_alarm)
//        {
//            index[i] = true;
//            flag = false;
//        }
//    }
//    if (flag)
//        cout << "Normal" << endl;
//    else
//    {
//        int sum = 0;
//        int tmp = 0;
//        for (int i = 1; i <= 4; i++)
//        {
//            if (index[i] == true)
//            {
//                tmp = i;
//                sum++;
//            }
//        }
//        if (sum >= 2)
//            cout << "Warning: please check all the tires!" << endl;
//        else
//            printf("Warning: please check #%d!\n", tmp);
//    }
//    return 0;
//}


//#include <iostream>
//#include <cstring>
//using namespace std;
//
//const int N = 100;
//char info[N];
//
//int main()
//{
//    int sum = 0, count = 0; //统计信息条数 统计特殊信息条数
//    int k = 0; //记录第一次出现是第几条
//    for (int i = 1; ; i++)
//    {
//        cin.getline(info, 100);
//        if (strlen(info) == 1 && info[0] == '.')
//            break;
//        if (strstr(info, "chi1 huo3 guo1") != NULL)
//        {
//            if (count == 0)
//                k = i;
//            count++;
//        }
//        sum++;
//    }
//    cout << sum << endl;
//    if (count != 0)
//        cout << k << ' ' << count << endl;
//    else
//        cout << "-_-#" << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int g[4][4];
//int goal[25] = { 0,0,0,0,0,0,10000,36,720,360,80,252,108,72,54,180,72,180,119,36,306,1080,144,1800,3600 };
//
//int main()
//{
//    int x, y;
//    int sum = 0;
//    for (int i = 1; i <= 3; i++)
//    {
//        for (int j = 1; j <= 3; j++)
//        {
//            cin >> g[i][j];
//            sum += g[i][j];
//            if (g[i][j] == 0)
//            {
//                x = i;
//                y = j;
//            }
//        }
//    }
//    g[x][y] = (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9) - sum;
//    for (int k = 0; k < 3; k++)
//    {
//        int a, b;
//        cin >> a >> b;
//        cout << g[a][b] << endl;
//    }
//    int d;
//    cin >> d;
//    int res = 0;
//    switch (d)
//    {
//    case 1:
//        res = g[1][1] + g[1][2] + g[1][3];
//        break;
//    case 2:
//        res = g[2][1] + g[2][2] + g[2][3];
//        break;
//    case 3:
//        res = g[3][1] + g[3][2] + g[3][3];
//        break;
//    case 4:
//        res = g[1][1] + g[2][1] + g[3][1];
//        break;
//    case 5:
//        res = g[1][2] + g[2][2] + g[3][2];
//        break;
//    case 6:
//        res = g[1][3] + g[2][3] + g[3][3];
//        break;
//    case 7:
//        res = g[1][1] + g[2][2] + g[3][3];
//        break;
//    case 8:
//        res = g[1][3] + g[2][2] + g[3][1];
//    }
//    cout << goal[res] << endl;
//    return 0;
//}


//#include <iostream>
//#include <cmath>
//using namespace std;
//
//const int N = 40;
//char ans[N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    while (m--)
//    {
//        int id = 1;
//        for (int j = 1; j <= n; j++)
//        {
//            cin >> ans[j];
//            if (ans[j] == 'y')
//                continue;
//            else if (ans[j] == 'n')
//                id += pow(2, n - j);
//        }
//        cout << id << endl;
//    }
//    return 0;
//}


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int getHeight(TreeNode* node)
//    {
//        if (node == nullptr)
//            return 0;
//        int leftHeight = getHeight(node->left);
//        int rightHeight = getHeight(node->right);
//        if (leftHeight == -1 || rightHeight == -1)
//            return -1;
//        if (abs(leftHeight - rightHeight) > 1)
//            return -1;
//        else
//            return max(leftHeight, rightHeight) + 1;
//    }
//    bool isBalanced(TreeNode* root) {
//        if (root == nullptr)
//            return true;
//        if (getHeight(root) == -1) return false;
//        else return true;
//    }
//};