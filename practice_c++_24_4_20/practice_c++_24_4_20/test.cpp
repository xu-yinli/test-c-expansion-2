#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//double a3, a2, a1, a0;
//
//double f(double x)
//{
//    return a3 * x * x * x + a2 * x * x + a1 * x + a0;
//}
//
//int main()
//{
//    cin >> a3 >> a2 >> a1 >> a0;
//    double a, b;
//    cin >> a >> b;
//    double mid = 0;
//    while (b - a >= 0.001 && f(a) * f(b) <= 0)
//    {
//        if (f(a) == 0)
//        {
//            mid = a;
//            break;
//        }
//        else if (f(b) == 0)
//        {
//            mid = b;
//            break;
//        }
//        mid = (a + b) / 2;
//        if (f(mid) == 0)
//            break;
//        else
//        {
//            if (f(a) * f(mid) > 0)
//                a = mid;
//            else
//                b = mid;
//        }
//    }
//    printf("%.2lf\n", mid);
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 110;
//int a[N];
//
//int main()
//{
//    int n, k;
//    cin >> n >> k;
//    for (int i = 0; i < n; i++)
//        cin >> a[i];
//    for (int i = 0; i < k; i++)
//    {
//        for (int j = 0; j < n - 1; j++)
//        {
//            if (a[j] > a[j + 1])
//            {
//                int tmp = a[j];
//                a[j] = a[j + 1];
//                a[j + 1] = tmp;
//            }
//        }
//    }
//    for (int i = 0; i < n; i++)
//    {
//        cout << a[i];
//        if (i != n - 1)
//            cout << ' ';
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    vector<int> dp(n + 1);
//    for (int i = 2; i <= n; i++)
//        dp[i] = (dp[i - 1] + 3) % i;
//    cout << dp[n] + 1 << endl;
//    return 0;
//}


//class Solution {
//public:
//    string multiply(string num1, string num2) {
//        int n = num1.size(), m = num2.size();
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        vector<int> str(n + m - 1);
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                str[i + j] += (num1[i] - '0') * (num2[j] - '0');
//        int cur = 0, t = 0;
//        string res;
//        while (cur < n + m - 1)
//        {
//            t += str[cur++];
//            res += t % 10 + '0';
//            t /= 10;
//        }
//        while (t)
//        {
//            res += t % 10 + '0';
//            t /= 10;
//        }
//        while (res.size() > 1 && res.back() == '0') res.pop_back();
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param s string字符串 第一个整数
//     * @param t string字符串 第二个整数
//     * @return string字符串
//     */
//    string solve(string s, string t) {
//        int n = s.size(), m = t.size();
//        reverse(s.begin(), s.end());
//        reverse(t.begin(), t.end());
//        string res;
//        vector<int> sum(n + m - 1);
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                sum[i + j] += (s[i] - '0') * (t[j] - '0');
//        int k = 0;
//        int i = 0;
//        while (i < n + m - 1)
//        {
//            k += sum[i];
//            res += (k % 10) + '0';
//            k /= 10;
//            i++;
//        }
//        while (k)
//        {
//            res += (k % 10) + '0';
//            k /= 10;
//        }
//        while (res.size() > 1 && res.back() == '0')
//            res.pop_back();
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxScore(string s) {
//        int n = s.size();
//        int res = 0;
//        for (int i = 1; i < n; i++)
//        {
//            int score = 0;
//            for (int j = 0; j < i; j++)
//            {
//                if (s[j] == '0')
//                    score++;
//            }
//            for (int j = i; j < n; j++)
//            {
//                if (s[j] == '1')
//                    score++;
//            }
//            res = max(res, score);
//        }
//        return res;
//    }
//};