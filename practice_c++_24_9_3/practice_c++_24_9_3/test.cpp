#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//#define PRINT(FORMAT,VALUE)\
//printf("The value is " FORMAT "\n",VALUE)
//
//int main()
//{
//    PRINT("%d", 10); //The value is 10
//    return 0;
//}


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @return int整型
//     */
//    int Fibonacci(int n) {
//        if (n == 1 || n == 2) return 1;
//        int sum = 0;
//        int a = 1, b = 1;
//        for (int i = 3; i <= n; i++)
//        {
//            sum = a + b;
//            a = b;
//            b = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    int memo[41];
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @return int整型
//     */
//    int dfs(int n)
//    {
//        if (memo[n] != -1) return memo[n];
//        if (n == 1 || n == 2)
//        {
//            memo[n] = 1;
//            return memo[n];
//        }
//        memo[n] = dfs(n - 1) + dfs(n - 2);
//        return memo[n];
//    }
//    int Fibonacci(int n) {
//        memset(memo, -1, sizeof(memo));
//        return dfs(n);
//    }
//};


//class Solution {
//private:
//    const int MOD = 1e9 + 7;
//public:
//    int fib(int n) {
//        if (n == 1) return 1;
//        int sum = 0;
//        int a = 0, b = 1;
//        for (int i = 2; i <= n; i++)
//        {
//            sum = (a + b) % MOD;
//            a = b;
//            b = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    const int MOD = 1e9 + 7;
//    int memo[101];
//public:
//    int dfs(int n)
//    {
//        if (memo[n] != -1) return memo[n];
//        if (n == 0 || n == 1)
//        {
//            memo[n] = n;
//            return memo[n];
//        }
//        memo[n] = (dfs(n - 1) + dfs(n - 2)) % MOD;
//        return memo[n];
//    }
//    int fib(int n) {
//        memset(memo, -1, sizeof(memo));
//        return dfs(n);
//    }
//};


//class Solution {
//private:
//    const int MOD = 1e9 + 7;
//public:
//    int fib(int n) {
//        if (n == 0) return 0;
//        vector<int> dp(n + 1);
//        dp[0] = 0, dp[1] = 1;
//        for (int i = 2; i <= n; i++)
//            dp[i] = (dp[i - 1] + dp[i - 2]) % MOD;
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int climbStairs(int n) {
//        if (n == 1 || n == 2) return n;
//        int a = 1, b = 2;
//        int sum = 0;
//        for (int i = 3; i <= n; i++)
//        {
//            sum = a + b;
//            a = b;
//            b = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//public:
//    int climbStairs(int n) {
//        if (n == 1) return 1;
//        vector<int> dp(n + 1);
//        dp[1] = 1, dp[2] = 2;
//        for (int i = 3; i <= n; i++)
//            dp[i] = dp[i - 1] + dp[i - 2];
//        return dp[n];
//    }
//};


//class Solution {
//private:
//    int memo[46];
//public:
//    int dfs(int n)
//    {
//        if (memo[n] != -1) return memo[n];
//        if (n == 1 || n == 2)
//        {
//            memo[n] = n;
//            return memo[n];
//        }
//        memo[n] = dfs(n - 1) + dfs(n - 2);
//        return memo[n];
//    }
//    int climbStairs(int n) {
//        memset(memo, -1, sizeof(memo));
//        return dfs(n);
//    }
//};


//class Solution {
//private:
//    int MOD = 1e9 + 7;
//public:
//    int trainWays(int num) {
//        if (num == 0) return 1;
//        if (num == 1 || num == 2) return num;
//        int sum = 0;
//        int a = 1, b = 2;
//        for (int i = 3; i <= num; i++)
//        {
//            sum = (a + b) % MOD;
//            a = b;
//            b = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param number int整型
//     * @return int整型
//     */
//    int jumpFloor(int number) {
//        if (number == 1 || number == 2) return number;
//        int a = 1, b = 2;
//        int sum = 0;
//        for (int i = 3; i <= number; i++)
//        {
//            sum = a + b;
//            a = b;
//            b = sum;
//        }
//        return sum;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int f(int n)
//{
//    if (n == 0 || n == 1) return n;
//    return 2 * f(n - 1);
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    cout << f(n) << endl;
//    return 0;
//}


//#include <iostream>
//#include <cmath>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    cout << pow(2, n - 1) << endl;
//    return 0;
//}


//#include <iostream>
//#include <cmath>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    int sum = 0;
//    int a = 0, b = 1;
//    while (n > sum)
//    {
//        sum = a + b;
//        a = b;
//        b = sum;
//    }
//    cout << min(n - a, sum - n) << endl;
//    return 0;
//}


//class Solution {
//private:
//    const int MOD = 1000000007;
//public:
//    int waysToStep(int n) {
//        if (n == 1 || n == 2) return n;
//        if (n == 3) return 4;
//        int sum = 0;
//        int a = 1, b = 2, c = 4;
//        for (int i = 4; i <= n; i++)
//        {
//            sum = ((a + b) % MOD + c) % MOD;
//            a = b;
//            b = c;
//            c = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    const int MOD = 1000000007;
//public:
//    int waysToStep(int n) {
//        if (n == 1 || n == 2) return n;
//        if (n == 3) return 4;
//        vector<int> dp(n + 1);
//        dp[1] = 1, dp[2] = 2, dp[3] = 4;
//        for (int i = 4; i <= n; i++)
//            dp[i] = ((dp[i - 1] + dp[i - 2]) % MOD + dp[i - 3]) % MOD;
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int rectCover(int number) {
//        if (number <= 2) return number;
//        int sum = 0;
//        int a = 1, b = 2;
//        for (int i = 3; i <= number; i++)
//        {
//            sum = a + b;
//            a = b;
//            b = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    int memo[39];
//public:
//    int dfs(int n)
//    {
//        if (memo[n] != -1) return memo[n];
//        if (n <= 2)
//        {
//            memo[n] = n;
//            return memo[n];
//        }
//        memo[n] = dfs(n - 1) + dfs(n - 2);
//        return memo[n];
//    }
//    int rectCover(int number) {
//        memset(memo, -1, sizeof(memo));
//        return dfs(number);
//    }
//};


//class Solution {
//public:
//    int rectCover(int number) {
//        if (number <= 2) return number;
//        return rectCover(number - 1) + rectCover(number - 2);
//    }
//};


//class Solution {
//public:
//    int rectCover(int number) {
//        if (number <= 2) return number;
//        vector<int> dp(number + 1);
//        dp[0] = 0, dp[1] = 1, dp[2] = 2;
//        for (int i = 3; i <= number; i++)
//            dp[i] = dp[i - 1] + dp[i - 2];
//        return dp[number];
//    }
//};