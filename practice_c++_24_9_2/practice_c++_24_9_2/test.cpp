#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int fib(int n) {
//        if (n == 0) return 0;
//        vector<int> dp(n + 1);
//        dp[0] = 0, dp[1] = 1;
//        for (int i = 2; i <= n; i++)
//            dp[i] = dp[i - 1] + dp[i - 2];
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int fib(int n) {
//        if (n <= 1) return n;
//        int sum = 0;
//        int a = 0, b = 1;
//        for (int i = 2; i <= n; i++)
//        {
//            sum = a + b;
//            a = b;
//            b = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//public:
//    int dfs(int n)
//    {
//        if (n == 0 || n == 1) return n;
//        return dfs(n - 1) + dfs(n - 2);
//    }
//    int fib(int n) {
//        return dfs(n);
//    }
//};


//class Solution {
//public:
//    int fib(int n) {
//        if (n == 0 || n == 1) return n;
//        return fib(n - 1) + fib(n - 2);
//    }
//};


//class Solution {
//private:
//    int memo[31];
//public:
//    int dfs(int n)
//    {
//        if (memo[n] != -1) return memo[n];
//        if (n == 0 || n == 1)
//        {
//            memo[n] = n;
//            return memo[n];
//        }
//        memo[n] = dfs(n - 1) + dfs(n - 2);
//        return memo[n];
//    }
//    int fib(int n) {
//        memset(memo, -1, sizeof(memo));
//        return dfs(n);
//    }
//};


//class Solution {
//public:
//    int tribonacci(int n) {
//        if (n == 0 || n == 1) return n;
//        vector<int> dp(n + 1);
//        dp[0] = 0, dp[1] = 1, dp[2] = 1;
//        for (int i = 3; i <= n; i++)
//            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int tribonacci(int n) {
//        if (n == 0 || n == 1) return n;
//        if (n == 2) return 1;
//        int sum = 0;
//        int a = 0, b = 1, c = 1;
//        for (int i = 3; i <= n; i++)
//        {
//            sum = a + b + c;
//            a = b;
//            b = c;
//            c = sum;
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    int memo[38];
//public:
//    int dfs(int n)
//    {
//        if (memo[n] != -1) return memo[n];
//        if (n == 0 || n == 1)
//        {
//            memo[n] = n;
//            return memo[n];
//        }
//        if (n == 2)
//        {
//            memo[n] = 1;
//            return memo[n];
//        }
//        memo[n] = dfs(n - 1) + dfs(n - 2) + dfs(n - 3);
//        return memo[n];
//    }
//    int tribonacci(int n) {
//        memset(memo, -1, sizeof(memo));
//        return dfs(n);
//    }
//};