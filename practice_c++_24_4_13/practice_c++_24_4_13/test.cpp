#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int lengthOfLastWord(string s) {
//        int n = s.size();
//        int k = n - 1;
//        while (s[k] == ' ')
//            k--;
//        int len = 0;
//        while (k >= 0 && s[k] != ' ')
//        {
//            len++;
//            k--;
//        }
//        return len;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* deleteDuplicates(ListNode* head) {
//        if (head == nullptr)
//            return head;
//        ListNode* cur = head;
//        while (cur->next)
//        {
//            if (cur->val == cur->next->val)
//                cur->next = cur->next->next;
//            else
//                cur = cur->next;
//        }
//        return head;
//    }
//};