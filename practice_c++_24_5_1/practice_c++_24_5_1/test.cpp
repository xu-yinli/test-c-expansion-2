#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    ListNode* deleteNode(ListNode* head, int val) {
//        if (head->val == val) return head->next;
//        ListNode* cur = head;
//        while (cur->next && cur->next->val != val)
//            cur = cur->next;
//        if (cur->next)
//            cur->next = cur->next->next;
//        return head;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    ListNode* deleteNode(ListNode* head, int val) {
//        if (head->val == val) return head->next;
//        ListNode* prev = head;
//        ListNode* cur = head;
//        while (cur && cur->val != val)
//        {
//            prev = cur;
//            cur = cur->next;
//        }
//        if (cur)
//            prev->next = cur->next;
//        return head;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    ListNode* deleteNode(ListNode* head, int val) {
//        if (head == NULL) return head;
//        head->next = deleteNode(head->next, val);
//        if (head->val == val) return head->next;
//        else return head;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    ListNode* deleteNode(ListNode* head, int val) {
//        ListNode* newHead = new ListNode(0);
//        newHead->next = head;
//        ListNode* cur = newHead;
//        while (cur->next)
//        {
//            if (cur->next->val == val)
//            {
//                cur->next = cur->next->next;
//                break;
//            }
//            else
//            {
//                cur = cur->next;
//            }
//        }
//        cur = newHead->next;
//        delete newHead;
//        return cur;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* ReverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr)
//            return head;
//        ListNode* left = head;
//        ListNode* mid = head->next;
//        ListNode* right = mid->next;
//        while (right)
//        {
//            mid->next = left;
//            left = mid;
//            mid = right;
//            right = right->next;
//        }
//        mid->next = left;
//        head->next = nullptr;
//        head = mid;
//        return head;
//    }
//};


///**
// * struct ListNode {
// *	int val;
// *	struct ListNode *next;
// *	ListNode(int x) : val(x), next(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param head ListNode类
//     * @return ListNode类
//     */
//    ListNode* ReverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr)
//            return head;
//        ListNode* newHead = nullptr;
//        while (head)
//        {
//            ListNode* p = head;
//            head = head->next;
//            p->next = newHead;
//            newHead = p;
//        }
//        return newHead;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newHead = nullptr;
//        while (head)
//        {
//            ListNode* p = head;
//            head = head->next;
//            p->next = newHead;
//            newHead = p;
//        }
//        return newHead;
//    }
//};