#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int m, d, y;
//    scanf("%d-%d-%d", &m, &d, &y);
//    printf("%d-%02d-%02d", y, m, d);
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n; //前来查询的用户数
//    cin >> n;
//    while (n--)
//    {
//        char gender; //性别
//        double h; //身高
//        cin >> gender >> h;
//        if (gender == 'F') //F表示女性
//            printf("%.2lf\n", h * 1.09);
//        else //M表示男性
//            printf("%.2lf\n", h / 1.09);
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int a[N];
//
//int main()
//{
//    int cnt = 0;
//    while (1)
//    {
//        cin >> a[cnt];
//        if (a[cnt++] == 250)
//        {
//            cout << cnt << endl;
//            break;
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int id; //书名
//        char key; //键值
//        int h, m; //小时 分钟
//        scanf("%d %c %d:%d", &id, &key, &h, &m);
//        int cnt = 0; //借书次数
//        int sum_read = 0; //总共阅读时间
//        int a[1010][2] = { 0 };
//        while (id) //id为0即借书当天操作
//        {
//            if (key == 'S') //借书
//            {
//                a[id][0] = 1; //标记被借出
//                a[id][1] = h * 60 + m; //被借阅的开始时间
//            }
//            else if (key == 'E' && a[id][0] == 1) //还书同时保证该书之前被借过
//            {
//                cnt++; //还书操作才对借书次数做+1操作
//                sum_read += h * 60 + m - a[id][1]; //还书时的时间-借书时的时间
//                a[id][0] = 0; //恢复为没被借出的状态
//            }
//            scanf("%d %c %d:%d", &id, &key, &h, &m);
//        }
//        if (cnt == 0)
//            cout << "0 0" << endl;
//        else
//            printf("%d %.0lf\n", cnt, 1.0 * sum_read / cnt);
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <map>
//#include <algorithm>
//using namespace std;
//
//struct cmp {
//    vector<int> v;
//    int k;
//    bool operator<(const cmp& vv) {
//        if (k == vv.k) return v < vv.v;
//        else return k > vv.k;
//    }
//};
//
//int main()
//{
//    int n, m; //功能模块的个数 系列测试输入的个数
//    cin >> n >> m;
//    map<vector<int>, int> hash;
//    for (int i = 0; i < n; i++)
//    {
//        vector<int> a;
//        for (int j = 0; j < m; j++)
//        {
//            int x;
//            cin >> x;
//            a.push_back(x);
//        }
//        hash[a]++;
//    }
//    cout << hash.size() << endl;
//    vector<cmp> res;
//    for (auto x : hash)
//    {
//        cmp c;
//        c.v = x.first;
//        c.k = x.second;
//        res.push_back(c);
//    }
//    sort(res.begin(), res.end());
//    for (auto x : res)
//    {
//        cout << x.k;
//        for (auto e : x.v)
//            cout << ' ' << e;
//        cout << endl;
//    }
//    return 0;
//}


//class Solution {
//public:
//    int minDistance(string word1, string word2) {
//        int n = word1.size(), m = word2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 0; i <= n; i++) dp[i][0] = i;
//        for (int j = 0; j <= m; j++) dp[0][j] = j;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (word1[i - 1] == word2[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1];
//                else
//                    dp[i][j] = min(dp[i - 1][j] + 1, min(dp[i][j - 1] + 1, dp[i - 1][j - 1] + 1));
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int countSubstrings(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        int cnt = 0;
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                {
//                    if (j - i <= 2)
//                    {
//                        dp[i][j] = true;
//                        cnt++;
//                    }
//                    else
//                    {
//                        if (dp[i + 1][j - 1])
//                        {
//                            dp[i][j] = true;
//                            cnt++;
//                        }
//                    }
//                }
//            }
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    int countSubstrings(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        int cnt = 0;
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j] && (j - i <= 2 || dp[i + 1][j - 1]))
//                {
//                    dp[i][j] = true;
//                    cnt++;
//                }
//            }
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    int longestPalindromeSubseq(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < n; j++)
//                if (i == j) dp[i][j] = 1;
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i + 1; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = dp[i + 1][j - 1] + 2;
//                else
//                    dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[0][n - 1];
//    }
//};


//class Solution {
//public:
//    int longestPalindromeSubseq(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = 0; i < n; i++)
//            dp[i][i] = 1;
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i + 1; j < n; j++)
//            {
//                if (s[i] == s[j])
//                    dp[i][j] = dp[i + 1][j - 1] + 2;
//                else
//                    dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[0][n - 1];
//    }
//};


//class Solution {
//public:
//    int numDistinct(string s, string t) {
//        int n = s.size(), m = t.size();
//        vector<vector<unsigned long long>> dp(n + 1, vector<unsigned long long>(m + 1));
//        dp[0][0] = 1;
//        for (int i = 1; i <= n; i++) dp[i][0] = 1;
//        for (int j = 1; j <= m; j++) dp[0][j] = 0;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (s[i - 1] == t[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];
//                else
//                    dp[i][j] = dp[i - 1][j];
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int numDistinct(string s, string t) {
//        int n = s.size(), m = t.size();
//        vector<vector<unsigned long long>> dp(n + 1, vector<unsigned long long>(m + 1));
//        for (int i = 0; i <= n; i++) dp[i][0] = 1;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (s[i - 1] == t[j - 1])
//                    dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];
//                else
//                    dp[i][j] = dp[i - 1][j];
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//private:
//    vector<string> path;
//    vector<vector<string>> res;
//public:
//    bool is_Palindrome(string& s, int st, int ed)
//    {
//        for (int i = st, j = ed; i < j; i++, j--)
//        {
//            if (s[i] != s[j]) return false;
//        }
//        return true;
//    }
//    void dfs(string& s, int startIndex)
//    {
//        if (startIndex >= s.size())
//        {
//            res.push_back(path);
//            return;
//        }
//        else
//        {
//            for (int i = startIndex; i < s.size(); i++)
//            {
//                if (is_Palindrome(s, startIndex, i))
//                {
//                    string str = s.substr(startIndex, i - startIndex + 1);
//                    path.push_back(str);
//                }
//                else
//                    continue;
//                dfs(s, i + 1);
//                path.pop_back();
//            }
//        }
//    }
//    vector<vector<string>> partition(string s) {
//        dfs(s, 0);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<string> res;
//public:
//    bool isValid(string s, int st, int ed)
//    {
//        if (st > ed) return false;
//        if (s[st] == '0' && st != ed) return false;
//        int sum = 0;
//        for (int i = st; i <= ed; i++)
//        {
//            if (s[i] < '0' || s[i]>'9')
//                return false;
//            sum = sum * 10 + (s[i] - '0');
//            if (sum > 255)
//                return false;
//        }
//        return true;
//    }
//    void dfs(string s, int startIndex, int pointSum)
//    {
//        if (pointSum == 3)
//        {
//            if (isValid(s, startIndex, s.size() - 1))
//            {
//                res.push_back(s);
//                return;
//            }
//        }
//        else
//        {
//            for (int i = startIndex; i < s.size(); i++)
//            {
//                if (isValid(s, startIndex, i))
//                {
//                    s.insert(s.begin() + i + 1, '.');
//                    pointSum++;
//                    dfs(s, i + 2, pointSum);
//                    pointSum--;
//                    s.erase(s.begin() + i + 1);
//                }
//                else break;
//            }
//        }
//    }
//    vector<string> restoreIpAddresses(string s) {
//        dfs(s, 0, 0);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<string> res;
//public:
//    bool isValid(string s, int st, int ed)
//    {
//        if (st > ed) return false;
//        if (s[st] == '0' && st != ed) return false;
//        int sum = 0;
//        for (int i = st; i <= ed; i++)
//        {
//            if (s[i] < '0' || s[i]>'9')
//                return false;
//            sum = sum * 10 + (s[i] - '0');
//            if (sum > 255)
//                return false;
//        }
//        return true;
//    }
//    void dfs(string s, int startIndex, int pointSum)
//    {
//        if (pointSum == 3)
//        {
//            if (isValid(s, startIndex, s.size() - 1))
//            {
//                res.push_back(s);
//                return;
//            }
//        }
//        else
//        {
//            for (int i = startIndex; i < s.size(); i++)
//            {
//                if (isValid(s, startIndex, i))
//                {
//                    s.insert(s.begin() + i + 1, '.');
//                    pointSum++;
//                    dfs(s, i + 2, pointSum);
//                    pointSum--;
//                    s.erase(s.begin() + i + 1);
//                }
//                else break;
//            }
//        }
//    }
//    vector<string> restoreIpAddresses(string s) {
//        if (s.size() < 4 || s.size() > 12)
//            return res;
//        dfs(s, 0, 0);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> sortedSquares(vector<int>& nums) {
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//            res.push_back(nums[i] * nums[i]);
//        sort(res.begin(), res.end());
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<int> sortedSquares(vector<int>& nums) {
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//            nums[i] = nums[i] * nums[i];
//        sort(nums.begin(), nums.end());
//        return nums;
//    }
//};


//class Solution {
//public:
//    vector<int> sortedSquares(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> res(n);
//        int k = n - 1;
//        int left = 0, right = n - 1;
//        while (left <= right)
//        {
//            if (nums[left] * nums[left] < nums[right] * nums[right])
//            {
//                res[k--] = nums[right] * nums[right];
//                right--;
//            }
//            else
//            {
//                res[k--] = nums[left] * nums[left];
//                left++;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int res = INT_MAX;
//        int n = nums.size();
//        int sum = 0;
//        int i = 0;
//        for (int j = 0; j < n; j++)
//        {
//            sum += nums[j];
//            while (sum >= target)
//            {
//                int len = j - i + 1;
//                if (len < res)
//                    res = len;
//                sum -= nums[i];
//                i++;
//            }
//        }
//        if (res != INT_MAX) return res;
//        else return 0;
//    }
//};


//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int res = INT_MAX;
//        int n = nums.size();
//        for (int i = 0; i < n; i++)
//        {
//            int sum = 0;
//            for (int j = i; j < n; j++)
//            {
//                sum += nums[j];
//                if (sum >= target)
//                {
//                    int len = j - i + 1;
//                    if (res > len)
//                        res = len;
//                    break;
//                }
//            }
//        }
//        if (res != INT_MAX) return res;
//        else return 0;
//    }
//};


//class Solution {
//public:
//    vector<vector<int>> generateMatrix(int n) {
//        vector<vector<int>> res(n, vector<int>(n));
//        int startX = 0, startY = 0;
//        int offset = 1;
//        int cnt = 1;
//        int loop = n / 2;
//        while (loop--)
//        {
//            int i = startX, j = startY;
//            for (j = startY; j < n - offset; j++)
//                res[startX][j] = cnt++;
//            for (i = startX; i < n - offset; i++)
//                res[i][j] = cnt++;
//            for (; j > startY; j--)
//                res[i][j] = cnt++;
//            for (; i > startX; i--)
//                res[i][j] = cnt++;
//            offset++;
//            startX++;
//            startY++;
//        }
//        if (n % 2 == 1)
//            res[n / 2][n / 2] = cnt;
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    queue<TreeNode*> q;
//public:
//    int countNodes(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        int cnt = 0;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            for (int i = 0; i < n; i++)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                cnt++;
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return cnt;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int getNodesNum(TreeNode* node)
//    {
//        if (node == nullptr)
//            return 0;
//        int leftNum = getNodesNum(node->left);
//        int rightNum = getNodesNum(node->right);
//        return leftNum + rightNum + 1;
//    }
//    int countNodes(TreeNode* root) {
//        return getNodesNum(root);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int countNodes(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        return countNodes(root->left) + countNodes(root->right) + 1;
//    }
//};


//#include <iostream>
//using namespace std;
//
//bool is_prime(int x)
//{
//    for (int i = 2; i <= x / i; i++)
//        if (x % i == 0) return false;
//    return true;
//}
//
//int pai(int n)
//{
//    int cnt = 0;
//    for (int i = 2; i <= n; i++)
//        if (is_prime(i))
//            cnt++;
//    return cnt;
//}
//
//int main()
//{
//    int t;
//    cin >> t;
//    for (int i = 1; i <= t; i++)
//    {
//        int n;
//        cin >> n;
//        cout << pai(n) << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	double k;
//	cin >> k;
//	double sum = 0;
//	int i = 1;
//	while (sum<=k)
//	{
//		sum += 1.0 / i;
//		i++;
//	}
//	cout << i-1 << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 2000010;
//bool light[N];
//
//int main()
//{
//	int n;
//	cin >> n;
//	while(n--)
//	{
//		double a;
//		int t;
//		cin >> a >> t;
//		for (int i = 1; i <= t; i++)
//		{
//			if (!light[(int)(i * a)])
//				light[(int)(i * a)] = true;
//			else
//				light[(int)(i * a)] = false;
//		}
//	}
//	for (int i = 1; i < N; i++)
//	{
//		if (light[i])
//		{
//			cout << i << endl;
//			break;
//		}
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	char s[14];
//	cin >> s;
//	char key;
//	int sum = 0;
//	int k = 1;
//	for (int i = 0; i < 11; i++)
//	{
//		if (s[i] != '-')
//		{
//			sum += k * (s[i] - '0');
//			k++;
//		}
//	}
//	if (sum % 11 == 10)
//		key = 'X';
//	else
//		key = (sum % 11) + '0';
//	if (key == s[12])
//		cout << "Right" << endl;
//	else
//	{
//		s[12] = key;
//		cout << s << endl;
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 10010;
//int h[N], w[N];
//
//int main()
//{
//	int n;
//	cin >> n;
//	for (int i = 0; i <= n; i++)
//		cin >> h[i];
//	for (int i = 1; i <= n; i++)
//		cin >> w[i];
//	double s = 0;
//	for (int i = 0; i < n; i++)
//	{
//		int upper = h[i];
//		int down = h[i + 1];
//		int height = w[i + 1];
//		s += 1.0 * (upper + down) * height / 2;
//	}
//	printf("%.1lf\n", s);
//	return 0;
//}


//#include <iostream>
//#include <cstring>
//#include <cmath>
//using namespace std;
//
//const int N = 100010, INF=1E9+7;
//int dp[N];
//
//int main()
//{
//	int m;
//	cin >> m;
//	memset(dp, INF, sizeof dp);
//	dp[0] = 0;
//	for (int i = 1; i <= m; i++)
//	{
//		for (int j = 1; j <= i; j++)
//		{
//			if (pow(j, 4) > i)
//				break;
//			else
//			{
//				int k = pow(j, 4);
//				dp[i] = min(dp[i], dp[i-k]+1);
//			}
//		}
//	}
//	cout << dp[m] << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e4 + 10;
//struct carpet {
//	int a;
//	int b;
//	int g;
//	int k;
//}c[N];
//
//int main()
//{
//	int n;
//	cin >> n;
//	for (int i = 1; i <= n; i++) //铺设地毯的左下角的坐标(a, b) 地毯在x轴和y轴方向的长度
//		cin >> c[i].a >> c[i].b >> c[i].g >> c[i].k;
//	int x, y; //所求的地面的点的坐标
//	cin >> x >> y;
//	int id = -1;
//	for (int i = n; i >= 1; i--)
//	{
//		if (x >= c[i].a && x <= c[i].a+c[i].g && y >= c[i].b && y <= c[i].b+c[i].k)
//		{
//			id = i;
//			break;
//		}
//	}
//	cout << id << endl;
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//
//bool check(int n)
//{
//	int d = n % 100;
//	int m = n / 100 % 100;
//	int y = n / 10000;
//	if (d <= 0) return false;
//	if (m <= 0 || m > 12) return false;
//	if (m != 2 && d > days[m]) return false;
//	if (m == 2)
//	{
//		if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0)
//		{
//			if (d > 29) return false;
//		}
//		else
//			if (d > 28) return false;
//	}
//	return true;
//}
//
//bool check_huiwen(string s)
//{
//	int n = s.size();
//	for (int i = 0, j = n - 1; i < j; i++, j--)
//		if (s[i] != s[j])
//			return false;
//	return true;
//}
//
//bool check_ABAB(string s)
//{
//	if (check_huiwen(s))
//	{
//		if (s[0] != s[2] || s[1] != s[3] || s[0] == s[1])
//			return false;
//		return true;
//	}
//	return false;
//}
//
//int main()
//{
//	int n;
//	cin >> n;
//	bool flag = false;
//	for (int i = n + 1; ; i++)
//	{
//		if (check(i))
//		{
//			string s = to_string(i);
//			if (check_huiwen(s) && !flag)
//			{
//				cout << i << endl;
//				flag = true;
//			}
//			if (flag && check_ABAB(s))
//			{
//				cout << i << endl;
//				return 0;
//			}
//		}
//	}
//	return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//typedef long long LL;
//
//const int N = 1e5 + 10;
//int a[N], b[N], c[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    for (int i = 0; i < n; i++) cin >> b[i];
//    for (int i = 0; i < n; i++) cin >> c[i];
//    sort(a, a + n);
//    sort(b, b + n);
//    sort(c, c + n);
//    LL res = 0;
//    LL left = 0, right = 0;
//    for (int k = 0; k < n; k++)
//    {
//        while (left < n && a[left] < b[k]) left++;
//        while (right < n && c[right] <= b[k]) right++;
//        res += (LL)(left * (n - right));
//    }
//    cout << res << endl;
//    return 0;
//}


//class Solution {
//public:
//    void duplicateZeros(vector<int>& arr) {
//        int cur = 0, dest = -1;
//        int n = arr.size();
//        while (cur < n)
//        {
//            if (arr[cur]) dest++;
//            else dest += 2;
//            if (dest >= n - 1) break;
//            cur++;
//        }
//        if (dest == n)
//        {
//            arr[dest - 1] = 0;
//            dest -= 2;
//            cur--;
//        }
//        while (cur >= 0)
//        {
//            if (arr[cur]) arr[dest--] = arr[cur--];
//            else
//            {
//                arr[dest--] = 0;
//                arr[dest--] = 0;
//                cur--;
//            }
//        }
//    }
//};


//class Solution {
//public:
//    int search(vector<int>& nums, int target) {
//        for (int i = 0; i < nums.size(); i++)
//            if (nums[i] == target)
//                return i;
//        return -1;
//    }
//};


//class Solution {
//public:
//    int search(vector<int>& nums, int target) {
//        int n = nums.size();
//        int left = 0, right = n - 1;
//        while (left <= right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] > target) right = mid - 1;
//            else if (nums[mid] < target) left = mid + 1;
//            else return mid;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    vector<int> searchRange(vector<int>& nums, int target) {
//        int n = nums.size();
//        if (n == 0) return { -1, -1 };
//        int begin = 0, end = 0;
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] != target) return { -1, -1 };
//        else begin = left;
//
//        left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] <= target) left = mid;
//            else right = mid - 1;
//        }
//        end = right;
//        return { begin, end };
//    }
//};


//class Solution {
//public:
//    vector<int> searchRange(vector<int>& nums, int target) {
//        int n = nums.size();
//        if (n == 0) return { -1, -1 };
//        int begin = 0;
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] < target) left = mid + 1;
//            else right = mid;
//        }
//        if (nums[left] != target) return { -1, -1 };
//        else begin = left;
//        right = n - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left + 1) / 2;
//            if (nums[mid] <= target) left = mid;
//            else right = mid - 1;
//        }
//        return { begin, right };
//    }
//};