#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <string>
//using namespace std;
//
//int main()
//{
//    string s;
//    getline(cin, s);
//    string temp = "You are right, but ";
//    if (s.substr(0, 19) == temp) cout << "AI" << endl;
//    else cout << "Human" << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    cout << "China" << endl;
//    return 0;
//}


//class Solution {
//public:
//    bool f(vector<int>& postorder, int st, int ed)
//    {
//        if (st >= ed) return true;
//        int root = postorder[ed];
//        int i = st;
//        while (i < ed && postorder[i] < root)
//            i++;
//        for (int j = i; j < ed; j++)
//            if (postorder[j] < root)
//                return false;
//        return f(postorder, st, i - 1) && f(postorder, i, ed - 1);
//    }
//    bool verifyTreeOrder(vector<int>& postorder) {
//        int n = postorder.size();
//        return f(postorder, 0, n - 1);
//    }
//};


//class Solution {
//public:
//    bool f(vector<int>& sequence, int st, int ed)
//    {
//        if (st >= ed) return true;
//        int root = sequence[ed];
//        int i = st;
//        while (i < ed && sequence[i] < root)
//            i++;
//        for (int j = i; j < ed; j++)
//            if (sequence[j] < root)
//                return false;
//        return f(sequence, st, i - 1) && f(sequence, i, ed - 1);
//    }
//    bool VerifySquenceOfBST(vector<int> sequence) {
//        int n = sequence.size();
//        if (n <= 0) return false;
//        return f(sequence, 0, n - 1);
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        res.push_back(nums[0]);
//        int n = nums.size();
//        for (int i = 1; i < n; i++)
//        {
//            if (nums[i] > res.back())
//            {
//                res.push_back(nums[i]);
//            }
//            else
//            {
//                int left = 0, right = res.size() - 1;
//                while (left < right)
//                {
//                    int mid = left + (right - left) / 2;
//                    if (res[mid] < nums[i]) left = mid + 1;
//                    else right = mid;
//                }
//                res[left] = nums[i];
//            }
//        }
//        return res.size();
//    }
//};


//class Solution {
//public:
//    bool increasingTriplet(vector<int>& nums) {
//        int a = nums[0], b = INT_MAX;
//        int n = nums.size();
//        for (int i = 1; i < n; i++)
//        {
//            if (nums[i] > b) return true;
//            else if (nums[i] > a) b = nums[i];
//            else a = nums[i];
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    int findLengthOfLCIS(vector<int>& nums) {
//        int n = nums.size();
//        int res = 1;
//        for (int i = 0; i < n; )
//        {
//            int j = i + 1;
//            while (j<n && nums[j]>nums[j - 1]) j++;
//            res = max(res, j - i);
//            i = j;
//        }
//        return res;
//    }
//};