#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int findContentChildren(vector<int>& g, vector<int>& s) {
//        sort(g.begin(), g.end());
//        sort(s.begin(), s.end());
//        int index = s.size() - 1;
//        int res = 0;
//        for (int i = g.size() - 1; i >= 0; i--)
//        {
//            if (index >= 0 && s[index] >= g[i])
//            {
//                res++;
//                index--;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int findContentChildren(vector<int>& g, vector<int>& s) {
//        sort(g.begin(), g.end());
//        sort(s.begin(), s.end());
//        int k = 0;
//        int res = 0;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (k < g.size() && s[i] >= g[k])
//            {
//                res++;
//                k++;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums) {
//        if (nums.size() <= 1)
//            return nums.size();
//        int prediff = 0, curdiff = 0;
//        int res = 1;
//        for (int i = 0; i < nums.size() - 1; i++)
//        {
//            curdiff = nums[i + 1] - nums[i];
//            if ((prediff <= 0 && curdiff > 0) || (prediff >= 0 && curdiff < 0))
//            {
//                res++;
//                prediff = curdiff;
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int res = -2e4;
//        int sum = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            sum += nums[i];
//            if (sum > res)
//                res = sum;
//            if (sum < 0)
//                sum = 0;
//        }
//        return res;
//    }
//};


//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//typedef pair<int, int> PII;
//vector<PII> q;
//
//bool cmp(PII x, PII y)
//{
//    return x.second < y.second;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b;
//        cin >> a >> b;
//        q.push_back({ a, b });
//    }
//    sort(q.begin(), q.end(), cmp);
//    int count = 0;
//    int ed = -2e9;
//    for (int i = 0; i < q.size(); i++)
//    {
//        if (ed < q[i].first)
//        {
//            count++;
//            ed = q[i].second - 1;
//        }
//    }
//    cout << count << endl;
//    return 0;
//}


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int profit = 0;
//        for (int i = 1; i < prices.size(); i++)
//        {
//            int k = prices[i] - prices[i - 1];
//            if (k > 0)
//                profit += k;
//        }
//        return profit;
//    }
//};


//class Solution {
//public:
//    bool canJump(vector<int>& nums) {
//        if (nums.size() == 1)
//            return true;
//        int cover = 0;
//        for (int i = 0; i <= cover; i++)
//        {
//            cover = max(cover, i + nums[i]);
//            if (cover >= nums.size() - 1)
//                return true;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        if (nums.size() == 1) return 0;
//        int cur_cover = 0, next_cover = 0;
//        int step = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            next_cover = max(next_cover, i + nums[i]);
//            if (i == cur_cover)
//            {
//                if (cur_cover != nums.size() - 1)
//                {
//                    step++;
//                    cur_cover = next_cover;
//                }
//                else
//                    break;
//            }
//        }
//        return step;
//    }
//};


//class Solution {
//public:
//    int jump(vector<int>& nums) {
//        if (nums.size() == 1) return 0;
//        int cur_cover = 0, next_cover = 0;
//        int step = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            next_cover = max(next_cover, i + nums[i]);
//            if (i == cur_cover)
//            {
//                if (cur_cover != nums.size() - 1)
//                {
//                    step++;
//                    cur_cover = next_cover;
//                    if (cur_cover >= nums.size() - 1)
//                        break;
//                }
//                else
//                    break;
//            }
//        }
//        return step;
//    }
//};


//class Solution {
//    static bool cmp(int x, int y)
//    {
//        return abs(x) > abs(y);
//    }
//public:
//    int largestSumAfterKNegations(vector<int>& nums, int k) {
//        sort(nums.begin(), nums.end(), cmp);
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (k > 0 && nums[i] < 0)
//            {
//                nums[i] *= -1;
//                k--;
//            }
//        }
//        int sum = 0;
//        while (k--)
//        {
//            nums[nums.size() - 1] *= -1;
//        }
//        for (int i = 0; i < nums.size(); i++)
//            sum += nums[i];
//        return sum;
//    }
//};


//class Solution {
//    static bool cmp(int x, int y)
//    {
//        return abs(x) > abs(y);
//    }
//public:
//    int largestSumAfterKNegations(vector<int>& nums, int k) {
//        sort(nums.begin(), nums.end(), cmp);
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (k > 0 && nums[i] < 0)
//            {
//                nums[i] *= -1;
//                k--;
//            }
//        }
//        int sum = 0;
//        if (k % 2 == 1)
//        {
//            nums[nums.size() - 1] *= -1;
//        }
//        for (int i = 0; i < nums.size(); i++)
//            sum += nums[i];
//        return sum;
//    }
//};


//class Solution {
//    static bool cmp(int x, int y)
//    {
//        return abs(x) > abs(y);
//    }
//public:
//    int largestSumAfterKNegations(vector<int>& nums, int k) {
//        sort(nums.begin(), nums.end(), cmp);
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (k > 0 && nums[i] < 0)
//            {
//                nums[i] *= -1;
//                k--;
//            }
//        }
//        int sum = 0;
//        if (k % 2 == 1)
//        {
//            nums[nums.size() - 1] *= -1;
//        }
//        for (auto e : nums)
//            sum += e;
//        return sum;
//    }
//};


//class Solution {
//public:
//    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
//        int cur_sum = 0, total_sum = 0;
//        int k = 0;
//        for (int i = 0; i < gas.size(); i++)
//        {
//            cur_sum += (gas[i] - cost[i]);
//            total_sum += (gas[i] - cost[i]);
//            if (cur_sum < 0)
//            {
//                k = i + 1;
//                cur_sum = 0;
//            }
//        }
//        if (total_sum < 0)
//            return -1;
//        return k;
//    }
//};


//class Solution {
//public:
//    int candy(vector<int>& ratings) {
//        int sum = 0;
//        vector<int> candy(ratings.size(), 1);
//        for (int i = 1; i < ratings.size(); i++)
//        {
//            if (ratings[i - 1] < ratings[i])
//            {
//                candy[i] = candy[i - 1] + 1;
//            }
//        }
//        for (int i = ratings.size() - 2; i >= 0; i--)
//        {
//            if (ratings[i] > ratings[i + 1])
//            {
//                candy[i] = max(candy[i], candy[i + 1] + 1);
//            }
//        }
//        for (int i = 0; i < candy.size(); i++)
//            sum += candy[i];
//        return sum;
//    }
//};


//class Solution {
//private:
//    int five = 0, ten = 0, twenty = 0;
//public:
//    bool lemonadeChange(vector<int>& bills) {
//        for (auto bill : bills)
//        {
//            if (bill == 5) five++;
//            else if (bill == 10)
//            {
//                if (five > 0)
//                {
//                    five--;
//                    ten++;
//                }
//                else return false;
//            }
//            else if (bill == 20)
//            {
//                if (five > 0 && ten > 0)
//                {
//                    five--;
//                    ten--;
//                    twenty++;
//                }
//                else if (five >= 3)
//                {
//                    five -= 3;
//                    twenty++;
//                }
//                else return false;
//            }
//        }
//        return true;
//    }
//};


//class Solution {
//private:
//    int a[3]; //a[0]-5美元 a[1]-10美元 a[2]-20美元
//public:
//    bool lemonadeChange(vector<int>& bills) {
//        for (auto bill : bills)
//        {
//            if (bill == 5) a[0]++;
//            else if (bill == 10)
//            {
//                if (a[0] > 0)
//                {
//                    a[0]--;
//                    a[1]++;
//                }
//                else return false;
//            }
//            else if (bill == 20)
//            {
//                if (a[0] > 0 && a[1] > 0)
//                {
//                    a[0]--;
//                    a[1]--;
//                    a[2]++;
//                }
//                else if (a[0] >= 3)
//                {
//                    a[0] -= 3;
//                    a[2]++;
//                }
//                else return false;
//            }
//        }
//        return true;
//    }
//};


//class Solution {
//    static bool cmp(vector<int> a, vector<int> b)
//    {
//        if (a[0] == b[0])
//            return a[1] < b[1];
//        return a[0] > b[0];
//    }
//public:
//    vector<vector<int>> reconstructQueue(vector<vector<int>>& people) {
//        sort(people.begin(), people.end(), cmp);
//        vector<vector<int>> q;
//        for (int i = 0; i < people.size(); i++)
//        {
//            int position = people[i][1];
//            q.insert(q.begin() + position, people[i]);
//        }
//        return q;
//    }
//};


//class Solution {
//public:
//    bool lemonadeChange(vector<int>& bills) {
//        int five = 0, ten = 0;
//        for (auto bill : bills)
//        {
//            if (bill == 5) five++;
//            else if (bill == 10)
//            {
//                if (five) five--, ten++;
//                else return false;
//            }
//            else if (bill == 20)
//            {
//                if (five && ten) five--, ten--;
//                else if (five >= 3) five -= 3;
//                else return false;
//            }
//            else return false;
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    int halveArray(vector<int>& nums) {
//        priority_queue<double> heap;
//        double sum = 0;
//        for (auto e : nums)
//        {
//            sum += e;
//            heap.push(e);
//        }
//        sum /= 2;
//        int count = 0;
//        while (sum > 0)
//        {
//            double t = heap.top() / 2.0;
//            heap.pop();
//            sum -= t;
//            count++;
//            heap.push(t);
//        }
//        return count;
//    }
//};


//class Solution {
//public:
//    int halveArray(vector<int>& nums) {
//        priority_queue<double> heap;
//        double sum = 0;
//        for (auto e : nums)
//        {
//            sum += e;
//            heap.push(e);
//        }
//        int count = 0;
//        double k = sum / 2.0;
//        while (sum > k)
//        {
//            double t = heap.top() / 2.0;
//            heap.pop();
//            sum -= t;
//            count++;
//            heap.push(t);
//        }
//        return count;
//    }
//};


//class Solution {
//public:
//    string largestNumber(vector<int>& nums) {
//        vector<string> s;
//        for (auto e : nums)
//            s.push_back(to_string(e));
//        sort(s.begin(), s.end(), [](string s1, string s2) {
//            return s1 + s2 > s2 + s1;
//            });
//        string res;
//        for (auto e : s)
//            res += e;
//        if (res[0] == '0') return "0";
//        else return res;
//    }
//};


//class Solution {
//public:
//    int wiggleMaxLength(vector<int>& nums) {
//        if (nums.size() <= 1) return nums.size();
//        int res = 1;
//        int left = 0, right = 0;
//        for (int i = 0; i < nums.size() - 1; i++)
//        {
//            right = nums[i + 1] - nums[i];
//            if (right == 0) continue;
//            if (left * right <= 0) res++;
//            left = right;
//        }
//        return res;
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int v[N], w[N];
//int f[N][N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 0; j <= m; j++)
//        {
//            if (j >= v[i])
//                f[i][j] = max(f[i - 1][j], f[i - 1][j - v[i]] + w[i]);
//            else
//                f[i][j] = f[i - 1][j];
//        }
//    }
//    cout << f[n][m] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int v[N], w[N];
//int f[N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = m; j >= v[i]; j--)
//        {
//            f[j] = max(f[j], f[j - v[i]] + w[i]);
//        }
//    }
//    cout << f[m] << endl;
//    return 0;
//}


//class Solution {
//public:
//    int fib(int n) {
//        if (n <= 1) return n;
//        vector<int> dp(n + 1);
//        dp[0] = 0, dp[1] = 1;
//        for (int i = 2; i <= n; i++)
//        {
//            dp[i] = dp[i - 1] + dp[i - 2];
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int climbStairs(int n) {
//        if (n <= 2) return n;
//        vector<int> dp(n + 1);
//        dp[1] = 1, dp[2] = 2;
//        for (int i = 3; i <= n; i++)
//        {
//            dp[i] = dp[i - 1] + dp[i - 2];
//        }
//        return dp[n];
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int timing[N], value[N];
//int f[N][N];
//
//int main()
//{
//    int t, m;
//    cin >> t >> m;
//    for (int i = 1; i <= m; i++)
//        cin >> timing[i] >> value[i];
//    for (int i = 1; i <= m; i++)
//    {
//        for (int j = 1; j <= t; j++)
//        {
//            if (j >= timing[i])
//                f[i][j] = max(f[i - 1][j], f[i - 1][j - timing[i]] + value[i]);
//            else
//                f[i][j] = f[i - 1][j];
//        }
//    }
//    cout << f[m][t] << endl;
//    return 0;
//}


#include <iostream>
using namespace std;

const int N = 3510, M = 13000;
int w[N], d[N];
int f[M];

int main()
{
    int n, m;
    cin >> n >> m;
    for (int i = 1; i <= n; i++)
        cin >> w[i] >> d[i];
    for (int i = 1; i <= n; i++)
    {
        for (int j = m; j >= w[i]; j--)
        {
            f[j] = max(f[j], f[j - w[i]] + d[i]);
        }
    }
    cout << f[m] << endl;
    return 0;
}