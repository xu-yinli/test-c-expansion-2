#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//long long dp[25][25];
//
//int main()
//{
//    int n, m, x, y;
//    cin >> n >> m >> x >> y;
//    x++;
//    y++;
//    dp[0][1] = 1;
//    for (int i = 1; i <= n + 1; i++)
//    {
//        for (int j = 1; j <= m + 1; j++)
//        {
//            if (i != x && j != y && abs(i - x) + abs(j - y) == 3 || (i == x && j == y))
//                dp[i][j] = 0;
//            else
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//        }
//    }
//    cout << dp[n + 1][m + 1] << endl;
//    return 0;
//}


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//    struct cmp
//    {
//        bool operator()(const ListNode* l1, const ListNode* l2)
//        {
//            return l1->val > l2->val;
//        }
//    };
//public:
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        priority_queue<ListNode*, vector<ListNode*>, cmp> heap;
//        for (auto l : lists)
//            if (l) heap.push(l);
//        ListNode* ret = new ListNode(0);
//        ListNode* cur = ret;
//        while (heap.size())
//        {
//            ListNode* t = heap.top();
//            heap.pop();
//            cur->next = t;
//            cur = t;
//            if (t->next) heap.push(t->next);
//        }
//        return ret->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* mergeTowList(ListNode* l1, ListNode* l2)
//    {
//        if (l1 == nullptr) return l2;
//        if (l2 == nullptr) return l1;
//
//        ListNode* newHead = new ListNode(0);
//        ListNode* cur1 = l1, * cur2 = l2, * prev = newHead;
//        while (cur1 && cur2)
//        {
//            if (cur1->val <= cur2->val)
//            {
//                prev->next = cur1;
//                prev = prev->next;
//                cur1 = cur1->next;
//            }
//            else
//            {
//                prev->next = cur2;
//                prev = prev->next;
//                cur2 = cur2->next;
//            }
//        }
//        if (cur1) prev->next = cur1;
//        if (cur2) prev->next = cur2;
//        return newHead->next;
//    }
//    ListNode* merge(vector<ListNode*>& lists, int left, int right)
//    {
//        if (left > right) return nullptr;
//        if (left == right) return lists[left];
//
//        int mid = left + (right - left) / 2;
//        //[left, mid] [mid+1, right]
//        ListNode* l1 = merge(lists, left, mid);
//        ListNode* l2 = merge(lists, mid + 1, right);
//
//        return mergeTowList(l1, l2);
//    }
//    ListNode* mergeKLists(vector<ListNode*>& lists) {
//        int n = lists.size();
//        return merge(lists, 0, n - 1);
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseKGroup(ListNode* head, int k) {
//        int n = 0;
//        ListNode* cur = head;
//        while (cur)
//        {
//            cur = cur->next;
//            n++;
//        }
//        n /= k;
//
//        ListNode* newHead = new ListNode(0);
//        ListNode* prev = newHead;
//        cur = head;
//        for (int i = 0; i < n; i++)
//        {
//            ListNode* tmp = cur;
//            for (int j = 0; j < k; j++)
//            {
//                ListNode* next = cur->next;
//                cur->next = prev->next;
//                prev->next = cur;
//                cur = next;
//            }
//            prev = tmp;
//        }
//        prev->next = cur;
//        return newHead->next;
//    }
//};


/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    vector<vector<int>> levelOrder(Node* root) {
//        if (root == nullptr) return res;
//        queue<Node*> q;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> v;
//            int n = q.size();
//            while (n--)
//            {
//                auto node = q.front();
//                q.pop();
//                v.push_back(node->val);
//                for (auto child : node->children)
//                    if (child != nullptr)
//                        q.push(child);
//            }
//            res.push_back(v);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
//        if (root == nullptr) return res;
//        queue<TreeNode*> q;
//        q.push(root);
//        int level = 1;
//        while (q.size())
//        {
//            vector<int> v;
//            int n = q.size();
//            while (n--)
//            {
//                auto node = q.front();
//                q.pop();
//                v.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            if (level % 2 == 0) reverse(v.begin(), v.end());
//            res.push_back(v);
//            level++;
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int widthOfBinaryTree(TreeNode* root) {
//        vector<pair<TreeNode*, unsigned int>> q;
//        q.push_back({ root, 1 });
//        unsigned int res = 0;
//        while (q.size())
//        {
//            auto& [x1, y1] = q[0];
//            auto& [x2, y2] = q.back();
//            res = max(res, y2 - y1 + 1);
//
//            vector<pair<TreeNode*, unsigned int>> tmp;
//            for (auto& [x, y] : q)
//            {
//                if (x->left) tmp.push_back({ x->left, y * 2 });
//                if (x->right) tmp.push_back({ x->right, y * 2 + 1 });
//            }
//            q = tmp;
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int widthOfBinaryTree(TreeNode* root) {
//        vector<pair<TreeNode*, unsigned int>> q;
//        q.push_back({ root, 1 });
//        unsigned int res = 0;
//        while (q.size())
//        {
//            auto& y1 = q[0].second;
//            auto& y2 = q.back().second;
//            res = max(res, y2 - y1 + 1);
//
//            vector<pair<TreeNode*, unsigned int>> tmp;
//            for (auto& [x, y] : q)
//            {
//                if (x->left) tmp.push_back({ x->left, y * 2 });
//                if (x->right) tmp.push_back({ x->right, y * 2 + 1 });
//            }
//            q = tmp;
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> largestValues(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            int max = INT_MIN;
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                if (node->val > max) max = node->val;
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            res.push_back(max);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int minPathSum(vector<vector<int>>& grid) {
//        int n = grid.size(), m = grid[0].size();
//        vector<vector<int>> dp(n, vector<int>(m));
//        dp[0][0] = grid[0][0];
//        for (int i = 1; i < n; i++) dp[i][0] = dp[i - 1][0] + grid[i][0];
//        for (int j = 1; j < m; j++) dp[0][j] = dp[0][j - 1] + grid[0][j];
//        for (int i = 1; i < n; i++)
//            for (int j = 1; j < m; j++)
//                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
//        return dp[n - 1][m - 1];
//    }
//};


//class Solution {
//public:
//    int minPathSum(vector<vector<int>>& grid) {
//        int n = grid.size(), m = grid[0].size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1, INT_MAX));
//        dp[0][1] = 0;
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= m; j++)
//                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int calculateMinimumHP(vector<vector<int>>& dungeon) {
//        int n = dungeon.size(), m = dungeon[0].size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1, INT_MAX));
//        dp[n][m - 1] = dp[n - 1][m] = 1;
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = m - 1; j >= 0; j--)
//            {
//                dp[i][j] = min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j];
//                dp[i][j] = max(1, dp[i][j]);
//            }
//        }
//        return dp[0][0];
//    }
//};


//class Solution {
//public:
//    //dp[i][0] - 不预约
//    //dp[i][1] - 预约 
//    int massage(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 0) return 0;
//        vector<vector<int>> dp(n, vector<int>(2));
//        dp[0][1] = nums[0];
//        for (int i = 1; i < n; i++)
//        {
//            dp[i][0] = max(dp[i - 1][0], dp[i - 1][1]);
//            dp[i][1] = dp[i - 1][0] + nums[i];
//        }
//        return max(dp[n - 1][0], dp[n - 1][1]);
//    }
//};


//class Solution {
//public:
//    //f[i] - 预约 
//    //g[i] - 不预约
//    int massage(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 0) return 0;
//        vector<int> f(n);
//        vector<int> g(n);
//        f[0] = nums[0];
//        for (int i = 1; i < n; i++)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[n - 1], g[n - 1]);
//    }
//};