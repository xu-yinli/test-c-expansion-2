#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int strStr(string haystack, string needle) {
//        int n = haystack.size(), m = needle.size();
//        for (int i = 0; i + m <= n; i++)
//        {
//            bool flag = true;
//            for (int j = 0; j < m; j++)
//            {
//                if (haystack[i + j] != needle[j])
//                {
//                    flag = false;
//                    break;
//                }
//            }
//            if (flag) return i;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    vector<int> plusOne(vector<int>& digits) {
//        int n = digits.size();
//        vector<int> res(n + 1);
//        for (int i = n - 1; i >= 0; i--)
//        {
//            if (digits[i] != 9)
//            {
//                digits[i]++;
//                return digits;
//            }
//            else
//            {
//                digits[i] = 0;
//            }
//        }
//        res[0] = 1;
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param input int整型vector
//     * @param k int整型
//     * @return int整型vector
//     */
//    vector<int> GetLeastNumbers_Solution(vector<int>& input, int k) {
//        int n = input.size();
//        if (n <= 0 || k <= 0 || k > n)
//            return res;
//        priority_queue<int> q;
//        for (int i = 0; i < n; i++)
//        {
//            int x = input[i];
//            if (i < k)
//            {
//                q.push(x);
//            }
//            else
//            {
//                if (x < q.top())
//                {
//                    q.pop();
//                    q.push(x);
//                }
//            }
//        }
//        for (int i = 0; i < k; i++)
//        {
//            int t = q.top();
//            q.pop();
//            res.push_back(t);
//        }
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> inventoryManagement(vector<int>& stock, int cnt) {
//        int n = stock.size();
//        if (n == 0 || cnt == 0 || cnt > n) return res;
//        priority_queue<int> q;
//        for (int i = 0; i < n; i++)
//        {
//            int x = stock[i];
//            if (i < cnt)
//            {
//                q.push(x);
//            }
//            else
//            {
//                if (x < q.top())
//                {
//                    q.pop();
//                    q.push(x);
//                }
//            }
//        }
//        for (int i = 0; i < cnt; i++)
//        {
//            res.push_back(q.top());
//            q.pop();
//        }
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


//class Solution {
//public:
//    int numberOfArithmeticSlices(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n);
//        int sum = 0;
//        for (int i = 2; i < n; i++)
//        {
//            if (nums[i - 1] - nums[i - 2] == nums[i] - nums[i - 1])
//                dp[i] = dp[i - 1] + 1;
//            else dp[i] = 0;
//            sum += dp[i];
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    int n;
//    int path, sum;
//public:
//    void dfs(vector<int>& nums, int pos)
//    {
//        sum += path;
//        for (int i = pos; i < n; i++)
//        {
//            path ^= nums[i];
//            dfs(nums, i + 1);
//            path ^= nums[i];
//        }
//    }
//    int subsetXORSum(vector<int>& nums) {
//        n = nums.size();
//        dfs(nums, 0);
//        return sum;
//    }
//};