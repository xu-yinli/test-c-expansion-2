#define _CRT_SECURE_NO_WARNINGS 1

//单例模式
//#include <iostream>
//#include <unordered_map>
//#include <vector>
//
//using namespace std;
//
//class Shopping
//{
//public:
//    static Shopping& getInstance()
//    {
//        static Shopping _eton;
//        return _eton;
//    }
//    void add(string item, int num)
//    {
//        order.push_back(item);
//        shopcar[item] += num;
//    }
//    void show()
//    {
//        for (auto e : order)
//        {
//            cout << e << ' ' << shopcar.at(e) << endl;
//        }
//    }
//    Shopping(const Shopping&) = delete;
//    ~Shopping() {}
//private:
//    Shopping()
//    {}
//private:
//    unordered_map<string, int> shopcar;
//    vector<string> order;
//};
//
//int main()
//{
//    Shopping& eton = Shopping::getInstance();
//    string item;
//    int num;
//    while (cin >> item >> num)
//    {
//        eton.add(item, num);
//    }
//    eton.show();
//    return 0;
//}


//工厂方法模式
//#include <iostream>
//#include <memory>
//
//using namespace std;
//
//class Block
//{
//public:
//    virtual void produce() = 0;
//};
//
//class Circle : public Block
//{
//public:
//    void produce() override
//    {
//        cout << "Circle Block" << endl;
//    }
//};
//
//class Square : public Block
//{
//public:
//    void produce() override
//    {
//        cout << "Square Block" << endl;
//    }
//};
//
//class BlockFactory
//{
//public:
//    virtual shared_ptr<Block> create() = 0;
//};
//
//class CircleBlockFactory : public BlockFactory
//{
//public:
//    shared_ptr<Block> create() override
//    {
//        return make_shared<Circle>();
//    }
//};
//
//class SquareBlockFactory : public BlockFactory
//{
//public:
//    shared_ptr<Block> create() override
//    {
//        return make_shared<Square>();
//    }
//};
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        string type;
//        int num;
//        cin >> type >> num;
//        if (type == "Circle")
//        {
//            while (num--)
//            {
//                std::shared_ptr<BlockFactory> bf(new CircleBlockFactory());
//                std::shared_ptr<Block> b = bf->create();
//                b->produce();
//            }
//        }
//        else if (type == "Square")
//        {
//            while (num--)
//            {
//                std::shared_ptr<BlockFactory> bf(new SquareBlockFactory());
//                std::shared_ptr<Block> b = bf->create();
//                b->produce();
//            }
//        }
//    }
//    return 0;
//}


//抽象工厂模式
//#include <iostream>
//#include <memory>
//
//using namespace std;
//
//class Modern
//{
//public:
//    virtual void show() = 0;
//};
//
//class ModernChair : public Modern
//{
//public:
//    void show() override
//    {
//        cout << "modern chair" << endl;
//    }
//};
//
//class ModernSofa : public Modern
//{
//public:
//    void show() override
//    {
//        cout << "modern sofa" << endl;
//    }
//};
//
//class Classical
//{
//public:
//    virtual void show() = 0;
//};
//
//class ClassicalChair : public Classical
//{
//public:
//    void show() override
//    {
//        cout << "classical chair" << endl;
//    }
//};
//
//class ClassicalSofa : public Classical
//{
//public:
//    void show() override
//    {
//        cout << "classical sofa" << endl;
//    }
//};
//
//class Factory
//{
//public:
//    virtual shared_ptr<Modern> getModern(const string& name) = 0;
//    virtual shared_ptr<Classical> getClassical(const string& name) = 0;
//};
//
//class ModernFactory : public Factory
//{
//public:
//    shared_ptr<Modern> getModern(const std::string& name) override
//    {
//        if (name == "chair")
//        {
//            return make_shared<ModernChair>();
//        }
//        else
//        {
//            return make_shared<ModernSofa>();
//        }
//    }
//
//    shared_ptr<Classical> getClassical(const string& name) override
//    {
//        return nullptr;
//    }
//};
//
//class ClassicalFactory : public Factory
//{
//public:
//    shared_ptr<Modern> getModern(const std::string& name) override
//    {
//        return nullptr;
//    }
//
//    shared_ptr<Classical> getClassical(const string& name) override
//    {
//        if (name == "sofa")
//        {
//            return make_shared<ClassicalSofa>();
//        }
//        else
//        {
//            return make_shared<ClassicalChair>();
//        }
//    }
//};
//
//class FactoryProducer
//{
//public:
//    static shared_ptr<Factory> create(const std::string& name) //直接引用静态成员接口，无需实例化对象即可完成工厂的创建
//    {
//        if (name == "modern")
//        {
//            return make_shared<ModernFactory>();
//        }
//        else
//        {
//            return make_shared<ClassicalFactory>();
//        }
//    }
//};
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        string type;
//        cin >> type;
//        shared_ptr<Factory> f = FactoryProducer::create(type);
//        if (type == "modern")
//        {
//            shared_ptr<Modern> m = f->getModern("chair");
//            m->show();
//            m = f->getModern("sofa");
//            m->show();
//        }
//        else
//        {
//            shared_ptr<Classical> m = f->getClassical("chair");
//            m->show();
//            m = f->getClassical("sofa");
//            m->show();
//        }
//    }
//    return 0;
//}


//建造者模式
//#include <iostream>
//#include <memory>
//
//using namespace std;
//
//class Bike
//{
//public:
//    Bike() {}
//    virtual void setFrame() = 0;
//
//    virtual void setTires() = 0;
//};
//
//class MountainBike : public Bike
//{
//public:
//    void setFrame() override
//    {
//        cout << "Aluminum Frame" << ' ';
//    }
//
//    void setTires() override
//    {
//        cout << "Knobby Tires" << endl;
//    }
//};
//
//class RoadBike : public Bike
//{
//public:
//    void setFrame() override
//    {
//        cout << "Carbon Frame" << ' ';
//    }
//
//    void setTires() override
//    {
//        cout << "Slim Tires" << endl;
//    }
//};
//
//class Builder
//{
//public:
//    virtual void buildFrame() = 0;
//    virtual void buildTires() = 0;
//};
//
//class MountainBikeBuilder : public Builder
//{
//public:
//    MountainBikeBuilder()
//        :_bike(new MountainBike())
//    {}
//    void buildFrame() override
//    {
//        _bike->setFrame();
//    }
//    void buildTires() override
//    {
//        _bike->setTires();
//    }
//private:
//    std::shared_ptr<Bike> _bike;
//};
//
//class RoadBikeBuilder : public Builder
//{
//public:
//    RoadBikeBuilder()
//        :_bike(new RoadBike())
//    {}
//    void buildFrame() override
//    {
//        _bike->setFrame();
//    }
//    void buildTires() override
//    {
//        _bike->setTires();
//    }
//private:
//    std::shared_ptr<Bike> _bike;
//};
//
//class Director
//{
//public:
//    Director(Builder* builder)
//        :_builder(builder)
//    {}
//    void construct()
//    {
//        _builder->buildFrame();
//        _builder->buildTires();
//    }
//private:
//    unique_ptr<Builder> _builder;
//};
//
//int main()
//{
//    int n;
//    cin >> n;
//
//    for (int i = 0; i < n; i++)
//    {
//        string type;
//        cin >> type;
//        if (type == "mountain")
//        {
//            Builder* builder = new MountainBikeBuilder();
//            unique_ptr<Director> director(new Director(builder));
//            director->construct();
//        }
//        else
//        {
//            Builder* builder = new RoadBikeBuilder();
//            unique_ptr<Director> director(new Director(builder));
//            director->construct();
//        }
//    }
//    return 0;
//}


//代理模式
//#include <iostream>
//
//using namespace std;
//
//class RentHouse
//{
//public:
//    virtual void rentHouse(int area) = 0;
//};
//
//class Landlord : public RentHouse
//{
//public:
//    void rentHouse(int area) override
//    {
//        cout << "YES" << endl;
//    }
//};
//
//class Proxy : public RentHouse
//{
//public:
//    void rentHouse(int area) override
//    {
//        if (area > 100)
//        {
//            _landlord.rentHouse(area);
//        }
//        else
//        {
//            cout << "NO" << endl;
//        }
//    }
//private:
//    Landlord _landlord;
//};
//
//int main()
//{
//    Proxy proxy;
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        int area;
//        cin >> area;
//        proxy.rentHouse(area);
//    }
//    return 0;
//}