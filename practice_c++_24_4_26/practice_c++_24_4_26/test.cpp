#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n, m, a, b;
//    cin >> n >> m >> a >> b;
//    long long cnt = 0;
//    for (long long x = 0; x <= min(n / 2, m); x++) //枚举1号礼包的个数
//    {
//        long long y = min(n - 2 * x, (m - x) / 2); //计算2号礼包的个数
//        cnt = max(cnt, a * x + b * y);
//    }
//    cout << cnt << endl;
//    return 0;
//}


//class Solution {
//public:
//    //f[i] - 偷到i位置时，偷nums[i]，此时的最高金额
//    //g[i] - 偷到i位置时，不偷nums[i]，此时的最高金额
//    int rob1(vector<int>& nums, int left, int right)
//    {
//        if (left > right) return 0;
//        int n = nums.size();
//        vector<int> f(n);
//        vector<int> g(n);
//        f[left] = nums[left];
//        for (int i = left + 1; i <= right; i++)
//        {
//            f[i] = g[i - 1] + nums[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[right], g[right]);
//    }
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1) return nums[0];
//        int res1 = rob1(nums, 1, n - 1);
//        int res2 = rob1(nums, 0, n - 2);
//        return max(res1, res2);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    unordered_map<TreeNode*, int> hash;
//public:
//    int rob(TreeNode* root) {
//        if (root == nullptr) return 0;
//        if (root->left == nullptr && root->right == nullptr) return root->val;
//        if (hash[root]) return hash[root];
//        int res1 = root->val;
//        if (root->left) res1 += rob(root->left->left) + rob(root->left->right);
//        if (root->right) res1 += rob(root->right->left) + rob(root->right->right);
//        int res2 = rob(root->left) + rob(root->right);
//        hash[root] = max(res1, res2);
//        return max(res1, res2);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    //[0] - 表示不偷
//    //[1] - 表示偷
//    vector<int> robTree(TreeNode* node)
//    {
//        if (node == nullptr) return { 0, 0 };
//        vector<int> left = robTree(node->left);
//        vector<int> right = robTree(node->right);
//        int res1 = max(left[0], left[1]) + max(right[0], right[1]);
//        int res2 = node->val + left[0] + right[0];
//        return { res1, res2 };
//    }
//    int rob(TreeNode* root) {
//        vector<int> res = robTree(root);
//        return max(res[0], res[1]);
//    }
//};


//class Solution {
//public:
//    //f[i] - 选到i位置时，arr[i]必选，此时能获得的最大点数
//    //g[i] - 选到i位置时，arr[i]选，此时能获得的最大点数
//    int deleteAndEarn(vector<int>& nums) {
//        const int N = 10001;
//        int arr[N] = { 0 };
//        for (auto x : nums)
//            arr[x] += x;
//        vector<int> f(N);
//        vector<int> g(N);
//        for (int i = 1; i < N; i++)
//        {
//            f[i] = g[i - 1] + arr[i];
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[N - 1], g[N - 1]);
//    }
//};


//class Solution {
//public:
//    //dp[i][0] - 粉刷到i位置时，最后一个位置粉刷上红色，此时的最少花费成本
//    //dp[i][1] - 粉刷到i位置时，最后一个位置粉刷上蓝色，此时的最少花费成本
//    //dp[i][2] - 粉刷到i位置时，最后一个位置粉刷上绿色，此时的最少花费成本
//    int minCost(vector<vector<int>>& costs) {
//        int n = costs.size();
//        vector<vector<int>> dp(n + 1, vector<int>(3));
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i][0] = min(dp[i - 1][1], dp[i - 1][2]) + costs[i - 1][0];
//            dp[i][1] = min(dp[i - 1][0], dp[i - 1][2]) + costs[i - 1][1];
//            dp[i][2] = min(dp[i - 1][0], dp[i - 1][1]) + costs[i - 1][2];
//        }
//        return min(dp[n][0], min(dp[n][1], dp[n][2]));
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @return int整型
//     */
//    int NumberOf1(int n) {
//        int cnt = 0;
//        while (n)
//        {
//            n &= (n - 1);
//            cnt++;
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @return int整型
//     */
//    int NumberOf1(int n) {
//        int cnt = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            if ((n & (1 << i)) != 0)
//                cnt++;
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param n int整型
//     * @return int整型
//     */
//    int NumberOf1(int n) {
//        int cnt = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            if (((n >> i) & 1) == 1)
//                cnt++;
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    bool isPowerOfTwo(int n) {
//        if (n > 0 && (n & (n - 1)) == 0) return true;
//        else return false;
//    }
//};