#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//LL gcd(LL a, LL b)
//{
//	return b ? gcd(b, a % b) : a;
//}
//
//LL lcm(LL a, LL b)
//{
//	return (a * b) / gcd(a, b);
//}
//
//int main()
//{
//	int t;
//	cin >> t;
//	while (t--)
//	{
//		LL x, y;
//		cin >> x >> y;
//		int a = 1;
//		LL b = lcm(x, y) / gcd(x, y);
//		cout << a << ' ' << b << endl;
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int t;
//	cin >> t;
//	while (t--)
//	{
//		int n, x, a, b; //参赛者人数 想吃板烧的人数 麦辣价格 板烧价格
//		cin >> n >> x >> a >> b;
//		int sum = x * b + (n - x) * a;
//		cout << sum << endl;
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//LL qmi(int a, int b, int p)
//{
//    LL res = 1;
//    while (b)
//    {
//        if (b & 1) res = (LL)res * a % p;
//        b >>= 1;
//        a = (LL)a * a % p;
//    }
//    return res;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b, p;
//        cin >> a >> b >> p;
//        cout << qmi(a, b, p) << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//typedef long long LL;
//
//LL qmi(int a, int b, int p)
//{
//    LL res = 1;
//    while (b)
//    {
//        if (b & 1) res = (LL)res * a % p;
//        a = (LL)a * a % p;
//        b >>= 1;
//    }
//    return res;
//}
//
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, p;
//        cin >> a >> p;
//        if (a % p == 0) cout << "impossible" << endl;
//        else cout << qmi(a, p - 2, p) << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//const int N = 1e6 + 10, MOD= 998244353;
//int a[N];
//
//LL qmi(int a, int b, int p)
//{
//	LL res = 1;
//	while (b)
//	{
//		if (b & 1) res = (LL)res * a % p;
//		a = (LL)a * a % p;
//		b >>= 1;
//	}
//	return res;
//}
//
//int main()
//{
//	int n;
//	cin >> n;
//	LL sum = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		cin >> a[i];
//		sum += a[i];
//		sum %= MOD;
//	}
//	cout << (sum*qmi(n, MOD-2, MOD))%MOD << endl;
//	return 0;
//}


/////////////////////////////////////////////////////


//#include <iostream>
//#include <cmath>
//using namespace std;
//
//const int N = 101;
//double INF = 1e9 + 7;
//int x[N], y[N];
//
//double dis(int a, int b, int c, int d)
//{
//	return 1.0 * sqrt((c - a) * (c - a) + (d - b) * (d - b));
//}
//
//int main()
//{
//	int t;
//	cin >> t;
//	while (t--)
//	{
//		int n;
//		cin >> n;
//		if (n <= 2)
//		{
//			cout << -1 << endl;
//			continue;
//		}
//		double res = INF;
//		for (int i = 1; i <= n; i++)
//			cin >> x[i] >> y[i];
//		for (int i = 1; i <= n; i++)
//		{
//			for (int j = i + 1; j <= n; j++)
//			{
//				for (int k = j + 1; k <= n; k++)
//				{
//					double a = dis(x[i], y[i], x[j], y[j]);
//					double b = dis(x[i], y[i], x[k], y[k]);
//					double c = dis(x[j], y[j], x[k], y[k]);
//					double p = (a + b + c) / 2.0;
//					double s = sqrt(p * (p - a) * (p - b) * (p - c));
//					if(s > 0) res = min(res, s);
//				}
//			}
//		}
//		if (res == INF) cout << -1 << endl;
//		else cout << res << endl;
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int gcd(int a, int b)
//{
//	return b ? gcd(b, a % b) : a;
//}
//
//int lcm(int a, int b)
//{
//	return (a * b) / gcd(a, b);
//}
//
//int main()
//{
//	int t;
//	cin >> t;
//	while (t--)
//	{
//		int a, b;
//		cin >> a >> b;
//		if (a == b)
//		{
//			cout << 0 << endl;
//			continue;
//		}
//		if (b % a == 0)
//		{
//			cout << b << endl;
//			continue;
//		}
//		else
//		{
//			int c = gcd(a, b);
//			if (c != 1) //说明不互质 要找最大公因子
//			{
//				int cost = 0;
//				cost += lcm(a, c);
//				cost += lcm(c, b);
//				cout << cost << endl;
//			}
//			else
//			{
//				for (int i = 2; i <= b / i; i++)
//				{
//					if (b % i == 0)
//					{
//						int cost = 0;
//						cost += lcm(a, i) + lcm(i, b);
//						cout << cost << endl;
//						break;
//					}
//				}
//			}
//		}
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e3 + 10;
//int x[N], y[N];
//int a[11];
//int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//
//int main()
//{
//	int n, m, k;
//	cin >> n >> m >> k;
//	for(int i=1; i<=k; i++)
//	{
//		cin >> x[i] >> y[i] >> a[i];
//		for (int k = 0; k < 4; k++)
//		{
//			int a = x[i] + dx[k], b = y[i] + dy[k];
//			if (a<1 || a>n || b<1 || b>m)
//				continue;
//
//		}
//	}
//	return 0;
//}