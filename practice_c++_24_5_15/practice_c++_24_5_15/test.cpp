#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    int res = n;
//    for (int i = 0; i < m - 1; i++)
//        res = res * (n - 1) % 109;
//    cout << res << endl;
//    return 0;
//}


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     * 计算成功举办活动需要多少名主持人
//     * @param n int整型 有n个活动
//     * @param startEnd int整型vector<vector<>> startEnd[i][0]用于表示第i个活动的开始时间，startEnd[i][1]表示第i个活动的结束时间
//     * @return int整型
//     */
//    int minmumNumberOfHost(int n, vector<vector<int> >& startEnd) {
//        sort(startEnd.begin(), startEnd.end());
//        priority_queue<int, vector<int>, greater<int>> heap;
//        heap.push(startEnd[0][1]);
//        for (int i = 1; i < n; i++)
//        {
//            if (startEnd[i][0] >= heap.top())
//            {
//                heap.pop();
//                heap.push(startEnd[i][1]);
//            }
//            else
//            {
//                heap.push(startEnd[i][1]);
//            }
//        }
//        return heap.size();
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    int maxSum = INT_MIN;
//public:
//    int maxGain(TreeNode* node)
//    {
//        if (node == nullptr) return 0;
//        int leftGain = max(0, maxGain(node->left));
//        int rightGain = max(0, maxGain(node->right));
//        int sum = leftGain + rightGain + node->val;
//        maxSum = max(maxSum, sum);
//        return node->val + max(leftGain, rightGain);
//    }
//    int maxPathSum(TreeNode* root) {
//        maxGain(root);
//        return maxSum;
//    }
//};