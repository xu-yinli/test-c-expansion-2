#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int maxDepth(TreeNode* root) {
//        if (root == nullptr) return 0;
//        int depth = 0;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            depth++;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* tmp = q.front();
//                q.pop();
//                if (tmp->left) q.push(tmp->left);
//                if (tmp->right) q.push(tmp->right);
//            }
//        }
//        return depth;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int minDepth(TreeNode* root) {
//        if (root == nullptr) return 0;
//        int depth = 0;
//        int res = 100010;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            depth++;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* tmp = q.front();
//                q.pop();
//                if (tmp->left) q.push(tmp->left);
//                if (tmp->right) q.push(tmp->right);
//                if (tmp->left == nullptr && tmp->right == nullptr) res = min(res, depth);
//            }
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> largestValues(TreeNode* root) {
//        if (root == nullptr) return res;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int k = INT_MIN;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* tmp = q.front();
//                q.pop();
//                k = max(k, tmp->val);
//                if (tmp->left) q.push(tmp->left);
//                if (tmp->right) q.push(tmp->right);
//            }
//            res.push_back(k);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<double> res;
//public:
//    vector<double> averageOfLevels(TreeNode* root) {
//        if (root == nullptr) return res;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            double sum = 0;
//            int n = q.size();
//            for (int i = 0; i < n; i++)
//            {
//                TreeNode* tmp = q.front();
//                q.pop();
//                sum += tmp->val;
//                if (tmp->left) q.push(tmp->left);
//                if (tmp->right) q.push(tmp->right);
//            }
//            res.push_back(1.0 * sum / n);
//        }
//        return res;
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    vector<Node*> children;
//
//    Node() {}
//
//    Node(int _val) {
//        val = _val;
//    }
//
//    Node(int _val, vector<Node*> _children) {
//        val = _val;
//        children = _children;
//    }
//};
//*/
//
//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    vector<vector<int>> levelOrder(Node* root) {
//        if (root == nullptr) return res;
//        queue<Node*> q;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> v;
//            int n = q.size();
//            while (n--)
//            {
//                Node* tmp = q.front();
//                q.pop();
//                v.push_back(tmp->val);
//                for (auto child : tmp->children)
//                    if (child)
//                        q.push(child);
//            }
//            res.push_back(v);
//        }
//        return res;
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    Node* left;
//    Node* right;
//    Node* next;
//
//    Node() : val(0), left(NULL), right(NULL), next(NULL) {}
//
//    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}
//
//    Node(int _val, Node* _left, Node* _right, Node* _next)
//        : val(_val), left(_left), right(_right), next(_next) {}
//};
//*/
//
//class Solution {
//public:
//    Node* connect(Node* root) {
//        if (root == NULL) return NULL;
//        queue<Node*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            while (n--)
//            {
//                Node* tmp = q.front();
//                q.pop();
//                if (n == 0) tmp->next = NULL;
//                else tmp->next = q.front();
//                if (tmp->left) q.push(tmp->left);
//                if (tmp->right) q.push(tmp->right);
//            }
//        }
//        return root;
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    Node* left;
//    Node* right;
//    Node* next;
//
//    Node() : val(0), left(NULL), right(NULL), next(NULL) {}
//
//    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}
//
//    Node(int _val, Node* _left, Node* _right, Node* _next)
//        : val(_val), left(_left), right(_right), next(_next) {}
//};
//*/
//
//class Solution {
//public:
//    Node* connect(Node* root) {
//        if (root == NULL) return NULL;
//        queue<Node*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            while (n--)
//            {
//                Node* tmp = q.front();
//                q.pop();
//                if (n == 0) tmp->next = NULL;
//                else tmp->next = q.front();
//                if (tmp->left) q.push(tmp->left);
//                if (tmp->right) q.push(tmp->right);
//            }
//        }
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> rightSideView(TreeNode* root) {
//        if (root == nullptr) return res;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int k = 0;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* tmp = q.front();
//                q.pop();
//                if (n == 0) k = tmp->val;
//                if (tmp->left) q.push(tmp->left);
//                if (tmp->right) q.push(tmp->right);
//            }
//            res.push_back(k);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* invertTree(TreeNode* root) {
//        if (root == nullptr) return nullptr;
//        invertTree(root->left);
//        invertTree(root->right);
//        swap(root->left, root->right);
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    bool cmp(TreeNode* node1, TreeNode* node2)
//    {
//        if (node1 == nullptr && node2 == nullptr) return true;
//        if (node1 == nullptr || node2 == nullptr) return false;
//        if (node1->val == node2->val)
//            return cmp(node1->right, node2->left) && cmp(node1->left, node2->right);
//        return false;
//    }
//    bool isSymmetric(TreeNode* root) {
//        if (root == nullptr) return true;
//        return cmp(root->left, root->right);
//    }
//};


///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param root TreeNode类
//     * @return int整型vector
//     */
//    vector<int> preorderTraversal(TreeNode* root) {
//        if (root == nullptr) return res;
//        res.push_back(root->val);
//        preorderTraversal(root->left);
//        preorderTraversal(root->right);
//        return res;
//    }
//};


///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param root TreeNode类
//     * @return int整型vector
//     */
//    vector<int> inorderTraversal(TreeNode* root) {
//        if (root == nullptr) return res;
//        inorderTraversal(root->left);
//        res.push_back(root->val);
//        inorderTraversal(root->right);
//        return res;
//    }
//};


///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param root TreeNode类
//     * @return int整型vector
//     */
//    vector<int> postorderTraversal(TreeNode* root) {
//        if (root == nullptr) return res;
//        postorderTraversal(root->left);
//        postorderTraversal(root->right);
//        res.push_back(root->val);
//        return res;
//    }
//};


///**
// * struct TreeNode {
// *	int val;
// *	struct TreeNode *left;
// *	struct TreeNode *right;
// *	TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// * };
// */
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param root TreeNode类
//     * @return int整型
//     */
//    int maxDepth(TreeNode* root) {
//        if (root == nullptr) return 0;
//        int depth = 0;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            depth++;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* tmp = q.front();
//                q.pop();
//                if (tmp->left) q.push(tmp->left);
//                if (tmp->right) q.push(tmp->right);
//            }
//        }
//        return depth;
//    }
//};


///*
//struct TreeNode {
//	int val;
//	struct TreeNode *left;
//	struct TreeNode *right;
//	TreeNode(int x) :
//			val(x), left(NULL), right(NULL) {
//	}
//};*/
//class Solution {
//public:
//	void inorder(TreeNode* root, TreeNode*& prev)
//	{
//		if (root == nullptr) return;
//		inorder(root->left, prev);
//		root->left = prev;
//		if (prev != nullptr) prev->right = root;
//		prev = root;
//		inorder(root->right, prev);
//	}
//	TreeNode* Convert(TreeNode* pRootOfTree) {
//		if (pRootOfTree == nullptr) return nullptr;
//		TreeNode* prev = nullptr;
//		inorder(pRootOfTree, prev);
//		TreeNode* newHead = pRootOfTree;
//		while (newHead->left)
//			newHead = newHead->left;
//		return newHead;
//	}
//};