#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    bool rotateString(string s, string goal) {
//        int n = s.size(), m = goal.size();
//        if (n != m) return false;
//        for (int i = 0; i < n; i++)
//        {
//            bool flag = true;
//            for (int j = 0; j < n; j++)
//            {
//                if (s[(i + j) % n] != goal[j])
//                {
//                    flag = false;
//                    break;
//                }
//            }
//            if (flag) return true;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    int countPrefixes(vector<string>& words, string s) {
//        int n = s.size();
//        int res = 0;
//        for (string word : words)
//        {
//            int m = word.size();
//            if (m > n) continue;
//            bool flag = true;
//            for (int i = 0; i < m; i++)
//            {
//                if (s[i] != word[i])
//                {
//                    flag = false;
//                    break;
//                }
//            }
//            if (flag) res++;
//        }
//        return res;
//    }
//};


//#include <iostream>
//#include <vector>
//#include <queue>
//#include <algorithm>
//using namespace std;
//
//struct tangle
//{
//    int x1, y1; //左下角
//    int x2, y2; //右上角
//    tangle(int a, int b, int c, int d)
//    {
//        x1 = a, y1 = b;
//        x2 = c, y2 = d;
//    }
//};
//
//// 判断两个矩形是否相交
//bool is_cross(const tangle& a, const tangle& b)
//{
//    return (a.x1 < b.x2 && b.x1 < a.x2 && a.y1 < b.y2 && b.y1 < a.y2);
//}
//
//int main()
//{
//    ios::sync_with_stdio(0);
//    cin.tie(0); cout.tie(0);
//    int n, l, h;
//    cin >> n >> l >> h;
//    vector<int> ready_x = { 0 };
//    vector<int> ready_y = { 0 };
//    vector<tangle> res;
//    for (int i = 0; i < n; i++)
//    {
//        int x, y;
//        cin >> x >> y;
//        bool flag = false; //标记当前矩形是否被成功放置
//        //遍历所有可能的x坐标起点
//        for (int j = 0; j < ready_x.size(); j++)
//        {
//            if (!flag)
//            {
//                //遍历所有可能的y坐标起点
//                for (int k = 0; k < ready_y.size(); k++)
//                {
//                    if (!flag)
//                    {
//                        int px = ready_x[j];
//                        int py = ready_y[k];
//                        if (px + x <= l && py + y <= h)
//                        {
//                            bool valid = true; //标记当前位置是否合法
//                            tangle tmp = tangle(px, py, px + x, py + y);
//                            for (int k = 0; k < res.size(); k++)
//                            {
//                                if (is_cross(tmp, res[k]))
//                                {
//                                    valid = false;
//                                    break;
//                                }
//                            }
//                            if (valid == true)
//                            {
//                                flag = true;
//                                cout << px << ' ' << py << endl;
//                                res.push_back(tmp);
//                                ready_x.push_back(tmp.x2);
//                                ready_y.push_back(tmp.y2);
//                                sort(ready_x.begin(), ready_x.end());
//                                sort(ready_y.begin(), ready_y.end());
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        if(!flag) cout << -1 << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int v[N], w[N];
//int dp1[N][N], dp2[N][N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 0; j <= m; j++)
//        {
//            dp1[i][j] = dp1[i - 1][j];
//            if (j >= v[i]) dp1[i][j] = max(dp1[i][j], dp1[i][j - v[i]] + w[i]);
//        }
//    }
//    cout << dp1[n][m] << endl;
//
//    for (int j = 1; j <= m; j++) dp2[0][j] = -1;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 0; j <= m; j++)
//        {
//            dp2[i][j] = dp2[i - 1][j];
//            if (j >= v[i] && dp2[i][j - v[i]] != -1) dp2[i][j] = max(dp2[i][j], dp2[i][j - v[i]] + w[i]);
//        }
//    }
//    if (dp2[n][m] == -1) cout << 0 << endl;
//    else cout << dp2[n][m] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int v[N], w[N];
//int dp1[N], dp2[N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = v[i]; j <= m; j++)
//        {
//            dp1[j] = max(dp1[j], dp1[j - v[i]] + w[i]);
//        }
//    }
//    cout << dp1[m] << endl;
//
//    for (int j = 1; j <= m; j++) dp2[j] = -1;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = v[i]; j <= m; j++)
//        {
//            if (dp2[j - v[i]] != -1) dp2[j] = max(dp2[j], dp2[j - v[i]] + w[i]);
//        }
//    }
//    if (dp2[m] == -1) cout << 0 << endl;
//    else cout << dp2[m] << endl;
//    return 0;
//}


#include <iostream>
using namespace std;

const int N = 1010;
int v[N], w[N];
int dp1[N], dp2[N];

int main()
{
    int n, m;
    cin >> n >> m;
    for (int i = 1; i <= n; i++)
        cin >> v[i] >> w[i];
    for (int i = 1; i <= n; i++)
    {
        for (int j = v[i]; j <= m; j++)
        {
            dp1[j] = max(dp1[j], dp1[j - v[i]] + w[i]);
        }
    }
    cout << dp1[m] << endl;

    for (int j = 1; j <= m; j++) dp2[j] = -0x3f3f3f3f;
    for (int i = 1; i <= n; i++)
    {
        for (int j = v[i]; j <= m; j++)
        {
            dp2[j] = max(dp2[j], dp2[j - v[i]] + w[i]);
        }
    }
    if (dp2[m] < 0) cout << 0 << endl;
    else cout << dp2[m] << endl;
    return 0;
}