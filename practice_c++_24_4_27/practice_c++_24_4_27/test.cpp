#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    string str;
//    cin >> str;
//    long long s = 0, h = 0, y = 0;
//    for (int i = 0; i < n; i++)
//    {
//        if (str[i] == 's') s++;
//        else if (str[i] == 'h') h += s;
//        else if (str[i] == 'y') y += h;
//    }
//    cout << y << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    string s;
//    cin >> s;
//    string t = "shy";
//    int m = t.size();
//    vector<vector<long long>> dp(n + 1, vector<long long>(m + 1));
//    for (int i = 0; i <= n; i++) dp[i][0] = 1;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            if (s[i - 1] == t[j - 1])
//                dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];
//            else
//                dp[i][j] = dp[i - 1][j];
//        }
//    }
//    cout << dp[n][m] << endl;
//    return 0;
//}


//class Solution {
//public:
//    int numTrees(int n) {
//        vector<int> dp(n + 1);
//        dp[0] = 1;
//        for (int i = 1; i <= n; i++)
//            for (int j = 1; j <= i; j++)
//                dp[i] += dp[j - 1] * dp[i - j];
//        return dp[n];
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    bool evaluateTree(TreeNode* root) {
//        if (root->left == nullptr && root->right == nullptr)
//        {
//            if (root->val == 0) return false;
//            if (root->val == 1) return true;
//        }
//        bool left = evaluateTree(root->left);
//        bool right = evaluateTree(root->right);
//        if (root->val == 2) return left || right;
//        return left && right;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int dfs(TreeNode* root, int prevSum)
//    {
//        if (root == nullptr) return 0;
//        prevSum = prevSum * 10 + root->val;
//        if (root->left == nullptr && root->right == nullptr) return prevSum;
//        int res = 0;
//        if (root->left) res += dfs(root->left, prevSum);
//        if (root->right) res += dfs(root->right, prevSum);
//        return res;
//    }
//    int sumNumbers(TreeNode* root) {
//        return dfs(root, 0);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int dfs(TreeNode* root, int prevSum)
//    {
//        if (root == nullptr) return 0;
//        prevSum = prevSum * 10 + root->val;
//        if (root->left == nullptr && root->right == nullptr) return prevSum;
//        else return dfs(root->left, prevSum) + dfs(root->right, prevSum);
//    }
//    int sumNumbers(TreeNode* root) {
//        return dfs(root, 0);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* pruneTree(TreeNode* root) {
//        if (root == nullptr) return nullptr;
//        root->left = pruneTree(root->left);
//        root->right = pruneTree(root->right);
//        if (root->left == nullptr && root->right == nullptr && root->val == 0)
//            return nullptr;
//        return root;
//    }
//};