#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//    vector<int> path;
//public:
//    void dfs(TreeNode* root, int targetSum)
//    {
//        if (root == nullptr) return;
//        path.push_back(root->val);
//        targetSum -= root->val;
//        if (root->left == nullptr && root->right == nullptr && targetSum == 0)
//            res.push_back(path);
//        dfs(root->left, targetSum);
//        dfs(root->right, targetSum);
//        path.pop_back();
//    }
//    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {
//        dfs(root, targetSum);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//    vector<int> path;
//public:
//    void dfs(TreeNode* root, int targetSum)
//    {
//        if (root == nullptr) return;
//        path.push_back(root->val);
//        targetSum -= root->val;
//        if (root->left == nullptr && root->right == nullptr && targetSum == 0)
//            res.push_back(path);
//        dfs(root->left, targetSum);
//        dfs(root->right, targetSum);
//        path.pop_back();
//    }
//    vector<vector<int>> pathTarget(TreeNode* root, int target) {
//        dfs(root, target);
//        return res;
//    }
//};


//class Solution {
//public:
//    bool isPalindrome(int x) {
//        if (x < 0) return false;
//        if (x != 0 && x % 10 == 0) return false;
//        string s = to_string(x);
//        int left = 0, right = s.size() - 1;
//        while (left < right)
//        {
//            if (s[left] != s[right])
//                return false;
//            left++;
//            right--;
//        }
//        return true;
//    }
//};


//class Solution {
//private:
//    typedef long long LL;
//public:
//    int dfs(LL n)
//    {
//        if (n == 1) return 0;
//        if (n % 2 == 0) return 1 + dfs(n / 2);
//        else return 1 + min(dfs(n - 1), dfs(n + 1));
//    }
//    int integerReplacement(int n) {
//        return dfs(n);
//    }
//};


//class Solution {
//private:
//    typedef long long LL;
//    unordered_map<int, int> memo;
//public:
//    int dfs(LL n)
//    {
//        if (memo.count(n)) return memo[n];
//        if (n == 1)
//        {
//            memo[1] = 0;
//            return 0;
//        }
//        if (n % 2 == 0)
//        {
//            memo[n] = 1 + dfs(n / 2);
//            return memo[n];
//        }
//        else
//        {
//            memo[n] = 1 + min(dfs(n - 1), dfs(n + 1));
//            return memo[n];
//        }
//    }
//    int integerReplacement(int n) {
//        return dfs(n);
//    }
//};


//class Solution {
//public:
//    int integerReplacement(int n) {
//        int res = 0;
//        while (n > 1)
//        {
//            if (n % 2 == 0)
//            {
//                n /= 2;
//                res++;
//            }
//            else
//            {
//                if (n == 3)
//                {
//                    res += 2;
//                    n = 1;
//                }
//                else if (n % 4 == 1)
//                {
//                    n = (n - 1) / 2;
//                    res += 2;
//                }
//                else if (n % 4 == 3)
//                {
//                    //n=(n+1)/2 会溢出
//                    n = n / 2 + 1; //与上面的效果相同
//                    res += 2;
//                }
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxEnvelopes(vector<vector<int>>& envelopes) {
//        sort(envelopes.begin(), envelopes.end(), [&](const vector<int>& v1, const vector<int>& v2)
//            {
//                if (v1[0] != v2[0]) return v1[0] < v2[0];
//                else return v1[1] > v2[1];
//            });
//        vector<int> res;
//        res.push_back(envelopes[0][1]);
//        int n = envelopes.size();
//        for (int i = 1; i < n; i++)
//        {
//            int t = envelopes[i][1];
//            if (t > res.back())
//                res.push_back(t);
//            else
//            {
//                int left = 0, right = res.size() - 1;
//                while (left < right)
//                {
//                    int mid = left + (right - left) / 2;
//                    if (res[mid] >= t) right = mid;
//                    else left = mid + 1;
//                }
//                res[left] = t;
//            }
//        }
//        return res.size();
//    }
//};