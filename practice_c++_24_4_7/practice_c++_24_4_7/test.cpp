#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int score[300];
//
//int main()
//{
//    int n, k, s;
//    cin >> n >> k >> s;
//    int res = 0;
//    while (n--)
//    {
//        int a, b;
//        cin >> a >> b;
//        if (a >= 175) //只考虑a>=175分的学生
//        {
//            if (b >= s) //通过pat面试分数线 不需要用到额外批次名额
//                res++;
//            else
//            {
//                score[a]++;
//                if (score[a] <= k) //一共接受K批次的推荐名单
//                    res++;
//            }
//        }
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string a, b;
//    cin >> a >> b;
//    string s1, s2;
//    for (int i = 1; i < a.length(); i++)
//    {
//        if (a[i] % 2 == a[i - 1] % 2)
//        {
//            s1 += max(a[i], a[i - 1]);
//        }
//    }
//    for (int i = 1; i < b.length(); i++)
//    {
//        if (b[i] % 2 == b[i - 1] % 2)
//        {
//            s2 += max(b[i], b[i - 1]);
//        }
//    }
//    if (s1 == s2) cout << s1 << endl;
//    else cout << s1 << endl << s2 << endl;
//	  return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n, m, k;
//    string x;
//    cin >> n >> x >> m >> k;
//    if (k == n)
//        cout << "mei you mai " << x << " de" << endl;
//    else if (k == m)
//        cout << "kan dao le mai " << x << " de" << endl;
//    else
//        cout << "wang le zhao mai " << x << " de" << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n, m, q;
//    cin >> n >> m >> q;
//    int g[n + 1][m + 1] = { 0 };
//    int safe = 0;
//    while (q--)
//    {
//        int t, c;
//        cin >> t >> c;
//        if (t == 0) //选择的是一整行
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (g[c][j] == 0)
//                    g[c][j] = 1;
//            }
//        }
//        else if (t == 1) //选择的是一整列
//        {
//            for (int i = 1; i <= n; i++)
//            {
//                if (g[i][c] == 0)
//                    g[i][c] = 1;
//            }
//        }
//    }
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= m; j++)
//            if (g[i][j] != 1)
//                safe++;
//    cout << safe << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 1010;
//
//struct Moodcake
//{
//    double w, v; //库存量 总售价
//    bool operator<(const Moodcake& k) const
//    {
//        return v / w > k.v / k.w; //按降序排列
//    }
//}mood[N];
//
//int main()
//{
//    int n, d; //月饼的种类数 市场最大需求量
//    cin >> n >> d;
//    for (int i = 0; i < n; i++)
//        cin >> mood[i].w;
//    for (int i = 0; i < n; i++)
//        cin >> mood[i].v;
//    sort(mood, mood + n);
//    double max_profit = 0;
//    for (int i = 0; i < n; i++)
//    {
//        if (mood[i].w <= d)
//        {
//            max_profit += mood[i].v;
//            d -= mood[i].w;
//            if (d == 0) break;
//        }
//        else
//        {
//            max_profit += mood[i].v / (mood[i].w / d);
//            d = 0;
//            break;
//        }
//    }
//    printf("%.2lf\n", max_profit);
//    return 0;
//}


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* invertTree(TreeNode* root) {
//        if (root == nullptr)
//            return root;
//        swap(root->left, root->right);
//        invertTree(root->left);
//        invertTree(root->right);
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* invertTree(TreeNode* root) {
//        if (root == nullptr)
//            return root;
//        invertTree(root->left);
//        invertTree(root->right);
//        swap(root->left, root->right);
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* invertTree(TreeNode* root) {
//        if (root == nullptr)
//            return root;
//        invertTree(root->left);
//        swap(root->left, root->right);
//        invertTree(root->left);
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    queue<TreeNode*> q;
//public:
//    TreeNode* invertTree(TreeNode* root) {
//        if (root == nullptr)
//            return root;
//        q.push(root);
//        while (q.size())
//        {
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                swap(node->left, node->right);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//        }
//        return root;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    bool cmp(TreeNode* left, TreeNode* right)
//    {
//        if (left == nullptr && right == nullptr) return true;
//        else if (left == nullptr && right != nullptr) return false;
//        else if (left != nullptr && right == nullptr) return false;
//        else if (left->val != right->val) return false;
//        else
//        {
//            bool outside = cmp(left->left, right->right);
//            bool inside = cmp(left->right, right->left);
//            return outside && inside;
//        }
//    }
//    bool isSymmetric(TreeNode* root) {
//        if (root == nullptr)
//            return root;
//        return cmp(root->left, root->right);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int maxDepth(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        int left_height = maxDepth(root->left);
//        int right_height = maxDepth(root->right);
//        int height = 1 + max(left_height, right_height);
//        return height;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int maxDepth(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        return 1 + max(maxDepth(root->left), maxDepth(root->right));
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    vector<Node*> children;
//
//    Node() {}
//
//    Node(int _val) {
//        val = _val;
//    }
//
//    Node(int _val, vector<Node*> _children) {
//        val = _val;
//        children = _children;
//    }
//};
//*/
//
//class Solution {
//private:
//    queue<Node*> q;
//public:
//    int maxDepth(Node* root) {
//        if (root == NULL)
//            return 0;
//        q.push(root);
//        int depth = 0;
//        while (q.size())
//        {
//            depth++;
//            int n = q.size();
//            while (n--)
//            {
//                Node* node = q.front();
//                q.pop();
//                for (int i = 0; i < node->children.size(); i++)
//                    if (node->children[i]) q.push(node->children[i]);
//            }
//        }
//        return depth;
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    vector<Node*> children;
//
//    Node() {}
//
//    Node(int _val) {
//        val = _val;
//    }
//
//    Node(int _val, vector<Node*> _children) {
//        val = _val;
//        children = _children;
//    }
//};
//*/
//
//class Solution {
//public:
//    int maxDepth(Node* root) {
//        if (root == NULL)
//            return 0;
//        int depth = 0;
//        for (int i = 0; i < root->children.size(); i++)
//            depth = max(depth, maxDepth(root->children[i]));
//        return depth + 1;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int minDepth(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        int leftDepth = minDepth(root->left);
//        int rightDepth = minDepth(root->right);
//        if (root->left == nullptr && root->right != nullptr)
//            return 1 + rightDepth;
//        if (root->left != nullptr && root->right == nullptr)
//            return 1 + leftDepth;
//        return 1 + min(leftDepth, rightDepth);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int minDepth(TreeNode* root) {
//        if (root == nullptr)
//            return 0;
//        if (root->left == nullptr && root->right != nullptr)
//            return 1 + minDepth(root->right);
//        if (root->left != nullptr && root->right == nullptr)
//            return 1 + minDepth(root->left);
//        return 1 + min(minDepth(root->left), minDepth(root->right));
//    }
//};


//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        if (n <= 1) return n;
//        vector<int> dp(n, 1);
//        int res = 0;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[j] < nums[i])
//                    dp[i] = max(dp[i], dp[j] + 1);
//            }
//            if (dp[i] > res)
//                res = dp[i];
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    int hash[256];
//public:
//    bool isUnique(string astr) {
//        for (auto c : astr)
//        {
//            if (hash[c] == 0) hash[c]++;
//            else return false;
//        }
//        return true;
//    }
//};


//class Solution {
//private:
//    int hash[26];
//public:
//    bool isUnique(string astr) {
//        for (auto c : astr)
//        {
//            if (hash[c - 'a'] == 0) hash[c - 'a']++;
//            else return false;
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    bool isUnique(string astr) {
//        int bitmap = 0;
//        for (auto c : astr)
//        {
//            int i = c - 'a';
//            if (((bitmap >> i) & 1) == 1) return false;
//            bitmap |= (1 << i);
//        }
//        return true;
//    }
//};


//class Solution {
//public:
//    bool isUnique(string astr) {
//        int n = astr.size();
//        if (n > 26) return false;
//        int bitmap = 0;
//        for (auto c : astr)
//        {
//            int i = c - 'a';
//            if (((bitmap >> i) & 1) == 1) return false;
//            bitmap |= (1 << i);
//        }
//        return true;
//    }
//};


//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
//char a[26];
//
//int main()
//{
//    string s;
//    cin >> s;
//    for (int i = 0; i < s.size(); i++)
//        a[s[i] - 'a']++;
//    char ch = 'a';
//    int res = 0;
//    for (int i = 0; i < 26; i++)
//    {
//        if (a[i] > res)
//        {
//            ch = i + 'a';
//            res = a[i];
//        }
//    }
//    cout << ch << endl << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 510, INF = 1e9 + 7;
//int a[N][N], f[N][N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= i; j++)
//            cin >> a[i][j];
//    for (int i = 0; i <= n; i++)
//        for (int j = 0; j <= i + 1; j++)
//            f[i][j] = -INF;
//    f[1][1] = a[1][1];
//    for (int i = 2; i <= n; i++)
//        for (int j = 1; j <= i; j++)
//            f[i][j] = max(f[i - 1][j - 1] + a[i][j], f[i - 1][j] + a[i][j]);
//    int res = -INF;
//    for (int j = 1; j <= n; j++)
//        res = max(res, f[n][j]);
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 110, INF = 1e9 + 7;
//int a[N][N], f[N][N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= i; j++)
//            cin >> a[i][j];
//    for (int i = 0; i <= n; i++)
//        for (int j = 0; j <= i + 1; j++)
//            f[i][j] = -INF;
//    f[1][1] = a[1][1];
//    for (int i = 2; i <= n; i++)
//        for (int j = 1; j <= i; j++)
//            f[i][j] = max(f[i - 1][j - 1] + a[i][j], f[i - 1][j] + a[i][j]);
//    int res = -INF;
//    if (n % 2 == 1) res = f[n][n / 2 + 1];
//    else res = max(f[n][n / 2], f[n][n / 2 + 1]);
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	printf(
//		"                ********\n"
//		"               ************\n"
//		"               ####....#.\n"
//		"             #..###.....##....\n"
//		"             ###.......######              ###            ###\n"
//		"                ...........               #...#          #...#\n"
//		"               ##*#######                 #.#.#          #.#.#\n"
//		"            ####*******######             #.#.#          #.#.#\n"
//		"           ...#***.****.*###....          #...#          #...#\n"
//		"           ....**********##.....           ###            ###\n"
//		"           ....****    *****....\n"
//		"             ####        ####\n"
//		"           ######        ######\n"
//		"##############################################################\n"
//		"#...#......#.##...#......#.##...#......#.##------------------#\n"
//		"###########################################------------------#\n"
//		"#..#....#....##..#....#....##..#....#....#####################\n"
//		"##########################################    #----------#\n"
//		"#.....#......##.....#......##.....#......#    #----------#\n"
//		"##########################################    #----------#\n"
//		"#.#..#....#..##.#..#....#..##.#..#....#..#    #----------#\n"
//		"##########################################    ############\n"
//		);
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int a, b;
//	cin >> a >> b;
//	cout << a + b << endl;
//	return 0;
//}


//#include <iostream>
//#include <set>
//using namespace std;
//
//int main()
//{
//	int n;
//	cin >> n;
//	set<int> set;
//	while(n--)
//	{
//		int x;
//		cin >> x;
//		set.insert(x);
//	}
//	int m = set.size();
//	cout << m << endl;
//	for (auto e : set)
//		cout << e << ' ';
//	return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//int a[110];
//bool used[1010];
//
//int main()
//{
//    int n;
//    cin >> n;
//    int m = 0;
//    for(int i=0; i<n; i++)
//    {
//        int x;
//        cin >> x;
//        if(used[x]) continue;
//        used[x] = true;
//        a[m++] = x;
//    }
//    sort(a, a+m);
//    cout << m << endl;
//    for (int i = 0; i < m; i++)
//        cout << a[i] << ' ';
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int m_budget[13];
//
//int main()
//{
//	for (int i = 1; i <= 12; i++)
//		cin >> m_budget[i];
//	int money = 0, give = 0, k = 0;
//	bool flag = true;
//	for (int i = 1; i <= 12; i++)
//	{
//		money += 300;
//		money -= m_budget[i];
//		if (money < 0)
//		{
//			flag = false;
//			k = i;
//			break;
//		}
//		give += money / 100;
//		money %= 100;
//	}
//	if (flag)
//	{
//		money += give * 100 * 1.2;
//		cout << money << endl;
//	}
//	else
//	{
//		cout << -k << endl;
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n;
//    cin >> n;
//    int res = 0;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        res ^= x;
//    }
//    if (res) cout << "Yes" << endl;
//    else cout << "No" << endl;
//    return 0;
//}


#include <iostream>
#include <cstring>
#include <unordered_set>
using namespace std;

const int N = 110, M = 10010;
int k, n;
int s[N], f[M];

int sg(int x)
{
    if (f[x] != -1) return f[x];
    unordered_set<int> S;
    for (int i = 0; i < k; i++)
    {
        int t = s[i];
        if (x >= t) S.insert(sg(x - t));
    }
    for (int i = 0; ; i++)
    {
        if (S.count(i) == 0)
        {
            f[x] = i;
            break;
        }
    }
    return f[x];
}

int main()
{
    cin >> k;
    for (int i = 0; i < k; i++) cin >> s[i];
    memset(f, -1, sizeof f);
    int res = 0;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        int x;
        cin >> x;
        res ^= sg(x);
    }
    if (res) cout << "Yes" << endl;
    else cout << "No" << endl;
    return 0;
}