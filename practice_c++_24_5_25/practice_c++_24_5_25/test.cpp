#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int n, m;
//	cin >> n >> m;
//	printf("%.2lf\n", n * 0.6 + m * 1);
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//int main()
//{
//    int t ;
//    cin >> t;
//    while (t--)
//    {
//        LL n;
//        cin >> n;
//        cout << "lose" << endl;
//    }
//    return 0;
//}


#include <iostream>
using namespace std;

int main()
{
    int t;
    cin >> t;
    while (t--)
    {
        int n, k;
        cin >> n >> k;
        string s;
        cin >> s;
        bool flag = false;
        int cnt = 0;
        for (auto x : s)
        {
            if (x == '1')
            {
                cnt++; //A中1的个数
            }
        }
        //枚举k B中1的个数
        for (int i = 0; i <= k; i++)
        {
            int sum = cnt + i; //1的总的个数
            sum %= (1 << k);
            int res = 0;
            for (int j = 0; j < k; j++)
            {
                if ((sum >> j) & 1)
                {
                    res++;
                }
            }
            if (res == i)
            {
                //输出B
                for (int j = k - 1; j >= 0; j--)
                {
                    if ((sum >> j) & 1)
                        cout << 1;
                    else
                        cout << 0;
                    flag = true;
                }
                cout << endl;
            }
            if (flag) break;
        }
        if(!flag) cout << "None" << endl;
    }
    return 0;
}