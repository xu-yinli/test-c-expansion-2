#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        dp[0][1] = 1;
//        for (int i = 1; i <= m; i++)
//        {
//            for (int j = 1; j <= n; j++)
//            {
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
//        int n = obstacleGrid.size();
//        int m = obstacleGrid[0].size();
//        if (obstacleGrid[0][0] == 1 || obstacleGrid[n - 1][m - 1] == 1) return 0;
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        dp[0][1] = 1; //或dp[1][0]=1
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (obstacleGrid[i - 1][j - 1] == 0)
//                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int jewelleryValue(vector<vector<int>>& frame) {
//        int n = frame.size();
//        int m = frame[0].size();
//        vector<vector<int>> dp(n, vector<int>(m));
//        dp[0][0] = frame[0][0];
//        for (int i = 1; i < n; i++)
//            dp[i][0] += dp[i - 1][0] + frame[i][0];
//        for (int j = 1; j < m; j++)
//            dp[0][j] += dp[0][j - 1] + frame[0][j];
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 1; j < m; j++)
//            {
//                dp[i][j] = max(dp[i - 1][j] + frame[i][j], dp[i][j - 1] + frame[i][j]);
//            }
//        }
//        return dp[n - 1][m - 1];
//    }
//};


//class Solution {
//public:
//    int jewelleryValue(vector<vector<int>>& frame) {
//        int n = frame.size();
//        int m = frame[0].size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                dp[i][j] = max(dp[i - 1][j] + frame[i - 1][j - 1], dp[i][j - 1] + frame[i - 1][j - 1]);
//            }
//        }
//        return dp[n][m];
//    }
//};


//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1) return nums[0];
//        vector<int> dp(n);
//        dp[0] = nums[0], dp[1] = max(nums[0], nums[1]);
//        for (int i = 2; i < n; i++)
//        {
//            dp[i] = max(dp[i - 2] + nums[i], dp[i - 1]);
//        }
//        return dp[n - 1];
//    }
//};


//class Solution {
//public:
//    int robDP(vector<int>& nums, int st, int ed) {
//        if (st == ed) return nums[st];
//        vector<int> dp(nums.size());
//        dp[st] = nums[st], dp[st + 1] = max(nums[st], nums[st + 1]);
//        for (int i = st + 2; i <= ed; i++)
//        {
//            dp[i] = max(dp[i - 2] + nums[i], dp[i - 1]);
//        }
//        return dp[ed];
//    }
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        if (n == 1) return nums[0];
//        int res1 = robDP(nums, 0, n - 2);
//        int res2 = robDP(nums, 1, n - 1);
//        return max(res1, res2);
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//char a[N], b[N];
//int f[N][N];
//
//int main()
//{
//    int n, m;
//    scanf("%d%s", &n, a + 1);
//    scanf("%d%s", &m, b + 1);
//    for (int i = 0; i <= m; i++) f[0][i] = i; //增加操作
//    for (int i = 0; i <= n; i++) f[i][0] = i; //删除操作
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            f[i][j] = min(f[i - 1][j] + 1, f[i][j - 1] + 1);
//            if (a[i] == b[j]) f[i][j] = min(f[i][j], f[i - 1][j - 1]);
//            else f[i][j] = min(f[i][j], f[i - 1][j - 1] + 1);
//        }
//    }
//    cout << f[n][m] << endl;
//    return 0;
//}


//#include <iostream>
//#include <string.h>
//using namespace std;
//
//const int N = 1010, M = 11;
//int a[M], b[M];
//int f[M][M];
//char str[N][M];
//
//int edit_distance(char s1[], char s2[])
//{
//    int a = strlen(s1 + 1), b = strlen(s2 + 1);
//    for (int i = 0; i <= b; i++) f[0][i] = i;
//    for (int i = 0; i <= a; i++) f[i][0] = i;
//    for (int i = 1; i <= a; i++)
//    {
//        for (int j = 1; j <= b; j++)
//        {
//            f[i][j] = min(f[i - 1][j] + 1, f[i][j - 1] + 1);
//            if (s1[i] == s2[j]) f[i][j] = min(f[i][j], f[i - 1][j - 1]);
//            else f[i][j] = min(f[i][j], f[i - 1][j - 1] + 1);
//        }
//    }
//    return f[a][b];
//}
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 0; i < n; i++) scanf("%s", str[i] + 1);
//    while (m--)
//    {
//        char s[N];
//        int limit;
//        scanf("%s%d", s + 1, &limit);
//        int res = 0;
//        for (int i = 0; i < n; i++)
//        {
//            if (edit_distance(str[i], s) <= limit)
//                res++;
//        }
//        cout << res << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <string.h>
//using namespace std;;
//
//const int N = 1010, M = 11;
//char a[M], b[M];
//int f[M][M];
//char str[N][M];
//
//int edit_distance(char a[], char b[])
//{
//    int len1 = strlen(a + 1), len2 = strlen(b + 1);
//    for (int i = 0; i <= len2; i++) f[0][i] = i;
//    for (int i = 0; i <= len1; i++) f[i][0] = i;
//    for (int i = 1; i <= len1; i++)
//    {
//        for (int j = 1; j <= len2; j++)
//        {
//            f[i][j] = min(f[i - 1][j] + 1, f[i][j - 1] + 1);
//            f[i][j] = min(f[i][j], f[i - 1][j - 1] + (a[i] != b[j]));
//        }
//    }
//    return f[len1][len2];
//}
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 0; i < n; i++) scanf("%s", str[i] + 1);
//    while (m--)
//    {
//        char s[M];
//        int limit;
//        scanf("%s%d", s + 1, &limit);
//        int res = 0;
//        for (int i = 0; i < n; i++)
//        {
//            if (edit_distance(str[i], s) <= limit)
//                res++;
//        }
//        cout << res << endl;
//    }
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010, MOD = 1e9 + 7;
//int f[N][N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    f[0][0] = 1;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 0; j <= n; j++)
//        {
//            if (j >= i) f[i][j] = (f[i - 1][j] + f[i][j - i]) % MOD;
//            else f[i][j] = f[i - 1][j];
//        }
//    }
//    cout << f[n][n] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010, MOD = 1e9 + 7;
//int f[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    f[0] = 1;
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = i; j <= n; j++)
//        {
//            f[j] = (f[j] + f[j - i]) % MOD;
//        }
//    }
//    cout << f[n] << endl;
//    return 0;
//}


//#include <iostream>
//#include <cstring>
//#include <algorithm>
//using namespace std;
//
//const int N = 310;
//int n, m;
//int h[N][N];
//int f[N][N];
//
//int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//
//int dp(int x, int y)
//{
//    if (f[x][y] != -1) return f[x][y];
//    f[x][y] = 1;
//    for (int i = 0; i < 4; i++)
//    {
//        int a = x + dx[i], b = y + dy[i];
//        if (a >= 1 && a <= n && b >= 1 && b <= m && h[a][b] < h[x][y])
//            f[x][y] = max(f[x][y], dp(a, b) + 1);
//    }
//    return f[x][y];
//}
//
//int main()
//{
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= m; j++)
//            cin >> h[i][j];
//    memset(f, -1, sizeof f);
//    int res = 0;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= m; j++)
//            res = max(res, dp(i, j));
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//bool is_prime(int x)
//{
//    if (x < 2) return false;
//    for (int i = 2; i <= x / i; i++)
//        if (x % i == 0) return false;
//    return true;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        if (is_prime(x)) cout << "Yes" << endl;
//        else cout << "No" << endl;
//    }
//    return 0;
//}


#include <iostream>
using namespace std;

const int N = 1e6 + 10;
int res;
bool st[N];

void get_primes(int n)
{
    for (int i = 2; i <= n; i++)
    {
        if (st[i]) continue;
        else
        {
            res++;
            for (int j = i + i; j <= n; j += i)
                st[j] = true;
        }
    }
}

int main()
{
    int n;
    cin >> n;
    get_primes(n);
    cout << res << endl;
    return 0;
}