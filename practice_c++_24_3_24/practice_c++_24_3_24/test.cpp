#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 20010;
//int v[N];
//int f[N][N];
//
//int main()
//{
//    int m;
//    cin >> m;
//    int n;
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 0; j <= m; j++)
//        {
//            if (j >= v[i])
//                f[i][j] = max(f[i - 1][j], f[i - 1][j - v[i]] + v[i]);
//            else
//                f[i][j] = f[i - 1][j];
//        }
//    }
//    cout << m - f[n][m] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 20010;
//int v[N];
//int f[N];
//
//int main()
//{
//    int m;
//    cin >> m;
//    int n;
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = m; j >= v[i]; j--)
//        {
//            f[j] = max(f[j], f[j - v[i]] + v[i]);
//        }
//    }
//    cout << m - f[m] << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 30010;
//int v[N], p[N]; //物品的价格 物品的重要度
//int f[30][N];
//
//int main()
//{
//    int n, m; // 总钱数 购买物品的个数
//    cin >> n >> m;
//    for (int i = 1; i <= m; i++)
//    {
//        cin >> v[i] >> p[i];
//        p[i] *= v[i];
//    }
//    for (int i = 1; i <= m; i++)
//    {
//        for (int j = 0; j <= n; j++)
//        {
//            if (j >= v[i])
//                f[i][j] = max(f[i - 1][j], f[i - 1][j - v[i]] + p[i]);
//            else
//                f[i][j] = f[i - 1][j];
//        }
//    }
//    cout << f[m][n] << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 30010;
//int v[N], p[N]; //物品的价格 物品的重要度
//int f[N];
//
//int main()
//{
//    int n, m; // 总钱数 购买物品的个数
//    cin >> n >> m;
//    for (int i = 1; i <= m; i++)
//    {
//        cin >> v[i] >> p[i];
//        p[i] *= v[i];
//    }
//    for (int i = 1; i <= m; i++)
//    {
//        for (int j = n; j >= v[i]; j--)
//        {
//            f[j] = max(f[j], f[j - v[i]] + p[i]);
//        }
//    }
//    cout << f[n] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 110, M = 10010;
//int a[N];
//int f[N][M]; //前i道菜用光j元钱的办法总数
//
//int main()
//{
//    int n, m; //n种方法 只剩m元
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> a[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 0; j <= m; j++)
//        {
//            if (j == a[i]) f[i][j] = f[i - 1][j] + 1; //前i-1种菜花费的相同金额方案数再+1
//            else if (j > a[i]) f[i][j] = f[i - 1][j] + f[i - 1][j - a[i]]; //前i-1种菜花费相同金额方案数+前i-1种菜剩余金额(j-a[i])的方案数
//            else f[i][j] = f[i - 1][j]; //方案数与前i-1种菜花费相同金额方案数相同
//        }
//    }
//    cout << f[n][m] << endl;
//    return 0;
//}


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int res = 0;
//        int prev_min = prices[0];
//        for (int i = 1; i < prices.size(); i++)
//        {
//            res = max(res, prices[i] - prev_min);
//            if (prev_min > prices[i])
//                prev_min = prices[i];
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int res = 0;
//        int prev_min = prices[0];
//        for (int i = 1; i < prices.size(); i++)
//        {
//            res = max(res, prices[i] - prev_min);
//            prev_min = min(prev_min, prices[i]);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int profit = 0;
//        for (int i = 0; i < prices.size(); i++)
//        {
//            int j = i;
//            while (j + 1 < prices.size() && prices[j + 1] > prices[j])
//                j++;
//            profit += prices[j] - prices[i];
//            i = j;
//        }
//        return profit;
//    }
//};


//class Solution {
//public:
//    int maxProfit(vector<int>& prices) {
//        int res = 0;
//        for (int i = 1; i < prices.size(); i++)
//        {
//            if (prices[i] - prices[i - 1] > 0)
//                res += prices[i] - prices[i - 1];
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int largestSumAfterKNegations(vector<int>& nums, int k) {
//        int sum = 0, t = 0;
//        int mini = 2e9;
//        for (auto e : nums)
//        {
//            if (e < 0) t++;
//            mini = min(mini, abs(e));
//        }
//        sort(nums.begin(), nums.end());
//        if (t > k)
//        {
//            for (int i = 0; i < k; i++)
//                sum += abs(nums[i]);
//            for (int i = k; i < nums.size(); i++)
//                sum += nums[i];
//        }
//        else
//        {
//            for (auto e : nums) sum += abs(e);
//            if ((k - t) % 2 == 1) sum -= 2 * mini;
//        }
//        return sum;
//    }
//};


//class Solution {
//    typedef pair<int, string> PII;
//    vector<PII> q;
//    static bool cmp(PII x, PII y)
//    {
//        return x.first > y.first;
//    }
//public:
//    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
//        for (int i = 0; i < names.size(); i++)
//            q.push_back({ heights[i], names[i] });
//        sort(q.begin(), q.end(), cmp);
//        vector<string> res;
//        for (int i = 0; i < q.size(); i++)
//            res.push_back(q[i].second);
//        return res;
//    }
//};


//class Solution {
//    unordered_map<int, string> map;
//public:
//    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
//        for (int i = 0; i < names.size(); i++)
//            map[heights[i]] = names[i];
//        sort(heights.begin(), heights.end()); //默认升序
//        vector<string> res;
//        for (int i = heights.size() - 1; i >= 0; i--)
//            res.push_back(map[heights[i]]);
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<string> sortPeople(vector<string>& names, vector<int>& heights) {
//        vector<int> index;
//        for (int i = 0; i < names.size(); i++)
//            index.push_back(i);
//        sort(index.begin(), index.end(), [&](int x, int y) {
//            return heights[x] > heights[y];
//            });
//        vector<string> res;
//        for (int i = 0; i < heights.size(); i++)
//            res.push_back(names[index[i]]);
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<int> advantageCount(vector<int>& nums1, vector<int>& nums2) {
//        int n = nums1.size();
//        vector<int> index(n);
//        for (int i = 0; i < n; i++)
//            index[i] = i;
//        sort(nums1.begin(), nums1.end());
//        sort(index.begin(), index.end(), [&](int x, int y) {
//            return nums2[x] < nums2[y];
//            });
//        vector<int> res(n);
//        int left = 0, right = n - 1;
//        for (int i = 0; i < n; i++)
//        {
//            if (nums1[i] > nums2[index[left]])
//            {
//                res[index[left]] = nums1[i];
//                left++;
//            }
//            else
//            {
//                res[index[right]] = nums1[i];
//                right--;
//            }
//        }
//        return res;
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int v[N], w[N];
//int f[N][N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 0; j <= m; j++)
//        {
//            for (int k = 0; k * v[i] <= j; k++)
//            {
//                f[i][j] = max(f[i - 1][j], f[i - 1][j - k * v[i]] + k * w[i]);
//            }
//        }
//    }
//    cout << f[n][m] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int v[N], w[N];
//int f[N][N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 0; j <= m; j++)
//        {
//            if (j >= v[i])
//                f[i][j] = max(f[i - 1][j], f[i][j - v[i]] + w[i]);
//            else
//                f[i][j] = f[i - 1][j];
//        }
//    }
//    cout << f[n][m] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int v[N], w[N];
//int f[N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = v[i]; j <= m; j++)
//        {
//            f[j] = max(f[j], f[j - v[i]] + w[i]);
//        }
//    }
//    cout << f[m] << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 1e4 + 10, M = 1e7 + 10;
//int a[N], b[N];
//long long f[M];
//
//int main()
//{
//    long long t, m;
//    cin >> t >> m;
//    for (int i = 1; i <= m; i++)
//        cin >> a[i] >> b[i];
//    for (int i = 1; i <= m; i++)
//    {
//        for (int j = a[i]; j <= t; j++)
//        {
//            f[j] = max(f[j], f[j - a[i]] + b[i]);
//        }
//    }
//    cout << f[t] << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 1e4 + 10;
//int p[N], t[N]; //解决第i类题得到的分数p 需要花费的时间t
//int f[N]; //最大的总分
//
//int main()
//{
//    int m, n; //竞赛时间m 题目类n
//    cin >> m >> n;
//    for (int i = 1; i <= n; i++)
//        cin >> p[i] >> t[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = t[i]; j <= m; j++)
//        {
//            f[j] = max(f[j], f[j - t[i]] + p[i]);
//        }
//    }
//    cout << f[m] << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 110;
//int v[N], w[N], s[N];
//int f[N][N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i] >> s[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 0; j <= m; j++)
//        {
//            for (int k = 0; k <= s[i] && k * v[i] <= j; k++)
//            {
//                f[i][j] = max(f[i][j], f[i - 1][j - k * v[i]] + k * w[i]);
//            }
//        }
//    }
//    cout << f[n][m] << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 110;
//int v[N], w[N], s[N];
//int f[N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        cin >> v[i] >> w[i] >> s[i];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = m; j >= v[i]; j--)
//        {
//            for (int k = 0; k <= s[i] && k * v[i] <= j; k++)
//            {
//                f[j] = max(f[j], f[j - k * v[i]] + k * w[i]);
//            }
//        }
//    }
//    cout << f[m] << endl;
//    return 0;
//}


//class Solution {
//public:
//    int fib(int n) {
//        if (n <= 1) return n;
//        int sum = 0;
//        int a = 0, b = 1;
//        for (int i = 2; i <= n; i++)
//        {
//            sum = a + b;
//            a = b;
//            b = sum;
//        }
//        return sum;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int dfs(int n)
//{
//    if (n == 0 || n == 1) return n;
//    return dfs(n - 1) + dfs(n - 2);
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        cout << dfs(x) << endl;
//    }
//    return 0;
//}


//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost) {
//        int n = cost.size();
//        vector<int> dp(n + 1);
//        dp[0] = 0, dp[1] = 0;
//        for (int i = 2; i <= n; i++)
//        {
//            dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int uniquePaths(int m, int n) {
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (int i = 1; i <= m; i++) dp[i][1] = 1;
//        for (int j = 1; j <= n; j++) dp[1][j] = 1;
//        for (int i = 2; i <= m; i++)
//        {
//            for (int j = 2; j <= n; j++)
//            {
//                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//public:
//    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
//        int n = obstacleGrid.size(); //行
//        int m = obstacleGrid[0].size(); //列
//        if (obstacleGrid[0][0] == 1 || obstacleGrid[n - 1][m - 1] == 1)
//            return 0;
//        vector<vector<int>> dp(n, vector<int>(m));
//        for (int i = 0; i < n && obstacleGrid[i][0] == 0; i++) dp[i][0] = 1;
//        for (int j = 0; j < m && obstacleGrid[0][j] == 0; j++) dp[0][j] = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 1; j < m; j++)
//            {
//                if (obstacleGrid[i][j] == 1)
//                    continue;
//                else
//                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
//            }
//        }
//        return dp[n - 1][m - 1];
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int grid[N][N];
//int dp[N][N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    while (m--)
//    {
//        int x, y;
//        cin >> x >> y;
//        grid[x][y] = 1; //障碍
//    }
//    for (int i = 1; i <= n && grid[i][1] == 0; i++) dp[i][1] = 1;
//    for (int j = 1; j <= n && grid[1][j] == 0; j++) dp[1][j] = 1;
//    for (int i = 2; i <= n; i++)
//    {
//        for (int j = 2; j <= n; j++)
//        {
//            if (grid[i][j] == 1)
//                continue;
//            else
//                dp[i][j] = (dp[i - 1][j] + dp[i][j - 1]) % 100003;
//        }
//    }
//    cout << dp[n][n] << endl;
//    return 0;
//}


//class Solution {
//public:
//    int integerBreak(int n) {
//        vector<int> dp(n + 1);
//        dp[0] = 0, dp[1] = 0, dp[2] = 1;
//        for (int i = 3; i <= n; i++)
//        {
//            for (int j = 1; j < i; j++)
//            {
//                dp[i] = max(max(j * (i - j), j * dp[i - j]), dp[i]);
//            }
//        }
//        return dp[n];
//    }
//};


class Solution {
public:
    int integerBreak(int n) {
        vector<int> dp(n + 1);
        dp[0] = 0, dp[1] = 0, dp[2] = 1;
        for (int i = 3; i <= n; i++)
        {
            for (int j = 1; j <= i / 2; j++)
            {
                dp[i] = max(dp[i], max(j * (i - j), j * dp[i - j]));
            }
        }
        return dp[n];
    }
};