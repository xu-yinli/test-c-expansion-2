#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    bool isLongPressedName(string name, string typed) {
//        int n = name.size(), m = typed.size();
//        int i = 0, j = 0;
//        while (j < m)
//        {
//            if (name[i] == typed[j])
//            {
//                i++;
//                j++;
//                continue;
//            }
//            if (j > 0 && typed[j] == typed[j - 1])
//            {
//                j++;
//            }
//            else return false;
//        }
//        if (i == n) return true;
//        return false;
//    }
//};


//class Solution {
//public:
//    bool isLongPressedName(string name, string typed) {
//        int n = name.size(), m = typed.size();
//        int i = 0, j = 0;
//        while (j < m)
//        {
//            if (i < n && name[i] == typed[j])
//            {
//                i++;
//                j++;
//            }
//            else if (j > 0 && typed[j] == typed[j - 1])
//            {
//                j++;
//            }
//            else return false;
//        }
//        if (i == n) return true;
//        return false;
//    }
//};


//class Solution {
//public:
//    bool backspaceCompare(string s, string t) {
//        int n = s.size(), m = t.size();
//        int i = n - 1, j = m - 1;
//        int skip_s = 0, skip_t = 0;
//        while (i >= 0 || j >= 0)
//        {
//            while (i >= 0)
//            {
//                if (s[i] == '#')
//                {
//                    skip_s++;
//                    i--;
//                }
//                else if (skip_s > 0)
//                {
//                    skip_s--;
//                    i--;
//                }
//                else break;
//            }
//            while (j >= 0)
//            {
//                if (t[j] == '#')
//                {
//                    skip_t++;
//                    j--;
//                }
//                else if (skip_t > 0)
//                {
//                    skip_t--;
//                    j--;
//                }
//                else break;
//            }
//            if (i >= 0 && j >= 0)
//            {
//                if (s[i] != t[j])
//                    return false;
//            }
//            else
//            {
//                if (i >= 0 || j >= 0)
//                    return false;
//            }
//            i--; j--;
//        }
//        return true;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//    unordered_map<string, int> hash1;
//public:
//    vector<int> findSubstring(string s, vector<string>& words) {
//        int n = s.size(), m = words.size();
//        for (string str : words)
//            hash1[str]++;
//        int len = words[0].size();
//        for (int i = 0; i < len; i++)
//        {
//            unordered_map<string, int> hash2;
//            int cnt = 0;
//            int left = i, right = i;
//            while (right < n)
//            {
//                string in = s.substr(right, len);
//                hash2[in]++;
//                if (hash1.count(in) && hash2[in] <= hash1[in]) cnt++;
//                if (right - left + 1 > m * len)
//                {
//                    string out = s.substr(left, len);
//                    if (hash1.count(out) && hash2[out] <= hash1[out]) cnt--;
//                    hash2[out]--;
//                    left += len;
//                }
//                if (cnt == m) res.push_back(left);
//                right += len;
//            }
//        }
//        return res;
//    }
//};


//leetcode
//class Solution {
//private:
//    int hash1[130];
//    int hash2[130];
//public:
//    string minWindow(string s, string t) {
//        int n = s.size(), m = t.size();
//        int kinds = 0;
//        for (char ch : t)
//        {
//            if (hash1[ch] == 0) kinds++;
//            hash1[ch]++;
//        }
//        int res = INT_MAX;
//        int types = 0;
//        int st = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash2[s[right]]++;
//            if (hash1[s[right]] == hash2[s[right]]) types++;
//            while (types == kinds)
//            {
//                if (right - left + 1 < res)
//                {
//                    res = right - left + 1;
//                    st = left;
//                }
//                if (hash1[s[left]] == hash2[s[left]]) types--;
//                hash2[s[left]]--;
//                left++;
//            }
//            right++;
//        }
//        if (res != INT_MAX) return s.substr(st, res);
//        return "";
//    }
//};


//#include <cctype>
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param s string字符串
//     * @param n int整型
//     * @return string字符串
//     */
//    string trans(string s, int n) {
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] >= 'a' && s[i] <= 'z') s[i] = toupper(s[i]);
//            else if (s[i] >= 'A' && s[i] <= 'Z') s[i] = tolower(s[i]);
//        }
//        reverse(s.begin(), s.end());
//        int i = 0, j = 0;
//        while (j <= n)
//        {
//            if (s[j] == ' ' || j == n)
//            {
//                reverse(s.begin() + i, s.begin() + j);
//                j++;
//                i = j;
//            }
//            else j++;
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param strs string字符串vector
//     * @return string字符串
//     */
//    string longestCommonPrefix(vector<string>& strs) {
//        int n = strs.size();
//        if (n == 0) return "";
//        for (int i = 1; i < n; i++)
//        {
//            int m = strs[0].size();
//            for (int j = 0; j < m; j++)
//            {
//                if (j == strs[i].size() || strs[0][j] != strs[i][j])
//                {
//                    return strs[0].substr(0, j);
//                }
//            }
//        }
//        return strs[0];
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     * 计算两个数之和
//     * @param s string字符串 表示第一个整数
//     * @param t string字符串 表示第二个整数
//     * @return string字符串
//     */
//    string add(string& A, string& B)
//    {
//        if (A.size() < B.size()) return add(B, A);
//        string C;
//        int t = 0;
//        for (int i = 0; i < A.size(); i++)
//        {
//            t += A[i] - '0';
//            if (i < B.size()) t += B[i] - '0';
//            C += t % 10 + '0';
//            t /= 10;
//        }
//        if (t != 0) C += t + '0';
//        return C;
//    }
//    string solve(string s, string t) {
//        string A, B;
//        for (int i = s.size() - 1; i >= 0; i--) A += s[i];
//        for (int i = t.size() - 1; i >= 0; i--) B += t[i];
//        string C = add(A, B);
//        reverse(C.begin(), C.end());
//        return C;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     * 验证IP地址
//     * @param IP string字符串 一个IP地址字符串
//     * @return string字符串
//     */
//    vector<string> split(string s, string spliter)
//    {
//        vector<string> res;
//        int i = s.find(spliter);
//        while (i != s.npos)
//        {
//            res.push_back(s.substr(0, i));
//            s = s.substr(i + 1, s.size());
//            i = s.find(spliter);
//        }
//        res.push_back(s);
//        return res;
//    }
//    bool isIPv4(string IP)
//    {
//        vector<string> s = split(IP, ".");
//        if (s.size() != 4) return false;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (s[i].size() == 0) return false;
//            if (s[i].size() > 3 || (s[i][0] == '0' && s[i].size() != 1)) return false;
//            for (int j = 0; j < s[i].size(); j++)
//                if (!isdigit(s[i][j]))
//                    return false;
//            int num = stoi(s[i]);
//            if (num < 0 || num>255) return false;
//        }
//        return true;
//    }
//    bool isIPv6(string IP)
//    {
//        vector<string> s = split(IP, ":");
//        if (s.size() != 8) return false;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (s[i].size() == 0) return false;
//            if (s[i].size() > 4)  return false;
//            for (int j = 0; j < s[i].size(); j++)
//            {
//                char ch = s[i][j];
//                if (!(isdigit(ch) || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F')))
//                    return false;
//            }
//        }
//        return true;
//    }
//    string solve(string IP) {
//        if (isIPv4(IP)) return "IPv4";
//        else if (isIPv6(IP)) return "IPv6";
//        return "Neither";
//    }
//};