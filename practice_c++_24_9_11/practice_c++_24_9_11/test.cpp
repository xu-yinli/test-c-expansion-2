#define _CRT_SECURE_NO_WARNINGS 1

//int cnt = 1;
//int sum = 0;
//
//class A
//{
//public:
//    A()
//    {
//        sum += cnt;
//        cnt++;
//    }
//};
//
//class Solution {
//public:
//    int Sum_Solution(int n) {
//        A arr[n];
//        return sum;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int monthDays[13] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
//
//int main()
//{
//    int y, m, d;
//    cin >> y >> m >> d;
//    int n = monthDays[m - 1] + d;
//    if (m > 2 && ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)))
//        n++;
//    cout << n << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int days[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//
//int getMonthDays(int year, int month)
//{
//    int day = days[month];
//    if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
//        day++;
//    return day;
//}
//
//int main()
//{
//    int y1, m1, d1;
//    int y2, m2, d2;
//    int sub = 0;
//    while (~scanf("%4d%2d%2d", &y1, &m1, &d1))
//    {
//        scanf("%4d%2d%2d", &y2, &m2, &d2);
//        if (y1 < y2 || m1 < m2 || d1 < d2)
//        {
//            while (y1 < y2 || m1 < m2 || d1 < d2)
//            {
//                d1++;
//                if (d1 == getMonthDays(y1, m1) + 1)
//                {
//                    m1++;
//                    d1 = 1;
//                }
//                if (m1 == 13)
//                {
//                    y1++;
//                    m1 = 1;
//                }
//                sub++;
//            }
//            cout << sub + 1 << endl;
//        }
//        else if (y1 > y2 || m1 > m2 || d1 > d2)
//        {
//            while (y1 > y2 || m1 > m2 || d1 > d2)
//            {
//                d2++;
//                if (d2 == getMonthDays(y2, m2) + 1)
//                {
//                    m2++;
//                    d2 = 1;
//                }
//                if (m2 == 13)
//                {
//                    y2++;
//                    m2 = 1;
//                }
//                sub++;
//            }
//            cout << sub + 1 << endl;
//        }
//        else cout << sub << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//bool isLeap(int year)
//{
//    if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) return true;
//    return false;
//}
//
//int days[2][13] = { {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
//                    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31} };
//
//int main()
//{
//    int m, n;
//    while (~scanf("%d %d", &m, &n))
//    {
//        if (isLeap(m))
//        {
//            int y = 1;
//            while (n > days[1][y])
//            {
//                n -= days[1][y];
//                y++;
//            }
//            printf("%04d-%02d-%02d\n", m, y, n);
//        }
//        else
//        {
//            int y = 1;
//            while (n > days[0][y])
//            {
//                n -= days[0][y];
//                y++;
//            }
//            printf("%04d-%02d-%02d\n", m, y, n);
//        }
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int days[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//
//int getMonthDays(int year, int month)
//{
//    int day = days[month];
//    if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
//        day++;
//    return day;
//}
//
//int main()
//{
//    int m;
//    cin >> m;
//    int year, month, day;
//    int add;
//    int sum;
//    while (m--)
//    {
//        cin >> year >> month >> day >> add;
//        day += add;
//        while (day > getMonthDays(year, month))
//        {
//            day -= getMonthDays(year, month);
//            month++;
//            if (month == 13)
//            {
//                year++;
//                month = 1;
//            }
//        }
//        printf("%04d-%02d-%02d\n", year, month, day);
//    }
//    return 0;
//}


//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& strs) {
//        int n = strs[0].size();
//        for (int i = 0; i < n; i++)
//        {
//            char ch = strs[0][i];
//            for (int j = 0; j < strs.size(); j++)
//            {
//                if (i == strs[j].size() || ch != strs[j][i])
//                    return strs[0].substr(0, i);
//            }
//        }
//        return strs[0];
//    }
//};


//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& strs) {
//        int k = strs[0].size();
//        for (int i = 1; i < strs.size(); i++)
//        {
//            for (int j = 0; j < strs[0].size(); j++)
//            {
//                if (j == strs[i].size() || strs[0][j] != strs[i][j])
//                {
//                    k = min(k, j);
//                    break;
//                }
//            }
//        }
//        return strs[0].substr(0, k);
//    }
//};


//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size();
//        int st = 0;
//        int len = 0;
//        for (int i = 0; i < n; i++)
//        {
//            int left = i, right = i;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                left--;
//                right++;
//            }
//            if (right - left - 1 > len)
//            {
//                len = right - left - 1;
//                st = left + 1;
//            }
//            left = i, right = i + 1;
//            while (left >= 0 && right < n && s[left] == s[right])
//            {
//                left--;
//                right++;
//            }
//            if (right - left - 1 > len)
//            {
//                len = right - left - 1;
//                st = left + 1;
//            }
//        }
//        return s.substr(st, len);
//    }
//};


//class Solution {
//public:
//    string addBinary(string a, string b) {
//        string sum;
//        int cur1 = a.size() - 1;
//        int cur2 = b.size() - 1;
//        int t = 0;
//        while (cur1 >= 0 || cur2 >= 0 || t)
//        {
//            if (cur1 >= 0) t += a[cur1--] - '0';
//            if (cur2 >= 0) t += b[cur2--] - '0';
//            sum += t % 2 + '0';
//            t /= 2;
//        }
//        reverse(sum.begin(), sum.end());
//        return sum;
//    }
//};


//class Solution {
//public:
//    string multiply(string num1, string num2) {
//        int n = num1.size(), m = num2.size();
//        reverse(num1.begin(), num1.end());
//        reverse(num2.begin(), num2.end());
//        vector<int> str(n + m - 1);
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                str[i + j] += (num1[i] - '0') * (num2[j] - '0');
//        string res;
//        int t = 0;
//        int cur = 0;
//        while (cur < n + m - 1 || t)
//        {
//            if (cur < n + m - 1) t += str[cur++];
//            res += t % 10 + '0';
//            t /= 10;
//        }
//        while (res.size() > 1 && res.back() == '0') res.pop_back();
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};