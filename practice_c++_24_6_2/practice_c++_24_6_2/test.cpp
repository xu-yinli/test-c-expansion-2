#define _CRT_SECURE_NO_WARNINGS 1

///*
//struct RandomListNode {
//    int label;
//    struct RandomListNode *next, *random;
//    RandomListNode(int x) :
//            label(x), next(NULL), random(NULL) {
//    }
//};
//*/
//class Solution {
//public:
//    RandomListNode* Clone(RandomListNode* pHead) {
//        RandomListNode* cur = pHead;
//        while (cur)
//        {
//            RandomListNode* tmp = new RandomListNode(cur->label);
//            tmp->next = cur->next;
//            cur->next = tmp;
//            cur = tmp->next;
//        }
//
//        RandomListNode* old = pHead;
//        RandomListNode* clone = nullptr;
//        while (old)
//        {
//            clone = old->next;
//            if (old->random != nullptr)
//            {
//                clone->random = old->random->next;
//            }
//            old = clone->next;
//        }
//
//        old = pHead;
//        RandomListNode* cloneHead = nullptr;
//        if (old)
//        {
//            cloneHead = clone = old->next;
//            old->next = clone->next;
//            old = old->next;
//        }
//        while (old)
//        {
//            clone->next = old->next;
//            clone = clone->next;
//            old->next = clone->next;
//            old = old->next;
//        }
//        return cloneHead;
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    Node* next;
//    Node* random;
//
//    Node(int _val) {
//        val = _val;
//        next = NULL;
//        random = NULL;
//    }
//};
//*/
//
//class Solution {
//public:
//    Node* copyRandomList(Node* head) {
//        Node* cur = head;
//        while (cur)
//        {
//            Node* tmp = new Node(cur->val);
//            tmp->next = cur->next;
//            cur->next = tmp;
//            cur = tmp->next;
//        }
//
//        Node* old = head;
//        Node* clone = nullptr;
//        while (old)
//        {
//            clone = old->next;
//            if (old->random != nullptr)
//            {
//                clone->random = old->random->next;
//            }
//            old = clone->next;
//        }
//
//        old = head;
//        Node* cloneHead = nullptr;
//        if (old)
//        {
//            cloneHead = clone = old->next;
//            old->next = clone->next;
//            old = old->next;
//        }
//        while (old)
//        {
//            clone->next = old->next;
//            clone = clone->next;
//            old->next = clone->next;
//            old = old->next;
//        }
//        return cloneHead;
//    }
//};


///*
//struct TreeNode {
//	int val;
//	struct TreeNode *left;
//	struct TreeNode *right;
//	TreeNode(int x) :
//			val(x), left(NULL), right(NULL) {
//	}
//};*/
//class Solution {
//public:
//	void InOrderConvert(TreeNode* cur, TreeNode*& prev) {
//		if (cur == nullptr) return;
//		InOrderConvert(cur->left, prev);
//		cur->left = prev;
//		if (prev)
//			prev->right = cur;
//		prev = cur;
//		InOrderConvert(cur->right, prev);
//	}
//
//	TreeNode* Convert(TreeNode* pRootOfTree) {
//		TreeNode* prev = nullptr;
//		InOrderConvert(pRootOfTree, prev);
//		TreeNode* head = pRootOfTree;
//		while (head && head->left)
//			head = head->left;
//		return head;
//	}
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    Node* left;
//    Node* right;
//
//    Node() {}
//
//    Node(int _val) {
//        val = _val;
//        left = NULL;
//        right = NULL;
//    }
//
//    Node(int _val, Node* _left, Node* _right) {
//        val = _val;
//        left = _left;
//        right = _right;
//    }
//};
//*/
//class Solution {
//public:
//    void InOrderConvert(Node* cur, Node*& prev) {
//        if (cur == nullptr) return;
//        InOrderConvert(cur->left, prev);
//        cur->left = prev;
//        if (prev)
//            prev->right = cur;
//        prev = cur;
//        InOrderConvert(cur->right, prev);
//    }
//    Node* treeToDoublyList(Node* root) {
//        if (root == nullptr) return nullptr;
//        Node* prev = nullptr;
//        InOrderConvert(root, prev);
//        Node* head = root;
//        Node* tail = root;
//        while (head && head->left)
//            head = head->left;
//        while (tail && tail->right)
//            tail = tail->right;
//        head->left = tail;
//        tail->right = head;
//        return head;
//    }
//};


///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    Node* left;
//    Node* right;
//
//    Node() {}
//
//    Node(int _val) {
//        val = _val;
//        left = NULL;
//        right = NULL;
//    }
//
//    Node(int _val, Node* _left, Node* _right) {
//        val = _val;
//        left = _left;
//        right = _right;
//    }
//};
//*/
//class Solution {
//private:
//    Node* prev;
//    Node* head;
//public:
//    void dfs(Node* cur)
//    {
//        if (cur == NULL) return;
//        dfs(cur->left);
//        if (prev) prev->right = cur;
//        else head = cur;
//        cur->left = prev;
//        prev = cur;
//        dfs(cur->right);
//    }
//    Node* treeToDoublyList(Node* root) {
//        if (root == NULL) return NULL;
//        dfs(root);
//        head->left = prev;
//        prev->right = head;
//        return head;
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 2e5 + 10;
//int arr[N], dp[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++) cin >> arr[i];
//    dp[0] = arr[0];
//    int res = arr[0];
//    for (int i = 1; i < n; i++)
//    {
//        dp[i] = max(dp[i - 1] + arr[i], arr[i]);
//        res = max(res, dp[i]);
//    }
//    cout << res << endl;
//    return 0;
//}


//class Solution {
//public:
//    int maxSubArray(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> dp(n + 1);
//        int res = -2e4;
//        for (int i = 1; i <= n; i++)
//        {
//            dp[i] = max(nums[i - 1], dp[i - 1] + nums[i - 1]);
//            if (dp[i] > res) res = dp[i];
//        }
//        return res;
//    }
//};


//#include <iostream>
//using namespace std;
//
//const int N = 2e5 + 10;
//int arr[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++) cin >> arr[i];
//    int sum = arr[0];
//    int res = arr[0];
//    for (int i = 1; i < n; i++)
//    {
//        if (sum >= 0) sum += arr[i];
//        else sum = arr[i];
//        res = max(res, sum);
//    }
//    cout << res << endl;
//    return 0;
//}


#include <iostream>
#include <string>
using namespace std;

bool IsPalindrome(string& s, int* st, int* ed)
{
    int left = 0, right = s.size() - 1;
    bool flag = true;
    while (left <= right)
    {
        if (s[left] != s[right])
        {
            flag = false;
            break;
        }
        left++;
        right--;
    }
    if (st != nullptr) *st = left;
    if (ed != nullptr) *ed = right;
    return flag;
}

int main()
{
    int t;
    cin >> t;
    while (t--)
    {
        string s;
        cin >> s;
        int st = 0, ed = s.size() - 1;
        if (IsPalindrome(s, &st, &ed)) cout << -1 << endl;
        else
        {
            s.erase(ed, 1);
            if (IsPalindrome(s, nullptr, nullptr)) cout << ed << endl;
            else cout << st << endl;
        }
    }
    return 0;
}