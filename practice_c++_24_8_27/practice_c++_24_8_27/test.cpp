#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string>
#include <assert.h>

//计数器方式
//size_t my_strlen(const char* str)
//{
//	assert(str != NULL);
//	size_t count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}

//不能创建临时变量计数器
//size_t my_strlen(const char* str)
//{
//	if (*str == '\0') return 0;
//	else return 1 + my_strlen(str + 1);
//}

//指针-指针的方式
//size_t my_strlen(const char* s)
//{
//	assert(s != NULL);
//	const char* p = s;
//	while (*p != '\0')
//		p++;
//	return p - s;
//}

//int main()
//{
//	const char* p = "abcdef";
//	int len = my_strlen(p);
//	printf("len = %d\n", len); //len = 6
//	return 0;
//}



//char* my_strcpy(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* p = dest;
//	while ((*dest++ = *src++))
//	{
//		;
//	}
//	return p;
//}


//char* my_strcat(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* p = dest;
//	while (*dest)
//		dest++;
//	strcpy(dest, src);
//	return p;
//}


//int main()
//{
//	char s1[100] = "hello ";
//	char s2[10] = "world";
//	char * res = strcat(s1, s1);
//	printf("%s\n", res);
//	return 0;
//}


//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1++ == *str2++)
//	{
//		if (*str1 == '\0' && *str2 == '\0')
//			return 0;
//		if (*str1 == '\0' || *str2 == '\0')
//			break;
//	}
//	return *str1 - *str2;
//}
//
//int main()
//{
//	char s1[20] = "asdabs";
//	char s2[10] = "abc";
//	int res = my_strcmp(s1, s2);
//	if (-1 == res) printf("str1<str2\n");
//	else if (1 == res) printf("str1>str2\n");
//	else printf("str1=str2\n");
//	return 0;
//}


//const char* my_strstr(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//
//	const char* s1 = str1;
//	const char* s2 = str2;
//	const char* cp = str1; //控制起始点
//	while (*cp)
//	{
//		s1 = cp;
//		s2 = str2;
//		while (*s1 && *s2 && (*s1 == *s2)) //防止空字符串
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//			return cp;
//		cp++;
//	}
//	return NULL;
//}

//const char* my_strstr(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	int len2 = strlen(str2);
//	const char* cp = str1;
//	while (*cp)
//	{
//		int ret = strncmp(cp, str2, len2);
//		if (ret == 0)
//			return cp;
//		cp++;
//		if (strlen(cp) < len2)
//			return NULL;
//	}
//}
//
//int main()
//{
//	char s1[] = "abcadefdef";
//	char s2[] = "def";
//	const char* res = my_strstr(s1, s2);
//	if (res) printf("%s\n", res);
//	else printf("没找到\n");
//	return 0;
//}


//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}


//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	if (dest > src)
//	{
//		// 从后往前
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	else if(dest < src)
//	{
//		// 从前往后
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	return ret;
//}


//#include <stdio.h>
//#include <string.h>
// 
//int main()
//{
//	char str[] = "- This, a sample string.";
//	char* pch;
//	printf("Splitting string \"%s\" into tokens:\n", str);
//	pch = strtok(str, " ,.-");
//	while (pch != NULL)
//	{
//		printf("%s\n", pch);
//		pch = strtok(NULL, " ,.-");
//	}
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>
//
//int main()
//{
//	char str[][5] = { "R2D2" , "C3PO" , "R2A6" };
//	for (int i = 0; i < 3; i++)
//	{
//		if (strncmp(str[i], "R2xx", 2) == 0)
//			printf("found %s\n", str[i]);
//	}
//	return 0;
//}


//leetcode
//class Solution {
//public:
//    int strStr(string haystack, string needle) {
//        int n = haystack.size(), m = needle.size();
//        for (int i = 0; i < n; i++)
//        {
//            int k = i;
//            int j = 0;
//            while (j < m && haystack[k] == needle[j])
//            {
//                k++; j++;
//            }
//            if (j == m) return i;
//        }
//        return -1;
//    }
//};


//class Solution {
//public:
//    bool repeatedSubstringPattern(string s) {
//        string str = "";
//        int n = s.size();
//        for (int i = 0; i < n - 1; i++)
//        {
//            str += s[i];
//            int sz = str.size();
//            if (n % sz != 0) continue;
//            int count = n / sz;
//            string tmp = "";
//            while (count--)
//            {
//                tmp += str;
//            }
//            if (tmp == s) return true;
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    bool repeatedSubstringPattern(string s) {
//        string str = s + s;
//        if (str.find(s, 1) != s.size()) return true;
//        return false;
//    }
//};


//#include <iostream>
//
//using namespace std;
//
//bool is_number(char ch)
//{
//    if (ch >= '0' && ch <= '9') return true;
//    return false;
//}
//
//int main()
//{
//    string s;
//    cin >> s;
//    int n = s.size();
//    for (int i = 0; i < n; i++)
//    {
//        if (is_number(s[i])) cout << "number";
//        else cout << s[i];
//    }
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//int main()
//{
//    int k;
//    cin >> k;
//    string s;
//    cin >> s;
//    int n = s.size();
//    string str;
//    if (k >= n) cout << s << endl;
//    else
//    {
//        int i = n - 1;
//        int count = k;
//        while (count--)
//        {
//            str += s[i--];
//        }
//        reverse(str.begin(), str.end());
//    }
//    for (int i = 0; i < n - k; i++)
//        str += s[i];
//    cout << str << endl;
//    return 0;
//}


//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//    int k;
//    cin >> k;
//    string s;
//    cin >> s;
//    int n = s.size();
//    string tmp = s.substr(n - k, k);
//    for (int i = 0; i < n - k; i++)
//        tmp += s[i];
//    cout << tmp << endl;
//    return 0;
//}


#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    int k;
    cin >> k;
    string s;
    cin >> s;
    int n = s.size();
    reverse(s.begin(), s.end());
    reverse(s.begin(), s.begin() + k);
    reverse(s.begin() + k, s.end());
    cout << s << endl;
    return 0;
}