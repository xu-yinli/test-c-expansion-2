﻿#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//private:
//    const int INF = 0x3f3f3f3f;
//public:
//    int coinChange(vector<int>& coins, int amount) {
//        int n = coins.size();
//        vector<vector<int>> dp(n + 1, vector<int>(amount + 1, INF));
//        dp[0][0] = 0;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 0; j <= amount; j++)
//            {
//                if (j >= coins[i - 1]) dp[i][j] = min(dp[i - 1][j], dp[i][j - coins[i - 1]] + 1);
//                else dp[i][j] = dp[i - 1][j];
//            }
//        }
//        if (dp[n][amount] == INF) return -1;
//        return dp[n][amount];
//    }
//};


//class Solution {
//private:
//    int hash[100010];
//public:
//    int totalFruit(vector<int>& fruits) {
//        int n = fruits.size();
//        int kinds = 0, res = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            if (hash[fruits[right]] == 0) kinds++;
//            hash[fruits[right]]++;
//            while (kinds > 2)
//            {
//                if (hash[fruits[left]] == 1) kinds--;
//                hash[fruits[left]]--;
//                left++;
//            }
//            res = max(res, right - left + 1);
//            right++;
//        }
//        return res;
//    }
//};


//#include <iostream>
//#include <string>
//using namespace std;
//
//typedef long long LL;
//
//int n, l, r;
//string s;
//
//// 找出字符种类在[1, x]之间的⼦串的个数
//LL find(int x)
//{
//    if (x == 0) return 0;
//    int hash[26] = { 0 };
//    int kinds = 0;
//    LL res = 0;
//    int left = 0, right = 0;
//    while (right < n)
//    {
//        if (hash[s[right] - 'a'] == 0) kinds++;
//        hash[s[right] - 'a']++;
//        while (kinds > x)
//        {
//            if (hash[s[left] - 'a'] == 1) kinds--;
//            hash[s[left] - 'a']--;
//            left++;
//        }
//        res += right - left + 1;
//        right++;
//    }
//    return res;
//}
//
//int main()
//{
//    cin >> n >> l >> r >> s;
//    cout << find(r) - find(l - 1) << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int t;
//    cin >> t;
//    while (t--)
//    {
//        int l, r;
//        cin >> l >> r;
//        int res = 0;
//        while (r > 1)
//        {
//            r /= 2;
//            res++;
//        }
//        cout << res << endl;
//    }
//    return 0;
//}


//class Solution {
//private:
//    int cnt;
//    int left, right;
//    string path;
//    vector<string> res;
//public:
//    void dfs()
//    {
//        if (right == cnt)
//        {
//            res.push_back(path);
//            return;
//        }
//        if (left < cnt)
//        {
//            path.push_back('(');
//            left++;
//            dfs();
//            left--;
//            path.pop_back();
//        }
//        if (right < left)
//        {
//            path.push_back(')');
//            right++;
//            dfs();
//            right--;
//            path.pop_back();
//        }
//    }
//    vector<string> generateParenthesis(int n) {
//        cnt = n;
//        dfs();
//        return res;
//    }
//};


//class Solution {
//private:
//    int left, right;
//    string path;
//    vector<string> res;
//public:
//    void dfs(int n)
//    {
//        if (right == n)
//        {
//            res.push_back(path);
//            return;
//        }
//        if (left < n)
//        {
//            path.push_back('(');
//            left++;
//            dfs(n);
//            left--;
//            path.pop_back();
//        }
//        if (right < left)
//        {
//            path.push_back(')');
//            right++;
//            dfs(n);
//            right--;
//            path.pop_back();
//        }
//    }
//    vector<string> generateParenthesis(int n) {
//        dfs(n);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> path;
//    vector<vector<int>> res;
//public:
//    void dfs(int pos, int n, int k)
//    {
//        if (path.size() == k)
//        {
//            res.push_back(path);
//            return;
//        }
//        for (int i = pos; i <= n; i++)
//        {
//            path.push_back(i);
//            dfs(i + 1, n, k);
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> combine(int n, int k) {
//        dfs(1, n, k);
//        return res;
//    }
//};


//class Solution {
//private:
//    int res;
//    int path;
//public:
//    void dfs(vector<int>& nums, int pos, int target)
//    {
//        if (pos == nums.size())
//        {
//            if (path == target) res++;
//            return;
//        }
//        //做加法
//        path += nums[pos];
//        dfs(nums, pos + 1, target);
//        path -= nums[pos];
//
//        //做减法
//        path -= nums[pos];
//        dfs(nums, pos + 1, target);
//        path += nums[pos];
//    }
//    int findTargetSumWays(vector<int>& nums, int target) {
//        dfs(nums, 0, target);
//        return res;
//    }
//};


//class Solution {
//private:
//    int res;
//public:
//    void dfs(vector<int>& nums, int pos, int path, int target)
//    {
//        if (pos == nums.size())
//        {
//            if (path == target) res++;
//            return;
//        }
//        //做加法
//        dfs(nums, pos + 1, path + nums[pos], target);
//
//        //做减法
//        dfs(nums, pos + 1, path - nums[pos], target);
//    }
//    int findTargetSumWays(vector<int>& nums, int target) {
//        dfs(nums, 0, 0, target);
//        return res;
//    }
//};


//class Solution {
//public:
//    int n;
//    vector<int> path;
//    vector<vector<int>> res;
//    void dfs(vector<int>& candidates, int pos, int sum, int target)
//    {
//        if (sum == target)
//        {
//            res.push_back(path);
//            return;
//        }
//        if (sum > target || pos == n) return;
//        for (int i = pos; i < n; i++)
//        {
//            path.push_back(candidates[i]);
//            dfs(candidates, i, sum + candidates[i], target);
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        n = candidates.size();
//        dfs(candidates, 0, 0, target);
//        return res;
//    }
//};


//class Solution {
//public:
//    int n;
//    vector<int> path;
//    vector<vector<int>> res;
//    void dfs(vector<int>& candidates, int pos, int sum, int target)
//    {
//        if (sum == target)
//        {
//            res.push_back(path);
//            return;
//        }
//        if (sum > target || pos == n) return;
//        for (int k = 0; k * candidates[pos] <= target; k++)
//        {
//            if (k) path.push_back(candidates[pos]);
//            dfs(candidates, pos + 1, sum + k * candidates[pos], target);
//        }
//        for (int k = 1; k * candidates[pos] <= target; k++)
//        {
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        n = candidates.size();
//        dfs(candidates, 0, 0, target);
//        return res;
//    }
//};


//class Solution {
//public:
//    int n;
//    vector<int> path;
//    vector<vector<int>> res;
//    void dfs(vector<int>& candidates, int pos, int sum, int target)
//    {
//        if (sum == target)
//        {
//            res.push_back(path);
//            return;
//        }
//        if (sum > target || pos == n) return;
//        for (int k = 0; k * candidates[pos] + sum <= target; k++)
//        {
//            if (k) path.push_back(candidates[pos]);
//            dfs(candidates, pos + 1, sum + k * candidates[pos], target);
//        }
//        for (int k = 1; k * candidates[pos] + sum <= target; k++)
//        {
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
//        n = candidates.size();
//        dfs(candidates, 0, 0, target);
//        return res;
//    }
//};


//class Solution {
//private:
//    string path;
//    vector<string> res;
//public:
//    char change(char x)
//    {
//        if (x >= 'a' && x <= 'z') x -= 32;
//        else if (x >= 'A' && x <= 'Z') x += 32;
//        return x;
//    }
//    void dfs(string& s, int pos)
//    {
//        if (pos == s.size())
//        {
//            res.push_back(path);
//            return;
//        }
//        //不变
//        path.push_back(s[pos]);
//        dfs(s, pos + 1);
//        path.pop_back();
//
//        //改变
//        if (s[pos] < '0' || s[pos]>'9')
//        {
//            char ch = change(s[pos]);
//            path.push_back(ch);
//            dfs(s, pos + 1);
//            path.pop_back();
//        }
//    }
//    vector<string> letterCasePermutation(string s) {
//        dfs(s, 0);
//        return res;
//    }
//};


//class Solution {
//private:
//    int res;
//    int check[20];
//public:
//    void dfs(int pos, int n)
//    {
//        if (pos == n + 1)
//        {
//            res++;
//            return;
//        }
//        for (int i = 1; i <= n; i++)
//        {
//            if (!check[i] && (pos % i == 0 || i % pos == 0))
//            {
//                check[i] = true;
//                dfs(pos + 1, n);
//                check[i] = false;
//            }
//        }
//    }
//    int countArrangement(int n) {
//        dfs(1, n);
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<string> path;
//    vector<vector<string>> res;
//    vector<bool> checkCol, checkDg, checkUdg;
//public:
//    void dfs(int row, int n)
//    {
//        if (row == n)
//        {
//            res.push_back(path);
//            return;
//        }
//        for (int col = 0; col < n; col++)
//        {
//            if (!checkCol[col] && !checkDg[row - col + n] && !checkUdg[row + col])
//            {
//                path[row][col] = 'Q';
//                checkCol[col] = checkDg[row - col + n] = checkUdg[row + col] = true;
//                dfs(row + 1, n);
//                checkCol[col] = checkDg[row - col + n] = checkUdg[row + col] = false;
//                path[row][col] = '.';
//            }
//        }
//    }
//    vector<vector<string>> solveNQueens(int n) {
//        checkCol = vector<bool>(n, false);
//        checkDg = vector<bool>(2 * n, false);
//        checkUdg = vector<bool>(2 * n, false);
//        path.resize(n);
//        for (int i = 0; i < n; i++)
//            path[i].append(n, '.');
//        dfs(0, n);
//        return res;
//    }
//};


//class Solution {
//public:
//    //f[i]:表示以i位置元素为结尾的所有子数组中，最后呈现上升状态下的最长湍流数组的长度
//    //g[i]:表示以i位置元素为结尾的所有子数组中，最后呈现下降状态下的最长湍流数组的长度
//    int maxTurbulenceSize(vector<int>& arr) {
//        int n = arr.size();
//        vector<int> f(n + 1, 1);
//        vector<int> g(n + 1, 1);
//        int res = 1;
//        for (int i = 1; i < n; i++)
//        {
//            if (arr[i - 1] < arr[i]) f[i] = g[i - 1] + 1;
//            else if (arr[i - 1] > arr[i]) g[i] = f[i - 1] + 1;
//            res = max(res, max(f[i], g[i]));
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    bool wordBreak(string s, vector<string>& wordDict) {
//        unordered_set<string> hash;
//        for (auto& s : wordDict)
//            hash.insert(s);
//        int n = s.size();
//        vector<bool> dp(n + 1, false);
//        dp[0] = true;
//        s = ' ' + s;
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = i; j >= 1; j--)
//            {
//                string word = s.substr(j, i - j + 1);
//                if (dp[j - 1] && hash.count(word))
//                {
//                    dp[i] = true;
//                    break;
//                }
//            }
//        }
//        return dp[n];
//    }
//};


//class Solution {
//public:
//    int findSubstringInWraproundString(string s) {
//        int n = s.size();
//        vector<int> dp(n, 1);
//        for (int i = 1; i < n; i++)
//            if (s[i - 1] + 1 == s[i] || (s[i - 1] == 'z' && s[i] == 'a'))
//                dp[i] += dp[i - 1];
//        int hash[26] = { 0 };
//        for (int i = 0; i < n; i++)
//            hash[s[i] - 'a'] = max(hash[s[i] - 'a'], dp[i]);
//        int sum = 0;
//        for (auto x : hash)
//            sum += x;
//        return sum;
//    }
//};


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//int main()
//{
//    int t;
//    cin >> t;
//    while (t--)
//    {
//        LL n, m;
//        cin >> n >> m;
//        // 确定了位运算的范围,保证下面操作不会越界
//        LL left = max((LL)0, n - m);
//        LL right = n + m;
//        LL cnt = 0;
//        while(left != right)
//        {
//            cnt++;
//            left >>= 1;
//            right >>= 1;
//        }
//        while(cnt--)
//            right = (right << 1) ^ 1;
//        cout << right << endl;
//    }
//    return 0;
//}