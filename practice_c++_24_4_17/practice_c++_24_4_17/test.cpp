#define _CRT_SECURE_NO_WARNINGS 1

///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
//        ListNode* cur1 = l1;
//        ListNode* cur2 = l2;
//        ListNode* newhead = new ListNode(0);
//        ListNode* prev = newhead;
//        int t = 0;
//        while (cur1 || cur2 || t)
//        {
//            if (cur1)
//            {
//                t += cur1->val;
//                cur1 = cur1->next;
//            }
//            if (cur2)
//            {
//                t += cur2->val;
//                cur2 = cur2->next;
//            }
//            prev->next = new ListNode(t % 10);
//            prev = prev->next;
//            t /= 10;
//        }
//        return newhead->next;
//    }
//};


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    cout << "To iterate is human, to recurse divine." << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n, k, m;
//    cin >> n >> k >> m;
//    cout << n - k * m << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    while (n--)
//    {
//        double p;
//        cin >> p;
//        if (p < m) printf("On Sale! %.1lf\n", p);
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string s;
//    cin >> s;
//    int n = s.size();
//    int y = 0, m = 0;
//    for (int i = 0; i < n - 2; i++)
//        y = y * 10 + (s[i] - '0');
//    for (int i = n - 2; i < n; i++)
//        m = m * 10 + (s[i] - '0');
//    if (y <= 21) y = 20 * 100 + y;
//    else if (y > 21 && y < 100) y = 19 * 100 + y;
//    printf("%04d-%02d\n", y, m);
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int n, m; //题目数量 做完了多少道
//    cin >> n >> m;
//    getchar();
//    while (n--)
//    {
//        string s;
//        getline(cin, s);
//        if (s.find("qiandao") == -1 && s.find("easy") == -1)
//            m--;
//        if (m < 0)
//        {
//            cout << s << endl;
//            break;
//        }
//    }
//    if (m >= 0) cout << "Wo AK le" << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int a[24];
//
//int main()
//{
//    for (int i = 0; i < 24; i++)
//        cin >> a[i];
//    while (1)
//    {
//        int t;
//        cin >> t;
//        if (t < 0 || t>23)
//            break;
//        if (a[t] > 50)
//            cout << a[t] << ' ' << "Yes" << endl;
//        else
//            cout << a[t] << ' ' << "No" << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e6 + 10, INF = 1e9 + 7;
//int cap[N];
//
//int main()
//{
//    int n; //参赛学生的总数
//    cin >> n;
//    int min = INF, max = -INF;
//    int a, b;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> cap[i];
//        if (cap[i] <= min)
//        {
//            if (cap[i] != min)
//                a = 0;
//            min = cap[i];
//            a++;
//        }
//        if (cap[i] >= max)
//        {
//            if (cap[i] != max)
//                b = 0;
//            max = cap[i];
//            b++;
//        }
//    }
//    cout << min << ' ' << a << endl;
//    cout << max << ' ' << b << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e3 + 10;
//int a[N];
//
//int main()
//{
//    int n;
//    cin >> a[1] >> a[2] >> n;
//    int k = 3;
//    for (int i = 1; i <= n; i++)
//    {
//        int t = a[i] * a[i + 1];
//        if (t / 10 == 0)
//            a[k++] = t;
//        else
//        {
//            a[k++] = t / 10;
//            a[k++] = t % 10;
//        }
//    }
//    for (int i = 1; i < n; i++)
//        cout << a[i] << ' ';
//    cout << a[n] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 100010;
//int p[N], visp[N];
//
//int status(int x)
//{
//    if (visp[x] == 0)
//    {
//        int cnt;
//        if (p[x] == -1)
//            cnt = 1;
//        else
//            cnt = status(p[x]) + 1;
//        visp[x] = cnt;
//        return visp[x];
//    }
//    return visp[x];
//}
//
//int main()
//{
//    int n; //家族人口总数
//    cin >> n;
//    int a;
//    for (int i = 1; i <= n; i++)
//        cin >> p[i];
//    int young = 1; //最小的辈分
//    for (int i = 1; i <= n; i++)
//        young = max(young, status(i));
//    cout << young << endl;
//    int flag = false;
//    for (int i = 1; i <= n; i++)
//    {
//        if (visp[i] == young)
//        {
//            if (flag)
//                cout << ' ';
//            cout << i;
//            flag = true;
//        }
//    }
//    cout << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 10010;
//
//struct Student
//{
//    string id;
//    int score;
//    bool operator<(const Student& t)
//    {
//        if (score != t.score) return score > t.score;
//        else return id < t.id;
//    }
//}stu[N];
//
//int main()
//{
//    int n, g, k; //学生总数 代金券等级分界线 进入名人堂的最低名次
//    cin >> n >> g >> k;
//    int pat = 0;
//    for (int i = 0; i < n; i++)
//    {
//        cin >> stu[i].id >> stu[i].score;
//        if (stu[i].score >= g) pat += 50;
//        else if (stu[i].score >= 60) pat += 20;
//    }
//    sort(stu, stu + n);
//    cout << pat << endl;
//    int j = 1;
//    int p = 0;
//    for (int i = 0; i < n; i++)
//    {
//        if (i > 0 && stu[i - 1].score != stu[i].score)
//            j = i + 1;
//        if (j > k) break;
//        cout << j << ' ' << stu[i].id << ' ' << stu[i].score << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e4 + 10;
//int p[N], s[N];
//
//int find(int x)
//{
//    if (p[x] != x) p[x] = find(p[x]);
//    return p[x];
//}
//
//void merge(int a, int b)
//{
//    if (find(a) == find(b)) return;
//    else
//    {
//        s[find(a)] += s[find(b)];
//        p[find(b)] = find(a);
//    }
//    return;
//}
//
//int main()
//{
//    int n; //小圈子的个数
//    cin >> n;
//    for (int i = 1; i <= N; i++)
//    {
//        p[i] = i;
//        s[i] = 1;
//    }
//    int max = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        int k;
//        cin >> k;
//        int per1;
//        cin >> per1;
//        if (per1 > max) max = per1;
//        for (int j = 2; j <= k; j++)
//        {
//            int per2;
//            cin >> per2;
//            if (per2 > max) max = per2;
//            merge(p[per1], per2);
//        }
//    }
//    int total = 0; //社区的总人数
//    int tribe = 0; //部落个数
//    for (int i = 1; i <= max; i++)
//    {
//        if (p[i] == i)
//        {
//            tribe++;
//            total += s[i];
//        }
//    }
//    cout << total << ' ' << tribe << endl;
//    int q;
//    cin >> q;
//    while (q--)
//    {
//        int x, y;
//        cin >> x >> y;
//        if (find(x) == find(y)) cout << 'Y' << endl;
//        else cout << 'N' << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <stack>
//#include <algorithm>
//using namespace std;
//
//int main()
//{
//    string s;
//    cin >> s;
//    stack<char> st;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (st.size() && s[i] == st.top())
//            st.pop();
//        else
//            st.push(s[i]);
//    }
//    string res;
//    while (st.size())
//    {
//        res += st.top();
//        st.pop();
//    }
//    reverse(res.begin(), res.end());
//    if (res.size() == 0) cout << 0 << endl;
//    else cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string s;
//    cin >> s;
//    string st;
//    for (int i = 0; i < s.size(); i++)
//    {
//        if (st.size() && s[i] == st.back())
//            st.pop_back();
//        else
//            st += s[i];
//    }
//    if (st.size() == 0) cout << 0 << endl;
//    else cout << st << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e7 + 10, INF = 1e9 + 7;
//int a[N];
//
//int main()
//{
//    int n, x;
//    cin >> n >> x;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    int sum = 0;
//    int len = INF;
//    int i = 1;
//    int l = 1, r = 1;
//    for (int j = 1; j <= n; j++)
//    {
//        sum += a[j];
//        while (sum >= x)
//        {
//            if (j - i + 1 < len)
//            {
//                len = j - i + 1;
//                l = i, r = j;
//            }
//            sum -= a[i];
//            i++;
//        }
//    }
//    cout << l << ' ' << r << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1e7 + 10;
//int a[N];
//
//int main()
//{
//    int n, x;
//    cin >> n >> x;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    int sum = 0;
//    int len = N;
//    int i = 1, j = 1;
//    int left = 1, right = 1;
//    while (j <= n)
//    {
//        sum += a[j];
//        while (sum >= x)
//        {
//            if (j - i + 1 < len)
//            {
//                len = j - i + 1;
//                left = i, right = j;
//            }
//            sum -= a[i++];
//        }
//        j++;
//    }
//    cout << left << ' ' << right << endl;
//    return 0;
//}


//class Solution {
//public:
//    string toLowerCase(string s) {
//        for (int i = 0; i < s.size(); i++)
//            s[i] = tolower(s[i]);
//        return s;
//    }
//};


//class Solution {
//public:
//    string toLowerCase(string s) {
//        for (int i = 0; i < s.size(); i++)
//            if (s[i] >= 'A' && s[i] <= 'Z')
//                s[i] += 32;
//        return s;
//    }
//};


//class Solution {
//public:
//    string pathEncryption(string path) {
//        for (int i = 0; i < path.size(); i++)
//            if (path[i] == '.')
//                path[i] = ' ';
//        return path;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param s string字符串
//     * @return string字符串
//     */
//    string replaceSpace(string s) {
//        string res;
//        for (int i = 0; i < s.size(); i++)
//        {
//            if (s[i] == ' ') res += "%20";
//            else res += s[i];
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param s string字符串
//     * @return string字符串
//     */
//    string replaceSpace(string s) {
//        if (s.size() == 0) return s;
//        int cnt = 0; //记录空格个数
//        for (auto c : s)
//        {
//            if (c == ' ')
//                cnt++;
//        }
//        int old_end = s.size() - 1;
//        for (int i = 0; i < cnt * 2; i++)
//            s.push_back(' ');
//        int new_end = s.size() - 1;
//        while (old_end >= 0 && new_end >= 0)
//        {
//            if (s[old_end] == ' ')
//            {
//                s[new_end--] = '0';
//                s[new_end--] = '2';
//                s[new_end--] = '%';
//                old_end--;
//            }
//            else
//            {
//                s[new_end--] = s[old_end--];
//            }
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    int bitSum(int x)
//    {
//        int sum = 0;
//        while (x)
//        {
//            int t = x % 10;
//            sum += t * t;
//            x /= 10;
//        }
//        return sum;
//    }
//    bool isHappy(int n) {
//        int slow = n, fast = bitSum(n);
//        while (slow != fast)
//        {
//            slow = bitSum(slow);
//            fast = bitSum(bitSum(fast));
//        }
//        if (slow == 1) return true;
//        else return false;
//    }
//};


//class Solution {
//private:
//    unordered_set<int> set;
//public:
//    int getSum(int x)
//    {
//        int sum = 0;
//        while (x)
//        {
//            int t = x % 10;
//            sum += t * t;
//            x /= 10;
//        }
//        return sum;
//    }
//    bool isHappy(int n) {
//        while (1)
//        {
//            int sum = getSum(n);
//            if (sum == 1)
//                return true;
//            if (set.find(sum) != set.end())
//                return false;
//            else
//                set.insert(sum);
//            n = sum;
//        }
//
//    }
//};


//class Solution {
//public:
//    int maxArea(vector<int>& height) {
//        int n = height.size();
//        int left = 0, right = n - 1;
//        int res = 0;
//        while (left < right)
//        {
//            int v = min(height[left], height[right]) * (right - left);
//            res = max(res, v);
//            if (height[left] < height[right]) left++;
//            else right--;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int triangleNumber(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        int n = nums.size();
//        int cnt = 0;
//        for (int c = n - 1; c >= 2; c--)
//        {
//            int a = 0, b = c - 1;
//            while (a < b)
//            {
//                if (nums[a] + nums[b] > nums[c])
//                {
//                    cnt += b - a;
//                    b--;
//                }
//                else a++;
//            }
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    vector<int> twoSum(vector<int>& price, int target) {
//        int n = price.size();
//        int left = 0, right = n - 1;
//        while (left < right)
//        {
//            int sum = price[left] + price[right];
//            if (sum > target) right--;
//            else if (sum < target) left++;
//            else return { price[left], price[right] };
//        }
//        return { -1, -1 };
//    }
//};


#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n;
    cin >> n;
    int a = 0, b = 1, c = 1;
    while (n > c)
    {
        a = b;
        b = c;
        c = a + b;
    }
    cout << min(n - b, c - n) << endl;
    return 0;
}