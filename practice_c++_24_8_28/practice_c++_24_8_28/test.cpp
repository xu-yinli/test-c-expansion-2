#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//struct S1
//{
//	char c1;//0
//	int i;	//4-7
//	char c2;//8
//			//9-11
//};
//struct S2
//{
//	char c1;//0
//	char c2;//1
//	int i;	//4-7
//};
//struct S3
//{
//	double d;	//0-7
//	char c;		//8
//				//9-11
//	int i;		//12-15
//};
//struct S4
//{
//	char c1;		//0
//					//1-7
//	struct S3 s3;	//8-23(结构体嵌套)
//	double d;		//24-31
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct S1));	//12
//	printf("%d\n", sizeof(struct S2));	//8
//	printf("%d\n", sizeof(struct S3));	//16
//	printf("%d\n", sizeof(struct S4));	//32
//	return 0;
//}


//#include <stdio.h>
//
//#pragma pack(8) //设置默认对齐数为8
//struct S1
//{
//	char c1;//0
//			//1-3
//	int i;	//4-7
//	char c2;//8
//			//9-11
//};
//#pragma pack() //取消设置的默认对齐数，还原为默认
//
//#pragma pack(1)//设置默认对齐数为1
//struct S2
//{
//	char c1;//0
//	int i;	//1-4
//	char c2;//5
//};
//#pragma pack() //取消设置的默认对齐数，还原为默认
//
//int main()
//{
//	printf("%d\n", sizeof(struct S1));	//12
//	printf("%d\n", sizeof(struct S2));  //6
//	return 0;
//}


//#include<stdio.h>
//
//struct S
//{
//    char c1;
//    int a;
//    char c2;
//};
//
//#define OFFSETOF(struct_name,member_name) (size_t)&(((struct_name*)0)->member_name)
//
//int main()
//{
//    printf("%d\n", OFFSETOF(struct S, c1)); //0
//    printf("%d\n", OFFSETOF(struct S, a));  //4
//    printf("%d\n", OFFSETOF(struct S, c2)); //8
//    return 0;
//}


//#include <stdio.h>
//
//struct S
//{
//	int data[1000];
//	int num;
//};
//struct S s = { {1,2,3,4}, 1000 };
//
//void print1(struct S s) //结构体传参
//{
//	printf("%d\n", s.num);
//}
//void print2(struct S* ps) //结构体地址传参
//{
//	printf("%d\n", ps->num);
//}
//
//int main()
//{
//	print1(s);	//1000
//	print2(&s);	//1000
//	return 0;
//}


//#include <stdio.h>
//
//union Un
//{
//	int i;
//	char c;
//};
//
//int main()
//{
//	union Un un;
//	un.i = 0x11223344;
//	un.c = 0x55;
//	printf("%x\n", un.i); //11223355
//	return 0;
//}


//#include <stdio.h>
//
//union Un1
//{
//	char c[5];  //0-4
//	int i;		//0-3
//				//5-8
//};
//union Un2
//{
//	short c[7];	//0-13
//	int i;		//0-3
//				//14-15
//};
//
//int main()
//{
//	printf("%d\n", sizeof(union Un1)); //8
//	printf("%d\n", sizeof(union Un2)); //16
//	return 0;
//}


//#include <stdio.h>
//#include <stdlib.h>
//
//int main()
//{
//	int* p = (int*)calloc(10, sizeof(int));
//	if (NULL != p)
//	{
//		//使用空间
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>
//#include <stdlib.h>

//void GetMemory(char* p)
//{
//	p = (char*)malloc(100);
//}
//
//int main()
//{
//	char* str = NULL;
//	GetMemory(str);
//	strcpy(str, "hello world");
//	// 函数将str作为实参出传入类型(char*)类型的p中，此时p只是str的临时拷贝，p的地址并不是并不是真正str的地址
//	// 所以当函数调用时，str无法通过临时拷贝的p获得malloc分配的100个动态分配的内存, 始终都是NULL，所以strcpy无法进行拷贝字符串
//	printf(str);
//	return 0;
//}


//char* GetMemory(void)
//{
//	char p[] = "hello world";
//	return p;
//}
//int main()
//{
//	char* str = NULL;
//	str = GetMemory();
//	printf(str);
//	// p数组是内存分布中栈区的局部变量，在函数返回后内存已经被释放，所以str里的内容不是hello world，而是乱码
//	return 0;
//}


//void GetMemory(char** p, int num)
//{
//	*p = (char*)malloc(num);
//}
//int main()
//{
//	char* str = NULL;
//	GetMemory(&str, 100);
//	strcpy(str, "hello");
//	printf(str); //hello
//	//在堆区中malloc()会动态分配内存，用完要free，否则会造成内存泄漏
//	return 0;
//}


//int main()
//{
//	char* str = (char*)malloc(100);
//	strcpy(str, "hello");
//	free(str);
//	if (str != NULL)
//	{
//		strcpy(str, "world");
//		printf(str); //world
//	}
//	//free完之后，没有将str指向NULL类型
//	return 0;
//}



//#include <stdio.h>
//#include <stdlib.h>
//
//struct S
//{
//    int n;
//    int arr[0];
//};
//int main()
//{
//    struct S* ps = (struct S*)malloc(sizeof(struct S) + sizeof(int));
//    ps->n = 10;
//    for (int i = 0; i < 10; i++)
//    {
//        ps->arr[i] = i;
//    }
//    struct S* ptr = (struct S*)realloc(ps, sizeof(struct S) + 20 * sizeof(int));
//    if (ptr != NULL)
//    {
//        ps = ptr;
//    }
//    free(ps);
//    ps = NULL;
//    return 0;
//}


//#include <stdio.h>
//#include <stdlib.h>
//
//struct S
//{
//    int n;
//    int* arr;
//};
//int main()
//{
//    struct S* ps = (struct S*)malloc(sizeof(struct S));
//    if (ps == NULL)
//        return -1;
//    ps->n = 10;
//    ps->arr = (int*)malloc(10 * sizeof(int));
//    if (ps->arr == NULL)
//        return -1;
//    for (int i = 0; i < 10; i++)
//    {
//        ps->arr[i];
//    }
//
//    int* ptr = (int*)realloc(ps->arr, 20 * sizeof(int));
//    if (ptr != NULL)
//    {
//        ps->arr = ptr;
//    }
//
//    // 这里需要回收2个空间，且回收必须有先后
//    free(ps->arr);
//    ps->arr = NULL;
//    free(ps);
//    ps = NULL;
//    return 0;
//}


//#include <stdio.h>
//
//#define PRINT(FORMAT, VALUE)\
//		printf("the value of " #VALUE " is "FORMAT "\n", VALUE);
//
//int main()
//{
//	int i = 10;
//	PRINT("%d", i+3); //the value of i+3 is 13
//	return 0;
//}


//#include <stdio.h>
//
//int sum5 = 0; //必须产生一个合法的标识符，否则其结果就是未定义的
//
//#define ADD_TO_SUM(num, value)\
//		sum##num += value;
//
//int main()
//{
//	ADD_TO_SUM(5, 10);//作用是：给sum5增加10
//	return 0;
//}


//#include <stdio.h>
//
//#define MAX(a, b) ( (a) > (b) ? (a) : (b) ) 
//
//int main()
//{
//	int x = 5;
//	int y = 8;
//	int z = MAX(x++, y++); //z = ( (x++) > (y++) ? (x++) : (y++) );
//	printf("x=%d y=%d z=%d\n", x, y, z); //x=6 y=10 z=9
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//#line 1 "a.c"
//	printf("[%s : %d] %s\n", __FILE__, __LINE__, "a.c");
//#line 5 "b.c"
//	printf("[%s : %d] %s\n", __FILE__, __LINE__, "b.c");
//#line 10 "c.c"
//	printf("[%s : %d] %s\n", __FILE__, __LINE__, "c.c");
//
//	return 0;
//}


//leetcode
//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size();
//        int len = INT_MAX;
//        int sum = 0;
//        int left = 0;
//        for (int right = 0; right < n; right++)
//        {
//            sum += nums[right];
//            while (sum >= target)
//            {
//                len = min(len, right - left + 1);
//                sum -= nums[left];
//                left++;
//            }
//        }
//        if (len != INT_MAX) return len;
//        return 0;
//    }
//};


//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size();
//        int len = INT_MAX;
//        int sum = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            sum += nums[right];
//            while (sum >= target)
//            {
//                len = min(len, right - left + 1);
//                sum -= nums[left];
//                left++;
//            }
//            right++;
//        }
//        if (len != INT_MAX) return len;
//        return 0;
//    }
//};


//class Solution {
//private:
//    int hash[100010];
//public:
//    int totalFruit(vector<int>& fruits) {
//        int n = fruits.size();
//        int res = 0;
//        int types = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            if (hash[fruits[right]] == 0) types++;
//            hash[fruits[right]]++;
//            while (types > 2)
//            {
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0) types--;
//                left++;
//            }
//            res = max(res, right - left + 1);
//            right++;
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x) {
//        int n = nums.size();
//        int sum = 0;
//        for (int x : nums)
//            sum += x;
//        if (sum < x) return -1;
//        int res = -1;
//        int add = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            add += nums[right];
//            while (add > sum - x)
//            {
//                add -= nums[left];
//                left++;
//            }
//            if (add == sum - x) res = max(res, right - left + 1);
//            right++;
//        }
//        if (res != -1) return n - res;
//        return res;
//    }
//};


//class Solution {
//public:
//    vector<int> getAverages(vector<int>& nums, int k) {
//        int n = nums.size();
//        vector<int> avgs(n, -1);
//        long long sum = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            sum += nums[right];
//            if (right - left + 1 > 2 * k + 1)
//            {
//                sum -= nums[left];
//                left++;
//            }
//            if (right - left + 1 == 2 * k + 1) avgs[right - k] = sum / (2 * k + 1);
//            right++;
//        }
//        return avgs;
//    }
//};


//class Solution {
//public:
//    int minimumSubarrayLength(vector<int>& nums, int k) {
//        int n = nums.size();
//        int res = INT_MAX;
//        for (int i = 0; i < n; i++)
//        {
//            int tmp = 0;
//            for (int j = i; j < n; j++)
//            {
//                tmp |= nums[j];
//                if (tmp >= k)
//                {
//                    res = min(res, j - i + 1);
//                    break;
//                }
//            }
//        }
//        if (res != INT_MAX) return res;
//        return -1;
//    }
//};


//class Solution {
//private:
//    char hash1[130];
//    char hash2[130];
//public:
//    string minWindow(string s, string t) {
//        int n = s.size(), m = s.size();
//        int kinds = 0;
//        for (char ch : t)
//        {
//            if (hash1[ch] == 0) kinds++;
//            hash1[ch]++;
//        }
//        int res = INT_MAX;
//        int st = 0;
//        int types = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash2[s[right]]++;
//            if (hash2[s[right]] == hash1[s[right]]) types++;
//            while (types == kinds)
//            {
//                if (res > right - left + 1)
//                {
//                    res = right - left + 1;
//                    st = left;
//                }
//                if (hash2[s[left]] == hash1[s[left]]) types--;
//                hash2[s[left]]--;
//                left++;
//            }
//            right++;
//        }
//        if (res != INT_MAX) return s.substr(st, res);
//        return "";
//    }
//};