#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int a[N];
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++) cin >> a[i];
//    int res = 0;
//    int i = 0;
//    while (i < n)
//    {
//        if (i == n - 1)
//        {
//            res++;
//            break;
//        }
//        if (a[i] < a[i + 1])
//        {
//            while (i + 1 < n && a[i] <= a[i + 1]) i++;
//            res++;
//        }
//        else if (a[i] > a[i + 1])
//        {
//            while (i + 1 < n && a[i] >= a[i + 1]) i++;
//            res++;
//        }
//        else
//        {
//            while (i + 1 < n && a[i] == a[i + 1]) i++;
//        }
//        i++;
//    }
//    cout << res << endl;
//    return 0;
//}


//class Solution {
//private:
//    int dp[100010];
//    int pos = 0;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     * 该数组最长严格上升子序列的长度
//     * @param a int整型vector 给定的数组
//     * @return int整型
//     */
//    int LIS(vector<int>& a) {
//        for (auto x : a)
//        {
//            if (pos == 0 || x > dp[pos])
//            {
//                pos++;
//                dp[pos] = x;
//            }
//            else
//            {
//                int left = 1, right = pos;
//                while (left < right)
//                {
//                    int mid = left + (right - left) / 2;
//                    if (dp[mid] >= x) right = mid;
//                    else left = mid + 1;
//                }
//                dp[left] = x;
//            }
//        }
//        return pos;
//    }
//};


//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums) {
//        int n = nums.size();
//        if (n <= 1) return n;
//        vector<int> dp(n, 1);
//        int res = 1;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j < i; j++)
//            {
//                if (nums[j] < nums[i])
//                    dp[i] = max(dp[i], dp[j] + 1);
//            }
//            if (dp[i] > res) res = dp[i];
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int longestCommonSubsequence(string text1, string text2) {
//        int n = text1.size(), m = text2.size();
//        vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//        for (int i = 1; i <= n; i++)
//        {
//            for (int j = 1; j <= m; j++)
//            {
//                if (text1[i - 1] == text2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//                else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[n][m];
//    }
//};


//#include <iostream>
//#include <vector>
//using namespace std;
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    string s1, s2;
//    cin >> s1 >> s2;
//    vector<vector<int>> dp(n + 1, vector<int>(m + 1));
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            if (s1[i - 1] == s2[j - 1]) dp[i][j] = dp[i - 1][j - 1] + 1;
//            else dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
//        }
//    }
//    cout << dp[n][m] << endl;
//    return 0;
//}


//class Solution {
//private:
//    vector<int> res;
//public:
//    void printCircle(vector<vector<int>>& matrix, int n, int m, int st)
//    {
//        int endX = n - 1 - st, endY = m - 1 - st;
//        //左->右
//        for (int j = st; j <= endY; j++)
//            res.push_back(matrix[st][j]);
//        //上->下
//        if (st < endX)
//            for (int i = st + 1; i <= endX; i++)
//                res.push_back(matrix[i][endY]);
//        //右->左
//        if (st < endX && st < endY)
//            for (int j = endY - 1; j >= st; j--)
//                res.push_back(matrix[endX][j]);
//        //下->上
//        if (st < endY && st < endX - 1)
//            for (int i = endX - 1; i >= st + 1; i--)
//                res.push_back(matrix[i][st]);
//    }
//    vector<int> spiralOrder(vector<vector<int>>& matrix) {
//        //n行m列
//        int n = matrix.size();
//        if (n == 0) return res;
//        int m = matrix[0].size();
//        int st = 0;
//        while (n > st * 2 && m > st * 2)
//        {
//            //一圈一圈打印
//            printCircle(matrix, n, m, st);
//            st++;
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    vector<int> res;
//public:
//    void printCircle(vector<vector<int>>& matrix, int n, int m, int st)
//    {
//        int endX = n - 1 - st, endY = m - 1 - st;
//        //左->右
//        for (int j = st; j <= endY; j++)
//            res.push_back(matrix[st][j]);
//        //上->下
//        if (st < endX)
//            for (int i = st + 1; i <= endX; i++)
//                res.push_back(matrix[i][endY]);
//        //右->左
//        if (st < endX && st < endY)
//            for (int j = endY - 1; j >= st; j--)
//                res.push_back(matrix[endX][j]);
//        //下->上
//        if (st < endY && st < endX - 1)
//            for (int i = endX - 1; i >= st + 1; i--)
//                res.push_back(matrix[i][st]);
//    }
//    vector<int> printMatrix(vector<vector<int> > matrix) {
//        int n = matrix.size();
//        if (n == 0) return {};
//        int m = matrix[0].size();
//        int st = 0;
//        while (n > st * 2 && m > st * 2)
//        {
//            //一圈一圈打印
//            printCircle(matrix, n, m, st);
//            st++;
//        }
//        return res;
//    }
//};


//class MinStack {
//private:
//    stack<int> val_stack;
//    stack<int> min_stack;
//public:
//    MinStack() {}
//
//    void push(int val) {
//        val_stack.push(val);
//        if (min_stack.empty() || val < min_stack.top())
//            min_stack.push(val);
//        else
//            min_stack.push(min_stack.top());
//    }
//
//    void pop() {
//        if (val_stack.empty() || min_stack.empty()) return;
//        val_stack.pop();
//        min_stack.pop();
//    }
//
//    int top() {
//        return val_stack.top();
//    }
//
//    int getMin() {
//        return min_stack.top();
//    }
//};
//
///**
// * Your MinStack object will be instantiated and called as such:
// * MinStack* obj = new MinStack();
// * obj->push(val);
// * obj->pop();
// * int param_3 = obj->top();
// * int param_4 = obj->getMin();
// */


//class Solution {
//private:
//    stack<int> val_stack;
//    stack<int> min_stack;
//public:
//    void push(int value) {
//        val_stack.push(value);
//        if (min_stack.empty() || value < min_stack.top())
//            min_stack.push(value);
//        else
//            min_stack.push(min_stack.top());
//    }
//    void pop() {
//        val_stack.pop();
//        min_stack.pop();
//    }
//    int top() {
//        return val_stack.top();
//    }
//    int min() {
//        return min_stack.top();
//    }
//};


//class MinStack {
//private:
//    stack<int> val_stack, min_stack;
//public:
//    /** initialize your data structure here. */
//    MinStack() {
//
//    }
//
//    void push(int x) {
//        val_stack.push(x);
//        if (min_stack.empty() || x < min_stack.top())
//            min_stack.push(x);
//        else
//            min_stack.push(min_stack.top());
//    }
//
//    void pop() {
//        val_stack.pop();
//        min_stack.pop();
//    }
//
//    int top() {
//        return val_stack.top();
//    }
//
//    int getMin() {
//        return min_stack.top();
//    }
//};
//
///**
// * Your MinStack object will be instantiated and called as such:
// * MinStack* obj = new MinStack();
// * obj->push(x);
// * obj->pop();
// * int param_3 = obj->top();
// * int param_4 = obj->getMin();
// */


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param pushV int整型vector
//     * @param popV int整型vector
//     * @return bool布尔型
//     */
//    bool IsPopOrder(vector<int>& pushV, vector<int>& popV) {
//        if (pushV.empty() || popV.empty() || pushV.size() != popV.size())
//            return false;
//        stack<int> st;
//        int j = 0;
//        for (int i = 0; i < pushV.size(); i++)
//        {
//            st.push(pushV[i]);
//            while (!st.empty() && st.top() == popV[j])
//            {
//                st.pop();
//                j++;
//            }
//        }
//        if (st.empty()) return true;
//        else return false;
//    }
//};


//class Solution {
//public:
//    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
//        if (pushed.empty() || popped.empty() || pushed.size() != popped.size())
//            return false;
//        stack<int> st;
//        int j = 0;
//        for (int i = 0; i < pushed.size(); i++)
//        {
//            st.push(pushed[i]);
//            while (!st.empty() && st.top() == popped[j])
//            {
//                st.pop();
//                j++;
//            }
//        }
//        return st.empty();
//    }
//};


//class Solution {
//public:
//    bool validateBookSequences(vector<int>& putIn, vector<int>& takeOut) {
//        if (putIn.empty() && takeOut.empty())
//            return true;
//        if (putIn.size() != takeOut.size())
//            return false;
//        stack<int> st;
//        int j = 0;
//        for (int i = 0; i < putIn.size(); i++)
//        {
//            st.push(putIn[i]);
//            while (!st.empty() && st.top() == takeOut[j])
//            {
//                st.pop();
//                j++;
//            }
//        }
//        return st.empty();
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//    queue<TreeNode*> q;
//public:
//    vector<int> decorateRecord(TreeNode* root) {
//        if (root == nullptr) return res;
//        q.push(root);
//        while (q.size())
//        {
//            TreeNode* node = q.front();
//            q.pop();
//            res.push_back(node->val);
//            if (node->left) q.push(node->left);
//            if (node->right) q.push(node->right);
//        }
//        return res;
//    }
//};


///*
//struct TreeNode {
//	int val;
//	struct TreeNode *left;
//	struct TreeNode *right;
//	TreeNode(int x) :
//			val(x), left(NULL), right(NULL) {
//	}
//};*/
//class Solution {
//private:
//	vector<int> res;
//	queue<TreeNode*> q;
//public:
//	vector<int> PrintFromTopToBottom(TreeNode* root) {
//		if (root == nullptr) return res;
//		q.push(root);
//		while (q.size())
//		{
//			TreeNode* node = q.front();
//			q.pop();
//			res.push_back(node->val);
//			if (node->left) q.push(node->left);
//			if (node->right) q.push(node->right);
//		}
//		return res;
//	}
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//    queue<TreeNode*> q;
//public:
//    vector<vector<int>> decorateRecord(TreeNode* root) {
//        if (root == nullptr)
//            return res;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> v;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                v.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            res.push_back(v);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//    queue<TreeNode*> q;
//public:
//    vector<vector<int>> decorateRecord(TreeNode* root) {
//        if (root == nullptr) return res;
//        int level = 0;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> v;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                v.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            if (level % 2 != 0) reverse(v.begin(), v.end());
//            res.push_back(v);
//            level++;
//        }
//        return res;
//    }
//};