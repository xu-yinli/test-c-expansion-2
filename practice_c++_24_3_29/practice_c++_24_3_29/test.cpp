#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 1e6 + 10;
//int count;
//int primes[N];
//bool st[N];
//
//void get_primes(int n)
//{
//    for (int i = 2; i <= n; i++)
//    {
//        if (!st[i]) primes[count++] = i;
//        for (int j = 0; primes[j] <= n / i; j++)
//        {
//            st[primes[j] * i] = true;
//            if (i % primes[j] == 0) break;
//        }
//    }
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    get_primes(n);
//    cout << count << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//vector<int> get_divisors(int n)
//{
//    vector<int> res;
//    for (int i = 1; i <= n / i; i++)
//    {
//        if (n % i == 0)
//        {
//            res.push_back(i);
//            if (i != n / i) res.push_back(n / i);
//        }
//    }
//    sort(res.begin(), res.end());
//    return res;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        vector<int> res = get_divisors(x);
//        for (auto e : res)
//            cout << e << ' ';
//        cout << endl;
//    }
//}


//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
//const int MOD = 1e9 + 7;
//
//typedef long long LL;
//
//int main()
//{
//    int n;
//    cin >> n;
//    unordered_map<int, int> primes;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        for (int i = 2; i <= x / i; i++)
//        {
//            while (x % i == 0)
//            {
//                primes[i]++;
//                x /= i;
//            }
//        }
//        if (x > 1) primes[x]++;
//    }
//    LL res = 1;
//    for (auto e : primes) res = res * (e.second + 1) % MOD;
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
//const int MOD = 1e9 + 7;
//
//typedef long long LL;
//
//int main()
//{
//    int n;
//    cin >> n;
//    unordered_map<int, int> primes;
//    while (n--)
//    {
//        int x;
//        cin >> x;
//        for (int i = 2; i <= x / i; i++)
//        {
//            while (x % i == 0)
//            {
//                primes[i]++;
//                x /= i;
//            }
//        }
//        if (x > 1) primes[x]++;
//    }
//    LL res = 1;
//    for (auto e : primes)
//    {
//        int p = e.first, a = e.second;
//        LL t = 1;
//        while (a--) t = (t * p + 1) % MOD;
//        res = res * t % MOD;
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int gcd(int a, int b)
//{
//    return b ? gcd(b, a % b) : a;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b;
//        cin >> a >> b;
//        cout << gcd(a, b) << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <queue>
//#include <cstring>
//using namespace std;
//
//typedef pair<int, int> PII;
//
//const int N = 110;
//int n, m;
//int g[N][N], d[N][N];
//queue<PII> q;
//
//int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//
//int bfs()
//{
//    memset(d, -1, sizeof d);
//    d[0][0] = 0;
//    q.push({ 0, 0 });
//    while (q.size())
//    {
//        auto t = q.front();
//        q.pop();
//        for (int i = 0; i < 4; i++)
//        {
//            int x = t.first + dx[i], y = t.second + dy[i];
//            if (x >= 0 && x < n && y >= 0 && y < m && d[x][y] == -1 && g[x][y] == 0)
//            {
//                q.push({ x, y });
//                d[x][y] = d[t.first][t.second] + 1;
//            }
//        }
//    }
//    return d[n - 1][m - 1];
//}
//
//int main()
//{
//    cin >> n >> m;
//    for (int i = 0; i < n; i++)
//        for (int j = 0; j < m; j++)
//            cin >> g[i][j];
//    cout << bfs() << endl;
//    return 0;
//}


//#include <iostream>
//#include <queue>
//#include <cstring>
//using namespace std;
//
//const int N = 1e5 + 10;
//int n, m;
//int h[N], e[N], ne[N], idx;
//int in[N];
//int top[N];
//int cnt;
//
//void add(int x, int y)
//{
//    e[idx] = y;
//    ne[idx] = h[x];
//    h[x] = idx++;
//}
//
//bool topsort()
//{
//    queue<int> q;
//    for (int i = 1; i <= n; i++)
//        if (in[i] == 0)
//            q.push(i);
//    while (q.size())
//    {
//        int t = q.front();
//        q.pop();
//        top[cnt++] = t;
//        for (int i = h[t]; i != -1; i = ne[i])
//        {
//            int j = e[i];
//            in[j]--;
//            if (in[j] == 0)
//                q.push(j);
//        }
//    }
//    return cnt == n;
//}
//
//int main()
//{
//    cin >> n >> m;
//    memset(h, -1, sizeof h);
//    while (m--)
//    {
//        int x, y;
//        cin >> x >> y;
//        add(x, y);
//        in[y]++;
//    }
//    if (!topsort()) cout << "-1" << endl;
//    else
//    {
//        for (int i = 0; i < n; i++)
//            cout << top[i] << ' ';
//        cout << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <cstring>
//#include <queue>
//using namespace std;
//
//typedef pair<int, int> PII;
//
//const int N = 110;
//int n, m;
//char g[N][N];
//int d[N][N];
//queue<PII> q;
//
//int dx[4] = { -1,0,1,0 }, dy[4] = { 0,1,0,-1 };
//
//bool bfs()
//{
//    memset(d, -1, sizeof d);
//    d[0][0] = 0;
//    q.push({ 0, 0 });
//    while (q.size())
//    {
//        auto t = q.front();
//        q.pop();
//        for (int i = 0; i < 4; i++)
//        {
//            int x = t.first + dx[i], y = t.second + dy[i];
//            if (x >= 0 && x < n && y >= 0 && y < m && d[x][y] == -1 && g[x][y] == '.')
//            {
//                q.push({ x, y });
//                d[x][y] = d[t.first][t.second] + 1;
//            }
//        }
//    }
//    if (d[n - 1][m - 1] != -1) return true;
//    else return false;
//}
//
//int main()
//{
//    cin >> n >> m;
//    for (int i = 0; i < n; i++)
//        for (int j = 0; j < m; j++)
//            cin >> g[i][j];
//    if (bfs()) cout << "Yes" << endl;
//    else cout << "No" << endl;
//    return 0;
//}


//#include <iostream>
//#include <queue>
//#include <vector>
//using namespace std;
//
//const int N = 10010;
//int n;
//int in[N];
//vector<int> e[N], top;
//
//void topsort()
//{
//    queue<int> q;
//    for (int i = 1; i <= n; i++)
//        if (in[i] == 0)
//            q.push(i);
//    while (q.size())
//    {
//        int t = q.front();
//        q.pop();
//        top.push_back(t);
//        for (auto j : e[t])
//        {
//            in[j]--;
//            if (in[j] == 0)
//                q.push(j);
//        }
//    }
//}
//
//int main()
//{
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//    {
//        int x;
//        while (1)
//        {
//            cin >> x;
//            if (x == 0) break;
//            e[i].push_back(x);
//            in[x]++;
//        }
//    }
//    topsort();
//    for (auto e : top)
//        cout << e << ' ';
//    cout << endl;
//    return 0;
//}


//#include <iostream>
//#include <queue>
//#include <vector>
//using namespace std;
//
//const int N = 1e5 + 10;
//int n, m;
//int in[N];
//vector<int> e[N], top;
//
//bool topsort()
//{
//    queue<int> q;
//    for (int i = 1; i <= n; i++)
//        if (in[i] == 0)
//            q.push(i);
//    while (q.size())
//    {
//        int t = q.front();
//        q.pop();
//        top.push_back(t);
//        for (auto j : e[t])
//        {
//            in[j]--;
//            if (in[j] == 0)
//                q.push(j);
//        }
//    }
//    return top.size() == n;
//}
//
//int main()
//{
//    cin >> n >> m;
//    while (m--)
//    {
//        int x, y;
//        cin >> x >> y;
//        e[x].push_back(y);
//        in[y]++;
//    }
//    if (!topsort()) cout << "-1" << endl;
//    else
//    {
//        for (auto a : top)
//            cout << a << ' ';
//        cout << endl;
//    }
//    return 0;
//}





//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//int main()
//{
//    LL n;
//    cin >> n;
//    n /= 1000;
//    LL h, m, s;
//    s = n % 60;
//    m = n / 60 % 60;
//    h = n / 60 / 60 % 24;
//    if (h < 10) cout << '0' << h << ':';
//    else cout << h << ':';
//    if (m < 10) cout << '0' << m << ':';
//    else cout << m << ':';
//    if (s < 10) cout << '0' << s << endl;
//    else cout << s << endl;
//}


//#include <iostream>
//using namespace std;
//
//int a[11];
//
//int main()
//{
//	memset(a, -1, sizeof a);
//	for (int i = 1; i <= 10; i++) a[i] = 2021;
//	int k = 0;
//	for (int j = 1; j < 10000; j++)
//	{
//		int q=0, b=0, s=0, g=0;
//		g = j % 10;
//		s = j / 10 % 10;
//		b = j / 100 % 10;
//		q = j / 1000 % 10;
//		if (g)
//		{
//			if (a[g])
//				a[g]--;
//			else
//			{
//				k = j-1;
//				break;
//			}
//		}
//		if (s)
//		{
//			if (a[s])
//				a[s]--;
//			else
//			{
//				k = j-1;
//				break;
//			}
//		}
//		if (b)
//		{
//			if (a[b])
//				a[b]--;
//			else
//			{
//				k = j-1;
//				break;
//			}
//		}
//		if (q)
//		{
//			if (a[q])
//				a[q]--;
//			else
//			{
//				k = j-1;
//				break;
//			}
//		}
//	}
//	cout << k << endl;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//LL a[1000];
//int cnt;
//
//void get_primes(LL n)
//{
//    for (LL i = 1; i <= n / i; i++)
//    {
//        if (n % i == 0)
//        {
//            a[cnt++] = i;
//            if (n / i != i)
//                a[cnt++] = n / i;
//        }
//    }
//}
//
//int main()
//{
//    LL n = 2021041820210418;
//    get_primes(n);
//    int res=0;
//    for(int i=0; i<cnt; i++)
//    {
//        for(int j=0; j<cnt; j++)
//        {
//            for(int k=0; k<cnt; k++)
//            {
//                if(a[i]*a[j]*a[k]==n)
//                    res++;
//            }
//        }
//    }
//    cout << res << endl;
//    return 0;
//}


class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> vv(numRows);
        for (int i = 0; i < numRows; i++)
        {
            vv[i].resize(i + 1);
            vv[i].front() = vv[i].back() = 1;
        }
        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < i; j++)
            {
                if (vv[i][j] == 0)
                    vv[i][j] = vv[i - 1][j - 1] + vv[i - 1][j];
            }
        }
        return vv;
    }
};