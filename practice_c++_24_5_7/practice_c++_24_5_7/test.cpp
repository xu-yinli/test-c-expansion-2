#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//    //f[i]:以i位置为结尾的所有子数组中的最大乘积
//    //g[i]:以i位置为结尾的所有子数组中的最小乘积
//public:
//    int maxProduct(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1);
//        vector<int> g(n + 1);
//        f[0] = g[0] = 1;
//        int res = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = max(nums[i - 1], max(f[i - 1] * nums[i - 1], g[i - 1] * nums[i - 1]));
//            g[i] = min(nums[i - 1], min(f[i - 1] * nums[i - 1], g[i - 1] * nums[i - 1]));
//            res = max(res, f[i]);
//        }
//        return res;
//    }
//};


//class Solution {
//    //f[i]:以i位置元素为结尾的所有子数组中乘积为正数的最长长度
//    //g[i]:以i位置元素为结尾的所有子数组中乘积为负数的最长长度
//public:
//    int getMaxLen(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1);
//        vector<int> g(n + 1);
//        int res = INT_MIN;
//        for (int i = 1; i <= n; i++)
//        {
//            if (nums[i - 1] > 0)
//            {
//                f[i] = f[i - 1] + 1;
//                if (g[i - 1] == 0) g[i] = 0;
//                else g[i] = g[i - 1] + 1;
//            }
//            else if (nums[i - 1] < 0)
//            {
//                if (g[i - 1] == 0) f[i] = 0;
//                else f[i] = g[i - 1] + 1;
//                g[i] = f[i - 1] + 1;
//            }
//            res = max(res, f[i]);
//        }
//        return res;
//    }
//};