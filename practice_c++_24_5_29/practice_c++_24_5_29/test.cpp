#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 1e5 + 10;
//int arr[N];
//int f[N], g[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 1; i <= n; i++) cin >> arr[i];
//    f[1] = arr[1], g[1] = 0;
//    for (int i = 2; i <= n; i++)
//    {
//        int x = arr[i];
//        f[i] = max(f[i - 1], arr[i]);
//        if (x >= f[i - 1]) g[i] = f[i - 1];
//        else if (x >= g[i - 1] && x < f[i - 1]) g[i] = x;
//        else g[i] = g[i - 1];
//    }
//    int q;
//    cin >> q;
//    while (q--)
//    {
//        int x;
//        cin >> x;
//        cout << g[x] << endl;
//    }
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 1010;
//int n;
//int g[N][N];
//
//void setRow()
//{
//    for (int i = 0; i < n / 2; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            swap(g[i][j], g[n - i - 1][j]);
//        }
//    }
//}
//
//void setCol()
//{
//    for (int j = 0; j < n / 2; j++)
//    {
//        for (int i = 0; i < n; i++)
//        {
//            swap(g[i][j], g[i][n - j - 1]);
//        }
//    }
//}
//
//int main()
//{
//    cin >> n;
//    for (int i = 0; i < n; i++)
//        for (int j = 0; j < n; j++)
//            cin >> g[i][j];
//    int q;
//    cin >> q;
//    int row = 0, col = 0;
//    while (q--)
//    {
//        int x;
//        cin >> x;
//        if (x == 1)
//        {
//            row++;
//            col++;
//        }
//        else if (x == 2)
//        {
//            row++;
//        }
//    }
//    row %= 2;
//    col %= 2;
//    if (row) setRow();
//    if (col) setCol();
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < n; j++)
//            cout << g[i][j] << ' ';
//        cout << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <cstring>
//using namespace std;
//
//typedef long long LL;
//const int N = 1010, INF = 1e9 + 7;
//LL a[N], dp[N][N];
//
//int main()
//{
//    int n, k;
//    cin >> n >> k;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    memset(dp, -INF, sizeof(dp));
//    dp[0][0] = 0;
//    for (int i = 1; i <= n; i++)
//        for (int j = 0; j < k; j++)
//            dp[i][j] = max(dp[i - 1][j], dp[i - 1][(j - a[i] % k + k) % k] + a[i]);
//    if (dp[n][0] == -INF || dp[n][0] == 0) cout << -1 << endl;
//    else cout << dp[n][0] << endl;
//    return 0;
//}


#include <iostream>
#include <cstring>
using namespace std;

typedef long long LL;
const int N = 1010, INF = 1e9 + 7;
LL a[N], dp[N][N];

int main()
{
    int n, k;
    cin >> n >> k;
    for (int i = 1; i <= n; i++) cin >> a[i];
    memset(dp, -INF, sizeof(dp));
    dp[0][0] = 0;
    for (int i = 1; i <= n; i++)
        for (int j = 0; j < k; j++)
            dp[i][j] = max(dp[i - 1][j], dp[i - 1][(j - a[i] % k + k) % k] + a[i]);
    if (dp[n][0] <= 0) cout << -1 << endl;
    else cout << dp[n][0] << endl;
    return 0;
}