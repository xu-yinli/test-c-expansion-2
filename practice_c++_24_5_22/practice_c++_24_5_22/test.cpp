#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int f(int n)
//{
//    if (n == 1) return 1;
//    return 2 * f(n - 1);
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//    cout << f(n) << endl;
//    return 0;
//}


//class Solution {
//private:
//    set<string> ans;
//    vector<string> res;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param str string字符串
//     * @return string字符串vector
//     */
//    void swap(string& str, int i, int j)
//    {
//        char temp = str[i];
//        str[i] = str[j];
//        str[j] = temp;
//    }
//    void dfs(string& str, int start)
//    {
//        if (start == str.length() - 1)
//        {
//            ans.insert(str);
//            return;
//        }
//        for (int i = start; i < str.size(); i++)
//        {
//            swap(str, start, i);
//            dfs(str, start + 1);
//            swap(str, start, i);
//        }
//    }
//    vector<string> Permutation(string str) {
//        int n = str.size();
//        if (n < 1) return { "" };
//        dfs(str, 0);
//        for (auto s : ans)
//            res.push_back(s);
//        return res;
//    }
//};


//class Solution {
//private:
//    set<string> ans;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param str string字符串
//     * @return string字符串vector
//     */
//    void dfs(string& str, int pos)
//    {
//        if (pos == str.length() - 1)
//        {
//            ans.insert(str);
//            return;
//        }
//        for (int i = pos; i < str.size(); i++)
//        {
//            swap(str[pos], str[i]);
//            dfs(str, pos + 1);
//            swap(str[pos], str[i]);
//        }
//    }
//    vector<string> Permutation(string str) {
//        if (str.size() < 1) return { "" };
//        dfs(str, 0);
//        return vector<string>({ ans.begin(), ans.end() });
//    }
//};


//class Solution {
//private:
//    set<string> ans;
//    vector<string> res;
//public:
//    void dfs(string& goods, int pos)
//    {
//        if (pos == goods.size() - 1)
//        {
//            ans.insert(goods);
//            return;
//        }
//        for (int i = pos; i < goods.size(); i++)
//        {
//            swap(goods[pos], goods[i]);
//            dfs(goods, pos + 1);
//            swap(goods[pos], goods[i]);
//        }
//    }
//    vector<string> goodsOrder(string goods) {
//        dfs(goods, 0);
//        for (auto s : ans)
//            res.push_back(s);
//        return res;
//    }
//};


//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
//int main()
//{
//    string s;
//    cin >> s;
//    int n = s.size();
//    int res = 0;
//    int cnt = 0;
//    unordered_map<char, int> hash;
//    int left = 0, right = 0;
//    while (right < n)
//    {
//        hash[s[right]]++;
//        if (hash[s[right]] == 1)
//            cnt++;
//        while (cnt > 2)
//        {
//            hash[s[left]]--;
//            if (hash[s[left]] == 0)
//                cnt--;
//            left++;
//        }
//        res = max(res, right - left + 1);
//        right++;
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//typedef long long LL;
//
//int main()
//{
//    int t;
//    cin >> t;
//    while (t--)
//    {
//        LL n, a, b;
//        cin >> n >> a >> b;
//        if (n <= 2)
//        {
//            cout << min(a, b) << endl;
//            continue;
//        }
//        LL money = 0;
//        if (a * 3 < b * 2)
//        {
//            money += n / 2 * a;
//            n %= 2;
//            if (n == 1) money += min(min(a, b), -a + b);
//        }
//        else
//        {
//            money += n / 3 * b;
//            n %= 3;
//            if (n == 1) money += min(min(a, b), -b + 2 * a);
//            if (n == 2) money += min(min(a, b), -b + 3 * a);
//        }
//        cout << money << endl;
//    }
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//typedef pair<int, int> PII;
//vector<PII> q;
//
//int main()
//{
//    int n;
//    cin >> n;
//    while (n--)
//    {
//        int a, b;
//        cin >> a >> b;
//        q.push_back({ a, b });
//    }
//    sort(q.begin(), q.end());
//    int cnt = 1;
//    int ed = q[0].second;
//    for (int i = 1; i < q.size(); i++)
//    {
//        if (q[i].first >= ed)
//        {
//            cnt++;
//            ed = q[i].second;
//        }
//        else
//        {
//            ed = min(ed, q[i].second);
//        }
//    }
//    cout << cnt << endl;
//    return 0;
//}


//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == nums.size() - 1)
//        {
//            res.push_back(nums);
//            return;
//        }
//        for (int i = pos; i < nums.size(); i++)
//        {
//            swap(nums[pos], nums[i]);
//            dfs(nums, pos + 1);
//            swap(nums[pos], nums[i]);
//        }
//        return;
//    }
//    vector<vector<int>> permute(vector<int>& nums) {
//        dfs(nums, 0);
//        return res;
//    }
//};


//class Solution {
//private:
//    set<vector<int>> ans;
//    vector<vector<int>> res;
//public:
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == nums.size() - 1)
//        {
//            ans.insert(nums);
//            return;
//        }
//        for (int i = pos; i < nums.size(); i++)
//        {
//            swap(nums[pos], nums[i]);
//            dfs(nums, pos + 1);
//            swap(nums[pos], nums[i]);
//        }
//        return;
//    }
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        dfs(nums, 0);
//        for (auto e : ans)
//            res.push_back(e);
//        return res;
//    }
//};


//class Solution {
//private:
//    string path;        //记录路径信息
//    vector<string> res; //收集叶子结点
//    bool vis[11] = { 0 };       //标记当前位置是否已经被使用过
//    int n;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param str string字符串
//     * @return string字符串vector
//     */
//    void dfs(string& str, int pos)
//    {
//        if (pos == n)
//        {
//            res.push_back(path);
//            return;
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (!vis[i])
//            {
//                if (i > 0 && str[i] == str[i - 1] && !vis[i - 1])
//                    continue;
//                path.push_back(str[i]);
//                vis[i] = true;
//                dfs(str, pos + 1);
//                vis[i] = false;
//                path.pop_back();
//            }
//        }
//    }
//    vector<string> Permutation(string str) {
//        n = str.size();
//        sort(str.begin(), str.end());
//        dfs(str, 0);
//        return res;
//    }
//};


#include <iostream>
using namespace std;

typedef long long LL;
const int N = 55, M = 15, INF = 0x3f3f3f3f3f3f3f3f;
LL a[N];
LL f[N][M], g[N][M];

int main()
{
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++) cin >> a[i];
    int k, d;
    cin >> k >> d;
    for (int i = 1; i <= n; i++)
    {
        f[i][1] = g[i][1] = a[i];
        for (int j = 2; j <= min(i, k); j++)
        {
            f[i][j] = -INF;
            g[i][j] = INF;
            for (int prev = max(i - d, j - 1); prev <= i - 1; prev++)
            {
                f[i][j] = max(max(f[prev][j - 1] * a[i], g[prev][j - 1] * a[i]), f[i][j]);
                g[i][j] = min(min(f[prev][j - 1] * a[i], g[prev][j - 1] * a[i]), g[i][j]);
            }
        }
    }
    LL res = -INF;
    for (int i = k; i <= n; i++)
        res = max(res, f[i][k]);
    cout << res << endl;
    return 0;
}