#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h> 
//#include <string>

//int main()
//{
//	char a[1000];
//	int i;
//	for (i = 0; i < 1000; i++)
//	{
//		a[i] = -1 - i;
//	}
//	printf("%d", strlen(a)); //255
//	return 0;
//}


//unsigned char i = 0;
//
//int main()
//{
//	for (i = 0; i <= 255; i++)
//	{
//		//��ѭ��
//		printf("hello world\n");
//	}
//	return 0;
//}


//class Solution {
//public:
//    void reverseString(vector<char>& s) {
//        int l = 0, r = s.size() - 1;
//        while (l < r)
//        {
//            swap(s[l], s[r]);
//            l++; r--;
//        }
//    }
//};


//class Solution {
//public:
//    string reverseVowels(string s) {
//        char arr[] = { 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' };
//        set<char> tmp(arr, arr + 10);
//        int l = 0, r = s.size() - 1;
//        while (l < r)
//        {
//            while (l < r && !tmp.count(s[l]))
//                l++;
//            while (l < r && !tmp.count(s[r]))
//                r--;
//            if (l < r)
//            {
//                swap(s[l], s[r]);
//                l++; r--;
//            }
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    string reverseStr(string s, int k) {
//        int n = s.size();
//        for (int i = 0; i < n; i += 2 * k)
//        {
//            if (i + k <= s.size()) reverse(s.begin() + i, s.begin() + i + k);
//            else reverse(s.begin() + i, s.end());
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    string finalString(string s) {
//        string res;
//        for (auto ch : s)
//        {
//            if (ch == 'i') reverse(res.begin(), res.end());
//            else res.push_back(ch);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    string reverseWords(string s) {
//        int n = s.size();
//        int a = 0, b = 0;
//        while (a < n)
//        {
//            if (s[a] == ' ')
//            {
//                reverse(s.begin() + b, s.begin() + a);
//                a++;
//                b = a;
//            }
//            else a++;
//        }
//        reverse(s.begin() + b, s.begin() + a);
//        return s;
//    }
//};


//class Solution {
//public:
//    string reverseWords(string s) {
//        int n = s.size();
//        int a = 0, b = 0;
//        while (a <= n)
//        {
//            if (s[a] == ' ' || a == n)
//            {
//                reverse(s.begin() + b, s.begin() + a);
//                a++;
//                b = a;
//            }
//            else a++;
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    void removeExtraSpaces(string& s)
//    {
//        int n = s.size();
//        int slow = 0;
//        for (int fast = 0; fast < n; fast++)
//        {
//            if (s[fast] != ' ')
//            {
//                if (slow != 0)
//                {
//                    s[slow++] = ' ';
//                }
//                while (fast < n && s[fast] != ' ')
//                {
//                    s[slow++] = s[fast++];
//                }
//            }
//        }
//        s.resize(slow);
//    }
//    string reverseWords(string s) {
//        reverse(s.begin(), s.end());
//        removeExtraSpaces(s);
//        int n = s.size();
//        int st = 0, ed = 0;
//        while (ed <= n)
//        {
//            if (s[ed] == ' ' || ed == n)
//            {
//                reverse(s.begin() + st, s.begin() + ed);
//                ed++;
//                st = ed;
//            }
//            else ed++;
//        }
//        return s;
//    }
//};


//class Solution {
//public:
//    bool is_Letter(char ch)
//    {
//        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
//            return true;
//        return false;
//    }
//    string reverseOnlyLetters(string s) {
//        int l = 0, r = s.size() - 1;
//        while (l < r)
//        {
//            while (l < r && !is_Letter(s[l])) l++;
//            while (l < r && !is_Letter(s[r])) r--;
//            if (l < r)
//            {
//                swap(s[l], s[r]);
//                l++; r--;
//            }
//        }
//        return s;
//    }
//};