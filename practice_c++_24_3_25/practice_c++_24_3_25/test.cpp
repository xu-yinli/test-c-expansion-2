#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums)
//            sum += e;
//        if (sum % 2 == 1)
//            return false;
//        int target = sum / 2;
//        vector<vector<int>> dp(n, vector<int>(target + 1));
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j <= target; j++)
//            {
//                if (j >= nums[i])
//                    dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - nums[i]] + nums[i]);
//                else
//                    dp[i][j] = dp[i - 1][j];
//                if (dp[i][j] == target)
//                    return true;
//            }
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums)
//            sum += e;
//        if (sum % 2 == 1)
//            return false;
//        int target = sum / 2;
//        vector<int> dp(20010);
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = target; j >= nums[i]; j--)
//            {
//                dp[j] = max(dp[j], dp[j - nums[i]] + nums[i]);
//                if (dp[j] == target)
//                    return true;
//            }
//        }
//        return false;
//    }
//};


//class Solution {
//public:
//    bool canPartition(vector<int>& nums) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums)
//            sum += e;
//        if (sum % 2 == 1)
//            return false;
//        int target = sum / 2;
//        vector<int> dp(20010);
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = target; j >= nums[i]; j--)
//            {
//                dp[j] = max(dp[j], dp[j - nums[i]] + nums[i]);
//            }
//        }
//        if (dp[target] == target) return true;
//        else return false;
//    }
//};


//class Solution {
//public:
//    int lastStoneWeightII(vector<int>& stones) {
//        int n = stones.size();
//        int sum = 0;
//        for (auto e : stones)
//            sum += e;
//        int target = sum / 2;
//        vector<int> dp(1510);
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = target; j >= stones[i]; j--)
//            {
//                dp[j] = max(dp[j], dp[j - stones[i]] + stones[i]);
//            }
//        }
//        return sum - dp[target] - dp[target];
//    }
//};


//class Solution {
//    //left-正数集合   right-负数集合
//    //                   left-right=target
//    //left+right=sum ==> right=sum-left
//    //               ==> left-(sum-left)=target
//    //               ==> left=(sum+target)/2
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums)
//            sum += e;
//        if (abs(target) > sum)
//            return 0;
//        if ((sum + target) % 2 == 1)
//            return 0;
//        int left = (target + sum) / 2;
//        vector<int> dp(left + 1);
//        dp[0] = 1;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = left; j >= nums[i]; j--)
//            {
//                dp[j] += dp[j - nums[i]];
//            }
//        }
//        return dp[left];
//    }
//};


//class Solution {
//    //left-正数集合   right-负数集合
//    //                   left-right=target
//    //left+right=sum ==> right=sum-left
//    //               ==> left-(sum-left)=target
//    //               ==> left=(sum+target)/2
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums)
//            sum += e;
//        if (abs(target) > sum)
//            return 0;
//        if ((sum + target) % 2 == 1)
//            return 0;
//        int left = (target + sum) / 2;
//        vector<vector<int>> dp(n + 1, vector<int>(left + 1));
//        dp[0][0] = 1;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j <= left; j++)
//            {
//                if (j >= nums[i])
//                    dp[i + 1][j] = dp[i][j] + dp[i][j - nums[i]];
//                else
//                    dp[i + 1][j] = dp[i][j];
//            }
//        }
//        return dp[n][left];
//    }
//};


//class Solution {
//public:
//	int lastStoneWeightII(vector<int>& stones) {
//		int n = stones.size();
//		int sum = 0;
//		for (auto e : stones)
//			sum += e;
//		int target = sum / 2;
//		vector<vector<int>> dp(n + 1, vector<int>(target + 1));
//		for (int i = 0; i < n; i++)
//		{
//			for (int j = 0; j <= target; j++)
//			{
//				if (j >= stones[i])
//					dp[i + 1][j] = max(dp[i][j], dp[i][j - stones[i]] + stones[i]);
//				else
//					dp[i + 1][j] = dp[i][j];
//			}
//		}
//		return sum - dp[n][target] - dp[n][target];
//	}
//};


//class Solution {
//    //left-正数集合   right-负数集合
//    //                   left-right=target
//    //left+right=sum ==> right=sum-left
//    //               ==> left-(sum-left)=target
//    //               ==> left=(sum+target)/2
//public:
//    int findTargetSumWays(vector<int>& nums, int target) {
//        int n = nums.size();
//        int sum = 0;
//        for (auto e : nums)
//            sum += e;
//        if (abs(target) > sum)
//            return 0;
//        if ((sum + target) % 2 == 1)
//            return 0;
//        int left = (target + sum) / 2;
//        vector<int> dp(left + 1);
//        dp[0] = 1;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = left; j >= nums[i]; j--)
//            {
//                dp[j] = dp[j] + dp[j - nums[i]];
//            }
//        }
//        return dp[left];
//    }
//};


//class Solution {
//public:
//    int findMaxForm(vector<string>& strs, int m, int n) {
//        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
//        for (string str : strs)
//        {
//            int x = 0, y = 0;
//            for (char c : str)
//            {
//                if (c == '0') x++;
//                else y++;
//            }
//            for (int i = m; i >= x; i--)
//            {
//                for (int j = n; j >= y; j--)
//                {
//                    dp[i][j] = max(dp[i][j], dp[i - x][j - y] + 1);
//                }
//            }
//        }
//        return dp[m][n];
//    }
//};


//class Solution {
//private:
//    unordered_map<char, int> a;
//public:
//    int longestPalindrome(string s) {
//        int lens = 0;
//        for (auto c : s)
//            a[c - 'A']++;
//        bool flag = false;
//        for (int i = 0; i < a.size(); i++)
//        {
//            if (a[i] % 2 == 0) lens += a[i];
//            else
//            {
//                flag = true;
//                lens += a[i] - 1;
//            }
//        }
//        if (flag) lens++;
//        return lens;
//    }
//};


//class Solution {
//public:
//    int longestPalindrome(string s) {
//        int res = 0;
//        int hash[127] = { 0 };
//        for (char c : s)
//            hash[c]++;
//        for (int x : hash)
//            res += x / 2 * 2;
//        return res < s.size() ? res + 1 : res;
//    }
//};


class Solution {
public:
    vector<int> diStringMatch(string s) {
        vector<int> res;
        int left = 0, right = s.size();
        for (auto c : s)
        {
            if (c == 'I')
            {
                res.push_back(left++);
            }
            else if (c == 'D')
            {
                res.push_back(right--);
            }
        }
        res.push_back(left);
        return res;
    }
};