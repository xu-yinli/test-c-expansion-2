#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string s;
//    cin >> s;
//    int n = s.size();
//    int len = 101;
//    for (int i = 0; i < n; i++)
//    {
//        //奇数长度的扩展
//        int left = i, right = i;
//        while (left >= 0 && right < n && s[left] == s[right])
//        {
//            if (right - left + 1 < len && right - left + 1 > 1) len = right - left + 1;
//            left--;
//            right++;
//        }
//
//        //偶数长度的扩展
//        left = i, right = i + 1;
//        while (left >= 0 && right < n && s[left] == s[right])
//        {
//            if (right - left + 1 < len && right - left + 1 > 1) len = right - left + 1;
//            left--;
//            right++;
//        }
//    }
//    if (len > 1 && len < 101) cout << len << endl;
//    else cout << -1 << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    string s;
//    cin >> s;
//    int n = s.size();
//    int res = -1;
//    for (int i = 0; i < n; i++)
//    {
//        if (i + 1 < n && s[i] == s[i + 1])
//        {
//            res = 2;
//            break;
//        }
//        if (i + 2 < n && s[i] == s[i + 2])
//            res = 3;
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 2e5 + 10;
//int a[N], f[N], g[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 1; i <= n; i++)
//        cin >> a[i];
//    for (int i = 1; i <= n; i++)
//    {
//        f[i] = max(f[i - 1], g[i - 1] + a[i]);
//        g[i] = max(f[i - 1], g[i - 1]);
//    }
//    cout << max(f[n], g[n]) << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 2e5 + 10;
//int a[N], dp[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//        cin >> a[i];
//    dp[0] = a[0], dp[1] = max(a[0], a[1]);
//    for (int i = 2; i < n; i++)
//        dp[i] = max(dp[i - 2] + a[i], dp[i - 1]);
//    cout << dp[n - 1] << endl;
//    return 0;
//}


//class Solution {
//public:
//    int rob(vector<int>& nums) {
//        int n = nums.size();
//        vector<int> f(n + 1);
//        vector<int> g(n + 1);
//        for (int i = 1; i <= n; i++)
//        {
//            f[i] = max(f[i - 1], g[i - 1] + nums[i - 1]);
//            g[i] = max(f[i - 1], g[i - 1]);
//        }
//        return max(f[n], g[n]);
//    }
//};


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//const int N = 1e6 + 10;
//int a[N];
//
//int main()
//{
//    int n, p;
//    cin >> n >> p;
//    p *= 2;
//    for (int i = 0; i < n; i++)
//        cin >> a[i];
//    sort(a, a + n);
//    int res = 0;
//    int left = 0, right = 0;
//    while (right < n)
//    {
//        while (a[right] - a[left] > p)
//            left++;
//        res = max(res, right - left + 1);
//        right++;
//    }
//    cout << res << endl;
//    return 0;
//}



//#include <iostream>
//using namespace std;
//
//int main()
//{
//	int t;
//	cin >> t;
//	while (t--)
//	{
//		string s;
//		cin >> s;
//		bool flag = false;
//		for (int i = 0; i < 5; i++)
//		{
//			if (s[4] == '0') flag = true;
//			else
//			{
//				if ((s[i] == '2' || s[i] == '4' || s[i] == '6' || s[i] == '8' || s[i] == '0'))
//				{
//					flag = true;
//					swap(s[i], s[4]);
//				}
//			}
//		}
//		if (flag) cout << s << endl;
//		else cout << "97531" << endl;
//	}
//	return 0;
//}


//#include <iostream>
//using namespace std;
//
//int main()
//{
//    int t;
//    cin >> t;
//    int res = 0;
//    while (t--)
//    {
//        string s;
//        cin >> s;
//        if (s.size() != 5 || s[2] != s[4] || s[0] == s[1] || s[0] == s[2] || s[0] == s[3] || s[1] == s[2] || s[1] == s[3] || s[2] == s[3])
//            continue;
//        else res++;
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//
//typedef long long LL;
//const int N = 2e5 + 10;
//int arr[N];
//
//int mi(int x)
//{
//    return x * x * x * x;
//}
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= m; i++)
//        cin >> arr[i];
//    vector<LL>dp(N, LONG_MAX); //排查到第i个bug时所需的最小时间
//    dp[0] = 0;
//    dp[1] = (LL)arr[1] + pow(1, 4); //运行时间+debug时间
//    for (int i = 2; i <= m; i++)
//    {
//        int j = 1;
//        if (i - j + 1 >= 22) j = i - 21;
//        for (; j <= i; j++)
//            dp[i] = min(dp[j - 1] + arr[i] + (i - j + 1) * (i - j + 1) * (i - j + 1) * (i - j + 1), dp[i]);
//    }
//    cout << dp[n] << endl;
//    return 0;
//}
//
//
//#include<bits/stdc++.h>
//#include<unordered_map>
//using namespace std;
//#define int long long
//using ll = long long;
//const int maxn = 2e5 + 7;
//const int INF = 1e18 + 7;
//
//int f(int x) {
//    return x * x * x * x;
//}
//
//signed main() {
//    ios::sync_with_stdio(0);
//    cin.tie(0); cout.tie(0);
//
//    int n, m; cin >> n >> m;
//    vector<int> x(m + 5), dp(m + 5, INF);
//
//    dp[0] = 0;
//    for (int i = 1; i <= m; i++) cin >> x[i];
//    for (int i = 1; i <= m; i++) {
//        // 往后30个已经炸到血妈亏了，不必再往后了
//        for (int j = i - 1; j >= max(0ll, i - 30); j--) {
//            // 转移方程为 解决j个，剩下i-j个本轮一并解决
//            int dp1 = dp[j] + f(i - j) + x[i];
//            dp[i] = min(dp[i], dp1);
//        }
//    }
//
//    cout << dp[m] << endl;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//const int N = 2e5 + 10;
//int c[N], dp[N]; //从前走到i位置遇到的最小价格
//
//int main()
//{
//	int n;
//	cin >> n;
//	for (int i = 1; i <= n; i++)
//		cin >> c[i];
//	dp[n] = c[n];
//	for (int i = n - 1; i > 0; i--)
//		dp[i] = min(dp[i + 1], c[i]);
//	int money = 0;
//	int res = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		money++;
//		if (money >= c[i] && c[i] <= dp[i])
//		{
//			int cnt = money / c[i];
//			res += cnt;
//			money -= cnt * c[i];
//		}
//	}
//	cout << res << endl;
//	return 0;
//}


#include <iostream>
#include <queue>
#include <unordered_map>
using namespace std;

typedef long long LL;
const int N = 2e5 + 10, MOD = 998244353;

LL qmi(int a, int b, int q)
{
    LL res = 1;
    while (b)
    {
        if (b & 1) res = (LL)res * a % q;
        a = (LL)a * a % q;
        b >>= 1;
    }
    return res;
}

int main()
{
    int n;
    cin >> n;
    priority_queue<int, vector<int>, greater<int>> q; //小根堆
    unordered_map<int, int> hash; //统计每个数字的个数
    LL a = 1, b = 1; 
    int maxi = 0;
    bool flag = true;
    for (int i = 0; i < 2 * n; i++)
    {
        int x;
        cin >> x;
        if (x == -1)
        {
            int t = q.top();
            a = (a * hash[t]) % MOD;
            hash[t]--;
            maxi = max(maxi, t);
            int sz = q.size();
            b = (b * sz) % MOD;
            q.pop();
        }
        else
        { 
            q.push(x);
            hash[x]++;
            if (x < maxi)
                flag = false;
        }
    }
    if (flag) cout << (a * qmi(b, MOD - 2, MOD)) % MOD << endl;
    else cout << 0 << endl;
    return 0;
}