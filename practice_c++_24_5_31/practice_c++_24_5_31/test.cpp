#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 2010;
//int a[N];
//int n;
//
//int bfs()
//{
//    int left = 1, right = 1;
//    int res = 0;
//    while (left <= right)
//    {
//        res++;
//        int r = right;
//        for (int i = left; i <= right; i++)
//        {
//            r = max(r, a[i] + i);
//            if (r >= n) return res;
//        }
//        left = right + 1;
//        right = r;
//    }
//    return -1;
//}
//
//int main()
//{
//    cin >> n;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    cout << bfs() << endl;
//    return 0;
//}


//class Solution {
//private:
//    int check[9];
//    vector<int> path;
//    vector<vector<int>> res;
//public:
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == nums.size())
//        {
//            res.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == true || (i != 0 && nums[i - 1] == nums[i] && !check[i - 1])) continue;
//            path.push_back(nums[i]);
//            check[i] = true;
//            dfs(nums, pos + 1);
//            check[i] = false;
//            path.pop_back();
//        }
//    }
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        dfs(nums, 0);
//        return res;
//    }
//};


//class Solution {
//private:
//    int check[9];
//    vector<int> path;
//    vector<vector<int>> res;
//public:
//    void dfs(vector<int>& nums, int pos)
//    {
//        if (pos == nums.size())
//        {
//            res.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false && (i == 0 || nums[i - 1] != nums[i] || check[i - 1]))
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums, pos + 1);
//                check[i] = false;
//                path.pop_back();
//            }
//        }
//    }
//    vector<vector<int>> permuteUnique(vector<int>& nums) {
//        sort(nums.begin(), nums.end());
//        dfs(nums, 0);
//        return res;
//    }
//};


//class Solution {
//private:
//    string path;
//    vector<string> res;
//    const string numToStr[10] = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
//public:
//    void dfs(const string& digits, int pos)
//    {
//        if (pos == digits.size())
//        {
//            res.push_back(path);
//            return;
//        }
//        string s = numToStr[digits[pos] - '0'];
//        for (int i = 0; i < s.size(); i++)
//        {
//            path.push_back(s[i]);
//            dfs(digits, pos + 1);
//            path.pop_back();
//        }
//    }
//    vector<string> letterCombinations(string digits) {
//        if (digits.size() == 0) return res;
//        dfs(digits, 0);
//        return res;
//    }
//};