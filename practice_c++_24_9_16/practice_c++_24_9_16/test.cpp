#define _CRT_SECURE_NO_WARNINGS 1

///*
//// Definition for a Node.
//class Node {
//public:
//    int val;
//    vector<Node*> children;
//
//    Node() {}
//
//    Node(int _val) {
//        val = _val;
//    }
//
//    Node(int _val, vector<Node*> _children) {
//        val = _val;
//        children = _children;
//    }
//};
//*/
//
//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    vector<vector<int>> levelOrder(Node* root) {
//        if (root == nullptr) return res;
//        queue<Node*> q;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> tmp;
//            int n = q.size();
//            while (n--)
//            {
//                Node* node = q.front();
//                q.pop();
//                tmp.push_back(node->val);
//                for (auto child : node->children)
//                {
//                    if (child != nullptr)
//                        q.push(child);
//                }
//            }
//            res.push_back(tmp);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
//        if (root == nullptr) return res;
//        queue<TreeNode*> q;
//        q.push(root);
//        int i = 1;
//        while (q.size())
//        {
//            vector<int> tmp;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                tmp.push_back(node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            if (i % 2 == 0) reverse(tmp.begin(), tmp.end());
//            res.push_back(tmp);
//            i++;
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    int widthOfBinaryTree(TreeNode* root) {
//        unsigned int res = 0;
//        vector<pair<TreeNode*, unsigned int>> q;
//        q.push_back({ root, 1 });
//        while (q.size())
//        {
//            vector<pair<TreeNode*, unsigned int>> tmp;
//            auto& [x1, y1] = q[0];
//            auto& [x2, y2] = q.back();
//            res = max(res, y2 - y1 + 1);
//            for (auto& [x, y] : q)
//            {
//                if (x->left) tmp.push_back({ x->left, 2 * y });
//                if (x->right) tmp.push_back({ x->right, 2 * y + 1 });
//            }
//            q = tmp;
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    vector<int> largestValues(TreeNode* root) {
//        if (root == nullptr) return res;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            int tmp = INT_MIN;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* node = q.front();
//                q.pop();
//                tmp = max(tmp, node->val);
//                if (node->left) q.push(node->left);
//                if (node->right) q.push(node->right);
//            }
//            res.push_back(tmp);
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int countStudents(vector<int>& students, vector<int>& sandwiches) {
//        int zero = 0, one = 0;
//        for (int x : students)
//        {
//            if (x == 0) zero++;
//            else if (x == 1) one++;
//        }
//        for (int x : sandwiches)
//        {
//            if (x == 0)
//            {
//                if (zero > 0) zero--;
//                else return one;
//            }
//            else if (x == 1)
//            {
//                if (one > 0) one--;
//                else return zero;
//            }
//        }
//        return 0;
//    }
//};


//class Solution {
//private:
//    vector<string> res;
//public:
//    vector<string> buildArray(vector<int>& target, int n) {
//        int cur = 0;
//        for (int i = 0; i < target.size(); i++)
//        {
//            for (int j = 0; j < target[i] - 1 - cur; j++)
//            {
//                res.push_back("Push");
//                res.push_back("Pop");
//            }
//            res.push_back("Push");
//            cur = target[i];
//        }
//        return res;
//    }
//};


//class MyCircularDeque {
//private:
//    vector<int> q;
//    int front;
//    int rear;
//    int capacity;
//public:
//    MyCircularDeque(int k) {
//        this->front = 0;
//        this->rear = 0;
//        this->capacity = k + 1;
//        this->q = vector<int>(capacity);
//    }
//
//    bool insertFront(int value) {
//        if (isFull()) return false;
//        front = (front - 1 + capacity) % capacity;
//        q[front] = value;
//        return true;
//    }
//
//    bool insertLast(int value) {
//        if (isFull()) return false;
//        q[rear] = value;
//        rear = (rear + 1) % capacity;
//        return true;
//    }
//
//    bool deleteFront() {
//        if (isEmpty()) return false;
//        front = (front + 1) % capacity;
//        return true;
//    }
//
//    bool deleteLast() {
//        if (isEmpty()) return false;
//        rear = (rear - 1 + capacity) % capacity;
//        return true;
//    }
//
//    int getFront() {
//        if (isEmpty()) return -1;
//        return q[front];
//    }
//
//    int getRear() {
//        if (isEmpty()) return -1;
//        return q[(rear - 1 + capacity) % capacity];
//    }
//
//    bool isEmpty() {
//        if (front == rear) return true;
//        return false;
//    }
//
//    bool isFull() {
//        if ((rear + 1) % capacity == front) return true;
//        return false;
//    }
//};
//
///**
// * Your MyCircularDeque object will be instantiated and called as such:
// * MyCircularDeque* obj = new MyCircularDeque(k);
// * bool param_1 = obj->insertFront(value);
// * bool param_2 = obj->insertLast(value);
// * bool param_3 = obj->deleteFront();
// * bool param_4 = obj->deleteLast();
// * int param_5 = obj->getFront();
// * int param_6 = obj->getRear();
// * bool param_7 = obj->isEmpty();
// * bool param_8 = obj->isFull();
// */


//class Solution {
//private:
//    vector<int> res;
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param num int整型vector
//     * @param size int整型
//     * @return int整型vector
//     */
//    class MyDeque
//    {
//    public:
//        deque<int> q;
//        void push(int x)
//        {
//            while (!q.empty() && x > q.back())
//                q.pop_back();
//            q.push_back(x);
//        }
//        void pop(int x)
//        {
//            if (!q.empty() && x == q.front())
//                q.pop_front();
//        }
//        int getMax()
//        {
//            return q.front();
//        }
//    };
//    vector<int> maxInWindows(vector<int>& num, int size) {
//        if (size == 0 || num.size() < size) return res;
//        MyDeque q;
//        for (int i = 0; i < size; i++)
//            q.push(num[i]);
//        res.push_back(q.getMax());
//        for (int i = size; i < num.size(); i++)
//        {
//            q.pop(num[i - size]);
//            q.push(num[i]);
//            res.push_back(q.getMax());
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param a int整型vector
//     * @param n int整型
//     * @param K int整型
//     * @return int整型
//     */
//    struct cmp
//    {
//        bool operator()(const int& a, const int& b)
//        {
//            return a < b;
//        }
//    };
//    int findKth(vector<int>& a, int n, int K) {
//        priority_queue<int, vector<int>, cmp> heap;
//        for (int i = 0; i < n; i++)
//            heap.push(a[i]);
//        for (int i = 0; i < K - 1; i++)
//            heap.pop();
//        return heap.top();
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param a int整型vector
//     * @param n int整型
//     * @param K int整型
//     * @return int整型
//     */
//    struct cmp
//    {
//        bool operator()(const int& a, const int& b)
//        {
//            return a > b;
//        }
//    };
//    int findKth(vector<int>& a, int n, int K) {
//        priority_queue<int, vector<int>, cmp> heap;
//        for (int i = 0; i < K; i++)
//            heap.push(a[i]);
//        for (int i = K; i < n; i++)
//        {
//            if (a[i] > heap.top())
//            {
//                heap.pop();
//                heap.push(a[i]);
//            }
//        }
//        return heap.top();
//    }
//};


//class Solution {
//private:
//    priority_queue<int> left;
//    priority_queue<int, vector<int>, greater<int>> right;
//public:
//    void Insert(int num) {
//        if (left.size() == right.size())
//        {
//            if (left.empty() || num <= left.top()) left.push(num);
//            else
//            {
//                right.push(num);
//                left.push(right.top());
//                right.pop();
//            }
//        }
//        else if (left.size() > right.size())
//        {
//            if (num < left.top())
//            {
//                left.push(num);
//                right.push(left.top());
//                left.pop();
//            }
//            else right.push(num);
//        }
//    }
//
//    double GetMedian() {
//        if (left.size() == right.size()) return (left.top() + right.top()) / 2.0;
//        else return left.top();
//    }
//
//};


//#include <iostream>
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     * 返回表达式的值
//     * @param s string字符串 待计算的表达式
//     * @return int整型
//     */
//    int solve(string s) {
//        int num = 0;
//        char ch = '+';
//        stack<int> st;
//        int n = s.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] >= '0' && s[i] <= '9')
//            {
//                num = num * 10 + (s[i] - '0');
//            }
//            else if (s[i] == '(')
//            {
//                i++;
//                int left = i;
//                int count = 1;
//                while (count > 0)
//                {
//                    if (s[i] == '(') count++;
//                    else if (s[i] == ')') count--;
//                    i++;
//                }
//                num = solve(s.substr(left, i - left - 1));
//                i--;
//            }
//            if (i == n - 1 || s[i] == '+' || s[i] == '-' || s[i] == '*')
//            {
//                if (ch == '+') st.push(num);
//                else if (ch == '-') st.push(-num);
//                else if (ch == '*') st.top() *= num;
//                ch = s[i];
//                num = 0;
//            }
//        }
//        int sum = 0;
//        while (st.size())
//        {
//            sum += st.top();
//            st.pop();
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    stack<int> ops;
//public:
//    int calculate(string s) {
//        int sign = 1;
//        ops.push(1);
//        int sum = 0;
//        int n = s.size();
//        for (int i = 0; i < n; i++)
//        {
//            if (s[i] == ' ') continue;
//            else if (s[i] == '+') sign = ops.top();
//            else if (s[i] == '-') sign = -ops.top();
//            else if (s[i] == '(') ops.push(sign);
//            else if (s[i] == ')') ops.pop();
//            else
//            {
//                int num = 0;
//                while (i < n && s[i] >= '0' && s[i] <= '9')
//                {
//                    num = num * 10 + (s[i] - '0');
//                    i++;
//                }
//                sum += sign * num;
//                i--;
//            }
//        }
//        return sum;
//    }
//};


//#include <iostream>
//using namespace std;
//
//class Base
//{
//public:
//    virtual void Func1()
//    {
//        cout << "Func1()" << endl;
//    }
//private:
//    int _b = 1;
//};
//
//int main()
//{
//    cout << sizeof(Base) << endl; //8
//    return 0;
//}


//#include <iostream>
//#include <string>
//using namespace std;
//
//class A {
//public:
//    A(const char* s)
//    {
//        cout << s << endl;
//    }
//    ~A() {}
//};
//
//class B :virtual public A
//{
//public:
//    B(const char* s1, const char* s2)
//        :A(s1)
//    {
//        cout << s2 << endl;
//    }
//};
//
//class C :virtual public A
//{
//public:
//    C(const char* s1, const char* s2)
//        :A(s1)
//    {
//        cout << s2 << endl;
//    }
//};
//
//class D :public B, public C
//{
//public:
//    D(const char* s1, const char* s2, const char* s3, const char* s4)
//        :B(s1, s2),
//         C(s1, s3),
//         A(s1)
//    {
//        cout << s4 << endl;
//    }
//};
//
//int main()
//{
//    D* p = new D("class A", "class B", "class C", "class D");
//    delete p;
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//class A
//{
//public:
//    virtual void func(int val = 1)
//    {
//        std::cout << "A->" << val << std::endl;
//    }
//    virtual void test()
//    {
//        func();
//    }
//};
//
//class B : public A
//{
//public:
//    void func(int val = 0)
//    {
//        std::cout << "B->" << val << std::endl;
//    }
//};
//
//int main(int argc, char* argv[])
//{
//    B* p = new B;
//    p->test();
//    return 0;
//}


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void postorder(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        postorder(root->left);
//        postorder(root->right);
//        res.push_back(root->val);
//    }
//    vector<int> postorderTraversal(TreeNode* root) {
//        if (root == nullptr) return res;
//        postorder(root);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void inorder(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        inorder(root->left);
//        res.push_back(root->val);
//        inorder(root->right);
//    }
//    vector<int> inorderTraversal(TreeNode* root) {
//        if (root == nullptr) return res;
//        inorder(root);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<int> res;
//public:
//    void preorder(TreeNode* root)
//    {
//        if (root == nullptr) return;
//        res.push_back(root->val);
//        preorder(root->left);
//        preorder(root->right);
//    }
//    vector<int> preorderTraversal(TreeNode* root) {
//        if (root == nullptr) return res;
//        preorder(root);
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    vector<vector<int>> levelOrder(TreeNode* root) {
//        if (root == nullptr) return res;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> tmp;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* p = q.front();
//                q.pop();
//                tmp.push_back(p->val);
//                if (p->left) q.push(p->left);
//                if (p->right) q.push(p->right);
//            }
//            res.push_back(tmp);
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//private:
//    vector<vector<int>> res;
//public:
//    vector<vector<int>> levelOrderBottom(TreeNode* root) {
//        if (root == nullptr) return res;
//        queue<TreeNode*> q;
//        q.push(root);
//        while (q.size())
//        {
//            vector<int> tmp;
//            int n = q.size();
//            while (n--)
//            {
//                TreeNode* p = q.front();
//                q.pop();
//                tmp.push_back(p->val);
//                if (p->left) q.push(p->left);
//                if (p->right) q.push(p->right);
//            }
//            res.push_back(tmp);
//        }
//        reverse(res.begin(), res.end());
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* traversal(vector<int>& preorder, int pre_st, int pre_ed, vector<int>& inorder, int in_st, int in_ed)
//    {
//        if (pre_st > pre_ed || in_st > in_ed) return nullptr;
//        TreeNode* root = new TreeNode(preorder[pre_st]);
//        for (int i = 0; i < inorder.size(); i++)
//        {
//            if (root->val == inorder[i])
//            {
//                root->left = traversal(preorder, pre_st + 1, (pre_st + 1) + (i - in_st) - 1, inorder, in_st, i - 1);
//                root->right = traversal(preorder, (pre_st + 1) + (i - in_st), pre_ed, inorder, i + 1, in_ed);
//                break;
//            }
//        }
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
//        int n = preorder.size(), m = inorder.size();
//        return traversal(preorder, 0, n - 1, inorder, 0, m - 1);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* traversal(vector<int>& inorder, int in_st, int in_ed, vector<int>& postorder, int post_st, int post_ed)
//    {
//        if (in_st > in_ed || post_st > post_ed) return nullptr;
//        TreeNode* root = new TreeNode(postorder[post_ed]);
//        for (int i = 0; i < inorder.size(); i++)
//        {
//            if (root->val == inorder[i])
//            {
//                root->left = traversal(inorder, in_st, i - 1, postorder, post_st, post_st + (i - in_st) - 1);
//                root->right = traversal(inorder, i + 1, in_ed, postorder, post_st + (i - in_st), post_ed - 1);
//                break;
//            }
//        }
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
//        int n = inorder.size(), m = postorder.size();
//        return traversal(inorder, 0, n - 1, postorder, 0, m - 1);
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
// *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
// *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// * };
// */
//class Solution {
//public:
//    string tree2str(TreeNode* root) {
//        string res;
//        if (root == nullptr) return res;
//        res += to_string(root->val);
//        if (root->left || root->right)
//        {
//            res += '(';
//            res += tree2str(root->left);
//            res += ')';
//        }
//        if (root->right)
//        {
//            res += '(';
//            res += tree2str(root->right);
//            res += ')';
//        }
//        return res;
//    }
//};


///**
// * Definition for a binary tree node.
// * struct TreeNode {
// *     int val;
// *     TreeNode *left;
// *     TreeNode *right;
// *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
// * };
// */
//class Solution {
//public:
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        if (root == NULL || root == p || root == q) return root;
//        TreeNode* left = lowestCommonAncestor(root->left, p, q);
//        TreeNode* right = lowestCommonAncestor(root->right, p, q);
//        if (left != NULL && right != NULL) return root;
//        else if (left == NULL && right != NULL) return right;
//        else if (left != NULL && right == NULL) return left;
//        else return NULL;
//    }
//};


///*
//struct TreeNode {
//	int val;
//	struct TreeNode *left;
//	struct TreeNode *right;
//	TreeNode(int x) :
//			val(x), left(NULL), right(NULL) {
//	}
//};*/
//class Solution {
//public:
//	void inorder(TreeNode* root, TreeNode*& prev)
//	{
//		if (root == nullptr) return;
//		inorder(root->left, prev);
//		root->left = prev;
//		if (prev != nullptr) prev->right = root;
//		prev = root;
//		inorder(root->right, prev);
//	}
//	TreeNode* Convert(TreeNode* pRootOfTree) {
//		if (pRootOfTree == nullptr) return nullptr;
//		TreeNode* prev = nullptr;
//		inorder(pRootOfTree, prev);
//		TreeNode* head = pRootOfTree;
//		while (head->left)
//			head = head->left;
//		return head;
//	}
//};