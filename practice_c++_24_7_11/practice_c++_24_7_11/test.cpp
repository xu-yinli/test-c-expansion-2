#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 200010;
//int a[N];
//
//int main()
//{
//    int n;
//    cin >> n;
//    for (int i = 0; i < n; i++)
//        cin >> a[i];
//    int res = 1;
//    int left = 0, right = 0;
//    while (right < n)
//    {
//        left = right;
//        while (right + 1 < n && a[right + 1] - a[right] <= 8)
//            right++;
//        res = max(res, right - left + 1);
//        right++;
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <vector>
//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param CityMap int整型vector<vector<>>
//     * @param n int整型
//     * @param m int整型
//     * @return int整型
//     */
//    int _n, _m;
//    int x1, y1, x2, y2;
//    int way[15][15] = { 0 }, dis[15][15] = { 0 };
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//    typedef pair<int, int> PII;
//    int bfs(vector<vector<int>>& CityMap)
//    {
//        memset(dis, -1, sizeof(dis));
//        queue<PII> q;
//        q.push({ x1, y1 });
//        way[x1][y1] = 1;
//        dis[x1][y1] = 0;
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < _n && y >= 0 && y < _m && CityMap[x][y] != -1)
//                {
//                    if (dis[x][y] == -1) //第一次到达这个位置
//                    {
//                        dis[x][y] = dis[a][b] + 1;
//                        way[x][y] += way[a][b];
//                        q.push({ x, y });
//                    }
//                    else
//                    {
//                        if (dis[x][y] == dis[a][b] + 1) //判断是否为最短路
//                            way[x][y] += way[a][b];
//                    }
//                }
//            }
//        }
//        return way[x2][y2];
//    }
//    int countPath(vector<vector<int>>& CityMap, int n, int m) {
//        _n = n, _m = m;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (CityMap[i][j] == 1)
//                {
//                    x1 = i;
//                    y1 = j;
//                }
//                else if (CityMap[i][j] == 2)
//                {
//                    x2 = i;
//                    y2 = j;
//                }
//            }
//        }
//        return bfs(CityMap);
//    }
//};


//class Solution {
//    //f[i][j]:第i天结束后，完成了j次交易，此时处于“买入”状态下的最大利润
//    //g[i][j]:第i天结束后，完成了j次交易，此时处于“卖出”状态下的最大利润
//private:
//    const int INF = 0x3f3f3f3f;
//public:
//    int maxProfit(int k, vector<int>& prices) {
//        int n = prices.size();
//        vector<vector<int>> f(n, vector<int>(k + 1, -INF));
//        vector<vector<int>> g(n, vector<int>(k + 1, -INF));
//        f[0][0] = -prices[0], g[0][0] = 0;
//        int res = 0;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = 0; j <= k; j++)
//            {
//                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//                g[i][j] = g[i - 1][j];
//                if (j >= 1) g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//                res = max(res, g[i][j]);
//            }
//        }
//        return res;
//    }
//};


//#include <iostream>
//#include <cstring>
//using namespace std;
//
//const int INF = 0x3f3f3f3f;
//const int N = 1010, M = 110;
//int prices[N];
//int f[N][M], g[N][M];
////f[i][j]:第i天结束后，完成了j次交易，此时处于“买入”状态下的最大利润
////g[i][j]:第i天结束后，完成了j次交易，此时处于“卖出”状态下的最大利润
//
//int main()
//{
//    int n, k;
//    cin >> n >> k;
//    for (int i = 0; i < n; i++)
//        cin >> prices[i];
//    memset(f, -INF, sizeof(f));
//    memset(g, -INF, sizeof(g));
//    int res = 0;
//    f[0][0] = -prices[0], g[0][0] = 0;
//    for (int i = 1; i < n; i++)
//    {
//        for (int j = 0; j <= k; j++)
//        {
//            f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
//            g[i][j] = g[i - 1][j];
//            if (j > 0) g[i][j] = max(g[i][j], f[i - 1][j - 1] + prices[i]);
//            res = max(res, g[i][j]);
//        }
//    }
//    cout << res << endl;
//    return 0;
//}


//#include <iostream>
//#include <algorithm>
//using namespace std;
//
//typedef long long LL;
//const int N = 200010;
//int a[N];
//
//int main()
//{
//    int n, x;
//    cin >> n >> x;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    sort(a + 1, a + n + 1);
//    LL ret = 0;
//    int k = max(0, n - x);
//    ret += a[k] * x;
//    for (int i = k + 1; i <= n; i++)
//        ret += a[i] - a[k];
//    cout << ret << endl;
//    return 0;
//}


//#include <iostream>
//#include <unordered_set>
//using namespace std;
//
//typedef long long LL;
//unordered_set<LL> row, col, dg, udg;
//const int N = 1e9 + 10;
//
//int main()
//{
//    int k;
//    cin >> k;
//    int tmp = 1e5 + 10;
//    for (int i = 1; i <= k; i++)
//    {
//        int x, y;
//        cin >> x >> y;
//        if (row.count(x) || col.count(y) || dg.count(y - x) || udg.count(x + y))
//        {
//            if (tmp == 1e5 + 10) tmp = i;
//        }
//        row.insert(x);
//        col.insert(y);
//        dg.insert(y - x);
//        udg.insert(x + y);
//    }
//    int t;
//    cin >> t;
//    while (t--)
//    {
//        int i;
//        cin >> i;
//        if (i >= tmp) cout << "Yes" << endl;
//        else cout << "No" << endl;
//    }
//    return 0;
//}


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param coins int整型vector
//     * @return int整型
//     */
//    int getCoins(vector<int>& coins) {
//        int n = coins.size();
//        vector<vector<int>> dp(n + 2, vector<int>(n + 2, 0));
//        vector<int> a(n + 2, 0);
//        for (int i = 1; i <= n; i++)
//            a[i] = coins[i - 1];
//        a[0] = a[n + 1] = 1;
//        for (int i = n; i >= 1; i--)
//        {
//            for (int j = i; j <= n; j++)
//            {
//                for (int k = i; k <= j; k++)
//                {
//                    dp[i][j] = max(dp[i][j], dp[i][k - 1] + dp[k + 1][j] + a[i - 1] * a[k] * a[j + 1]);
//                }
//            }
//        }
//        return dp[1][n];
//    }
//};


//class Solution {
//public:
//    int countSubstrings(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        int cnt = 0;
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                {
//                    if (i + 1 >= j) dp[i][j] = true; //i==j 或 i+1==j
//                    else dp[i][j] = dp[i + 1][j - 1];
//                    if (dp[i][j]) cnt++;
//                }
//            }
//        }
//        return cnt;
//    }
//};


//class Solution {
//public:
//    string longestPalindrome(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        int len = 1;
//        int st = 0;
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                {
//                    if (i + 1 < j) dp[i][j] = dp[i + 1][j - 1];
//                    else dp[i][j] = true;
//                }
//                if (dp[i][j] && j - i + 1 > len)
//                {
//                    len = j - i + 1;
//                    st = i;
//                }
//            }
//        }
//        return s.substr(st, len);
//    }
//};


//class Solution {
//public:
//    bool checkPartitioning(string s) {
//        int n = s.size();
//        vector<vector<bool>> dp(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                {
//                    if (i + 1 < j) dp[i][j] = dp[i + 1][j - 1];
//                    else dp[i][j] = true;
//                }
//            }
//        }
//        for (int i = 1; i < n - 1; i++)
//            for (int j = i; j < n - 1; j++)
//                if (dp[0][i - 1] && dp[i][j] && dp[j + 1][n - 1])
//                    return true;
//        return false;
//    }
//};


//class Solution {
//public:
//    //dp[i]:表示s[0,i]区间上最长的子串，最少分割次数
//    int minCut(string s) {
//        int n = s.size();
//        vector<vector<bool>> isPal(n, vector<bool>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            for (int j = i; j < n; j++)
//            {
//                if (s[i] == s[j])
//                {
//                    if (i + 1 < j) isPal[i][j] = isPal[i + 1][j - 1];
//                    else isPal[i][j] = true;
//                }
//            }
//        }
//        vector<int> dp(n, INT_MAX);
//        for (int i = 0; i < n; i++)
//        {
//            if (isPal[0][i]) dp[i] = 0;
//            else
//            {
//                for (int j = 1; j <= i; j++)
//                {
//                    if (isPal[j][i])
//                    {
//                        dp[i] = min(dp[i], dp[j - 1] + 1);
//                    }
//                }
//            }
//        }
//        return dp[n - 1];
//    }
//};


//class Solution {
//public:
//    //dp[i][j]:表示s[i, j]区间内所有的子序列，最长的回文子序列的长度
//    int longestPalindromeSubseq(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            dp[i][i] = 1;
//            for (int j = i + 1; j < n; j++)
//            {
//                if (s[i] == s[j])
//                {
//                    if (i + 1 == j) dp[i][j] = 2;
//                    else dp[i][j] = dp[i + 1][j - 1] + 2;
//                }
//                else dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);
//            }
//        }
//        return dp[0][n - 1];
//    }
//};


//class Solution {
//public:
//    //dp[i][j]:表示s[i, j]区间内的子串，使它称为回文串的最小操作次数
//    int minInsertions(string s) {
//        int n = s.size();
//        vector<vector<int>> dp(n, vector<int>(n));
//        for (int i = n - 1; i >= 0; i--)
//        {
//            dp[i][i] = 0;
//            for (int j = i + 1; j < n; j++)
//            {
//                if (s[i] == s[j]) dp[i][j] = dp[i + 1][j - 1];
//                else dp[i][j] = min(dp[i + 1][j] + 1, dp[i][j - 1] + 1);
//            }
//        }
//        return dp[0][n - 1];
//    }
//};