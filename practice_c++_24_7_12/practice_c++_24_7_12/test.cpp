#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     * 栈排序
//     * @param a int整型vector 描述入栈顺序
//     * @return int整型vector
//     */
//    vector<int> solve(vector<int>& a) {
//        int n = a.size();
//        vector<int> ret;
//        stack<int> st;
//        int maxi = n;
//        int hash[50010] = { 0 };
//        for (int i = 0; i < n; i++)
//        {
//            int x = a[i];
//            st.push(x);
//            hash[x] = 1;
//            while (hash[maxi])
//                maxi--;
//            while (st.size() && st.top() >= maxi)
//            {
//                ret.push_back(st.top());
//                st.pop();
//            }
//        }
//        return ret;
//    }
//};


#include <iostream>
#include <algorithm>
using namespace std;

typedef long long LL;
const int N = 1e5 + 10;
LL a[N], s[N];

LL cal(int l, int r)
{
    int mid = l + (r - l) / 2;
    return (mid - l - r + mid) * a[mid] - (s[mid - 1] - s[l - 1]) + (s[r] - s[mid]);
}

int main()
{
    LL n, k;
    cin >> n >> k;
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    sort(a + 1, a + 1 + n);
    for (int i = 1; i <= n; i++)
        s[i] = s[i - 1] + a[i];
    int res = 1;
    int left = 1, right = 1;
    while (right <= n)
    {
        LL cost = cal(left, right);
        while (cost > k)
        {
            left++;
            cost = cal(left, right);
        }
        res = max(res, right - left + 1);
        right++;
    }
    cout << res << endl;
    return 0;
}