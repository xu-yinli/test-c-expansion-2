#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//private:
//    stack<int> st;
//public:
//    int trap(vector<int>& height) {
//        int n = height.size();
//        if (n <= 2) return 0;
//        st.push(0);
//        int sum = 0;
//        for (int i = 1; i < n; i++)
//        {
//            if (height[i] < height[st.top()]) st.push(i);
//            else if (height[i] == height[st.top()])
//            {
//                st.pop();
//                st.push(i);
//            }
//            else
//            {
//                while (!st.empty() && height[i] > height[st.top()])
//                {
//                    int mid = st.top();
//                    st.pop();
//                    if (!st.empty())
//                    {
//                        int h = min(height[st.top()], height[i]) - height[mid];
//                        int w = i - st.top() - 1;
//                        sum += h * w;
//                    }
//                }
//                st.push(i);
//            }
//        }
//        return sum;
//    }
//};


//class Solution {
//private:
//    stack<int> st;
//public:
//    int largestRectangleArea(vector<int>& heights) {
//        int res = 0;
//        heights.insert(heights.begin(), 0);
//        heights.push_back(0);
//        int n = heights.size();
//        st.push(0);
//        for (int i = 1; i < n; i++)
//        {
//            if (heights[i] > heights[st.top()]) st.push(i);
//            else if (heights[i] == heights[st.top()])
//            {
//                st.pop();
//                st.push(i);
//            }
//            else
//            {
//                while (!st.empty() && heights[i] < heights[st.top()])
//                {
//                    int mid = st.top();
//                    st.pop();
//                    if (!st.empty())
//                    {
//                        int h = heights[mid];
//                        int w = i - st.top() - 1;
//                        res = max(res, h * w);
//                    }
//                }
//                st.push(i);
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//public:
//    int lastStoneWeight(vector<int>& stones) {
//        priority_queue<int> heap;
//        for (auto e : stones)
//            heap.push(e);
//        while (heap.size() > 1)
//        {
//            int a = heap.top(); heap.pop();
//            int b = heap.top(); heap.pop();
//            if (a > b) heap.push(a - b);
//        }
//        if (heap.empty()) return 0;
//        return heap.top();
//    }
//};


//class KthLargest {
//private:
//    priority_queue<int, vector<int>, greater<int>> heap;
//    int _k;
//public:
//    KthLargest(int k, vector<int>& nums) {
//        _k = k;
//        for (auto e : nums)
//        {
//            heap.push(e);
//            if (heap.size() > _k)
//                heap.pop();
//        }
//    }
//
//    int add(int val) {
//        heap.push(val);
//        if (heap.size() > _k)
//            heap.pop();
//        return heap.top();
//    }
//};
//
///**
// * Your KthLargest object will be instantiated and called as such:
// * KthLargest* obj = new KthLargest(k, nums);
// * int param_1 = obj->add(val);
// */


//class Solution {
//private:
//    unordered_map<string, int> hash;
//    typedef pair<string, int> PSI;
//    struct cmp
//    {
//        bool operator()(const PSI& v, const PSI& t)
//        {
//            if (v.second == t.second) return v.first < t.first;
//            return v.second > t.second;
//        }
//    };
//public:
//    vector<string> topKFrequent(vector<string>& words, int k) {
//        vector<string> res(k);
//        for (auto s : words)
//            hash[s]++;
//        priority_queue<PSI, vector<PSI>, cmp> heap;
//        for (auto psi : hash)
//        {
//            heap.push(psi);
//            if (heap.size() > k)
//                heap.pop();
//        }
//        for (int i = k - 1; i >= 0; i--)
//        {
//            res[i] = heap.top().first;
//            heap.pop();
//        }
//        return res;
//    }
//};


//class MedianFinder {
//private:
//    priority_queue<int> left;
//    priority_queue<int, vector<int>, greater<int>> right;
//public:
//    MedianFinder() {
//
//    }
//
//    void addNum(int num) {
//        if (left.size() == right.size())
//        {
//            if (left.empty() || num <= left.top()) left.push(num);
//            else
//            {
//                right.push(num);
//                left.push(right.top());
//                right.pop();
//            }
//        }
//        else if (left.size() > right.size())
//        {
//            if (num <= left.top())
//            {
//                left.push(num);
//                right.push(left.top());
//                left.pop();
//            }
//            else right.push(num);
//        }
//    }
//
//    double findMedian() {
//        if (left.size() == right.size()) return (left.top() + right.top()) / 2.0;
//        else return left.top();
//    }
//};
//
///**
// * Your MedianFinder object will be instantiated and called as such:
// * MedianFinder* obj = new MedianFinder();
// * obj->addNum(num);
// * double param_2 = obj->findMedian();
// */