#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <cmath>
//using namespace std;
//
//const int N = 15, M = 1010;
//int n, a[N];
//bool use[M];
//int sum;
//int res = 0x3f3f3f3f;
//
//bool isPrim(int x)
//{
//    if (x <= 1) return false;
//    for (int i = 2; i <= x / i; i++)
//        if (x % i == 0)
//            return false;
//    return true;
//}
//
//void dfs(int pos)
//{
//    if (pos == n)
//    {
//        res = min(res, sum);
//        return;
//    }
//    for (int i = 2; i <= a[pos]; i++)
//    {
//        if (a[pos] % i == 0 && isPrim(i) && !use[i])
//        {
//            sum += i;
//            use[i] = true;
//            dfs(pos + 1);
//            use[i] = false;
//            sum -= i;
//        }
//    }
//}
//
//int main()
//{
//    cin >> n;
//    for (int i = 0; i < n; i++)
//        cin >> a[i];
//    dfs(0);
//    if (res == 0x3f3f3f3f) cout << -1 << endl;
//    else cout << res << endl;
//    return 0;
//}


#include <iostream>
using namespace std;

const int N = 1e6 + 10;
char dp[N];

int main()
{
    int n;
    cin >> n;
    string s;
    cin >> s;
    int res = 0;
    for (int i = 0; i < n; i++)
    {
        char ch = s[i];
        if (res == 0 || ch >= dp[res])
        {
            res++;
            dp[res] = ch;
        }
        else
        {
            int left = 1, right = res;
            while (left < right)
            {
                int mid = (left + right) / 2;
                if (dp[mid] > ch) right = mid;
                else left = mid + 1;
            }
            dp[left] = ch;
        }
    }
    cout << n - res << endl;
    return 0;
}