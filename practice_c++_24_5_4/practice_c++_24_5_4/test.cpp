#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
//int n, m;
//unordered_map<int, int> cnt;
//
//bool check(int x)
//{
//    int g = 0;
//    for (auto& [a, b] : cnt)
//    {
//        g += b / x;
//        if (b % x != 0) g++;
//    }
//    if (g <= m) return true;
//    else return false;
//}
//
//int main()
//{
//    cin >> n >> m;
//    int maxCount = 0;
//    for (int i = 0; i < n; i++)
//    {
//        int x;
//        cin >> x;
//        cnt[x]++;
//        if (cnt[x] > maxCount)
//            maxCount = cnt[x];
//    }
//    int size = cnt.size();
//    if (size > m)
//        cout << -1 << endl;
//    else
//    {
//        for (int i = 1; i <= maxCount; i++)
//        {
//            if (check(i))
//            {
//                cout << i << endl;
//                break;
//            }
//        }
//    }
//    return 0;
//}


//#include <iostream>
//#include <unordered_map>
//using namespace std;
//
//int n, m;
//unordered_map<int, int> cnt;
//
//bool check(int x)
//{
//    int g = 0;
//    for (auto& [a, b] : cnt)
//    {
//        g += b / x;
//        if (b % x != 0) g++;
//    }
//    if (g <= m) return true;
//    else return false;
//}
//
//int main()
//{
//    cin >> n >> m;
//    int maxCount = 0;
//    for (int i = 0; i < n; i++)
//    {
//        int x;
//        cin >> x;
//        cnt[x]++;
//        if (cnt[x] > maxCount)
//            maxCount = cnt[x];
//    }
//    int size = cnt.size();
//    if (size > m)
//        cout << -1 << endl;
//    else
//    {
//        int l = 1, r = maxCount;
//        while (l < r)
//        {
//            int mid = l + (r - l) / 2;
//            if (check(mid)) r = mid;
//            else l = mid + 1;
//        }
//        cout << l << endl;
//    }
//    return 0;
//}


#include <iostream>
using namespace std;

typedef long long LL;
const int N = 100010;
LL row[N], col[N];

int main()
{
    int n, m;
    scanf("%d %d", &n, &m);
    int a[n][m];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            scanf("%d", &a[i][j]);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            row[i] += a[i][j];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            col[j] += a[i][j];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            printf("%lld ", row[i] + col[j] - a[i][j]);
        printf("\n");
    }
    return 0;
}