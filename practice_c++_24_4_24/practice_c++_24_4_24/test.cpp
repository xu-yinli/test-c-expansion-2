#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//using namespace std;
//
//const int N = 510;
//char g[N][N];
//int dp[N][N];
//
//int main()
//{
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++)
//        for (int j = 1; j <= m; j++)
//            cin >> g[i][j];
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//        {
//            int score = 0;
//            if (g[i][j] == 'l') score = 4;
//            else if (g[i][j] == 'o') score = 3;
//            else if (g[i][j] == 'v') score = 2;
//            else if (g[i][j] == 'e') score = 1;
//            dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + score;
//        }
//    }
//    cout << dp[n][m] << endl;
//    return 0;
//}


//class Solution {
//public:
//    int climbStairs(int n) {
//        if (n <= 2) return n;
//        int dp[3];
//        dp[1] = 1, dp[2] = 2;
//        for (int i = 3; i <= n; i++)
//        {
//            int sum = dp[1] + dp[2];
//            dp[1] = dp[2];
//            dp[2] = sum;
//        }
//        return dp[2];
//    }
//};


//class Solution {
//public:
//    int climbStairs(int n) {
//        if (n <= 2) return n;
//        int a = 1, b = 2;
//        for (int i = 3; i <= n; i++)
//        {
//            int c = a + b;
//            a = b;
//            b = c;
//        }
//        return b;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     *
//     * @param number int整型
//     * @return int整型
//     */
//    int jumpFloor(int number) {
//        int dp[number + 1];
//        dp[0] = dp[1] = 1;
//        dp[2] = 2;
//        for (int i = 2; i <= number; i++)
//            dp[i] = dp[i - 1] + dp[i - 2];
//        return dp[number];
//    }
//};


//class Solution {
//private:
//    int MOD = 1e9 + 7;
//public:
//    int trainWays(int num) {
//        if (num == 0) return 1;
//        if (num == 1 || num == 2) return num;
//        int a = 1, b = 2;
//        for (int i = 3; i <= num; i++)
//        {
//            int sum = (a + b) % MOD;
//            a = b;
//            b = sum;
//        }
//        return b;
//    }
//};


//class Solution {
//public:
//    int rectCover(int number) {
//        if (number <= 2)
//            return number;
//        int dp[number + 1];
//        dp[1] = 1, dp[2] = 2;
//        for (int i = 3; i <= number; i++)
//            dp[i] = dp[i - 1] + dp[i - 2];
//        return dp[number];
//    }
//};


//class CQueue {
//private:
//    stack<int> inStack;
//    stack<int> outStack;
//public:
//    CQueue() {}
//
//    void appendTail(int value) {
//        inStack.push(value);
//    }
//
//    int deleteHead() {
//        if (inStack.empty())
//            return -1;
//        while (!inStack.empty())
//        {
//            int tmp = inStack.top();
//            inStack.pop();
//            outStack.push(tmp);
//        }
//        int dele = outStack.top();
//        outStack.pop();
//        while (!outStack.empty())
//        {
//            int tmp = outStack.top();
//            outStack.pop();
//            inStack.push(tmp);
//        }
//        return dele;
//    }
//};
//
///**
// * Your CQueue object will be instantiated and called as such:
// * CQueue* obj = new CQueue();
// * obj->appendTail(value);
// * int param_2 = obj->deleteHead();
// */


//class CQueue {
//private:
//    stack<int> inStack;
//    stack<int> outStack;
//public:
//    CQueue() {}
//
//    void appendTail(int value) {
//        inStack.push(value);
//    }
//
//    int deleteHead() {
//        if (outStack.empty())
//        {
//            if (inStack.empty())
//                return -1;
//            while (!inStack.empty())
//            {
//                int tmp = inStack.top();
//                inStack.pop();
//                outStack.push(tmp);
//            }
//        }
//        int dele = outStack.top();
//        outStack.pop();
//        return dele;
//    }
//};
//
///**
// * Your CQueue object will be instantiated and called as such:
// * CQueue* obj = new CQueue();
// * obj->appendTail(value);
// * int param_2 = obj->deleteHead();
// */


//class MyStack {
//public:
//    queue<int> q1;
//    queue<int> q2;
//    MyStack() {}
//
//    void push(int x) {
//        q1.push(x);
//    }
//
//    int pop() {
//        int n = q1.size();
//        n--;
//        while (n--)
//        {
//            q2.push(q1.front());
//            q1.pop();
//        }
//        int res = q1.front();
//        q1.pop();
//        q1 = q2;
//        while (!q2.empty())
//            q2.pop();
//        return res;
//    }
//
//    int top() {
//        return q1.back();
//    }
//
//    bool empty() {
//        return q1.empty() && q2.empty();
//    }
//};
//
///**
// * Your MyStack object will be instantiated and called as such:
// * MyStack* obj = new MyStack();
// * obj->push(x);
// * int param_2 = obj->pop();
// * int param_3 = obj->top();
// * bool param_4 = obj->empty();
// */


//class MyQueue {
//public:
//    stack<int> inStack;
//    stack<int> outStack;
//    MyQueue() {}
//
//    void push(int x) {
//        inStack.push(x);
//    }
//
//    int pop() {
//        if (outStack.empty())
//        {
//            if (inStack.empty())
//                return -1;
//            while (!inStack.empty())
//            {
//                int tmp = inStack.top();
//                inStack.pop();
//                outStack.push(tmp);
//            }
//        }
//        int dele = outStack.top();
//        outStack.pop();
//        return dele;
//    }
//
//    int peek() {
//        int res = this->pop();
//        outStack.push(res);
//        return res;
//    }
//
//    bool empty() {
//        return inStack.empty() && outStack.empty();
//    }
//};
//
///**
// * Your MyQueue object will be instantiated and called as such:
// * MyQueue* obj = new MyQueue();
// * obj->push(x);
// * int param_2 = obj->pop();
// * int param_3 = obj->peek();
// * bool param_4 = obj->empty();
// */


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color) {
//        int n = image.size(), m = image[0].size();
//        int oldColor = image[sr][sc];
//        if (oldColor == color) return image;
//        queue<PII> q;
//        q.push({ sr, sc });
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            image[a][b] = color;
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < n && y >= 0 && y < m && image[x][y] == oldColor)
//                    q.push({ x, y });
//            }
//        }
//        return image;
//    }
//};


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int n, m;
//    bool vis[310][310];
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    void bfs(vector<vector<char>>& grid, int i, int j)
//    {
//        queue<PII> q;
//        q.push({ i, j });
//        vis[i][j] = true;
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < n && y >= 0 && y < m && grid[x][y] == '1' && !vis[x][y])
//                {
//                    q.push({ x, y });
//                    vis[x][y] = true;
//                }
//            }
//        }
//    }
//    int numIslands(vector<vector<char>>& grid) {
//        n = grid.size(), m = grid[0].size();
//        int res = 0;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (grid[i][j] == '1' && !vis[i][j])
//                {
//                    res++;
//                    bfs(grid, i, j);
//                }
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int n, m;
//    bool vis[60][60];
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    int bfs(vector<vector<int>>& grid, int i, int j)
//    {
//        int cnt = 0;
//        queue<PII> q;
//        q.push({ i, j });
//        vis[i][j] = true;
//        cnt++;
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < n && y >= 0 && y < m && grid[x][y] == 1 && !vis[x][y])
//                {
//                    q.push({ x, y });
//                    vis[x][y] = true;
//                    cnt++;
//                }
//            }
//        }
//        return cnt;
//    }
//    int maxAreaOfIsland(vector<vector<int>>& grid) {
//        n = grid.size(), m = grid[0].size();
//        int res = 0;
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (grid[i][j] == 1 && !vis[i][j])
//                {
//                    res = max(res, bfs(grid, i, j));
//                }
//            }
//        }
//        return res;
//    }
//};


//class Solution {
//private:
//    typedef pair<int, int> PII;
//    int n, m;
//    int dx[4] = { 0,0,1,-1 }, dy[4] = { 1,-1,0,0 };
//public:
//    void bfs(vector<vector<char>>& board, int i, int j)
//    {
//        queue<PII> q;
//        q.push({ i, j });
//        board[i][j] = '.';
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k], y = b + dy[k];
//                if (x >= 0 && x < n && y >= 0 && y < m && board[x][y] == 'O')
//                {
//                    q.push({ x, y });
//                    board[x][y] = '.';
//                }
//            }
//        }
//    }
//    void solve(vector<vector<char>>& board) {
//        n = board.size(), m = board[0].size();
//        for (int j = 0; j < m; j++)
//        {
//            if (board[0][j] == 'O') bfs(board, 0, j);
//            if (board[n - 1][j] == 'O') bfs(board, n - 1, j);
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (board[i][0] == 'O') bfs(board, i, 0);
//            if (board[i][m - 1] == 'O') bfs(board, i, m - 1);
//        }
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < m; j++)
//            {
//                if (board[i][j] == 'O') board[i][j] = 'X';
//                if (board[i][j] == '.') board[i][j] = 'O';
//            }
//        }
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newHead = new ListNode(0);
//        newHead->next = head;
//        ListNode* prev = newHead, * cur = prev->next, * next = cur->next, * nnext = next->next;
//        while (cur && next)
//        {
//            prev->next = next;
//            next->next = cur;
//            cur->next = nnext;
//
//            prev = cur;
//            cur = nnext;
//            if (cur) next = cur->next;
//            if (next) nnext = next->next;
//        }
//        cur = newHead->next;
//        delete newHead;
//        return cur;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    void reorderList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr || head->next->next == nullptr) return;
//
//        // 1.找到链表的中间节点 - 快慢指针
//        ListNode* fast = head, * slow = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//
//        // 2.将slow后面部分的链表进行逆置 - 头插法
//        ListNode* head2 = nullptr;
//        ListNode* cur = slow->next;
//        slow->next = nullptr;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = head2;
//            head2 = cur;
//            cur = next;
//        }
//
//        // 3.合并两个链表
//        ListNode* prev = new ListNode(0);
//        ListNode* cur1 = head, * cur2 = head2;
//        while (cur1)
//        {
//            prev->next = cur1;
//            cur1 = cur1->next;
//            prev = prev->next;
//
//            if (cur2)
//            {
//                prev->next = cur2;
//                cur2 = cur2->next;
//                prev = prev->next;
//            }
//        }
//    }
//};