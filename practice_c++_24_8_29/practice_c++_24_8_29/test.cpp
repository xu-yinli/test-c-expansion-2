#define _CRT_SECURE_NO_WARNINGS 1


//#include <stdio.h>
//
//void fun1()
//{
//	int i = 0;
//	i++;
//	printf("no static: i=%d\n", i);
//}
//void fun2()
//{
//	static int i = 0;
//	i++;
//	printf("has static: i=%d\n", i);
//}
//int main()
//{
//	for (int i = 0; i < 10; i++)
//	{
//		fun1();
//		fun2();
//	}
//	return 0;
//}


//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums) {
//        int n = nums.size();
//        int len = INT_MAX;
//        int sum = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            sum += nums[right];
//            while (sum >= target)
//            {
//                len = min(len, right - left + 1);
//                sum -= nums[left++];
//            }
//            right++;
//        }
//        if (len != INT_MAX) return len;
//        return 0;
//    }
//};


//class Solution {
//private:
//    int hash[130];
//public:
//    int lengthOfLongestSubstring(string s) {
//        int n = s.size();
//        int len = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash[s[right]]++;
//            while (hash[s[right]] > 1)
//            {
//                hash[s[left]]--;
//                left++;
//            }
//            len = max(len, right - left + 1);
//            right++;
//        }
//        return len;
//    }
//};


#include <stdio.h>

//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k) {
//        int n = nums.size();
//        int count = 0;
//        int len = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            if (nums[right] == 0) count++;
//            while (count > k)
//            {
//                if (nums[left] == 0) count--;
//                left++;
//            }
//            len = max(len, right - left + 1);
//            right++;
//        }
//        return len;
//    }
//};


//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x) {
//        int n = nums.size();
//        int sum = 0;
//        for (int x : nums)
//            sum += x;
//        if (sum < x) return -1;
//        int total = 0;
//        int len = -1;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            total += nums[right];
//            while (total > sum - x)
//            {
//                total -= nums[left];
//                left++;
//            }
//            if (total == sum - x) len = max(len, right - left + 1);
//            right++;
//        }
//        if (len != -1) return n - len;
//        return -1;
//    }
//};


//class Solution {
//private:
//    unordered_map<int, int> hash;
//public:
//    int totalFruit(vector<int>& fruits) {
//        int n = fruits.size();
//        int len = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash[fruits[right]]++;
//            while (hash.size() > 2)
//            {
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0) hash.erase(fruits[left]);
//                left++;
//            }
//            len = max(len, right - left + 1);
//            right++;
//        }
//        return len;
//    }
//};


//class Solution {
//private:
//    int hash1[130];
//    int hash2[130];
//    vector<int> res;
//public:
//    vector<int> findAnagrams(string s, string p) {
//        int n = s.size(), m = p.size();
//        for (char ch : p)
//            hash1[ch]++;
//        int count = 0;
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            hash2[s[right]]++;
//            if (hash2[s[right]] <= hash1[s[right]]) count++;
//            if (right - left + 1 > m)
//            {
//                if (hash2[s[left]] <= hash1[s[left]]) count--;
//                hash2[s[left]]--;
//                left++;
//            }
//            if (count == m) res.push_back(left);
//            right++;
//        }
//        return res;
//    }
//};