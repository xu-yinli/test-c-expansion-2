#define _CRT_SECURE_NO_WARNINGS 1

//class Solution {
//public:
//    int removeElement(vector<int>& nums, int val) {
//        int n = nums.size();
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            if (nums[right] == val) right++;
//            else nums[left++] = nums[right++];
//        }
//        return left;
//    }
//};


//class Solution {
//public:
//    int removeDuplicates(vector<int>& nums) {
//        int n = nums.size();
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            if (nums[left] == nums[right]) right++;
//            else nums[++left] = nums[right++];
//        }
//        return left + 1;
//    }
//};


//class Solution {
//public:
//    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
//        int sum = m + n;
//        int cur = sum - 1;
//        int prev1 = m - 1, prev2 = n - 1;
//        while (prev1 >= 0 && prev2 >= 0)
//        {
//            if (nums1[prev1] < nums2[prev2]) nums1[cur--] = nums2[prev2--];
//            else nums1[cur--] = nums1[prev1--];
//        }
//        while (prev1 >= 0)
//            nums1[cur--] = nums1[prev1--];
//        while (prev2 >= 0)
//            nums1[cur--] = nums2[prev2--];
//    }
//};


//class Solution {
//public:
//    int removeElement(vector<int>& nums, int val) {
//        int n = nums.size();
//        int left = 0, right = 0;
//        while (right < n)
//        {
//            if (nums[right] != val)
//            {
//                nums[left] = nums[right];
//                left++;
//            }
//            right++;
//        }
//        return left;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        while (head && head->val == val)
//            head = head->next;
//        ListNode* cur = head;
//        while (cur && cur->next)
//        {
//            if (cur->next->val == val) cur->next = cur->next->next;
//            else cur = cur->next;
//        }
//        return head;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* removeElements(ListNode* head, int val) {
//        ListNode* dummyHead = new ListNode(0);
//        dummyHead->next = head;
//        ListNode* cur = dummyHead;
//        while (cur->next)
//        {
//            if (cur->next->val == val) cur->next = cur->next->next;
//            else cur = cur->next;
//        }
//        return dummyHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* prev = nullptr;
//        ListNode* cur = head;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = next;
//        }
//        return prev;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* newHead = reverseList(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return newHead;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* middleNode(ListNode* head) {
//        ListNode* fast = head;
//        ListNode* slow = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        return slow;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    int kthToLast(ListNode* head, int k) {
//        ListNode* fast = head;
//        ListNode* slow = head;
//        while (k--)
//        {
//            fast = fast->next;
//        }
//        while (fast)
//        {
//            fast = fast->next;
//            slow = slow->next;
//        }
//        return slow->val;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        ListNode* newHead = nullptr;
//        ListNode* end = nullptr;
//        while (list1 && list2)
//        {
//            ListNode* cur = nullptr;
//            if (list1->val <= list2->val)
//            {
//                cur = list1;
//                list1 = list1->next;
//            }
//            else
//            {
//                cur = list2;
//                list2 = list2->next;
//            }
//            if (newHead == nullptr)
//            {
//                newHead = cur;
//                end = cur;
//            }
//            else
//            {
//                end->next = cur;
//                end = end->next;
//            }
//        }
//        if (list1) end->next = list1;
//        if (list2) end->next = list2;
//        return newHead;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        if (list1->val <= list2->val)
//        {
//            list1->next = mergeTwoLists(list1->next, list2);
//            return list1;
//        }
//        else
//        {
//            list2->next = mergeTwoLists(list1, list2->next);
//            return list2;
//        }
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
//        if (list1 == nullptr) return list2;
//        if (list2 == nullptr) return list1;
//        ListNode* newHead = nullptr;
//        if (list1->val <= list2->val)
//        {
//            newHead = list1;
//            list1 = list1->next;
//        }
//        else
//        {
//            newHead = list2;
//            list2 = list2->next;
//        }
//        newHead->next = mergeTwoLists(list1, list2);
//        return newHead;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* partition(ListNode* head, int x) {
//        ListNode* greaterHead = new ListNode(0);
//        ListNode* greaterTail = greaterHead;
//        ListNode* lessHead = new ListNode(0);
//        ListNode* lessTail = lessHead;
//        while (head)
//        {
//            if (head->val < x)
//            {
//                lessTail->next = head;
//                lessTail = lessTail->next;
//            }
//            else
//            {
//                greaterTail->next = head;
//                greaterTail = greaterTail->next;
//            }
//            head = head->next;
//        }
//        lessTail->next = greaterHead->next;
//        greaterTail->next = nullptr;
//        return lessHead->next;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* middleNode(ListNode* head)
//    {
//        ListNode* fast = head;
//        ListNode* slow = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        return slow;
//    }
//    ListNode* reverseList(ListNode* head)
//    {
//        if (head == nullptr || head->next == nullptr) return head;
//        ListNode* prev = nullptr;
//        ListNode* cur = head;
//        while (cur)
//        {
//            ListNode* next = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = next;
//        }
//        return prev;
//    }
//    bool isPalindrome(ListNode* head) {
//        ListNode* mid = middleNode(head);
//        ListNode* rHead = reverseList(mid);
//        while (head && rHead)
//        {
//            if (head->val == rHead->val)
//            {
//                head = head->next;
//                rHead = rHead->next;
//            }
//            else return false;
//        }
//        return true;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    int getLength(ListNode* head)
//    {
//        if (head == NULL) return 0;
//        int len = 0;
//        while (head)
//        {
//            len++;
//            head = head->next;
//        }
//        return len;
//    }
//    ListNode* getIntersectionNode(ListNode* headA, ListNode* headB) {
//        if (headA == NULL) return NULL;
//        if (headB == NULL) return NULL;
//        int lenA = getLength(headA);
//        int lenB = getLength(headB);
//        if (lenA > lenB)
//        {
//            int gap = lenA - lenB;
//            while (gap--)
//                headA = headA->next;
//        }
//        else
//        {
//            int gap = lenB - lenA;
//            while (gap--)
//                headB = headB->next;
//        }
//        while (headA && headB)
//        {
//            if (headA == headB) return headA;
//            headA = headA->next;
//            headB = headB->next;
//        }
//        return NULL;
//    }
//};


//class Solution {
//public:
//    /**
//     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
//     *
//     * 比较版本号
//     * @param version1 string字符串
//     * @param version2 string字符串
//     * @return int整型
//     */
//    int compare(string version1, string version2) {
//        int n = version1.size(), m = version2.size();
//        int i = 0, j = 0;
//        while (i < n || j < m)
//        {
//            int num1 = 0;
//            while (i < n && version1[i] != '.')
//            {
//                num1 = num1 * 10 + (version1[i] - '0');
//                i++;
//            }
//            i++;
//            int num2 = 0;
//            while (j < m && version2[j] != '.')
//            {
//                num2 = num2 * 10 + (version2[j] - '0');
//                j++;
//            }
//            j++;
//            if (num1 > num2) return 1;
//            if (num1 < num2) return -1;
//        }
//        return 0;
//    }
//};


///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode(int x) : val(x), next(NULL) {}
// * };
// */
//class Solution {
//public:
//    bool hasCycle(ListNode* head) {
//        ListNode* fast = head;
//        ListNode* slow = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//            if (fast == slow) return true;
//        }
//        return false;
//    }
//};


/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* next;
    Node* random;

    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};
*/


//class Solution {
//public:
//    Node* copyRandomList(Node* head) {
//        if (head == NULL) return NULL;
//        Node* cur = head;
//        while (cur)
//        {
//            Node* tmp = new Node(cur->val);
//            tmp->next = cur->next;
//            cur->next = tmp;
//            cur = tmp->next;
//        }
//        Node* old = head;
//        Node* clone = NULL;
//        while (old)
//        {
//            clone = old->next;
//            if (old->random)
//                clone->random = old->random->next;
//            old = clone->next;
//        }
//        old = head;
//        Node* cloneHead = old->next;
//        clone = cloneHead;
//        while (old)
//        {
//            old->next = clone->next;
//            old = old->next;
//            if (old)
//            {
//                clone->next = old->next;
//                clone = clone->next;
//            }
//        }
//        return cloneHead;
//    }
//};